---
template: BlogPost
path: /147
date: 2024-01-18T06:15:50.738Z
title: ep147:遺伝子検査キット結果編
thumbnail: /assets/ep147.png
metaDescription:
---
![ep147](/assets/ep147.png)    



***

<a href="https://jamming.fm/142" target="_blank" rel="ugc noopener noreferrer">ep142:遺伝子検査キット購入編</a>   
<a href="https://www.amazon.co.jp/%E9%81%BA%E4%BC%9D%E5%AD%90%E6%A4%9C%E6%9F%BB%E3%82%AD%E3%83%83%E3%83%88%EF%BC%9CGeneLife-Genesis2-0-%E3%82%B8%E3%83%BC%E3%83%B3%E3%83%A9%E3%82%A4%E3%83%95-%E3%82%B8%E3%82%A7%E3%83%8D%E3%82%B7%E3%82%B9-%E3%83%97%E3%83%A9%E3%82%B9/dp/B09CGN1L5F" target="_blank" rel="ugc noopener noreferrer">新 [GeneLife Genesis2.0 Plus] ジーンライフ 360項目のプレミアム遺伝子検査 / がんなどの疾患リスクや肥満体質など解析</a>  

<iframe width="560" height="315" src="https://www.youtube.com/embed/yTAnFRuvVAw?si=ZAm7JMGf85mYIUAD&amp;start=2200" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

「どうも、紅海を渡ってきたグループです」  
EDなのかが気になる  
日本人だけでも十分に多様だ（そもそもDNAレベルで多様性があるなら国籍なんて最近できた枠組みは関係ない）  
<a href="https://ja.wikipedia.org/wiki/%E3%83%92%E3%83%88%E3%82%B2%E3%83%8E%E3%83%A0%E8%A8%88%E7%94%BB" rel="ugc noopener noreferrer" target="_blank">ヒトゲノム計画</a>  
