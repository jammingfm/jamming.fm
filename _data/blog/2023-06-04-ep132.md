---
template: BlogPost
path: /132
date: 2023-06-04T06:15:50.738Z
title: ep132:持ち家 or 賃貸 2023
thumbnail: /assets/ep132.png
metaDescription:
---
![ep1３2](/assets/ep132.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/4yqbA49uC18UoutzOy0dlx?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>


***
  

[大人の障害物レース スパルタンレース](https://spartanracejapan.jp/)  
疲れすぎて熱が出た  
[SEA TO SUMMIT](http://www.seatosummit.jp/)  
3年半振りのテーマ  
[pe14: 持ち家 vs 賃貸 2019](https://jamming.fm/14)  
2023年版を録る  
2019年はお互いに賃貸  
2020年にマンション購入した@naoharu  
[ep59 マンション買った](https://jamming.fm/59)  
家を買ったときはNRIだった  
日本トップレベルの信用  
買ってすぐ転職した  
なんで買いたいと思ったのか  
子供が5歳になったときに前の家が手狭になった  
広いところに住む必要が出てきた  
賃貸を選ばなかった理由  
一族が住んでるのでここに住む  
都内で賃貸で３LDKは高い  
所有しないことに高いお金を払うべきか  
どんな価値観でその判断に至ったのかを明らかにしたい  
人生1回きりの判断は間違ってない買ったと思わないとメンタル保てない  
元々は買いたいと思ってなかった  
奥さんが家ほしいと言って買う人も多い  
戸建賃貸　庭が60平米、駐車場2台  
そもそも持ち家という選択肢がない@dada_ism  
注文住宅はリセールバリュー考えちゃいけない  
注文住宅楽しそう  
ただ知識がないと失敗しそう  
マンションはそういう失敗は少ない  
マンションのデメリット  
コピペの部屋に住んでる  
ずっと住むって思ってない場合  
買ったらリセールバリュー落とさないようにしなければいけない  
という意識がストレス  
新築も2週間でどうでもよくなる  
子供ありがとう  
解脱  
自分でやらかしたい  
賃貸でも壁紙張り替えたり床を張り替えたりしている  
[ep114:理想的な結婚相手の選び方から学ぶ不動産の選び方](https://jamming.fm/114)  
家を購入したら仕事に身が入ることはなかった  
家を買ったら転勤が決まるあるある  
住宅ローン減税享受したくて泣く泣く単身赴任  
購入派「売ればいいじゃん」は結構きびしい  
その地域で一番いいモノを選ぶべき  
定期的にこのテーマ録りたい  
ポッドキャストの定点観測性はおもしろい  
エチエチ看護師  