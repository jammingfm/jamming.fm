---
template: BlogPost
path: /199
date: 2024-12-07T06:15:50.738Z
title: ep199:保育料は月27万円
thumbnail: /assets/ep199.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/2UPR8EWTIyRoarIPFYhQro?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
  
***  

(00:11) 家族がシンガポールで合流（約3ヶ月ぶり）  
(01:00) 引っ越し最終フェーズでのトラブル（仕分けや車処分、観葉植物の譲渡など）  
(04:28) 予定外のクリスマスツリーが船便に乗ってしまうハプニング  
(05:20) 真夏のシンガポールでもクリスマスムード  
(07:16) [ユニバーサル・スタジオ・シンガポール(公式)](https://www.sentosa.com.sg/en/things-to-do/attractions/universal-studios-singapore/) へのお出かけ  
(07:30) 高額な保育園探しと政府補助の有無（私立は月2500ドル超、補助ありでも約1000ドル）  
(11:11) 将来の教育費、海外大学進学にかかる学費の高さと資金計画の必要性  
(13:45) 家に設置した遠隔カメラで家族や知人の様子を観察  
(14:06) [インターステラー(IMDb)](https://www.imdb.com/title/tt0816692/)的な「時空を超えた観察」の比喩  
(16:45) 「死んだ人」のような視点で遠くから生活音を盗み聞く不思議な感覚
