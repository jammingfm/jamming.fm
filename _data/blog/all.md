---  
template: BlogPost  
path: /1  
date: 2019-05-21T06:15:50.738Z  
title: ep1:jamming.fmについて語るときに僕たちの語ること  
thumbnail: /assets/ep1.png
metaDescription: df sdf df  
---  
![ep1](/assets/ep1.png)  
 

<iframe src="https://open.spotify.com/embed/episode/02e0C2pL1uW2uWODk1MB6e" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  

自己紹介  
naoharuとdada、2010年に出会う  
[Flickr](https://www.flickr.com/)   
[@positiveflat](https://twitter.com/positiveflat)  
ボイスメディアにハマっている  
[Rebuild](http://rebuild.fm/)  
omoiyari.fm  
[TuningCompleteFM](https://turingcomplete.fm/)  
松本人志の放送室  
なんでボイスメディアが流行ってきているんだろう  
デジタルタトゥー  
立ち上げ過程すらも配信するスタイル  
jamming fm ってなに  
ジャムってる感  
アジャイル(スクラム)開発  
スクラムを始めるきっかけ  
アジャイル開発のスピードが出るわけ  
プラクティス厨  
CSM  
スクラムマスターズナイト  
会社の外に出てみる  
炎上しにくいメディアで炎上する  
物議を醸したい  
濱口秀司さん  
[ビメオではなくビミオ(vimeo)](https://vimeo.com/jp/)  
「イノベーションは狙って起こせる」  
[WSL_濱口秀司_「認知バイアス」にイノベーションのカギがある](https://vimeo.com/48997854)  
「前例がない」「実現可能である」「賛否両論を呼ぶ」  
共感だけでもだめ  
ツイッターは鍵アカウント  
[ソフトウェアの6-7割の機能は使われていない](https://risingsun-system.biz/pdca-cycle-it-investment/)  
ミニマムで配信するのは難しい  
[ユーザーストーリーマッピング](https://amzn.to/2ZrIkFX)  
[OREILLY Ebook](https://www.oreilly.co.jp/ebook/)   
MVP（minimum viable product）の難しさ  
スクラム導入時の問題  
メンバーがスクラムに100%コミットできない  
スクラムをカスタマイズ  
本当に俺らがやっているのはスクラムなの？  
[スクラムガイド](https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-Japanese.pdf)  
スクラムガイドはゴール派  
スクラムガイドはスタート派  
[Scrum but](https://www.scrum.org/resources/what-scrumbut)   
ウォーターフォール開発を斜に見る  
ウォーターフォールの規模の見積り  
FP見積り  
相対見積りとバーンダウンチャートは理にかなっている  
スクラムを知ったきっかけ  
高校の部活で知ったチームワークのよさ  
Command and Control  
収録日は2月22日  
はじめてから軌道を修正するスタイル  ---  
template: BlogPost  
path: /2  
date: 2019-06-05T06:15:50.738Z  
title: ep2:「jamming.fmから来ました！」
thumbnail: /assets/ep2.png
metaDescription: df sdf df  
---  
![ep2](/assets/ep2.png)  

<iframe src="https://open.spotify.com/embed/episode/6n4GlXnTvm9QGkPFbbd1jX" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
</br>


***
  
</br>
<p>前回のTRY（鍵垢を外す）<br>最悪炎上ある<br>会社の人にバレた<br>Trelloでタスク管理 trello.com/<br>第1回のフィードバック<br><a href="https://twitter.com/sirtoc" target="_blank" rel="noopener noreferrer">サントスというあだ名の日本人&nbsp;</a><br><a href="https://twitter.com/hc100" target="_blank" rel="noopener noreferrer">ॐ カレー国™&nbsp;</a><br>「炎上しにくいメディアpodcastでデジタルタトゥーを入れてください」<br>マイクロドローン<br>収録から中二日でリリース<br><a href="https://www.canva.com/" target="_blank" rel="noopener noreferrer">Canvaでアイコンをつくる&nbsp;</a><br>みんなPodcastをどのアプリで購読しているのか？<br>Jamming.fmへの流入<br>名刺代わりにはドメインが必要<br>dada_ism がテレビに出た<br><a href="https://www.instagram.com/niboshism/?hl=ja" target="_blank" rel="noopener noreferrer">Daily niboshiさん(niboshism) • Instagram写真と動画&nbsp;</a><br><a href="http://www.ntv.co.jp/nextbreak/program/20180915.html" target="_blank" rel="noopener noreferrer">#渡辺直美のヤバスタグラム｜ネクストブレイク｜日本テレビ - 日テレ&nbsp;</a><br>TVは揮発性の高いメディア<br>朝起きたらフォロワーが1100人くらい増えてた<br>「jamming.fmから来ました！」はNG<br>alert();で補導<br>取り締まる側のITリテラシー<br>スクリーンショットと著作権侵害<br>niboshismが盗用された<br>若い子はスクショで共有<br>URLとshare<br>スクラム開発の悩み<br>スプリントプランニングで宣言したことが終わらない<br>バッファを積む<br>見積もりは永遠のテーマ系<br>PO「そもそもスケジュール管理できてないじゃん」<br>バッファを積むことによって距離が生まれる<br>完了しなかったPBIの扱い<br>ベロシティと開発チームのモチベーション<br>透明性 検査 適応　に忠実に立ち向かう<br>スクラムの見積もりって開発者の言い値じゃんって話かと思ってたけど、そうじゃなかった<br>受発注って関係があるなかで見積もりの妥当性をどう伝えるか<br>「文化祭じゃねえぞ」<br>内製だとしても受発注の力関係はある<br>ワンチーム感がないとバッファは生まれる<br>SPを過剰に高い数値を見積もってしまうのはなぜか<br>次の収録までのトライ<br><a href="https://ja.wikipedia.org/wiki/%E9%87%A3%E3%82%8A%E3%83%90%E3%82%AB%E6%97%A5%E8%AA%8C" target="_blank" rel="noopener noreferrer">釣りバカ日誌に触れる</a><br><a href="https://www.strava.com/">Stravaで3人友達つくって活動する&nbsp;</a></p>---  
template: BlogPost  
path: /3
date: 2019-06-06T06:15:50.738Z  
title: ep3:それ、デラックスやない。デジタルトランスフォーメーションや。
thumbnail: /assets/ep3.png
metaDescription: df sdf df  
---  
![ep3](/assets/ep3.png)  

<iframe src="https://open.spotify.com/embed/episode/6cRuk7lUIoqxIfL33ixzhX" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>


<p>DX(デジタルトランスフォーメーション)<br>世の中的に叫ばれるDX<br>経産省も動いた<br>DXのXってどこからきたの？<br>DXの定義は共通認識がない<br>Web系企業はDX後の世界？<br>モノからコトへ<br>ECサイトはDX？<br>狭義のDX・広義のDX<br>広義な意味のDXを遂げたものは既に世の中にあるの？<br>価値は青天井<br>DXの3ステップ<br>DXはいろいろな方向から語られる<br>危機感が漂うバズワード<br><a href="https://www.meti.go.jp/press/2018/12/20181212004/20181212004.html" target="_blank" rel="noreferrer noopener" aria-label="経済産業省 「デジタルトランスフォーメーションを推進するためのガイドライン（DX推進ガイドライン）を取りまとめました」 (新しいタブで開く)">経済産業省 「デジタルトランスフォーメーションを推進するためのガイドライン（DX推進ガイドライン）を取りまとめました」</a><br>日本のIT投資の９割が既存のシステムの保守<br>新しいことにチャレンジしなくなった日本のIT<br>DXはアジャイルやスクラムの価値観にリンクしている<br>DXに困っている会社<br>経営からの丸投げ<br>後ろ盾になる経営</p>
<p>モチベーション<br>チームが自発的に取り組む状態をどのように作り出すか<br>上長「自発的なチームになれー！」（ならない）<br><a href="https://ja.wikipedia.org/wiki/%E3%83%80%E3%83%8B%E3%82%A8%E3%83%AB%E3%83%BB%E3%83%94%E3%83%B3%E3%82%AF" target="_blank" rel="noreferrer noopener" aria-label="ダニエル・ピンク (新しいタブで開く)">ダニエル・ピンク</a><br><a href="https://gate.sc/?url=https%3A%2F%2Famzn.to%2F2Jiis77&amp;token=f0056c-1-1561259257453">モチベーション3.0</a><br><a href="https://ja.wikipedia.org/wiki/%E5%A4%A7%E5%89%8D%E7%A0%94%E4%B8%80" target="_blank" rel="noreferrer noopener" aria-label="大前研一さん (新しいタブで開く)">大前研一さん</a><br><a href="https://bbt.ac/" target="_blank" rel="noreferrer noopener" aria-label="BTT大学 (新しいタブで開く)">BTT大学</a><br><a href="https://www.ted.com/talks/dan_pink_on_motivation?language=ja" target="_blank" rel="noreferrer noopener" aria-label="TED Global 2009 やる気に関する驚きの科学 ダニエル・ピンク (新しいタブで開く)">TED Global 2009 やる気に関する驚きの科学 ダニエル・ピンク</a><br>自立性、熟達、目的／Autonomy, Mastery, Purpose<br>クリエイティビティを発揮しする仕事は内的モチベーションが必要<br>スクラムは内的モチベーションを触発する<br>自分たちを褒める<br>日本人は褒めるのが苦手？<br>SONKEN<br>日本人の謙遜スタイルは自己肯定感を下げているかもしれない<br>仕事はモチベーションじゃなくて使命感でするものだろ<br>モチベーションとやる気の違い<br>Loveは愛ではない<br>月が綺麗ですね （夏目漱石）<br>強いチームや良いチームは内的モチベーションが必要<br>マネジメントの方たちにはチームの内的モチベーションを引き出してほしい<br><a href="https://ja.wikipedia.org/wiki/%E3%83%80%E3%83%B3%E3%83%BB%E3%82%A2%E3%83%AA%E3%82%A8%E3%83%AA%E3%83%BC" target="_blank" rel="noreferrer noopener" aria-label="ダン・アリエリー (新しいタブで開く)">ダン・アリエリー</a><br>売れなかったケーキミックス<br>自分のケーキとして出したい消費者<br>自分でやっていると思わせる教育<br>マイクロマネジメントで下がるモチベーション<br>自分のマイクロマネジメントに気づく瞬間<br>naoharuの娘たち<br>子育てはマネジメント<br>家庭はチームビルディング</p>
<p>迫る結婚式・迫る引越し<br>Podcastにかける時間が増えてきている<br>次回TRY<br>naoharu：釣りコンテンツに触れる＋家族と釣り体験をしてみる<br>鍼灸やってみたいけど行けない<br>dada：夫婦で運動<br>WWW爆誕30周年<br><a href="https://japan.cnet.com/article/35133798/" target="_blank" rel="noreferrer noopener" aria-label="堕落したウェブはまだ直せる--WWW誕生から30年、生みの親が語る現状と展望 (新しいタブで開く)">堕落したウェブはまだ直せる--WWW誕生から30年、生みの親が語る現状と展望</a><br>WebがWebたらしめているものは何か<br>オープンな考え方がモチベーションを生む</p>---  
template: BlogPost  
path: /4
date: 2019-06-07T06:15:50.738Z  
title: ep4:東京都北区赤羽
thumbnail: /assets/ep4.png
metaDescription: df sdf df  
---  
![ep4](/assets/ep4.png)  
<iframe src="https://open.spotify.com/embed/episode/65T3X0VG62RGn7HneQfpGR" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

<p>新元号<br>忙しかった年度末<br>トライふりかえり<br>釣りに挑戦<br><a href="https://ja.wikipedia.org/wiki/%E9%87%A3%E3%82%8A%E3%83%90%E3%82%AB%E6%97%A5%E8%AA%8C" target="_blank" rel="noopener noreferrer">釣りバカ日誌</a><br>夫婦で運動に挑戦<br>配偶者の呼び方<br>妻・嫁さん・奥さん・カミさん・家内<br>しっくりベースで妻<br>かぁちゃん<br>妻とさんぽをした話<br>---<br>引越しの話<br>東京北区赤羽<br>湘南から赤羽へ<br>江戸っ子っぽいおっちゃん<br><a href="https://ja.wikipedia.org/wiki/%E5%AF%8C%E5%B2%A1%E5%85%AB%E5%B9%A1%E5%AE%AE%E6%AE%BA%E4%BA%BA%E4%BA%8B%E4%BB%B6" target="_blank" rel="noopener noreferrer">富岡八幡宮殺人事件</a><br>初めて降り立った赤羽で見たもの<br>駐車場が見つからない<br>駐車場っぽいところの周辺でオーナーを探す<br>魚屋のおっちゃんのチラ裏賃貸契約書(赤羽スタイル)<br>ネットで検索したものがすべて<br>隠れ物件は意外にある？<br>リアルな出会い<br>門前仲町では食べログを使いたくない<br>ネットで調べて知った気になる<br>一次情報にあたる<br>すべてに深く精通するのは難しい<br>足を使うことは大事<br>通勤が乗り換え２回から０回に<br>判断することに使うパワー<br>通勤は無駄<br>山手線の自動運転テスト<br>在宅勤務<br>リモートスクラムの難しさ<br>---<br><a href="https://www.meti.go.jp/press/2018/12/20181212004/20181212004-1.pdf" target="_blank" rel="noopener noreferrer">DX推進ガイドラインのつまみ食い&nbsp;</a><br>事業部門のオーナーシップと要件定義能力<br>Howが悪かったのかWhatが悪かったのかWhyが悪かったのか<br>ゴールの設定は発注側が行う<br>プロダクトオーナーはROIを最適化する<br>要件定義をできるようになりたいIT部門<br>登場人物は、事業部門、IT部門、ベンダー<br>ゴールを決めれない事業部門<br>ベンダーが情シスとしか話ができない<br>プロジェクトマネジメントは誰がやるの？<br>要件定義を請負契約にした場合<br>よくある現行機能保証はコストを投じて本当にやりたいことなのか<br>失敗を許容されるマインドセットが醸成されているか<br>経営「できない理由を考える人が多い、できるにはどうしたらいいのか考えろ」<br>できない理由を考えるのは生き物本来の思考パターン<br>どのようにして死なない状況を作れるか<br>自動テストがあると攻めることができる<br>ITベンダームカつく<br>IT部門の力がなくなっている<br>選択可能な提案ができていない<br><a href="https://kray.jp/blog/attractive-product-backlog/" target="_blank" rel="noopener noreferrer">プロダクトバックログのINVEST</a><br>交渉可能な状態<br>---<br>次回以降のトライ<br>naoharu：対外的な発表を行う<br>dada：展示をする<br>ポリバレント<br><a href="https://zwift.com/ja/" target="_blank" rel="noopener noreferrer">ZWIFT</a></p>---  
template: BlogPost  
path: /5-1
date: 2019-06-08T06:15:50.738Z  
title: ep5-1:でてこいとびきりZENKAIパワー!
thumbnail: /assets/ep5-1.png
metaDescription: df sdf df  
---  
![ep5-1](/assets/ep5-1.png)  

<iframe src="https://open.spotify.com/embed/episode/6QFHgHYbVAUkPiDSadBkZO" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

<p>今回の収録環境はあまりよくない<br>元号変わって<br>大型連休<br>5月5日はdadaismの結婚式<br><a href="https://kamakura-koga.com/" target="_blank" rel="noopener noreferrer">鎌倉 古我邸</a><br>当日あは天気がよくてよかった<br>結婚式ってほんとうにいいものですね<br>結婚式タスクはトレロで管理<br>席次表の肩書<br>トライ進捗状況<br>Naoharu:対外発表をする<br>ターゲットをAgile Japanに定めた<br>気になる方は当日来てください<br><a href="https://www.agilejapan.org/" target="_blank" rel="noopener noreferrer">Agile Japan</a><br><a href="https://ja.wikipedia.org/wiki/%E6%9E%97%E8%A6%81_(%E5%AE%9F%E6%A5%AD%E5%AE%B6)" target="_blank" rel="noopener noreferrer">基調講演は 林要さん</a><br>ソフトバンクアカデミア<br>孫さんむちゃくちゃすごい<br>ドラゴンボールでいうと・・・<br><a href="https://dic.pixiv.net/a/%E6%88%A6%E9%97%98%E5%8A%9B5%E3%81%AE%E3%81%8A%E3%81%A3%E3%81%95%E3%82%93" target="_blank" rel="noopener noreferrer">ショットガンを持ったおっさん(戦闘力5のおっさん)</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>dadaism:個展をやる<br>展示会場との打ち合わせ<br>グッズ準備<br>jammingFMステッカー<br>Rebuild Meetupに行ってきた<br>フォロワー5万達成<br>3万リツイート<br>ちょｗｗおまえｗｗ有名人じゃんｗｗｗ<br>著名人フォロワー<br><a href="https://www.youtube.com/channel/UCMod1HDUu_SZslmDApR8zOQ" target="_blank" rel="noopener noreferrer">ゆたぽん</a><br>jammingFMから来ました！はNG<br><a href="https://www.mot-art-museum.jp/" target="_blank" rel="noopener noreferrer">東京都現代美術館 MOT</a><br><a href="https://bluebottlecoffee.jp/" target="_blank" rel="noopener noreferrer">ブルーボトルコーヒー</a><br><a href="https://bijutsutecho.com/magazine/news/exhibition/19691" target="_blank" rel="noopener noreferrer">トム・サックスの展示&nbsp;</a><br><a href="https://www.youtube.com/channel/UCtinbF-Q-fVthA0qrFQTgXQ" target="_blank" rel="noopener noreferrer">CaseyNeistat</a><br><a href="https://www.youtube.com/watch?v=h0nO27dUlHU" target="_blank" rel="noopener noreferrer">ケイシー、NY出るってよ</a><br>Knolling<br>ストラクチャード・ケイオス<br><a href="https://www.youtube.com/watch?v=49p1JVLHUos" target="_blank" rel="noopener noreferrer">10 bullet</a><br><a href="https://www.makita.co.jp/" target="_blank" rel="noopener noreferrer">株式会社マキタ</a><br><a href="https://www.makita.co.jp/product/category/kateiyoukiki/cm501d/cm501d.html" target="_blank" rel="noopener noreferrer">マキタのコーヒーメーカー&nbsp;</a><br><a href="https://www.makita.co.jp/product/syuuzin_series/rc200dz/rc200dz.html" target="_blank" rel="noopener noreferrer">マキタのお掃除ロボット</a><br>駒込の展示はトム・サックスのノーリングが見れる</p>---  
template: BlogPost  
path: /5-2
date: 2019-06-09T06:15:50.738Z  
title: ep5-2:ロゴス、エトス、パトス
thumbnail: /assets/ep5-2.png
metaDescription: df sdf df  
---  
![ep5-2](/assets/ep5-2.png)  
<iframe width="100%" height="20" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/630861192&color=%23ff5500&inverse=false&auto_play=false&show_user=true"></iframe>

</br>


***


</br>

<p>イントロ<br><a href="https://fukabori.fm/episode/14" target="_blank" rel="noopener noreferrer">fukabori.fm ep.14</a><br>計画が悪い vs 実装が悪い　犯人探しが始まりがち<br>企画が終わって開発を<br>オーシャンズ11<br>組織が別れていても良くてもお互いのフィードバックは早く<br>現場判断をするためには<br>統一された意思決定をするためには透明性が必要<br>野球のスコアボード<br><a href="https://diamond.jp/articles/-/202063" target="_blank" rel="noopener noreferrer">エストニアの透明性</a><br>透明性・検査・適応<br>この話のフィードバックください　#jammingfm　へ<br>物を伝える事において大切なこと<br><a href="https://www.amazon.co.jp/%E6%AD%A6%E5%99%A8%E3%81%AB%E3%81%AA%E3%82%8B%E5%93%B2%E5%AD%A6-%E4%BA%BA%E7%94%9F%E3%82%92%E7%94%9F%E3%81%8D%E6%8A%9C%E3%81%8F%E3%81%9F%E3%82%81%E3%81%AE%E5%93%B2%E5%AD%A6%E3%83%BB%E6%80%9D%E6%83%B3%E3%81%AE%E3%82%AD%E3%83%BC%E3%82%B3%E3%83%B3%E3%82%BB%E3%83%97%E3%83%8850-%E5%B1%B1%E5%8F%A3-%E5%91%A8/dp/4046023910/ref=as_li_ss_tl?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&amp;keywords=%E6%AD%A6%E5%99%A8%E3%81%AB%E3%81%AA%E3%82%8B%E5%93%B2%E5%AD%A6&amp;qid=1558167624&amp;s=gateway&amp;sr=8-1&amp;linkCode=sl1&amp;tag=dada05-22&amp;linkId=148248fefc82a9f43c33ef6c4271846a&amp;language=ja_JP" target="_blank" rel="noopener noreferrer">武器になる哲学&nbsp;</a><br>アリストテレス<br>ロゴス・エトス・パトス<br>プラトン<br>ダイアローグ<br>論破しがち<br>リスナーとの対話が大事<br>理想と現実<br>弁証法</p>
<p>トイレ休憩から帰ってきて・・・<br>配信スタイルについて<br>初めてテレビが出たときのコンテンツ<br>早くて丈夫な馬がほしい<br>車を提供すればいいのでは<br>トークテーマでエピソード分けるかどうか<br>リモート収録<br>なんでやってるの？<br>トークテーマバックログを公開する</p>
<p>jammigfmへのお便り紹介<br>写真でRAWデータ渡すのは恥ずかしい</p>
<p>jammingfmにフィードバックください！</p>---  
template: BlogPost  
path: /6-1
date: 2019-06-10T06:15:50.738Z  
title: ep6-1:ネタバレやめて！
thumbnail: /assets/ep6-1.png
metaDescription: df sdf df  
---  
![ep6-1](/assets/ep6-1.png)  

<iframe src="https://open.spotify.com/embed/episode/1EG0z3McTDTsPBTeWKcqE1" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>イントロ<br>フィードバック紹介<br>分割配信<br>30分ぐらいがよい<br>頭出しあるとよい<br><a href="https://matome.naver.jp/odai/2142425553034879801" target="_blank" rel="noopener noreferrer">壮大すぎるネタバレ「城之内死す」</a><br>インスタの鍵を開けてみた<br>ドラゴンボールも30分で次回予告あるじゃん</p></p>
<p>うどん2.0<br>自分のコストの見える化<br>効果の見えなさ<br>バリューストリームマップ</p>
<p>トライ<br>naoharu:対外発表をする<br><a rel="noopener noreferrer" href="https://www.agilejapan.org/" target="_blank">Agile Japan2019</a>に申し込んだ<br>アジャイルコミュニティ<br>dada:展示をする<br>日程が決まった<br>DMで恋愛相談<br>　<br><a rel="noopener noreferrer" href="https://note.mu/fladdict/n/nfec0b9e529ca" target="_blank">カメラという機械の行く末</a><br><a rel="noopener noreferrer" href="https://note.mu/fladdict" target="_blank">The Guild 深津さん&nbsp;</a><br>ハードからソフトへ<br>光学のボケ、擬似的なボケ<br>ソフトで作るボケはNG<br>暗所できれいに取れるようにソフトウェアできれいにするのはOK<br>老害になりつつある<br>最新の技術ではなくなったら芸術になる<br>今のレンズ資産</p>---  
template: BlogPost  
path: /6-2
date: 2019-06-17T06:15:50.738Z  
title: ep6-2:恥を恐れるな
thumbnail: /assets/ep6-2.png
metaDescription: df sdf df  
---  
![ep6-2](/assets/ep6-2.png)  

<iframe src="https://open.spotify.com/embed/episode/6Si8z3MNUwvLN2ScNIeJJi" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>



***


</br>

<p>4ヶ月やって6回配信した<br>新しいことにチャレンジしたい<br>正味どれだけのリスナーがいるのか<br><a href="https://ja.wikipedia.org/wiki/%E3%82%AA%E3%83%95%E3%83%A9%E3%82%A4%E3%83%B3%E3%83%9F%E3%83%BC%E3%83%86%E3%82%A3%E3%83%B3%E3%82%B0" target="_blank" rel="noopener noreferrer">オフラインミートアップ（いわゆる古き良き時代のオフ会）</a><br><a href="https://ja.wikipedia.org/wiki/%E3%82%BC%E3%83%AD%E9%99%A4%E7%AE%97" target="_blank" rel="noopener noreferrer">話し手対リスナー比（最悪ゼロ除算あるぞ）</a><br><a href="https://twitter.com/rebuildfm/status/1117204110573330432" target="_blank" rel="noopener noreferrer">rebuild.fmのオフラインミートアップ&nbsp;</a><br>jamming.fmのオフラインミートアップを新しいトライに追加<br>いつもお便りくれてる2人が来てくれたら話し手対リスナー比は1（2:2）<br>ハッカソンで副賞とかにもカスらずボロ負けした<br>若い子のものづくりの速さ<br>おっさん感を感じるだけでもハッカソン出てよかった<br>最近のコーディング話<br>敗因はアイデア力の無さ（全然bias breakしてない）<br><a href="https://www.danpink.com/2011/07/how-to-deliver-innovation-overnight/" target="_blank" rel="noopener noreferrer">ダニエルピンクが言っていたアトラシアンのFedEx Day</a><br>誰からもやれと言われないことをやる<br>jamming.fmの2人とハッカソン出たい人は&nbsp;#jammingfm&nbsp;まで<br>かいた恥の量がKPI<br>イケダヤハトさんも「量が大事」って言っている<br>@dada_ism&nbsp;の新婚旅行どこにしよう<br>@dada_ism&nbsp;は大学時代に世界旅行研究会に所属していた<br>最初は初心者なので東南アジア（タイとか）<br><a href="https://ja.wikipedia.org/wiki/%E6%B7%B1%E5%A4%9C%E7%89%B9%E6%80%A5" target="_blank" rel="noopener noreferrer">沢木耕太郎「深夜特急」の影響を御多分に漏れず受ける&nbsp;</a><br>南アフリカも行ってみたい。（クライミングやってる人には結構有名な場所）<br>ウズベキスタンの陶器とか料理<br>@naoharuはもっぱらリゾート旅行<br><a href="http://defenceless.org/2019/03/31/hawaii-trip-2019-day-2/" target="_blank" rel="noopener noreferrer">オアフ島のレンタカーのタイヤがバーストするくらいでめっちゃハフハフする&nbsp;</a><br><a href="http://defenceless.org/2016/01/21/central-america-trip-7/" target="_blank" rel="noopener noreferrer">@naoharuの新婚旅行は中米（ベリーズのカリビアンな風土がすごく良かった）</a><br>社会主義終了直前のキューバの子供が遊戯王カードで遊んでいた話<br>館内放送がなったので今週はそろそろ終わり<br>新婚旅行おすすめ場所（この国おもしろい！など）は&nbsp;#jammingfm&nbsp;まで</p>---  
template: BlogPost  
path: /7
date: 2019-06-24T06:15:50.738Z  
title: ep7.後医は名医
thumbnail: /assets/ep7.png
metaDescription: df sdf df  
---  
![ep7](/assets/ep7.png)  

<iframe src="https://open.spotify.com/embed/episode/3EOkEDIjwg1wBjE4ga4PY3" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>フィードバック<br>道具として好きなカメラ<br>一眼レフを超えたとき<br>道具がアップデート<br>ミラーレス移行の動機<br><a href="https://amzn.to/3boLqgO">Sony RX100M5&nbsp;</a><br><a href="https://amzn.to/3boLqgO">RICOH GR&nbsp;</a><br>編集点<br>トライ進捗<br>naoharu:申し込んで結果待ち<br>acseptされなかった場合どうするの<br>dada:展示やる<br>業者に見積もりを依頼している<br>見積もりの内訳だして<br>ネゴシエイシブルな状態<br>ステッカーを再販<br>新トライ：ハッカソン出る<br>次回までにハッカソン情報を集める<br>dadaの結婚式で一番感動したこと<br>dada妻「初めてあったときに私はこの人と結婚すると決めたんです」<br>「え？どこでそう思ったんだろう？」て言った<br>主語によってはいろんな解釈があるぞ<br>そんな深い意味はない<br>夫婦の会話で<br>後医は名医<br>名医になる３つのアドバンテージ<br>批判したところで患者は治らない<br>医療業界全体にも悪い影響がある<br>IT業界にも同じような状況があるぞ<br>失敗を許容する文化　</p>---  
template: BlogPost  
path: /8
date: 2019-07-01T06:15:50.738Z  
title: ep8:自己肯定感を高めるには
thumbnail: /assets/ep8.png
metaDescription: df sdf df  
---  
![ep8](/assets/ep8.png)  

<iframe src="https://open.spotify.com/embed/episode/6WrJdarnJHWkIiNbbQwxl5" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>新しいイントロスタイル<br>天然パーマはおしゃれ<br>髭生えてるけど大丈夫なの<br>去年からTシャツOKになった<br>前回のフィードバック<br>聴視数はとれないの？<br>アクセスカウンター自分で回しちゃう<br>キリ番ゲットー！<br>インターネットの昔話<br><a rel="noreferrer noopener" href="https://moognyk.hateblo.jp/entry/2019/06/24/080000" target="_blank">脅威のカメラ性能！ Google Pixel 3で撮ったネパール＆エベレスト街道</a>Pixel3スゲェ<br>カリッカリのシャープネス<br><a rel="noreferrer noopener" href="https://www.flickr.com/photos/100325057@N07/48112928362" target="_blank">ヌプツェの岩肌の描写</a><br>何が写っているかを判定して補正をかける<br><a rel="noreferrer noopener" href="https://www.flickr.com/photos/100325057@N07/48112910332" target="_blank">カトマンズの雑踏</a><br>HDR感強め<br>撮って出しでこれ<br>ちょっと不自然？<br>みんながいいと思うのはどうやって情報をとってるの？<br>Adobeのクラウド<br><a rel="noreferrer noopener" href="https://www.flickr.com/photos/100325057@N07/48118577846" target="_blank">ボケが不自然？</a><br>SNS時代はアップするまでにいかに手間をなくすか<br>完全にコンデジいらなくなったな<br>pixel3欲しくなった<br>Naoharuは娘が2人いる<br>子育ては悩む<br>初めてだし不確実だし、先輩を当てにしていいのか<br>子育てについて積極的に勉強している<br>読んだ本で、これは仕事にも通じるものがあるのではとハッとした<br><a rel="noreferrer noopener" href="https://amzn.to/3Eq4kjI" target="_blank">世界標準の子育て</a><br>日本と欧米の子育ての違いは？<br>私はあなたの味方<br>夫婦喧嘩は世界の終わり<br>仕事も同じ<br>あなたは何があってもチームに必要とされているのです<br>好きと伝えるのが上手い人<br>dadaismの結婚式の誓いの言葉<br>自己肯定感はいちばんの土台<br>dadaismは自己肯定感が低い</p>---  
template: BlogPost  
path: /9
date: 2019-07-29T06:15:50.738Z  
title: ep.9:ポリコレ
thumbnail: /assets/ep9.png
metaDescription: df sdf df  
---  
![ep9](/assets/ep9.png)  
<iframe src="https://open.spotify.com/embed/episode/3KD8VlKgbgQZHvbLW1uuie" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***


</br>

<p>イントロ<br>お寺の聴聞会に行ってきた<br>聴聞会って？<br>浄土真宗本願寺派<br>自分だけの力では生きていない<br>自己の肥大化<br>努力では回避できないこと、例えば老いや病い<br>させていただいているという考え方<br>仏教の教えと自己肯定感はコンフリクトする？<br>世界標準の子育ての話の中で<br>どちらが良い悪いではなく、考え方の違いで世界的なトレンド<br>聴聞会の雰囲気<br>アジャイルと仏教<br>日本の製造業からくる流れ<br>第二次世界大戦<br><a href="https://ja.wikipedia.org/wiki/%E3%83%99%E3%83%88%E3%83%8A%E3%83%A0%E6%88%A6%E4%BA%89" target="_blank" rel="noreferrer noopener" aria-label=" (新しいタブで開く)"> ベトナム戦争 1955年-1975年</a><br>反体制からヒッピー<br>ジャンキーになったヒッピーが拠り所にしたもの<br>東洋思想がカリフォルニアンカルチャーやハッカー文化に取り込まれる<br>型を大事にする空手<br>まず型で覚えてから理解する<br><a href="https://agilemanifesto.org/" target="_blank" rel="noreferrer noopener" aria-label=" アジャイルソフトウェア開発宣言 (新しいタブで開く)"> アジャイルソフトウェア開発宣言</a><br>不同意の同意 or 不合意の合意<br><a href="https://agilemanifesto.org/iso/ja/principles.html　" target="_blank" rel="noreferrer noopener" aria-label=" アジャイル宣言の背後にある原則 (新しいタブで開く)"> アジャイル宣言の背後にある原則</a><br>無常　変わらないものはない</p>
<p><a href="https://www.disney.co.jp/movie/aladdin.html" target="_blank" rel="noreferrer noopener" aria-label="実写版アラジンを観た  (新しいタブで開く)">実写版アラジンを観た </a><br>コスったらランプの魔人出てくるやつ<br><a href="https://ja.wikipedia.org/wiki/%E5%8D%83%E5%A4%9C%E4%B8%80%E5%A4%9C%E7%89%A9%E8%AA%9E" target="_blank" rel="noreferrer noopener" aria-label="原作は千一夜物語  (新しいタブで開く)">原作は千一夜物語 </a><br>アラビアンナイトの一番最初は中国の話だという説　<br>ディズニーアニメと実写版の違い<br>ジャスミンがむちゃくちゃ強い　<br>女系天皇<br><a href="https://ja.wikipedia.org/wiki/MeToo" target="_blank" rel="noreferrer noopener" aria-label="#Metooムーブメント (新しいタブで開く)">#Metooムーブメント</a><br>時代を反映している<br><a href="https://www.disney.co.jp/fc/anayuki.html" target="_blank" rel="noreferrer noopener" aria-label="アナと雪の女王 (新しいタブで開く)">アナと雪の女王</a><br>女の子がなんとか乗り切るやる<br>ホワイトウォッシュ問題<br>みんな英語話す問題<br>源氏物語の光源氏が英語を話すか？<br><a href="https://www.netflix.com/title/80025172" target="_blank" rel="noreferrer noopener" aria-label=" Netflix ナルコス  (新しいタブで開く)"> Netflix ナルコス </a><br>スペイン語の発音の中でも違うらしい<br>どこまで厳密にやるか<br>歌舞伎ですら何言ってるかよくわからない<br>アイデンティティの毀損があるかどうか<br><a href="https://eiga.com/news/20190709/5/" target="_blank" rel="noreferrer noopener" aria-label=" 実写版 リトル・マーメイド  (新しいタブで開く)"> 実写版 リトル・マーメイド </a><br>アリエルが黒人女性<br><a href="https://ja.wikipedia.org/wiki/%E3%82%B7%E3%83%A7%E3%83%BC%E3%82%B7%E3%83%A3%E3%83%B3%E3%82%AF%E3%81%AE%E7%A9%BA%E3%81%AB">ショーシャンクの空に </a><br>モーガン・フリーマン演じるレッド<br>スティーブン・キングの原作との違い<br>原作と配役の違いをあえてジョークにする<br>スティーブン・キング 原作「刑務所のリタ・ヘイワース」<br>ショーシャンクの空には古典なのでネタバレというほどでもない<br>映画の原題は「The Shawshank Redemption」<br><a href="https://www.cinematoday.jp/news/N0109975" target="_blank" rel="noreferrer noopener" aria-label=" 007新作 (新しいタブで開く)"> 007新作</a><br>無類のダニエル・クレイグ好き<br>ジェームズ・ボンドが主役じゃない？<br>黒人女性が主役<br>ボンドガール<br>濡れ場<br><a href="https://ja.wikipedia.org/wiki/%E8%AA%B2%E9%95%B7%E5%B3%B6%E8%80%95%E4%BD%9C" target="_blank" rel="noreferrer noopener" aria-label=" 課長 島耕作  (新しいタブで開く)"> 課長 島耕作 </a><br><a href="https://togetter.com/li/979067" target="_blank" rel="noreferrer noopener" aria-label=" セレブレーション○○  (新しいタブで開く)"> セレブレーション○○ </a><br>ボンドガイ<br>逆にボンドガールとのカラミ<br>セクシャルマイノリティ<br>ポリコレ全部盛り<br>一歩先に行って逆に皮肉<br>イギリスのリベラルってどんな感じなんだろう<br>エリザベス女王<br>ボンドカーが世界一高い広告枠<br><a href="https://www.astonmartin.com/ja" target="_blank" rel="noreferrer noopener" aria-label=" (新しいタブで開く)"> アストンマーチン</a> <br>セルシオのボンドカー<br><a href="https://www.landrover.co.jp/index.html" target="_blank" rel="noreferrer noopener" aria-label="ランドローバー   (新しいタブで開く)">ランドローバー </a></p>---  
template: BlogPost  
path: /10
date: 2019-08-03T06:15:50.738Z  
title: ep10:アジャイルをするな、アジャイルであれ。
thumbnail: /assets/ep10.png
metaDescription: df sdf df  
---  
![ep10](/assets/ep10.png)  
<iframe src="https://open.spotify.com/embed/episode/54WqKLRWk52baieEV2imIE" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>イントロ<br><a rel="noreferrer noopener" aria-label=" Kindle Paperwhite  電子書籍リーダー (新しいタブで開く)" href="https://amzn.to/2Krafe3" target="_blank">Kindle Paperwhite  電子書籍リーダー</a><br><a rel="noreferrer noopener" aria-label=" AgileJapan2019 (新しいタブで開く)" href="https://www.agilejapan.org/" target="_blank">AgileJapan2019</a><br><a rel="noreferrer noopener" aria-label=" SIerって本当のところどうなの？ (新しいタブで開く)" href="https://www.agilejapan.org/session.html#g-03" target="_blank">SIerって本当のところどうなの？</a><br>裏KPIがあった<br>転職するならSIer or ユーザー企業<br>エンタープライズ系 or サービス系という分け方もあった<br>結果10％増えた<br>このポッドキャストでハイヤリング<br>@naoharuの会社で働きたい方は#jammingfmまで<br>jamming.fmリスナーが会いに来てくれた！<br><a rel="noreferrer noopener" aria-label="基調講演 マネージャー不在の洞窟型組織 (新しいタブで開く)" href="https://www.agilejapan.org/session.html#e-03" target="_blank">基調講演 マネージャー不在の洞窟型組織</a><br>ハードウェアでアジャイルを実践している<br><a rel="noreferrer noopener" aria-label="あなたの組織のマネージャーは本当に必要ですか？ (新しいタブで開く)" href="https://www.agilejapan.org/session.html#e-09" target="_blank">あなたの組織のマネージャーは本当に必要ですか？</a><br>林要さんの知的レベルやばい<br>人前でしゃべるのが極度に苦手と思わせない基調講演だった<br>100億円を溶かしながらも売上は未だ0<br>プロダクトオーナーはやっぱり大変<br><a rel="noreferrer noopener" aria-label="LOVOT (新しいタブで開く)" href="https://lovot.life/" target="_blank">LOVOT</a><br>ファービーとはぜんぜん違う<br>自動運転車の技術<br>役に立たないけど意味があるロボット<br>林要さんに寄ってきてしまう仕様<br>なつく感じを再現している<br>値段は可愛くない<br><a rel="noreferrer noopener" aria-label="AIBO (新しいタブで開く)" href="http://aibo.sony.jp/" target="_blank">AIBO</a><br>情操教育に使えるロボット<br>AIBOの耳を噛みちぎる娘<br><a rel="noreferrer noopener" aria-label="PMP (新しいタブで開く)" href="https://www.pmi-japan.org/pmp_license/" target="_blank">PMP</a><br><a rel="noreferrer noopener" aria-label="日本プロジェクトマネジメント協会 PMAJ (新しいタブで開く)" href="https://www.pmaj.or.jp/" target="_blank">日本プロジェクトマネジメント協会 PMAJ</a><br><a rel="noreferrer noopener" aria-label="破壊的な時代に生き残るためのアジャイルなプロジェクトマネジメント (新しいタブで開く)" href="https://www.agilejapan.org/session.html#e-16" target="_blank">破壊的な時代に生き残るためのアジャイルなプロジェクトマネジメント</a><br>戦略的持続的緊急対応＝アジャイル<br> メーカー系の人たちが多かった印象<br><a rel="noreferrer noopener" aria-label="デンソー 「大企業をリファクタリングしてみる」 (新しいタブで開く)" href="https://www.agilejapan.org/session.html#kb-12" target="_blank">デンソー 「大企業をリファクタリングしてみる」</a><br>CI&amp;Tにインタビューしに行こう<br>次のトライどうするのか<br>痩せたい<br>理想的な体重<br>東京都の公共スポーツセンター<br>dadaのトライ<br>展示は10月21日〜11月2日<br><a rel="noreferrer noopener" aria-label="中目黒 TheWorks (新しいタブで開く)" href="http://theworks.tokyo/" target="_blank">中目黒 TheWorks</a><br>認定ヤバスタグラマー<br>アジャイルコミュニティに還元できた<br><a rel="noreferrer noopener" aria-label="Pythonは商標登録済み？　IT関係者ツイートで騒然  (新しいタブで開く)" href="https://www.nikkei.com/article/DGXMZO47713850U9A720C1000000/" target="_blank">Pythonは商標登録済み？　IT関係者ツイートで騒然 </a></p>---  
template: BlogPost  
path: /11
date: 2019-08-15T06:15:50.738Z  
title: ep11:メキシコ旅行
thumbnail: /assets/ep11.png
metaDescription: df sdf df  
---  
![ep11](/assets/ep11.png)  
<iframe src="https://open.spotify.com/embed/episode/7nJivGTXi0dqSoK569UerN" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
</br>


***


</br>

<p>イントロ<br>朝走ると一日中汗がでる<br>7時半〜8時頃走っている<br>夜9時に寝て5時に起きる生活になっている<br>naoharuの新トライ<br>年内に-5kg痩せる<br>ボルダリング　マスタークラスのコンペで決勝まで行きたいから痩せる<br>マスタークラスってなに？<br>naoharuは2級か1級<br>マスタークラスは実力より少し上の初段くらい<br>dadaのトライ進捗（展示をやる）<br>Tシャツの仕様を決めて発注した<br>オリジナルロゴをTシャツに入れる<br>思わぬところからの収入でTシャツグレードアップ<br>契約とかいろいろあるテレビ業界<br>普通に請求書を発行した<br>メキシコ旅行に行ってきた<br><a rel="noreferrer noopener" aria-label="成田→メキシコシティ→カンクン→プラヤ・デル・カルメン (新しいタブで開く)" href="https://goo.gl/maps/cgqNc56kMpECHQgX9" target="_blank">成田→メキシコシティ→カンクン→プラヤ・デル・カルメン</a><br>2018年メキシコで2位になったホテル<br>日本の代理店を通さずにホテルを予約<br>正規料金は高い<br>生きてきた中で泊まったことのない贅沢なホテル<br>正面は海、裏はマングローブ林<br>赤ちゃんのワニがいた<br>ディズニーランドばりのスタッフサービス<br><a rel="noreferrer noopener" aria-label="ROSEWOOD MAYAKOBA (新しいタブで開く)" href="https://www.rosewoodhotels.com/en/mayakoba-riviera-maya" target="_blank">ROSEWOOD MAYAKOBA</a><br>金が溶ける<br>学生のときは、タイのカオサンロードのドミトリーに200円で泊まってた<br>1000倍高いホテルは1000倍満足しない<br><a rel="noreferrer noopener" aria-label="プラヤ・デル・カルメン→カンクン→メキシコシティ→グァナファト (新しいタブで開く)" href="https://goo.gl/maps/xjWdTcsm2GWnjswW7" target="_blank">プラヤ・デル・カルメン→カンクン→メキシコシティ→グァナファト</a><br>街全体が世界遺産の町<br>治安が悪いイメージのあるメキシコ<br>観光地は安全<br><a rel="noreferrer noopener" aria-label="Netflix ナルコス (新しいタブで開く)" href="https://www.netflix.com/title/80025172" target="_blank">Netflix ナルコス</a><br>メキシコの警察も賄賂でなんとかなる<br><a rel="noreferrer noopener" aria-label="インドで騙されたホッテントリ「緊急帰国」　 (新しいタブで開く)" href="https://note.mu/shogo622/n/n99e40d665438" target="_blank">インドで騙されたホッテントリ「緊急帰国」</a><br>dadaは20歳のころに同じ手口の詐欺にあっていた<br>初めての海外旅行の経験<br>マレーシア　KLのおばさんに心を開く<br>指輪ジャラジャラ、ジュラルミンケースのブルネイの石油王登場<br>宿に帰ったらサークルの先輩がいた<br>本当の親切かどうかはわからない<br>ブラックジャック詐欺<br><a rel="noreferrer noopener" aria-label=" 映画リメンバー・ミー（原題：Coco） (新しいタブで開く)" href="https://www.disney.co.jp/movie/remember-me.html" target="_blank">映画リメンバー・ミー（原題：Coco）</a><br>死者の街のモチーフになった町　グァナファト<br><a rel="noreferrer noopener" aria-label="007 スペクター 冒頭シーン (新しいタブで開く)" href="https://www.youtube.com/watch?v=9RtsTVMYCbA" target="_blank">007 スペクター 冒頭シーン</a><br>グァナファトには野良犬いっぱいいた<br>スペイン語で「犬」は「ペロ」<br>新婚旅行感出すためにウェディングドレス持ってって写真とった<br>家族で行けるお手軽リゾートは？（ハワイ以外）<br> 現像したらアップする<br><a rel="noreferrer noopener" aria-label=" instagram @dadaism (新しいタブで開く)" href="https://www.instagram.com/dada617/" target="_blank">Instagram @dadaism</a><br>テキーラそんなに飲んでない<br><a rel="noreferrer noopener" aria-label=" コロナ缶のイノベーション (新しいタブで開く)" href="https://www.youtube.com/watch?time_continue=46&amp;v=mPdfbjzCXXY" target="_blank">コロナ缶のイノベーション</a><br>プラスチックのストロー使わない白人<br>斜に構えすぎだろ<br>メキシコのコーヒー<br>Chiapas州のコーヒー豆<br>コーヒー豆の袋をクライミングのチョーク入れにした<br>8月お盆の旅行<br>jamming.fmアウトドアオフラインミートアップ<br>いつもフィードバック頂いてるサントスさんおめでとうございます<br>沢木耕太郎 深夜特急の功罪<br>この場所に戻ってこれるかどうか</p>---  
template: BlogPost  
path: /12
date: 2019-08-22T06:15:50.738Z  
title: ep12:人は喜んで自己の望むものを信じる
thumbnail: /assets/ep12.png
metaDescription: df sdf df  
---  
![ep12](/assets/ep12.png)  
<iframe src="https://open.spotify.com/embed/episode/5EZID6bXi0xj2cbcSgtTNT" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***


</br>

<p>イントロ<br>お昼ごはんはサラダとおにぎり1個<br>社長が社食（下界）に<br>支払いはSuica<br>社長は危ないのでSuica使わない<br>食堂のおばちゃん<br>2本取り<br>出がらし状態<br>おすすめコンテンツ<br><a rel="noreferrer noopener" aria-label=" マインクラフト (新しいタブで開く)" href="https://www.minecraft.net/ja-jp/" target="_blank">マインクラフト</a><br><a rel="noreferrer noopener" aria-label=" youtube マインクラフト荒らしたったww&nbsp; (新しいタブで開く)" href="https://www.youtube.com/channel/UCXofX1K0I8UbC19TZ_0Hw3A" target="_blank">youtube マインクラフト荒らしたったww&nbsp;</a><br>悪意があるワールドを荒らす<br>小中学生がキレる配信<br><a rel="noreferrer noopener" aria-label=" ジェームス・ハーデンのトレーニング動画 (新しいタブで開く)" href="https://m.youtube.com/watch?v=d4obWcbQ5Sg" target="_blank">ジェームス・ハーデンのトレーニング動画</a><br>トレーニング中にトレーニング動画を観てテストステロンを誘発する<br><a rel="noreferrer noopener" aria-label="人生の品質向上委員会 (新しいタブで開く)" href="https://scrapbox.io/iqol/%E4%BA%BA%E7%94%9F%E3%81%AE%E5%93%81%E8%B3%AA%E5%90%91%E4%B8%8A%E5%A7%94%E5%93%A1%E4%BC%9A%E3%81%A8%E3%81%AF" target="_blank">人生の品質向上委員会</a><br><a rel="noreferrer noopener" aria-label="劣化するオッサン社会の処方箋 / 山口周著 (新しいタブで開く)" href="https://amzn.to/3Guy1Cd" target="_blank">劣化するオッサン社会の処方箋 / 山口周著</a><br>オッサンは負のフィードバックループの中にいる<br>自主性を重んじるチーム<br>支配的な人から離れる人<br>支配的な人が集める人<br>正常性バイアスが働く<br>オッサンに対応するにはオピニオンとイグジット<br>80年周期で社会が変わる<br>2025年の壁<br><a rel="noreferrer noopener" aria-label="経産省DXレポート (新しいタブで開く)" href="https://www.meti.go.jp/shingikai/mono_info_service/digital_transformation/20180907_report.html" target="_blank">経産省DXレポート</a><br><a rel="noreferrer noopener" aria-label="jamming.fm ep3 (新しいタブで開く)" href="https://jamming.fm/ep3/" target="_blank">jamming.fm ep3</a><br><a rel="noreferrer noopener" aria-label="ニューラリンク (新しいタブで開く)" href="https://japan.cnet.com/article/35140149/" target="_blank">ニューラリンク</a><br>次の時代を作る人<br>マトリックスの世界<br>世界を変えるイーロン・マスク<br>日本を変える林要さん<br>毎回同じ話しをしている<br>ランダムジャンプ必要<br>ゲスト回やりたい<br>フィルターバブル<br><a rel="noreferrer noopener" aria-label="エコーチェンバー (新しいタブで開く)" href="https://plan-ltd.co.jp/plog/8611" target="_blank">エコーチェンバー</a><br>tiktokは女の子しか出てこない？<br>お互いのtiktokを見せ合ってみる<br><a rel="noreferrer noopener" aria-label="袋麺でトイレを直す (新しいタブで開く)" href="https://www.narinari.com/Nd/20190554502.html" target="_blank">袋麺でトイレを直す</a><br>人によってぜんぜん違うコンテンツが見えている<br>Webを繁栄させる人たちは価値観の蛸壺に加担している<br>欲しい物じゃないものを提示する<br>Break the biasのやり方<br>新しいレコメンデーション<br>世界は思ったよりも分断されている</p>---  
template: BlogPost  
path: /13
date: 2019-08-28T06:15:50.738Z  
title: ep13:Jeep Wave
thumbnail: /assets/ep13.png
metaDescription: df sdf df  
---  
![ep13](/assets/ep13.png)  
<iframe src="https://open.spotify.com/embed/episode/10M7fn6ZPIY3qPCF9shUI2" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>イントロ<br>お詫び：guysの言い換えは正しくはヤーズではなくY’allでした<br>ひとり旅の海外の詐欺体験へのリプライ<br>スリスリとスリ<br>イタリアの詐欺手口<br>山手線で妖怪に会った<br>エジプトのピラミッドで口説かれた<br>とあるムスリムの名言<br>お盆やってたこと<br>静岡県浜名湖<br>愛知県渥美半島伊良湖岬<br>三重県鳥羽熊野伊勢<br>そして和歌山まわって東京1300kmくらい<br><a rel="noreferrer noopener" aria-label="年に一度は車中泊旅行 (新しいタブで開く)" href="http://dada-ism.net/overland-2018-hokkaido/" target="_blank">年に一度は車中泊旅行</a><br><a rel="noreferrer noopener" aria-label="マキタの扇風機 (新しいタブで開く)" href="https://amzn.to/2ZBfSBm" target="_blank">マキタの扇風機</a><br><a rel="noreferrer noopener" aria-label="Jeep Wrangler (新しいタブで開く)" href="https://www.instagram.com/p/Bobyhn2FrVb/" target="_blank">Jeep Wrangler</a><br><a rel="noreferrer noopener" aria-label="Jeep Wave (新しいタブで開く)" href="http://www.chukyo-chrysler.co.jp/shop/hamamatsuchuo/blog/2017/07/jeep-wave.html" target="_blank">Jeep Wave</a><br>あおり運転<br>BMW X5<br>煽られたらどうする<br>運転席側のドラレコ必要<br>京アニ放火事件<br>ガソリンがどう燃えるのか<br>爆発を動力にしている自動車古くないか<br>休みはお盆とは別に取得するスタイル<br>最初の要件定義からリリースまで携わった<br><a rel="noreferrer noopener" aria-label="NHK撃退シール (新しいタブで開く)" href="http://www.nhkkara.jp/seal.html" target="_blank">NHK撃退シール</a><br>インターネットに受信料を載せるのはありか<br>NHK集金人こわい<br><a rel="noreferrer noopener" aria-label="えらいてんちょう (新しいタブで開く)" href="https://www.youtube.com/channel/UC8sqFN_BPTa-m0sO_mpnVhg" target="_blank">えらいてんちょう</a><br><a rel="noreferrer noopener" aria-label="ZIP FM (新しいタブで開く)" href="https://zip-fm.co.jp/" target="_blank">ZIP FM</a><br>マツコデラックス 対 N国党<br>ポリティカルニュートラル<br>言論弾圧では<br>放送法が時代にあってない<br>スクランブル放送を実現したら解党する<br><a rel="noreferrer noopener" aria-label="イシューよりはじめよ (新しいタブで開く)" href="https://amzn.to/3CvcCX8" target="_blank">イシューよりはじめよ</a></p>---  
template: BlogPost  
path: /14
date: 2019-09-02T06:15:50.738Z  
title: ep14:持ち家 vs 賃貸
thumbnail: /assets/ep14.png
metaDescription: df sdf df  
---  
![ep14](/assets/ep14.png)  
<iframe src="https://open.spotify.com/embed/episode/21rDrj8y0J2H8XQa89IlbD" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>イントロ<br>thug life <br>都内でネズミが大量発生している？<br><a rel="noreferrer noopener" aria-label=" 浅草橋ヤング洋品展 (新しいタブで開く)" href="https://www.instagram.com/p/B0s9kzLF8Eh/?utm_source=ig_embed" target="_blank">浅草橋ヤング洋品展</a><br>持ち家賃貸論争<br><a rel="noreferrer noopener" aria-label="エディタ戦争 (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/%E3%82%A8%E3%83%87%E3%82%A3%E3%82%BF%E6%88%A6%E4%BA%89" target="_blank">エディタ戦争</a><br>妻が大変な資産家<br> フラット35<br>タワマン派の田端氏<br><a rel="noreferrer noopener" aria-label="2022年、タワマンの「大量廃墟化」が始まることをご存じですか (新しいタブで開く)" href="https://gendai.ismedia.jp/articles/-/56992" target="_blank">2022年、タワマンの「大量廃墟化」が始まることをご存じですか</a><br>不動産ほど動かしづらいものはない<br>総コストは賃貸のほうが大きくなる<br>都心の地価は下がりにくい<br>民泊やろうかと思ったけど結構大変<br>シェアリングエコノミー<br>評価経済社会<br><a rel="noreferrer noopener" aria-label="Netflix ブラックミラー シーズン3 ランク社会 (新しいタブで開く)" href="https://www.netflix.com/title/70264888" target="_blank">Netflix ブラックミラー シーズン3 ランク社会</a><br><a rel="noreferrer noopener" aria-label="芝麻信用 (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/%E8%8A%9D%E9%BA%BB%E4%BF%A1%E7%94%A8" target="_blank">芝麻信用</a><br>保険そんなに入っていない<br>保険よりも予防医療<br><a rel="noreferrer noopener" aria-label="むだ死にしない技術 (新しいタブで開く)" href="https://amzn.to/3mmjkJg" target="_blank">むだ死にしない技術</a><br><a href="https://www.quora.com/Why-do-some-developers-at-strong-companies-like-Google-consider-Agile-development-to-be-nonsense" target="_blank" rel="noreferrer noopener" aria-label="なぜGoogleといった強い会社のエンジニアは「アジャイルはナンセンスだ」と考えるか、元Googleの人が答えた (新しいタブで開く)">なぜGoogleといった強い会社のエンジニアは「アジャイルはナンセンスだ」と考えるか、元Googleの人が答えた</a><br>すごいやつがいないと意味がない<br>靴紐を結べない人がスクラムやったらどうなる？<br>アジャイルやるためにはウォーターフォールを知っている必要があるか<br>アジャイルネタは需要あるのか？<br>30代前半でSNSの見える風景が変わってきた<br>子供の写真を上げるモチベーションってなに<br>子供の肖像権問題は何が問題になるの？<br>SNSでいろいろ特定できる<br>役に立たないSNS<br>世界の隔たりを全部取り除く<br>街コン<br>地理的な断絶<br>全方位に偏りがないのは無理<br>トライ進捗がよくない<br>厳かにならないように<br>ふりかえり回あってもいいな</p>---  
template: BlogPost  
path: /15
date: 2019-09-10T06:15:50.738Z  
title: ep15:豊かな心がなければ、富は醜い物乞いでしかない。（ゲスト：よなよなこさん）
thumbnail: /assets/ep15.png
metaDescription: df sdf df  
---  
![ep15](/assets/ep15.png)  
<iframe src="https://open.spotify.com/embed/episode/76mbCCYLD5t3MusqOoH5qT" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
***


</br>

<p>イントロ<br>ゲスト：よなよな子さん<br>記念すべき初ゲスト回<br>naoharuとよなよな子さんの出会い<br>元国営大手企業のインターンシップ<br>大学院進学<br>インターンシップで負の評価<br>社内向けシステム開発担当<br>jammingfmのリスナー像<br>大企業の情シスってどんなの<br>社内で影響力のある情シス<br>20年前から同じシステムをぐちゃぐちゃしている（いい意味で）<br>情シスに入りたかったわけではない<br>配属ガチャ<br>エンドユーザー向けシステムがやりたかった<br>法人向けの部署に異動<br>そしてまたシステム担当<br>希望は言えるけども、、、<br>組織的な戦略の中で藻屑となった<br>会社「情シスめっちゃ強化したるで」<br>入らないとわからない新卒採用<br>jammingfmのフィードバック少ない<br>綴り難しい問題<br>#jamharuism?<br>リスナー増やさないと<br>かわいい声で釣る<br>そして転職へ<br><a href="https://www.wantedly.com/" target="_blank" rel="noreferrer noopener" aria-label="Wantedly (新しいタブで開く)">Wantedly</a><br><a rel="noreferrer noopener" aria-label=" (新しいタブで開く)" href="https://doda.jp/" target="_blank">duda</a><br>深田恭子好き<br>Webディレクター専門の転職エージェントサービス<br>年収ダウン<br>生ける屍よりも楽しい仕事<br>職場環境はむちゃくちゃよくなった<br>MacBookPro配られる<br>最新のグループウェア揃ってる<br>オフィスおしゃれ（なんとか門）<br>スムージー<br>会社から大事にされている<br>会社にウォーターサーバーがある<br>水道水を飲まされる職場<br>デスク広い<br>日本有数の大企業の実態</p>---  
template: BlogPost  
path: /16
date: 2019-09-16T06:15:50.738Z  
title: ep16:マッチングアプリ概論（ゲスト：よなよなこさん）
thumbnail: /assets/ep16.png
metaDescription: df sdf df  
---  
![ep16](/assets/ep16.png)  
<iframe src="https://open.spotify.com/embed/episode/43haOqKs8pymHKQOgFLPxC" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>イントロ<br> 引き続きよなよな子さんをゲストにお迎えして<br> 好きなビール<br><a rel="noreferrer noopener" aria-label="builderscon tokyo 2019 (新しいタブで開く)" href="https://builderscon.io/tokyo/2019" target="_blank">builderscon tokyo 2019</a><br><a rel="noreferrer noopener" aria-label="エンジニア・コミュニティにはオープンであってほしい (新しいタブで開く)" href="https://nekogata.hatenablog.com/entry/2019/08/30/005448" target="_blank">エンジニア・コミュニティにはオープンであってほしい</a><br><a rel="noreferrer noopener" aria-label="運営公式アナウンス (新しいタブで開く)" href="https://blog.builderscon.io/entry/2019/08/30/120807 CROSS MEhttps://crossme.jp/" target="_blank">運営公式アナウンス</a><br>主要なマッチングアプリを制覇した<br>2014年にpairs、tinderはじめた<br><a rel="noreferrer noopener" aria-label="paris (新しいタブで開く)" href="https://www.pairs.lv/" target="_blank">paris</a><br><a rel="noreferrer noopener" aria-label="tinder (新しいタブで開く)" href="https://apps.apple.com/jp/app/tinder-%E3%83%86%E3%82%A3%E3%83%B3%E3%83%80%E3%83%BC/id547702041" target="_blank">tinder</a><br>釣り堀<br>合コンとマッチングアプリの比較<br>効率と信頼のトレードオフ<br>うそはうそであると見抜ける人でないと（マッチングアプリを使うのは）難しい<br>別人が来る<br>やたら散歩したがる人<br>カシオレくん<br>Jamming.fmリスナーには刺激強め<br>時間かけてやりとりした方が変な人は少なくなる<br>マッチングアプリはアジャイル<br><a href="https://dine.dating/ja" target="_blank" rel="noreferrer noopener" aria-label="まず会いたい人へのオススメはdine (新しいタブで開く)">まず会いたい人へのオススメはdine</a><br>ドタキャン対策<br>マッチした時の温度感と実際に会うときのめんどくささ<br><a rel="noreferrer noopener" aria-label="偽装不倫 (新しいタブで開く)" href="https://www.ntv.co.jp/gisouhurin/" target="_blank">偽装不倫</a><br>初対面対応スキル<br>マッチングアプリ修行<br>1セット90分・120分<br>延長なければ帰ります<br>最近の若い子のコミュ力<br><a rel="noreferrer noopener" aria-label="マッチングアプリで結婚したら離婚しにくい調査結果 (新しいタブで開く)" href="http://agora-web.jp/archives/2028951.html" target="_blank">マッチングアプリで結婚したら離婚しにくい調査結果</a><br><a rel="noreferrer noopener" aria-label="with (新しいタブで開く)" href="https://with.is " target="_blank">with</a><br>メンタリストDAIGO監修性格診断付きマッチングアプリ<br>アメリカでのマッチングアプリの調査<br>女性の方がストライクゾーン狭い<br>男はスペックと顔で判断されがち<br><a rel="noreferrer noopener" aria-label="グノシー (新しいタブで開く)" href="https://gunosy.com/" target="_blank">グノシー</a><br>知らないものは検索できない<br><a rel="noreferrer noopener" aria-label="airCloset (新しいタブで開く)" href="https://www.air-closet.com/" target="_blank">airCloset</a><br>オススメコンテンツ<br><a rel="noreferrer noopener" aria-label="バーフバリ 王の凱旋 (新しいタブで開く)" href="http://www.baahubali-movie.com/" target="_blank">バーフバリ 王の凱旋</a><br>人生の全てが全部入り<br>見るカンフル剤<br>Naoharuオススメのインセプション<br>人に期待をするな自分に期待しろ</p>---  
template: BlogPost  
path: /17
date: 2019-09-22T06:15:50.738Z  
title: ep17:判子文化
thumbnail: /assets/ep17.png
metaDescription: df sdf df  
---  
![ep17](/assets/ep17.png)  
<iframe src="https://open.spotify.com/embed/episode/72xclOWP0W2LACEdMxCHvJ" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
***


</br>

<p>イントロ<br>フィードバック紹介<br><a rel="noreferrer noopener" aria-label="IT担当相就任 (新しいタブで開く)" href="https://www.jiji.com/jc/article?k=2019012200725&amp;g=pol" target="_blank">IT担当相就任</a><br>78歳　はんこ議連会長<br>竹本さんの公式サイトドメイン<br><a rel="noreferrer noopener" aria-label="竹本さん公式サイト (新しいタブで開く)" href="http://takemotonaokazu.com/" target="_blank">竹本さん公式サイト</a><br><a rel="noreferrer noopener" aria-label="台湾のIT担当 (新しいタブで開く)" href="https://www.buzzfeed.com/jp/harunayamazaki/reberunoigasugoi78noitgano38puroguramaitgani" target="_blank">台湾のIT担当</a><br><a rel="noreferrer noopener" aria-label="セキュリティ担当 PC使ったことない (新しいタブで開く)" href="https://www.nikkansports.com/general/nikkan/news/201811150000224.html" target="_blank">セキュリティ担当 PC使ったことない</a><br>社内のはんこ文化ある？<br>インフルエンサーたちははんこ文化の企業とは仕事しない<br>はんこで権限を移譲<br>銀行のはんこ目Diff<br>日本の男性平均寿命約81歳<br>スマートフォンでSNSを投稿できればIT担当相になれる<br><a rel="noreferrer noopener" aria-label="あるIT会社の幹部から聴いたんだが、、、 (新しいタブで開く)" href="https://twitter.com/toukatsujin/status/1169396598507356161" target="_blank">あるIT会社の幹部から聴いたんだが、、、</a><br><a rel="noreferrer noopener" aria-label="マクドナルドで女子高生が言ってたんだけど、、、 (新しいタブで開く)" href="https://togetter.com/li/1213684" target="_blank">マクドナルドで女子高生が言ってたんだけど、、、</a><br>ZOZO、Yahoo!に買われる<br><a rel="noreferrer noopener" aria-label="バスキア展  (新しいタブで開く)" href="https://macg.roppongihills.com/jp/exhibitions/basquiat/ " target="_blank">バスキア展 </a><br>前澤さんは煽り耐性ない<br>アパレル業界の闇<br>ゾゾスーツ<br>背広<br><a rel="noreferrer noopener" aria-label="株式会社BANK 解散 (新しいタブで開く)" href="https://yusuke.tokyo/bank_kaisan/" target="_blank">株式会社BANK 解散</a><br><a rel="noreferrer noopener" aria-label="株式会社BANK (新しいタブで開く)" href="https://bank.co.jp/" target="_blank">株式会社BANK</a><br><a rel="noreferrer noopener" aria-label="株式会社価格自由 (新しいタブで開く)" href="https://kakakujiyu.jp/" target="_blank">株式会社価格自由</a><br><a rel="noreferrer noopener" aria-label="Yousuck (新しいタブで開く)" href="https://twitter.com/yousuck2020 " target="_blank">Yousuck</a><br><a rel="noreferrer noopener" aria-label="ランドローバー ディフェンダー (新しいタブで開く)" href="https://www.landrover.co.jp/vehicles/defender/index.html" target="_blank">ランドローバー ディフェンダー</a><br><a rel="noreferrer noopener" aria-label="スズキ ハスラー (新しいタブで開く)" href="https://www.suzuki.co.jp/car/hustler/" target="_blank">スズキ ハスラー</a><br> ランクル的な戦略？<br> ラダーフレームではなくアルミモノコックになってしまった<br> SUVかっこいい<br> 次の車なににしよう<br><a rel="noreferrer noopener" aria-label=" メルセデス・ベンツ　Gクラス (新しいタブで開く)" href="https://www.mercedes-benz.co.jp/passengercars/mercedes-benz-cars/models/g-class/g-class-suv/individualize.html" target="_blank">メルセデス・ベンツ　Gクラス</a><br><a rel="noreferrer noopener" aria-label="ランドローバー　イヴォーク (新しいタブで開く)" href="https://www.landrover.co.jp/vehicles/new-range-rover-evoque/index.html" target="_blank">ランドローバー　イヴォーク</a><br><a rel="noreferrer noopener" aria-label="スズキ　ジムニー (新しいタブで開く)" href="https://www.suzuki.co.jp/car/jimny/" target="_blank">スズキ　ジムニー</a><br>iPhone11<br>タピオカレンズ<br>トライフォビア大丈夫か<br>蓮コラ<br><a rel="noreferrer noopener" aria-label="MacPro (新しいタブで開く)" href="https://www.apple.com/jp/mac-pro/" target="_blank">MacPro</a><br><a href="https://weekly.ascii.jp/elem/000/000/199/199668/" target="_blank" rel="noreferrer noopener" aria-label="Power Mac G3／G4 ポリタンク (新しいタブで開く)">Power Mac G3／G4 ポリタンク</a><br>中華スマホどうよ<br>義理の母の中華スマホが中国のフォントになっている</p>---  
template: BlogPost  
path: /18
date: 2019-09-29T06:15:50.738Z  
title: ep18:ジェネレーションZ
thumbnail: /assets/ep18.png
metaDescription: df sdf df  
---  
![ep18](/assets/ep18.png)  
<iframe src="https://open.spotify.com/embed/episode/6P883lXpZpaMh4NzxKaGH3" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>

<p>東大農学部キャンパスにはハチ公の臓器がある<br> Unicodeで登録されているemojiのモアイ像はモアイでなくモヤイ<br> 肉の下にチーズがあるハンバーガー:ハンバーガー:<br> 赤ワインあるけど白ワインのemojiがない<br> tofu in the fire<br> 保存のアイコン、フロッピーディスクや黒電話の実物<br> ショウノート見れば何が話されているかわかる<br> 打ち合わせで水を飲むのはいいが従業員が勝手に持っていって飲んじゃダメ<br> 京都の人「しっかり経費管理してはるわぁ」<br> ソフトウェア開発とお医者さんの手術<br> 手術はソフトウェア開発のコンテキストでいうと商用障害なのでは<br> ブラックジャックによろしくの世界観<br> お医者さんになるの&amp;なった後も超大変<br> 日本の知的エリートは待遇的に報われてるのか<br> お医者さんの専門性はソフトウェア開発でいうとどの程度なのか<br> 手術のリスク（医療ミス）は青天井？<br> @dada_ism の奥さんはこれ聞いてどう思うんだ<br> 世間知らずがバレるのが恥ずかしい @naoharu<br> ジェネレーションZの能力<br> インターンに来た学生が激烈に優秀だった<br> ソフトスキルもハードスキルも高い<br> 知の高速道路、巨人の肩にのる<br> 10年前にマネジメントがやってたことを現場でやってる<br> どんどん若者に仕事をさせた方がいい<br> 我々の経験を活かそうと思わない方がいい<br> 一方で「負けたくない、とか無いの？」と自分で思う<br> 自分の市場価値<br> 何者かにならずにお金を稼げる時代がいつまで続くのか<br> ニコンがスポーツクライミングに協賛<br> スポーツクライミングでは事前の下調べに双眼鏡を使う<br> 双眼鏡にAI機能が搭載されててルートが解析されるのはありか？<br> アウトロ（今日話した話題）<br> 今の大学生は後に何かを教えるときにインスタのタグを教えてくれる<br> Googleの検索結果が汚染されすぎてる<br> 食べログやアマゾンの点数も怪しい<br> 食べログ最下位のお店に行ってみるのはどうか<br> ご飯は食べログ2でも配信的には4</p>---  
template: BlogPost  
path: /19
date: 2019-10-07T06:15:50.738Z  
title: ep19:Listner feedbacks
thumbnail: /assets/ep19.png
metaDescription: df sdf df  
---  
![ep19](/assets/ep19.png)  
<iframe src="https://open.spotify.com/embed/episode/3RBKSMi5hFanJvHkUKdhq1" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>
<p>クールビズ期間終了<br>半袖シャツやっぱないよなと気づいた　@naoharu<br>服装の縛りあるけど水飲めるからいいもん<br>物販やってるんだけど増税対応ミスって詫びステッカーを送った&nbsp;@dada_ism<br><a rel="noreferrer noopener" aria-label="詫びソースコード&nbsp; (新しいタブで開く)" href="https://nlab.itmedia.co.jp/nl/articles/1711/16/news121.html" target="_blank">詫びソースコード&nbsp;</a><br>フィードバック紹介<br>ちゃこさん「EP1は勉強回だと感じた。オススメ本あれば教えて。」<br><a rel="noreferrer noopener" aria-label="改めて聴き直すとEP1は確かに意識高い (新しいタブで開く)" href="https://jamming.fm/ep1/" target="_blank">改めて聴き直すとEP1は確かに意識高い</a><br>最近は意識レベル低下してる<br>アジャイルとかスクラムってIT以外でも使えるものなのでは<br>オススメ本は何か<br><a rel="noreferrer noopener" aria-label=" (新しいタブで開く)" href="http:// https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-Japanese.pdf" target="_blank">スクラムガイド</a><br><a href="https://amzn.to/3pOL9vO" target="_blank" rel="noreferrer noopener" aria-label="SCRUM BOOT CAMP THE BOOK (新しいタブで開く)">SCRUM BOOT CAMP THE BOOK</a><br>サントスさん「ハンコの強要は面倒。厳密な確認なんてしていないのでは。」<br>サントスさん「車プレゼント企画期待しています。」<br>ランドローバー社さんスポンサーお願いします<br>スポンサーがつくと公正な配信ができなくなる<br>ウィキペディアに寄付してる？<br>サントスさん「ハイスペックスマホで皆何をしているの？」<br>iPhoneはハイスペックスマホなのか<br>中華スマホはどうか？<br>Web閲覧でも「できる」と「快適」は違う<br>オノアツキさん「初代ムラーノからCX-8乗り継いでるけどSUVは良い」<br><a rel="noreferrer noopener" aria-label="初代ムラーノはカッコいい (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/%E6%97%A5%E7%94%A3%E3%83%BB%E3%83%A0%E3%83%A9%E3%83%BC%E3%83%8E" target="_blank">初代ムラーノはカッコいい</a><br>CX-3、CX-5、CX-8<br><a rel="noreferrer noopener" aria-label="CX-30発売 (新しいタブで開く)" href="https://www.mazda.co.jp/cars/cx-30/" target="_blank">CX-30発売</a><br>オノアツキさん「フォードエクスプローラでむちうち済み」<br>オノアツキさん「ハンコは無くなって欲しいけど実印はどうなるんだろう」<br>実印って何なのか<br>100均で売ってるハンコはユニークな印影ではない<br>シャチハタは実印にできない<br>イ<a rel="noreferrer noopener" aria-label="サノさん「ポケモン判子とかは面白いとは思う」 (新しいタブで開く)" href="https://www.rakuten.ne.jp/gold/hankos/pokemon/kanto/" target="_blank">サノさん「ポケモン判子とかは面白いとは思う」</a><br>どうでも良いハンコとかはピカチュウポンで子供に権限委譲<br>俺はフシギダネが好きだぞ〜という主張も可能<br>1トピック1配信という試み<br>今回はフィードバック回（沢山のフィードバックありがとうございました）</p>---  
template: BlogPost  
path: /20
date: 2019-10-09T06:15:50.738Z  
title: ep20:コタキナバル旅行
thumbnail: /assets/ep20.png
metaDescription: df sdf df  
---  
![ep20](/assets/ep20.png)  
<iframe src="https://open.spotify.com/embed/episode/1Rx5lT9QYL8gdPKkO2ZFW8" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***


</br>
<p>iPhone11Proを買いそびれた&nbsp;@dada_ism<br>欲しいと思ったときに店舗で買いたい<br>妻がiPhone11Proを買って羨ましい @naoharu<br>色はモスグリーン<br>@naoharu は XRなのでまだ買い替え早い<br>今日も業務終わりで収録か<br>1テーマで1配信で細かく出していく試み実施中<br>今回のテーマは佐々木さんの夏休み（さりげなく名字をバラされる）<br>ボルネオ島に行ってきました<br>@naoharu の娘が自然が好きなので行ってみた<br>娘「テングザルはオスが鼻が丸くてメスがとがってるんだお！」<br>内的モチベーションのすごさ<br>ポケモンの名前を暗記させる「興味」の凄さ<br>ボルネオ島のでっかい花（ラフレシア）の匂い<br>植物も昆虫もデカイ<br>超強力虫除けバンド（ファンがついてるやつ）も効かない<br>むしろそこにとまるんかい<br>制汗スプレーやサロンパスの香料が他の動物のフェロモンになったりする<br><a rel="noreferrer noopener" aria-label="GRAB (新しいタブで開く)" href="https://kozuresaton.com/grab_kl" target="_blank">GRAB</a><br><a rel="noreferrer noopener" aria-label="イスラム文化圏 (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/%E3%82%A4%E3%82%B9%E3%83%A9%E3%83%A0%E4%B8%96%E7%95%8C" target="_blank">イスラム文化圏</a><br>コタキナバル市立モスク<br><a rel="noreferrer noopener" aria-label="ラクサが美味だった (新しいタブで開く)" href="https://gotrip.jp/2017/01/53431/" target="_blank">ラクサが美味だった</a><br><a rel="noreferrer noopener" aria-label="走る練習カルトキャップ (新しいタブで開く)" href="https://replicantfm.shop/items/5d1d70993a7e967f2933e9cb" target="_blank">走る練習カルトキャップ</a><br><a rel="noreferrer noopener" aria-label="クアラルンプールやジャカルタの成長感すごい (新しいタブで開く)" href="https://www.digima-japan.com/knowhow/malaysia/14101.php" target="_blank">クアラルンプールやジャカルタの成長感すごい</a><br>みんな騙してくる<br>「BILLABONG！BILLABONG！」ってTシャツ売ってくる<br>コタキナバルのfake shopでシュプリームのバッグが1500円<br>神様が人間に与えたものは何か<br>@dada_ism は今お金めっちゃ使ってる<br>今日はこんなところで</p>---  
template: BlogPost  
path: /21
date: 2019-10-12T06:15:50.738Z  
title: ep21:スキル不足で居場所がなくなる
thumbnail: /assets/ep21.png
metaDescription: df sdf df  
---  
![ep21](/assets/ep21.png)  
<iframe src="https://open.spotify.com/embed/episode/6IEUFt4UrtduJvcAQn5xbr" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>
<p>パーソナル申し込んだ<br>ダイエット進捗　マイナス2kg<br><a rel="noreferrer noopener" aria-label="サイボウズの新人研修資料公開 (新しいタブで開く)" href="https://blog.cybozu.io/entry/2019/09/05/080000" target="_blank">サイボウズの新人研修資料公開</a><br>dadaの会社（メーカー）の新人研修ってどうなるの<br><a rel="noreferrer noopener" aria-label="開運ことはじめ (新しいタブで開く)" href="https://speakerdeck.com/cybozuinsideout/2019-01-orientation" target="_blank">開運ことはじめ</a><br>スクラム の資料が非公開になってるの気になる<br>サイボウズのスクラム<br><a rel="noreferrer noopener" aria-label="omoiyari.fm&nbsp;#31-1 スクラム導入の道のり (新しいタブで開く)" href="https://lean-agile.fm/episode/31-1" target="_blank">omoiyari.fm&nbsp;#31-1 スクラム導入の道のり</a><br>ジェネレーションz の知の高速道路<br>エンジニアカルチャー<br>うちの会社でやろうとしたら上司から「社外に公開して恥ずかしくないの？」って言われる<br><a rel="noreferrer noopener" aria-label="「スキル不足で職場に居場所がないおじさん」の救済プロジェクトに関わった時の話 (新しいタブで開く)" href="https://blog.tinect.jp/?p=62002" target="_blank">「スキル不足で職場に居場所がないおじさん」の救済プロジェクトに関わった時の話</a><br>45歳早期退職<br><a rel="noreferrer noopener" aria-label="キリンが早期退職を実施、過去最高益なのにリストラ着手の裏事情 (新しいタブで開く)" href="https://diamond.jp/articles/-/215955" target="_blank">キリンが早期退職を実施、過去最高益なのにリストラ着手の裏事情</a><br>スキルがないおじさんも過去はすごい人だった<br>その人しかできない仕事をそのままにしてしまった<br>自分じゃないとだめという仕事は気持ちがいい<br>自分じゃなくてもいい仕事だと俺なんだろうってなる<br>労働の流動性が低いと、会社従業員双方にとって不幸<br>転職してないのはビハインド<br>いい会社入ちゃったかも<br><a href="https://jamming.fm/ep15/" target="_blank" rel="noreferrer noopener" aria-label="よなよなこさんゲスト回ep15 (新しいタブで開く)">よなよなこさんゲスト回ep15</a><br>30半ばは転職は迷う</p>
---  
template: BlogPost  
path: /22
date: 2019-10-21T06:15:50.738Z  
title: ep22:信頼関係
thumbnail: /assets/ep22.png
metaDescription: df sdf df  
---  
![ep22](/assets/ep22.png)  
<iframe src="https://open.spotify.com/embed/episode/35XRTgwOk4Urxv5xIfEkif" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>
<p>DIYでクライミングのトレーニングボード作った<br>コーナンの1.5トントラック貸し出しUX<br>単管好き<br><a rel="noreferrer noopener" aria-label=" Mickipedia (新しいタブで開く)" href="https://micki-pedia.com/" target="_blank">Mickipedia</a><br>dadaが眠そう<br><a rel="noreferrer noopener" aria-label=" 心から楽しくクライミングをする (新しいタブで開く)" href="https://micki-pedia.com/sincerely-enjoy-climbing.html" target="_blank">心から楽しくクライミングをする</a><br>夫婦でクライミング中心の生活をしている<br>義務になって楽しめない<br>夫婦で旅行しているときに感じる<br>自発的に選んで辛いと思っていないと<br>常に自発的に楽しんで行われなければ長続きはしないし効果も出ない<br>安易に楽しくやろうよ！はハラタツ<br>成果を出すことは目指すべきゴール<br>楽しいのが最終ゴールなの？<br>成果が出るから楽しいんじゃないの？<br>楽しいが先に来ると成果でないのでは？<br>笑いながら山登りに挑戦している人はいない<br>成果がないと必要とされないのは辛い<br>誰かに必要とされるために成果をだす<br>健康じゃなければ成果がでない<br>楽しくなければ成果がでない<br>意見は対立するが、そこを乗り越えるのは信頼。<br>衝突して議論になればいい。議論にすら発生しない<br>信頼関係がないと議論にもならず機能しないチームとなる<br>この流れ1 on 1 ぽい<br>dadaは論破厨？<br>論破ではなく、ロジカルに諭す<br>心理的リアクタンス<br><a href="https://ja.wikipedia.org/wiki/XY%E7%90%86%E8%AB%96" target="_blank" rel="noreferrer noopener" aria-label="X理論Y理論 (新しいタブで開く)">X理論Y理論</a><br>スラムダンク読みきらないのは理解できない<br>「バスケットは好きか？」<br>配信スタイルにもフィードバックください</p>
---  
template: BlogPost  
path: /23
date: 2019-11-03T06:15:50.738Z  
title: ep23:お便り紹介
thumbnail: /assets/ep23.png
metaDescription: df sdf df  
---  
![ep23](/assets/ep23.png)  
<iframe src="https://open.spotify.com/embed/episode/3tTeFbILiZgdd1j3I6urR4" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>
<p>エニタイムフィットネスご成約<br> ジム行けてない<br> フィードバック紹介<br> ワントピックワントピックいいね（ちゃこさん）<br> テクニカルインシデント対策にもなる<br><a rel="noreferrer noopener" aria-label=" @mic_u (Mikipedia)さんフィードバック (新しいタブで開く)" href="https://twitter.com/mic_u/status/1186273958259085313" target="_blank"> @mic_u (Mikipedia)さんフィードバック</a><br> Mikipediaさんのゲスト回ある？<br> 夜勤で聴いてます（サントスさん）<br> 一本のエスカレーターで定年まで上がれない時代（せとっしーさん）<br> せとっしーさんゲスト回ある？<br><a rel="noreferrer noopener" aria-label=" @Oza5a516さんのフィードバック (新しいタブで開く)" href="https://twitter.com/Oza5a516/status/1179425035917893633" target="_blank">@Oza5a516さんのフィードバック</a><br>ワイヤレスイヤホン購入<br><a rel="noreferrer noopener" aria-label="GRDE 完全ワイヤレス イヤホン (新しいタブで開く)" href="https://amzn.to/3pP3o4c" target="_blank">GRDE 完全ワイヤレス イヤホン</a><br>人生の品質向上委員会にお便り出すレベル<br><a rel="noreferrer noopener" aria-label="Powerpeats Pro (新しいタブで開く)" href="https://amzn.to/3EwAaeN" target="_blank">Powerpeats Pro</a><br>Jamming.fmオリジナルトレーニングプレイリスト製作<br><a rel="noreferrer noopener" aria-label="LEAN COFFEE (新しいタブで開く)" href="https://www.slideshare.net/araratakeshi/lean-coffee-60156736" target="_blank">LEAN COFFEE</a><br>2分延長<br><a rel="noreferrer noopener" aria-label="AirPod Pro出た (新しいタブで開く)" href="https://www.apple.com/jp/shop/product/MWP22J/A/airpods-pro" target="_blank">AirPod Pro出た</a><br>イヤホンは消耗品<br>ウェブサイトあります <a href="https://jamming.fm/"><a href="https://jamming.fm/">https://jamming.fm/</a></a></p>---  
template: BlogPost  
path: /24
date: 2019-11-07T06:15:50.738Z  
title: ep24:台風19号
thumbnail: /assets/ep24.png
metaDescription: df sdf df  
---  
![ep24](/assets/ep24.png)  
<iframe src="https://open.spotify.com/embed/episode/2ozxXwhs2WKXE410dLYCmF" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>
<p>昨夜餃子を食べた @dada_ism<br>最近餃子流行ってる<br>サウナも流行ってる<br>サウナ大好き @naoharu<br>外気浴は良い<br>水風呂はマイルド系でお願いします<br>今日のテーマは台風<br>久しぶりの有事感（いつもとは違う感じ）<br>@dada_ism は荒川の氾濫にハラハラ<br>@naoharu は隅田川にハラハラ<br>今回もSNSで情報錯綜<br>集団心理は怖い<br>ムサコのタワマン全部ダメ、みたいなデマ<br>とはいえ都度検証も難しい<br>台東区避難所ホームレス受け入れ拒否はあったのか？<br>「避難所に入ることができるのは基本的人権」という意見<br>「受け入れろって人が自分の家にあげろ」という意見<br>「税金払ってるから自分は優遇されるべき」という意見<br>生きるか死ぬかの時に「税金を払ってる」ことを主張することに対する違和感<br>正常性バイアス<br>@dada_ism は車（車中泊）で避難した<br>キャンプで車中泊やっててよかった<br>理性が正常性バイアスに勝っている<br>@naoharu の意見なんて典型的な NIMBY なのではないか</p>
---  
template: BlogPost  
path: /25
date: 2019-11-10T06:15:50.738Z  
title: ep25:展覧会開催
thumbnail: /assets/ep25.png
metaDescription: df sdf df  
---  
![ep25](/assets/ep25.png)  
<iframe src="https://open.spotify.com/embed/episode/5NXqLWgHmQZiosJp8aQIMa" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


</br>
<p>国立天文台一般公開に言ってきた @naoharu<br>展覧会在廊してた&nbsp;@dada_ism<br>超人気ヤバスタグラマー<br>長期的TRY「展示をする」達成<br>niboshism&nbsp;<a rel="noreferrer noopener" target="_blank" href="https://slack-redir.net/link?url=https%3A%2F%2Fwww.instagram.com%2Fniboshism%2F%3Fhl%3Dja">https://www.instagram.com/niboshism/?hl=ja</a><br>第2回展示 niboshism2&nbsp;<a rel="noreferrer noopener" target="_blank" href="https://slack-redir.net/link?url=https%3A%2F%2Fniboshism.com">https://niboshism.com</a><br>物販もやってる<br>観てるとゲシュタルト崩壊してくる<br>深層学習で作られた煮干し<br>眉毛に見えましたという意見もある<br>若い女の子の反応が多くなってきた（ジェネレーションZ世代）<br>女の子「（煮干し）かわいい〜」<br>展覧会の見どころ<br>大きく出すのはフォトグラファーとして怖い<br>インターネットでなんかしたいと思って始めた<br>運用は労力そんなにかからない<br>「よく続けれますね」と言われることが多くなった<br>1人じゃなくて2人でやってるのがポイント<br>jamming.fmも2人でやっているから続く<br>深層学習で作らているものではなかった<br>展示は特異点<br>今回も握手してください案件発生<br>「ニボシズムさんですか？！」<br>「そうです。わたしがニボシズムです。」<br>名古屋から来た人　<a rel="noreferrer noopener" target="_blank" href="https://slack-redir.net/link?url=https%3A%2F%2Fwww.instagram.com%2Fpetitpoissonami%2F%3Fhl%3Dja">https://www.instagram.com/petitpoissonami/?hl=ja</a><br>抽象化された煮干しの絵を書く<br>海の生き物に煮干しが隠れている　<a rel="noreferrer noopener" target="_blank" href="https://slack-redir.net/link?url=https%3A%2F%2Fwww.instagram.com%2Fp%2FBy-O5aRpNvw%2F%3Figshid%3D1boh63du3sa88">https://www.instagram.com/p/By-O5aRpNvw/?igshid=1boh63du3sa88</a><br>河野太郎防衛大臣の似顔絵　<a rel="noreferrer noopener" target="_blank" href="https://slack-redir.net/link?url=https%3A%2F%2Fwww.instagram.com%2Fp%2FB4CddeFpWun%2F%3Figshid%3D1h4by7ov4o3dk">https://www.instagram.com/p/B4CddeFpWun/?igshid=1h4by7ov4o3dk</a><br>11月2日までやっている（リリース間に合わなかった）<br>展覧会のフィードバックもお待ちしています</p>---  
template: BlogPost  
path: /26
date: 2019-11-17T06:15:50.738Z  
title: ep26:レガシーコードからの脱却
thumbnail: /assets/ep26.png
metaDescription: df sdf df  
---  
![ep26](/assets/ep26.png)  

<iframe src="https://open.spotify.com/embed/episode/5ann4TdJ6OwUriAIfURVXx" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>ペアプロでTDDやっている&nbsp;@dada<br>モブワークやっている @naoharu<br>jamming.fm特別企画?<br><a rel="noreferrer noopener" aria-label="アクティブブックダイアログ (新しいタブで開く)" href="http://www.abd-abd.com/" target="_blank">アクティブブックダイアログ</a><br><a rel="noreferrer noopener" aria-label="レガシーコードからの脱却 (新しいタブで開く)" href="https://amzn.to/2ZIeKMX" target="_blank">レガシーコードからの脱却</a><br>全体2時間　ひとり50ページくらい<br>アウトプット前提で読む<br>構造化して理解しなければプレゼンできない<br>ひとりでもやればいいじゃんでもできない<br>10分後にアウトプットしなければいけないとなると脳みそフル回転<br>会社が違う人でダイアログをすると違う視点が混ざってよい<br>jamming.gm特別企画アクティブブックダイアログ決定！<br>ABDミートアップ参加者募集中<br>読みたい本も募集中<br>積ん読を消化できるメリットも<br>本をバラバラにする工程を始めてみた<br>その後は捨てるか自炊するか<br>ABDやっても同じ本を買ってしまう<br>レガシーコードからの脱却についての感想<br>チームで作っていたコードがレガシーとなったのがきっかけ<br>コーディングだけの話ではなかった<br>IT組織のマネージャーも読んでるものはある<br>アジャイルの文脈<br>プロダクトオーナーはHowは言うな<br>スクラムの運営方法のヒントが書かれている<br>著者・訳者をABDに参加してもらうのはおもしろそう<br>著者の前で発表するのはドキドキする<br>@naoharu はRSA暗号の発明者にRSA暗号について説明するというイベントをした<br>著者David Scott Bernsteinは認定スクラムディベロッパー講師<br><a rel="noreferrer noopener" aria-label="マーティンファウラー (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/%E3%83%9E%E3%83%BC%E3%83%86%E3%82%A3%E3%83%B3%E3%83%BB%E3%83%95%E3%82%A1%E3%82%A6%E3%83%A9%E3%83%BC" target="_blank">マーティンファウラー</a><br><a rel="noreferrer noopener" aria-label="ドメイン駆動開発 (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/%E3%83%89%E3%83%A1%E3%82%A4%E3%83%B3%E9%A7%86%E5%8B%95%E8%A8%AD%E8%A8%88" target="_blank">ドメイン駆動開発</a><br><a rel="noreferrer noopener" aria-label="エンタープライズアプリケーションアーキテクチャパターン (新しいタブで開く)" href="https://amzn.to/3jOyvt0" target="_blank">エンタープライズアプリケーションアーキテクチャパターン</a><br><a rel="noreferrer noopener" aria-label="Beyond Legacy Code (新しいタブで開く)" href="https://www.amazon.com/gp/product/1680500791/" target="_blank">Beyond Legacy Code</a><br>最悪壊れないという不安がないことで強気の提案ができる＝命綱<br>カバレッジ率100％の安心感<br>複雑なトライキャッチをプロダクションコードから作るとテストが漏れる<br>テストできないコードは書くな<br>こういう議論ができるのがABDのよさ<br>在廊してて疲れた<br><a href="https://twilog.org/ryuzee" target="_blank" rel="noreferrer noopener" aria-label="＠Ryuzee (新しいタブで開く)">＠Ryuzee</a>さんゲストに呼びたい<br>我々の戦略は、無理やりご本人にボールをぶつけていくスタイル</p>---  
template: BlogPost  
path: /27
date: 2019-11-30T06:15:50.738Z  
title: ep27:iPhone 11 Pro vs デジタル一眼レフカメラ
thumbnail: /assets/ep27.png
metaDescription: df sdf df  
---  
![ep27](/assets/ep27.png)  

<iframe src="https://open.spotify.com/embed/episode/2qhNj7J81lJy8FzkUvrwLe" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>iPhone11 Pro 買った @dada_ism<br>フィルムを画面に貼ってくれるアップルストアのサービスがオススメ<br>青山の赤い壺っていう激辛料理屋さんで食べたキンコンカーンコーン<br><a rel="noreferrer noopener" aria-label="青山の赤い壺っていう激辛料理屋さんで食べた激辛チヂミで胃痙攣を起こした  (新しいタブで開く)" href="https://tabelog.com/tokyo/A1306/A130602/13153198/" target="_blank">青山の赤い壺っていう激辛料理屋さんで食べた激辛チヂミで胃痙攣を起こした </a>@naoharu<br>表参道のアップルストアに逃げ込む<br>激辛好きの @naoharu と、もっと好きな @naoharuの妻<br>それでも次元の違う辛さ<br>チヂミが今体内のどこにいるか明確にわかる<br>「これで体調不良になっても文句言いません」っていう誓約書<br>クリスマスの表参道でサーモグラフィー状態の男一人<br><a rel="noreferrer noopener" aria-label="一眼レフでとった写真とiPhoneでとった写真を見分けられるかという企画 (新しいタブで開く)" href="https://iphone-mania.jp/news-265039/" target="_blank">一眼レフでとった写真とiPhoneでとった写真を見分けられるかという企画</a> （これから先は正解を書かないので上記記事を一緒にみながら試してみてください）結論その１<br>@dada_ism はエッジで判別する<br>@naoharu はボケで判別する結論その２<br>見分けるぞって見ないとほぼほぼ見分けがつかない。50万円クラスと10万円クラスのiPhoneでこれなら一眼レフいらないのでは説<br>最近Pixel4も出てる<br>これもうスマホの製品Webサイトじゃない。<br>みんなカメラ買うためにスマホ買ってるのでは。<br>@naoharu「ストレージつきますよってのもすごい」<br>@dada_ism 「それはニコンもある」<br>@naoharu 「すまん」<br>iPhone11でSNSに広角画像がめっちゃ増えた<br>みんな広角で撮る技術が育ち始めるのでは<br>クライミングでも今までなかったことが広角動画で起きている<br>Pixel4とiPhone11Proの決定的な違いは暗所で撮るところ<br>星空を4分露光でとってるけどズレってないってことは絶対ソフトウェア<br>写真って何なんだ。<br>100%ソフトウェアで作られた画像と写真は区別がつかない。<br><a href="https://nlab.itmedia.co.jp/nl/articles/1902/19/news078.html" target="_blank" rel="noreferrer noopener" aria-label="架空の人物の写真を作ることができる (新しいタブで開く)">架空の人物の写真を作ることができる</a><br>久々の写真トーク<br>写真のことになると話しすぎる<br>これについて話してほしい！というテーマがあればぜひ #jammingfm まで！</p>---  
template: BlogPost  
path: /28
date: 2019-12-07T06:15:50.738Z  
title: ep28:伝わらなかったらかっこよくても意味がない（ゲスト：グラフィックデザイナー森さん）
thumbnail: /assets/ep28.png
metaDescription: df sdf df  
---  
![ep28](/assets/ep28.png)  

<iframe src="https://open.spotify.com/embed/episode/6gCBSYyM42qRIEbp7OiVpC" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>Guest:<a href="https://www.facebook.com/yuya.mori.35" target="_blank" rel="noreferrer noopener" aria-label="グラフィックデザイナー 森くん (新しいタブで開く)">グラフィックデザイナー 森くん</a></p>
<p>Shownote:<br> 迷子になった @naoharu<br> イベントに行ったら誰もいなかった @dada_ism<br> 天一に豚キムチ入れたらうまかった　@森くん<br> 天下一品の日（10月1日）の週に3回行った<br> @dada_ismの妻の小学生の頃の友達<br> daada_ismと森くんはniboshism一緒にやってる<br> 京都上京区出身<br> 金沢美術工芸大学に行く<br> 美大卒の両親を親に持つ<br> 画材が転がっている家<br> 平行定規が転がっている家<br> 軍手が転がっている家<br> 新卒で広告業界に就職<br> 2回の転職を経てパッケージデザインをしている<br> デザイナーと聞いて<br> プロダクトデザイン<br> ウェブデザイン<br> 森くんはグラフィックデザイン<br> 紙に印刷されているものはグラフィックデザイナーの仕事<br> 広告業界から出版社へ<br> 広告業界よりもブラックだった<br> 年間170冊作ってた<br> そして現職は食品メーカーのインハウスデザイナー<br> 手にとってもらうためのデザイン<br> 広告業界とスピード感が違う<br> 広告業界はギリギリまでモノ作ってる<br> 夜中の1時2時に入稿して夕刊で刷られる<br> サッカー天皇杯の勝敗によって2パターン広告作る<br><a href="https://ja.wikipedia.org/wiki/投機的実行" target="_blank" rel="noreferrer noopener" aria-label=" 投機的実行（積極的実行) (新しいタブで開く)"> 投機的実行（積極的実行)</a><br> あの日から広告業界の残業、休日出勤が減った<br> 自分は残業しまくってた最後の世代なので古いのかもしれない<br> 9時-17時で終わらない仕事はマネジメントの問題<br> 働き方改革で働く時間短くなったけどクオリティは落とせない<br> クオリティは落として出したくない<br> 本当は自分の裁量で残業や家への持ち帰りを許すべき<br> ワークライフバランス<br> デザイナーは仕事とライフが結構あいまい<br> 平日はパッケージデザイン、休日はniboshismでデザイン<br> システムエンジニアはオンとオフで同じことができない？<br> 素人がデザインスキルを上げるためには？<br> デザインは使うためのもの<br> 自分が使いやすいかどうか、わかりやすいかどうか<br> パワーポイントのデザイン<br> 優れたグラフィックデザイン<br> デザイナーとして良いモノ、世の中的に良いモノ<br> 伝わらなかったらかっこよくても意味がない</p>---  
template: BlogPost  
path: /29
date: 2019-12-15T06:15:50.738Z  
title: ep29:アイデアファースト（ゲスト：グラフィックデザイナー森さん）
thumbnail: /assets/ep29.png
metaDescription: df sdf df  
---  
![ep29](/assets/ep29.png)  

<iframe src="https://open.spotify.com/embed/episode/0ahhcEjgA1afGkCjHMwtvu" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>Guest:グラフィックデザイナー 森くん https://www.facebook.com/yuya.mori.35</p>
<p>Shownote:<br> 忘年会行けない＠naoharu<br> 飲んでる＠dada<br> iPhoneSE2 @森<br> みんなが思うデザイナーの仕事って？<br> 絵を書くのがデザイナーではない<br> デザインを構成する5つの要素<br> 観察力・問題発見力・発想力・視覚化力・造形力<br> ロジックがないと仕事にならない<br> UX調査<br> デザイナーのインプットの仕方<br> デスクが汚い<br> 自分ではどこに何があるかはわかっている<br> 混沌から生まれるアイデア（<a rel="noreferrer noopener" aria-label="トム・サックス回 (新しいタブで開く)" href="https://jamming.fm/ep5-1/" target="_blank">トム・サックス回</a>）<br> 真っ白な机の上でデザインするという流行<br> 基盤の構成は最初手書きあとからPCで清書<br> 思考の限界よりも先にパワポの限界が来る<br> デッサンは造形力？観察力？<br> 大学によって学ぶことが違う<br> アイデアファースト<br> すごくかっこいいものをつくてもアイデアがだめなら面白くない<br><a href="https://amzn.to/2ZyYeyT" target="_blank" rel="noreferrer noopener" aria-label=" イシューからはじめよ (新しいタブで開く)"> イシューからはじめよ</a><br> 東京の美大生に置いてかれる<br> 電通博報堂が企画する<br> 仕上げはプロダクションの仕事<br> 下積み大事って言っちゃうのは老害なのか<br> デザイナーのキャリアとは<br> 巨匠になってしまえば勝ち組<br> サラリーマンデザイナーは管理職になるしかない<br> システムでは前工程ほど大事なので経験を積むと前工程にいく<br> デザイナーも同じ<br> プロダクション→代理店→メーカー<br> メーカーのインハウスデザイナーはいろいろ<br> 仕事は一対一の抜けるか抜けないかの世界<br> デザイナーは個人なのかチームワークなのか<br> デザイナーはどの段階から企画に加わるのか？<br> オリエンシート：デザイナーへの発注するための企画書<br> 商品企画のプロジェクトマネジメントはマーケティングがやっている<br> マーケや経営が迷ったときに代理店に相談する<br> クリエイティブディレクター：全体像をコントロールしている人<br> アートディレクター：細かいデザインをコントロールしている人<br> キャッチコピーが決まると全員が同じ方向を向ける</p>---  
template: BlogPost  
path: /30
date: 2019-12-22T06:15:50.738Z  
title: ep30:シズル感（ゲスト：グラフィックデザイナー森さん）
thumbnail: /assets/ep30.png
metaDescription: df sdf df  
---  
![ep30](/assets/ep30.png)  

<iframe src="https://open.spotify.com/embed/episode/1MkZDRUqZp17MvaaOMySuO" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>Guest:<a href="https://www.facebook.com/yuya.mori.35" target="_blank" rel="noreferrer noopener" aria-label="グラフィックデザイナー 森くん (新しいタブで開く)">グラフィックデザイナー 森くん</a><br>
森くんのデザインしたパッケージについて<br>海外のパッケージは日本人に受けない？<br>シズルカット<br>イギリス人はシズルカットを嫌う<br>日本人は絵をつけたがる<br>漢字はシズル感ある<br>袋麺に載っている写真は実際入っているものではない不思議<br>”調理の一例”<br>フルーツジュースでオレンジの断面を載せれるのは果汁100％だけ<br>果汁が低いと写真はダメ、イラストはOK<br>CMで虫歯菌ちょっと残るやつ<br>広告代理店の世代間<br>台風のときに仕方なく食べるラーメンはうまい<br>ミニマルなデザインのパッケージ多い<br><a rel="noreferrer noopener" aria-label="meiji THE Chocolate (新しいタブで開く)" href="https://www.meiji.co.jp/sweets/chocolate/the-chocolate/" target="_blank">meiji THE Chocolate</a><br>歳取ったら何やればいいの<br>若手をのびのびやさせる<br>何も言わなくてもがんばってくれるならいい<br>デザインはロジカル<br>アウトプットは永遠にできる<br>サーヴァント型はとりあえずやらせてみる<br>上司が怒られているところを部下に見せる<br>壺を割っちゃう職人<br>若手に自由にやらせようという時代<br>自発的にやらせて失敗させて気づかせるマネジメント<br>ブランドを持っている企業はチャレンジできる<br>ブランドは意外に消えている<br>当たり前品質に問題がある場合にブランドは毀損される<br>当たり前品質と魅力的品質<br>ブランドが確立されすぎて逆に足枷になる場合<br>ハイブランドはロイヤルカスタマーはうるさい<br><a rel="noreferrer noopener" aria-label=" Ape (新しいタブで開く)" href="https://bape.com/index/" target="_blank">Ape</a><br><a rel="noreferrer noopener" aria-label="SEEDA (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/SEEDA" target="_blank">SEEDA</a><br><a href="https://www.youtube.com/watch?v=2JuIziynF2o" target="_blank" rel="noreferrer noopener" aria-label="“おっと服に猿がついてる” (新しいタブで開く)">“おっと服に猿がついてる”</a></p>
---  
template: BlogPost  
path: /31
date: 2020-01-01T06:15:50.738Z  
title: ep31:ヤクブーツはやめろ
thumbnail: /assets/ep31.png
metaDescription: df sdf df  
---  
![ep31](/assets/ep31.png)  

<iframe src="https://open.spotify.com/embed/episode/0IBbqG5tVDwUOCWjEke16V" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

<p>咳のしすぎで肋骨が疲労骨折した@naoharu<br>ストレスで咳が止まらなくなる<br>fitbit versa2買いました@dada<br>Google fitbit買収する<br>モチベーションを保つために課金する<br>パーソナル使うと追い込める<br><a rel="noreferrer noopener" aria-label="Rebuildで知った (新しいタブで開く)" href="http://rebuild.fm/254/" target="_blank">Rebuildで知った</a><br><a rel="noreferrer noopener" aria-label="Chocchi(@andutys_0910)さんのフィードバック  (新しいタブで開く)" href="https://twitter.com/andutys_0910/status/1196423348944658432" target="_blank">Chocchi(@andutys_0910)さんのフィードバック </a><br>当たり屋スタイル<br>大麻ベンチャーや薬物関連<br>嗜好品大麻がアメリカで広がる<br><a rel="noreferrer noopener" aria-label=" 犬式 (新しいタブで開く)" href="https://twitter.com/inushiki2017" target="_blank">犬式</a><br>CBDオイル<br>大麻は栽培するには国の許可必要<br>ソフトドラッグとハードドラッグの違い<br>芸能人の薬物<br>インドの坊さんマリファナ普通に吸っている<br><a rel="noreferrer noopener" aria-label="マジックマッシュルームの鼻に噴射する製品 (新しいタブで開く)" href="https://buzzap.jp/news/20191212-magic-mushroom-nasal-spray/" target="_blank">マジックマッシュルームの鼻に噴射する製品</a><br>SLDと似た効用？<br>マジックマッシュルーム＝毒キノコ<br><a rel="noreferrer noopener" aria-label="アダム・オンドラのフラッシュ (新しいタブで開く)" href="https://vimeo.com/130242972" target="_blank">アダム・オンドラのフラッシュ</a><br>3:50あたり注目<br>ハワイで嗜好品のマリファナ合法化<br>現地で日本人が使うのは大丈夫なの？<br>あえて合法化することによってコントロールする<br>UBERガンジャ<br><a href="https://www.sankei.com/affairs/news/191120/afr1911200032-n1.html">栽培セット売って逮捕</a><br><a href="https://www.youtube.com/watch?v=fYtevmyiYmU" target="_blank" rel="noreferrer noopener" aria-label="SHO - 薬物はやめろ &quot;ヤクブーツはやめろ”(OFFICIAL MUSIC VIDEO) Japanese HIP HOP (新しいタブで開く)">SHO - 薬物はやめろ "ヤクブーツはやめろ”(OFFICIAL MUSIC VIDEO) Japanese HIP HOP</a></p>---  
template: BlogPost  
path: /32
date: 2020-02-08T06:15:50.738Z  
title: ep32:木こりのジレンマ
thumbnail: /assets/ep32.png
metaDescription: df sdf df  
---  
![ep32](/assets/ep32.png)  

<iframe src="https://open.spotify.com/embed/episode/6PNpUPs9kjvTd5AVXuqZYz" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>＠naoharu 絶賛Available<br>WeWork 丸の内北口スタジオからお届け<br>@dada_ism 耳栓して寝る<br>スマートウォッチの振動目覚まし意外に起きれるのでおすすめ<br>あけましておめでとうございます<br>木こりのジレンマ<br>その時、スクラムマスターは<br>バリューストリームマップやってみた<br>収録配信プロセスの無駄<br>ごっそり無駄プロセスが消える<br>大幅なリードタイム削減　<a rel="noreferrer noopener" aria-label=" 大幅なリードタイム削減　
 街頭技工（ストリートテクニック） (新しいタブで開く)" href="https://note.com/mandokorotakano/n/n82f3b0b0d5c9" target="_blank"><br>街頭技工（ストリートテクニック）</a><br><a rel="noreferrer noopener" aria-label=" @naoharu街頭技工に出会う   (新しいタブで開く)" href="https://twitter.com/naoharu/status/1221443729879252992" target="_blank">@naoharu街頭技工に出会う  </a><br><a rel="noreferrer noopener" aria-label=" 高野政所@mandokoroさん (新しいタブで開く)" href="https://twitter.com/mandokoro" target="_blank">高野政所@mandokoroさん</a><br><a rel="noreferrer noopener" aria-label="前科おじさん高野政所&nbsp;(著) (新しいタブで開く)" href="https://amzn.to/3BvwTu1" target="_blank">前科おじさん高野政所&nbsp;(著)</a></p>
 

---  
template: BlogPost  
path: /33
date: 2020-02-12T06:15:50.738Z  
title: ep33:スクラムマスターの仕事
thumbnail: /assets/ep33.jpg
metaDescription: df sdf df  
---  
![ep33](/assets/ep33.jpg) 

<iframe src="https://open.spotify.com/embed/episode/2Aq7TZTPcFbG3OX7wAmHCV" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>閑散とした京都を見た @dada_ism<br>
いろんなanytime fitness店舗に行ってる @naoharu<br>
スポンサーはしないでください<br>
「スクラムマスターって既存の価値観や場を支配する空気を疑ったりぶっ壊したりするあれのように思えてきた」https://twitter.com/naoharu/status/1224914213916704768<br>
目に見えないチームの障壁<br>
組織の文化、思想と関係ないところで市場価値を出さないといけない<br>
「俺らが良しとしているものって本当か？」<br>
リベラルアーツ回<br>
「品質は高いほどいい」は本当か<br>
「ほんと？ほんとほんと？」って言い続けるのは大変<br>
脈々と受け継がれる価値観がチームの障壁になっているのなら取り除くのはスクラムマスターの役目<br>
当たり前の価値観を疑うのはウザいと思われがち<br>
答えがないなかで疑い続けるのは難しい<br>
ウザいと思われないことがゴールではない（チームに対して良い影響を与えてるか）<br>
スクラムマスターたちの意見を聞いてみたい<br>
価値観壊したいけど悶々としている人たちいませんか<br>
既存の価値観と敵対するのも違うと思う<br>
会社の昇格試験での問答<br>
「既存のビジネスに敬意を払うことが大事」って言ったけど矛盾あるな<br>
構造主義的な二項対立ではなくて相手の立場になるポスト構造主義<br>
アジャイルソフトウェア開発宣言はポスト構造主義的な思想が入っている気がする<br>
これは何テクニックなのか</p>---  
template: BlogPost  
path: /34
date: 2020-02-15T06:15:50.738Z  
title: ep34:ストリートスナップテクニック
thumbnail: /assets/ep34.png
metaDescription: df sdf df  
---  
![ep33](/assets/ep34.png) 

<iframe src="https://open.spotify.com/embed/episode/1csvAmjYUOY58ipIFpp9FJ" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
@naoharu sigma fp ほぼ買いかける  
[sigma fp](https://www.sigma-global.com/jp/cameras/fp-series/)  
youtube進出に向けて  
無限にビールが出てくる収録環境  
@dada_ism 久々に走ったらキロ1分早くなった  
[FUJIFILM X100v](https://fujifilm-x.com/ja-jp/products/cameras/x100v/)  
「こんな撮られ方はイヤだ」富士フイルム『X100V』公式動画が炎上。公開後すぐ削除に→お詫びを発表  
価値観を疑うために両方の立場で意見を言ってみる実験  
否定的な立場になってみる  
肯定的な立場になってみる  
ストリートアナザーストーリーイマジンテクニック  
飲みすぎたかもしれない  
ゲスト募集中  
[よなよなこさんの回](https://jamming.fm/ep15/)  
[デザイナー森くんの回](https://jamming.fm/ep28/)  

---  
template: BlogPost  
path: /35
date: 2020-02-19T06:15:50.738Z  
title: ep35:プロダクト開発がやりたい！（ゲスト：ビジネスプロダクトマーケター Chia_keyさん）
thumbnail: /assets/ep35.png
metaDescription: df sdf df  
---  
![ep35](/assets/ep35.png)  

<iframe src="https://open.spotify.com/embed/episode/07JXuaxCrdupdlwA7ADHdO" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>Guest: ビジネスプロダクトマーケター Chia_keyさん<br>
娘を泣き笑いさせた @naoharu<br> <a rel="noreferrer noopener" aria-label="CP+中止 (新しいタブで開く)" href="http://www.cpplus.jp/" target="_blank">CP+中止</a> に驚く @dada_ism<br> バレンタインテクニックを息子に伝授する夫　@chia-key<br> @dada_ism 就活<br> ＠Chia-keyさんがいた外資系IT企業<br> 実力主義<br> でも年功序列<br> 派手リストラもあった<br> お客さんは事業会社の情シス<br> 親会社の壁<br> ストリートジョブチェンジテクニック<br> 転職経験ないのはリスクになりうる<br> ユーザーに近いプロダクト開発がやりたかったのが転職の動機<br> SIerからWeb系に転職したけど楽しくて仕方がない<br> <a href="https://shiganai.org/" target="_blank" rel="noreferrer noopener" aria-label="しがないラジオ  (新しいタブで開く)">しがないラジオ </a><br> マーケティングの４P<br> プロダクト以外の、プライス、プレイス、プロモーションを担当している<br> プロダクトオーナーとプロダクトマネージャー<br> 営業と開発の間にいる立場<br> 営業さんの見ている顧客は解像度が高い<br> 我々は解像度低いけど森をみて最大公約数を探す仕事<br> 全てはプロダクト開発である＝オールウェイズ　プロダクト　ディベロップメント　テクニック</p>---  
template: BlogPost  
path: /36
date: 2020-02-28T06:15:50.738Z  
title: ep36:チームワーク（ゲスト：ビジネスプロダクトマーケター Chia_keyさん）
thumbnail: /assets/ep36.png
metaDescription: df sdf df  
---  
![ep36](/assets/ep36.png)  

<iframe src="https://open.spotify.com/embed/episode/3efTdUcUUvhTyPCrg2l3dl" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>丸の内スタジオからお届けしている @<a href="https://soundcloud.com/dada_ism">dada_ism</a>&nbsp;@<a href="https://soundcloud.com/naoharu">naoharu</a>&nbsp;@<a href="https://soundcloud.com/chia_key">chia_key</a><br>高田馬場の某大学の @<a href="https://soundcloud.com/dada_ism">dada_ism</a>&nbsp;と @<a href="https://soundcloud.com/chia_key">chia_key</a><br>所沢体育大学出身と大久保工科大学<br>早稲田と言っても色々ある<br>本キャン<br>「インカレ」って久々に言った<br>調布通信対戦大学<br>チームのためのプロダクト<br>チームワークが会社のミッション<br>本部長レベルの会議の議事録や資料も見える化されている @<a href="https://soundcloud.com/chia_key">chia_key</a>&nbsp;の会社<br>可視化されている情報こそが自発的な行動を誘発する<br>「良しとされている価値観を」疑うプラクティス<br>全ての情報が可視化されていた方がいいのか？<br>疑うことで学びが深まる「ストリートディープラーニングテクニック」<br>スクラムでチームワークを広げていきたい @<a href="https://soundcloud.com/dada_ism">dada_ism</a><br>SAFeは割と早い段階で経営にアタックをかける&nbsp;<a rel="noreferrer noopener" href="https://gate.sc/?url=https%3A%2F%2Fwww.scaledagileframework.com%2F&amp;token=e8cc82-1-1582847812899" target="_blank">www.scaledagileframework.com/</a><br>自己組織化は本当に大事か。凄いリーダーとソルジャーたくさんというモデルはどうか。<br>経営戦略がどのように従業員に伝達されるか？<br>トヨタの年頭挨拶&nbsp;<a rel="noreferrer noopener" href="https://gate.sc/?url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DvJ8DsIiSb-U&amp;token=e34db4-1-1582847812899" target="_blank">www.youtube.com/watch?v=vJ8DsIiSb-U</a><br>豊田章男のサーバントリーダシップ<br>社長会議のチャット実況スレが立つ<br>「その日はマッスル部の活動がありまして」<br>レポートラインをスキップする弊害は全てがオープンになった世界では発生しないのでは？<br>「疑う」をあえてプラクティスとしてやっていくのは学びが多い<br>今日はすごく勉強になった</p>---  
template: BlogPost  
path: /37
date: 2020-03-03T06:15:50.738Z  
title: ep37:価値観を疑え！
thumbnail: /assets/ep37.png
metaDescription: df sdf df  
---  
![ep37](/assets/ep37.png)  

<iframe src="https://open.spotify.com/embed/episode/22jiG2ynCze4svfIALN6BN" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>大阪出張で赤福を買いました @naoharu<br>赤福は三重のお菓子では？<br>赤福のあんこだけたべる娘<br>半年で5kg痩せました @dada<br>年明けレコーディングしはじめた<br>プロテインはしっかり摂っている<br>せとっしーさんのフィードバック紹介<br>break the bias　イノベーションを意図的に起こすためのテクニック<br>濱口秀司さん<br>仕事でも使うけど難しい<br>既存の価値観を疑うにつながる<br> <a rel="noreferrer noopener" aria-label="京都のEV車の話 (新しいタブで開く)" href="https://www.youtube.com/watch?v=nXXy_Ln3E9M" target="_blank">京都のEV車の話</a>　<br> <a rel="noreferrer noopener" aria-label="USBメモリの話 (新しいタブで開く)" href="https://vimeo.com/48997854" target="_blank">USBメモリの話</a><br><a rel="noreferrer noopener" aria-label=" Akihito Watanabeさんのフィードバック紹介 @nabe_merchant (新しいタブで開く)" href="https://twitter.com/nabe_merchant" target="_blank">Akihito Watanabeさんのフィードバック紹介 @nabe_merchant</a>　<br><a rel="noreferrer noopener" aria-label=" ep33の気づきで仕事に活きた　 (新しいタブで開く)" href="https://twitter.com/dada_ism/status/1229772957821566976" target="_blank">ep33の気づきで仕事に活きた</a><br>あれれれ〜<br>OPがチームに要件を全部詳細に伝えるのはどうなのか<br>嫌なヤツになる必要はない<br>認定価値観疑い師<br>ゲスト募集中</p>---  
template: BlogPost  
path: /38
date: 2020-03-07T06:15:50.738Z  
title: ep38:なぜHowを言うべきではないのか
thumbnail: /assets/ep38.png
metaDescription: df sdf df  
---  
![ep38](/assets/ep38.png)  

<iframe src="https://open.spotify.com/embed/episode/6Cj0zZnNjLS2K6U3FiWWVv" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>河口湖のほとりで休暇予定の @<a href="https://soundcloud.com/dada_ism">dada_ism</a><br>星野リゾート八ヶ岳で休暇予定の @<a href="https://soundcloud.com/naoharu">naoharu</a><br>業務連絡（1回目）<br>jamming.fm はキリ番踏み逃げ禁止！<br>古き良きホームページが無くなった<br>新型肺炎の話<br>大型イベントの開催自粛<br>就職活動などのイベントも影響<br>岩田教授がダイヤモンド・プリンセス号の内部状況を告発<br>船のなかは治外法権？<br>沈黙の艦隊 <a rel="noreferrer noopener" href="https://gate.sc/?url=https%3A%2F%2Fja.wikipedia.org%2Fwiki%2F%25E6%25B2%2588%25E9%25BB%2599%25E3%2581%25AE%25E8%2589%25A6%25E9%259A%258A&amp;token=73d700-1-1583581766238" target="_blank">ja.wikipedia.org/wiki/%E6%B2%88%E…8%89%A6%E9%9A%8A</a><br>電車が空いてる<br>スクラムネタ「POは開発チームに対してHowは言うな」について<br>「POは開発チームに対してHowは言うな」はHowであってWhyは何なのか考えないとだめ<br>POの発言力が強すぎる時に問題が起こる<br>「誰々は何々をすべき」という境界線を設けるべきではない<br>バレーボールでの経験を重ねる @<a href="https://soundcloud.com/dada_ism">dada_ism</a><br>バレーボールのハイタッチは信頼関係を作るセレモニー<br>あなたのチームは機能していますか&nbsp;<a rel="noreferrer noopener" href="https://amzn.to/31eLKgc" target="_blank">https://amzn.to/31eLKgc</a><br>業務連絡（2回目）<br>replicant.fm や turingcomplete.fm に強く影響を受けている<br>業務連絡2回するのはオリジナリティ</p>---  
template: BlogPost  
path: /39
date: 2020-03-10T06:15:50.738Z  
title: ep39:星野リゾートのUX
thumbnail: /assets/ep39.png
metaDescription: df sdf df  
---  
![ep39](/assets/ep39.png)  

<iframe src="https://open.spotify.com/embed/episode/2tku93fncsMxu1enoTe32r" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>新しいダウンを買った @<a href="https://soundcloud.com/naoharu">naoharu</a><br>isaora&nbsp;<a rel="noreferrer noopener" href="https://gate.sc/?url=https%3A%2F%2Fwww.isaora.com%2F&amp;token=f54580-1-1583835363283" target="_blank">www.isaora.com/</a><br>モンベルのダウンはセカンダリへ<br>歯医者に行きました@<a href="https://soundcloud.com/dada_ism">dada_ism</a><br>フレックスを活用した歯の治療技術<br>
星野リゾート　リゾナーレ八ヶ岳　<a href="https://gate.sc/?url=https%3A%2F%2Frisonare.com%2Fyatsugatake%2F&amp;token=4929a2-1-1583835363283" rel="noreferrer noopener" target="_blank">risonare.com/yatsugatake/</a><br>八ヶ岳&nbsp;<a href="https://gate.sc/?url=https%3A%2F%2Fja.wikipedia.org%2Fwiki%2F%25E5%2585%25AB%25E3%2583%25B6%25E5%25B2%25B3&amp;token=fd9ad4-1-1583835363283" rel="noreferrer noopener" target="_blank">ja.wikipedia.org/wiki/%E5%85%AB%E3%83%B6%E5%B2%B3</a><br>星野リゾート　<a href="https://gate.sc/?url=https%3A%2F%2Fhoshinoresorts.com%2Fja%2Fhotels%2Faomoriya%2F&amp;token=d0d22b-1-1583835363283" rel="noreferrer noopener" target="_blank">hoshinoresorts.com/ja/hotels/aomoriya/</a><br>星野リゾートはUXがすごい<br>星野リゾートのスタイル<br>ハードウェアじゃなくてソフトウェアで勝負<br>星野リゾートの情シス　<a href="https://gate.sc/?url=https%3A%2F%2Fwww.slideshare.net%2Fssuser91c7c7%2Fdevops-176765846&amp;token=372a14-1-1583835363283" rel="noreferrer noopener" target="_blank">www.slideshare.net/ssuser91c7c7/devops-176765846</a><br>星野リゾート 情シスの藤井 崇介さん(@<a href="https://soundcloud.com/ZooBonta">ZooBonta</a>)&nbsp;<a href="https://gate.sc/?url=https%3A%2F%2Ftwitter.com%2FZooBonta&amp;token=b9cffa-1-1583835363283" rel="noreferrer noopener" target="_blank">twitter.com/ZooBonta</a><br>システムは調達するものという古い考え方　<br>ふふ河口湖&nbsp;<a href="https://gate.sc/?url=https%3A%2F%2Fwww.fufukawaguchiko.jp%2F&amp;token=5ea6c8-1-1583835363283" rel="noreferrer noopener" target="_blank">www.fufukawaguchiko.jp/</a><br>@<a href="https://soundcloud.com/dada_ism">dada_ism</a>が200円の宿に泊まる話　<a href="https://gate.sc/?url=https%3A%2F%2Fjamming.fm%2Fep11%2F&amp;token=fb2a7a-1-1583835363283" rel="noreferrer noopener" target="_blank">jamming.fm/ep11/</a><br>牛丼 500円と10000円のコース料理<br>1泊 200円の宿と1泊100,000円のリゾート<br>サウナ設備充実<br>お風呂上がりに飲みたいビールやコーヒー牛乳飲み放題<br>迷わずがぶ飲みした<br>富士山を観ながらご飯が食べれる<br>子連れ禁止はむしろありがたい<br>Panasonic 電動アシスト自転車&nbsp;<a href="https://gate.sc/?url=https%3A%2F%2Fcycle.panasonic.com%2Fproducts%2F&amp;token=533c1-1-1583835363283" rel="noreferrer noopener" target="_blank">cycle.panasonic.com/products/</a><br>手袋を貸してくれた<br>初めて電動アシスト自転車に乗った<br>運動したくて電源切って乗ってた<br>100kgを運ぶ力すごい<br>原動力付き自転車ではなくアシストであえて止めているのでは<br>自動車の半自動運転的なものを感じる<br>法律の線引きによりテクノロジーの進化の仕方が変わってくる<br>官公庁がんばれ<br>Google、行動規範からみんなのモットー｢Don't Be Evil（邪悪になるな）｣を外す&nbsp;<a href="https://gate.sc/?url=https%3A%2F%2Fwww.gizmodo.jp%2F2018%2F05%2Fgoogle-removes-dontbeevil.html&amp;token=2ce7b8-1-1583835363283" rel="noreferrer noopener" target="_blank">www.gizmodo.jp/2018/05/google-re…s-dontbeevil.html</a><br>絶対悪はあるのか<br>放送開始から1年でゲスト出たい方が現れたらしい</p>
---  
template: BlogPost  
path: /40
date: 2020-03-21T06:15:50.738Z  
title: ep40:グランピング
thumbnail: /assets/ep40.png
metaDescription: df sdf df  
---  
![ep40](/assets/ep40.png)  

<iframe src="https://open.spotify.com/embed/episode/5hlWVl4PFGaVXFi9w1KjTS" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>飲み物難民@naoharu<br>
コンビニのおでんばっかり食べてる＠dada_ism<br>
ストリート美食テクニック</p>
<p>グラマラス✕キャンピング＝グランピング<br><a rel="noreferrer noopener" aria-label=" ピュアコテージ (新しいタブで開く)" href="https://www.pure-cottages.jp/" target="_blank">ピュアコテージ</a><br><a rel="noreferrer noopener" aria-label="透明なテント（AURA） (新しいタブで開く)" href="https://www.pure-cottages.jp/rooms/glamping" target="_blank">透明なテント（AURA）</a><br><a rel="noreferrer noopener" aria-label="ジュラシックパークの球体の乗り物　ジャイロスフィア (新しいタブで開く)" href="https://www.youtube.com/watch?v=PfAclkDkYaU" target="_blank">ジュラシックパークの球体の乗り物　ジャイロスフィア</a><br>インスタでもツイッターでも投稿に反響が来た<br><a rel="noreferrer noopener" aria-label="AURA DOME (新しいタブで開く)" href="https://earthdome.net/auradome/" target="_blank">AURA DOME</a><br><a rel="noreferrer noopener" aria-label="アースドーム　楽天市場 (新しいタブで開く)" href="https://item.rakuten.co.jp/yumehinoki/c/0000000133/" target="_blank">アースドーム　楽天市場</a><br><a rel="noreferrer noopener" aria-label="那須ハイランドパーク (新しいタブで開く)" href="https://www.nasuhai.co.jp/" target="_blank">那須ハイランドパーク</a><br>dadaも来週、車中泊で那須に行く予定ある<br><a rel="noreferrer noopener" aria-label="佐野プレミアム・アウトレット (新しいタブで開く)" href="https://www.premiumoutlets.co.jp/sano/" target="_blank">佐野プレミアム・アウトレット</a><br><a rel="noreferrer noopener" aria-label="SHOZO CAFE (新しいタブで開く)" href="http://www.shozo.co.jp/" target="_blank">SHOZO CAFE</a><br><a href="http://taimahak.jp/" target="_blank" rel="noreferrer noopener" aria-label="大麻博物館 (新しいタブで開く)">大麻博物館</a><br>卑屈マウント<br>ゲスト登場、乞うご期待</p>
---  
template: BlogPost  
path: /41
date: 2020-03-25T06:15:50.738Z  
title: ep41:戦略的職探し（ゲスト：スモックさん）
thumbnail: /assets/ep41.png
metaDescription: df sdf df  
---  
![ep41](/assets/ep41.png)  

<iframe src="https://open.spotify.com/embed/episode/66IV1Re39TOuKwS8w8rC8s" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

<p>檸檬堂のんだ @<a href="https://soundcloud.com/naoharu">naoharu</a><br>古畑任三郎みてる @<a href="https://soundcloud.com/dada_ism">dada_ism</a><br>楽しみにしてる英語が延期になってるスモック<br>誇張しすぎた古畑任三郎<br>スモックはナオハルのパイセン<br>戦略的就職活動<br>ヤバい撮影スタジオ<br>コピーだけしていればいい職場<br>やることなさすぎて胃を悪くする<br>時給千円<br>すごい貧乏だったけど充実していたパイセン<br>音楽好きなら憧れるレコード会社勤務<br>業界っぽさ（いわゆるシースー）<br>まだシーズン1</p>---  
template: BlogPost  
path: /42
date: 2020-03-28T06:15:50.738Z  
title: ep42:ファッションはすたれるがスタイルは永遠だ（ゲスト：スモックさん）
thumbnail: /assets/ep42.png
metaDescription: df sdf df  
---  
![ep42](/assets/ep42.png)  

<iframe src="https://open.spotify.com/embed/episode/2aHlZI10rZf2H9ED1GsbF7" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

<p>ヨガスタジオ行きたいけコロナなので行けない smockパイセン<br>通勤で消費できないカロリーをジムで消費する人たち<br>レコード会社の苦労<br>レコード会社の最前線で働いている人は転職して違う職種になる人多い<br>事業部門大変なんだな<br>イマジンジョブチェンジテクニック<br>正社員で一番基本給が高い会社を選んでアパレルに転職<br>超有名アパレルブランド<br>名刺が硬い<br>かるたみたいな名刺<br>1枚100円くらいする名刺<br>位が上がると厚くなる名刺<br><a rel="noreferrer noopener" aria-label="MD　マーチャンダイジング　 (新しいタブで開く)" href="https://ja.wikipedia.org/wiki/%E3%83%9E%E3%83%BC%E3%83%81%E3%83%A3%E3%83%B3%E3%83%80%E3%82%A4%E3%82%B8%E3%83%B3%E3%82%B0" target="_blank">MD　マーチャンダイジング　</a><br>おかあさんポジション<br>デザイナー採用で入社1ヶ月で情シスに異動<br>数学はできなくても生きていける<br>数字無理なんで！って言ったらデザインと情シス両方やることになった<br>社長から販促部門立ち上げ命令<br>グラフィックを作れ！<br>会社が大きくなり組織が大きくなる<br>マネジメントを経験<br>映像を作れ！<br>失敗は絶対に許されない<br>心理的安全性と情報統制<br>ワンマン社長のもとで働いたら恐い人になってしまった　<br>ハイパースピードモデル<br><a href="https://ja.wikipedia.org/wiki/%E8%97%A4%E5%8E%9F%E3%83%92%E3%83%AD%E3%82%B7" target="_blank" rel="noreferrer noopener" aria-label="藤原ヒロシ (新しいタブで開く)">藤原ヒロシ</a><br>30代はアパレルに費やした<br>今思えば幸せだった30代</p>---  
template: BlogPost  
path: /43  
date: 2020-04-03T06:15:50.738Z  
title: ep43:職業訓練（ゲスト：スモックさん）
thumbnail: /assets/ep43.png
metaDescription: df sdf df  
---  
![ep43](/assets/ep43.png)  

<iframe src="https://open.spotify.com/embed/episode/6Isf8OjDcigbeRPTDiLH7T" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>
<p>
缶のパッケージが気になるスモックパイセン<a href="https://www.takarashuzo.co.jp/products/soft_alcohol/ochawari/">https://www.takarashuzo.co.jp/products/soft_alcohol/ochawari/</a><br>
今日はシーズン3<br>
起承転結人生<br>
100日後に死ぬワニ <a href="https://twitter.com/yuukikikuchi">https://twitter.com/yuukikikuchi</a><br>
社長「MD（マーチャンダイザー）になれ」<br>
録音するようになったスモックパイセン<br>
社長「転職するの遅かったね」<br>
40歳までに骨を埋める会社と出会っていたほうがいいかもしれない<br>
某Webメディアに転職する<br>
隣にいる人にメールする「確認お願いします」「確認しました」<br>
何もわからない中で仕事スタート<br>
企画が通らない<br>
胃カメラの結果、休むことを決意する<br>
職業訓練校に通いだす<br>
ザ・ノンフィクション<br>
エンジャパンbot説<br>
月給19万円〜（都内）<br>
大手を蹴って再びアパレル会社へ<br>
回りだす運命の歯車<br>
</p>---  
template: BlogPost  
path: /44  
date: 2020-04-06T06:15:50.738Z  
title: ep44:ワークアズライフ（ゲスト：スモックさん）
thumbnail: /assets/ep44.png
metaDescription: df sdf df  
---  
![ep44](/assets/ep44.png)  

<iframe src="https://open.spotify.com/embed/episode/3LgGIuUnYeUhMg0M3UG1Zv" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

<p>Jamming.fm自体そんな役に立つ情報を発信してない<br> ここまでで転職4回<br> 契約時と違う仕事を強いられる<br> ご両親が要介護な状態になる<br> 「広報をやってほしい」<br> ティピカルブラック企業<br> 組織に裏切られた話<br> インセプションデッキとやらないことリスト https://www.slideshare.net/NobuhiroYoshitake/ss-114790708<br> 「ダメ」っていうことで価値を出す人<br> バブみを抱えた結果裏切られる<br> Jamming.fmオファー結果「え、平日！？」<br> 年末年始の休みがなくてストレスランキングランクイン<br> 自慰行為を見られてるやつは正常<br> 薬はマジで効く<br> 気付いたら11kg太ってた<br> 「転職しよう」と思えなかったことで気付いた<br> 普通とはなんぞや<br> スモックパイセンにアクセスいただければと思います<br> @naoharu か @dada_ism まで<br> 40からチャレンジングな転職はきついぞ。</p>---  
template: BlogPost  
path: /45  
date: 2020-04-11T06:15:50.738Z  
title: ep45:サウナイキタイ（ゲスト：サウナクライマーなおこさん）
thumbnail: /assets/ep45.png
metaDescription: df sdf df  
---  
![ep45](/assets/ep45.png)  

<iframe src="https://open.spotify.com/embed/episode/1fXCKXXnyjJbQxqriPXr5O" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>



***
  
</br>

Guest:[サウナクライマー なおこさん](https://twitter.com/hqW4hwLBS9TVl5)  


<p>花粉症発症していない@dadaism<br>花粉症発症した＠naoharu<br>花粉症10代から、子供が6歳で発症 なおこさん<br>サウナイキタイ https://sauna-ikitai.com/<br>風俗レポは第一人称小生、二人称は同志<br><a rel="noreferrer noopener" aria-label="風俗レポートの文体で、富士そばのレビューを書きました@yoppymodel (新しいタブで開く)" href="https://twitter.com/yoppymodel/status/976299255257210880" target="_blank">風俗レポートの文体で、富士そばのレビューを書きました@yoppymodel</a><br>無心になる時間に考えたことを書くとポエムになる<br>レビュー＝サ活<br><a rel="noreferrer noopener" aria-label="ポエムで100いいね (新しいタブで開く)" href="https://sauna-ikitai.com/saunners/385/posts/248299" target="_blank">ポエムで100いいね</a><br>クライミングイキタイ<br>週に2回いっている<br>コロナでクライミングジム自粛<br><a rel="noreferrer noopener" aria-label="コロナビール (新しいタブで開く)" href="https://corona-extra.jp/" target="_blank">コロナビール</a><br>赤羽のエニタイムフィットネス人増えている<br>せんべろしましょう<br>jamming.fm せんべろ企画<br>声がいい<br><a rel="noreferrer noopener" aria-label=" ワニのさきがけ　クニクニさん (新しいタブで開く)" href="https://twitter.com/kunihiko9215" target="_blank">ワニのさきがけ　クニクニさん</a><br> 大阪王将 文野社長のモノマネ<br> サウナからの餃子<br> <a rel="noreferrer noopener" aria-label="池袋 みやこ湯  (新しいタブで開く)" href="https://sauna-ikitai.com/saunas/1649" target="_blank">池袋 みやこ湯 </a><br>クライマーの宿題<br>エンジニア　サウナ　ハマる率　高い<br><a rel="noreferrer noopener" aria-label="金春湯 大崎  (新しいタブで開く)" href="https://sauna-ikitai.com/saunas/2027" target="_blank">金春湯 大崎 </a><br><a rel="noreferrer noopener" aria-label="宮城湯 (新しいタブで開く)" href="https://sauna-ikitai.com/saunas/2018" target="_blank">宮城湯</a><br><a rel="noreferrer noopener" aria-label="品川ロッキー (新しいタブで開く)" href="https://www.rockyclimbing.com/shinagawa/" target="_blank">品川ロッキー</a><br><a rel="noreferrer noopener" aria-label="ピグレットクライミングジム (新しいタブで開く)" href="http://piglet-climb.com/" target="_blank">ピグレットクライミングジム</a></p>---  
template: BlogPost  
path: /46  
date: 2020-04-18T06:15:50.738Z  
title: ep46:カーシェアリングで死にかけた（ゲスト：サウナクライマーなおこさん）
thumbnail: /assets/ep46.png
metaDescription: df sdf df  
---  
![ep46](/assets/ep46.png)  

<iframe src="https://open.spotify.com/embed/episode/1exKa2MJX6vgwCl5u7yTpy" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


</br>


***
  

  
</br>

Guest:[サウナクライマー なおこさん](https://twitter.com/hqW4hwLBS9TVl5)  


<p>ムーンアイランドで職種変更 ＠なおこ<br>事務系から技術系へ<br>アンダーフォレストで暮らしている@naoharu<br>レッドウィングで暮らしている@dadaism<br>他の店舗と違う　エニタイムフィットネス　赤羽北<br>ピースアイランドのエニタイムに通う＠なおこ<br>カーシェアリングをやっている<br>14年間ペーパーゴールド免許<br>車の運転がセンスがないのでアメリカンの中型バイクに乗る<br><a rel="noreferrer noopener" href="http://www.harley-davidson.com/jp/ja/index.html" target="_blank">ハーレーダビッドソン</a><br>ぶろろろ<br><a href="http://share.timescar.jp/">タイムズカーシェア</a><br>アクアラインひとりで運転できた！<br>カーナビに「死ぬぞ！」って言われた<br>GoogleMapのカーナビはレスポンスいい<br>ナビゲートされすぎはよくない<br>仕事中にGoogleストリートビューみてる<br>GoogleMapでいい店か判断できる<br>ストリートディープラーニングテクニック<br>長崎はいいお店が多い<br>肩幅が膨張<br>テキーラが好き<br>よなよな子さんと飲みたい</p>---  
template: BlogPost  
path: /47 
date: 2020-05-10T06:15:50.738Z  
title: ep47:男尊女卑社会（ゲスト：サウナクライマーなおこさん）
thumbnail: /assets/ep47.png
metaDescription: df sdf df  
---  
![ep47](/assets/ep47.png)  

<iframe src="https://open.spotify.com/embed/episode/1Ov16mmh0f732R8Jz6azEI" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

Guest:[サウナクライマー なおこさん](https://twitter.com/hqW4hwLBS9TVl5)  

仕事はヤク @naoharu  
Youtube Premium入った @dadaism  
宝山ハイボール飲んでる  
ストロング缶は危険ドラッグ  
男尊女卑社会  
とある大手建設  
奴隷契約書　※訴訟リスクあるので添付はなし  
事務職に戻ろうとしたら死ぬぞ  
人生一度きり感  
家族に相談する @naoharu  
家族に相談しない @なおこ  
「俺は止めたけどね」うまくいかなかったら「ほれみろ！」  
子「お父さんが理屈っぽい」  
ボイラー資格とって温浴施設で働きたい  
老後のために自腹でフォーク・リフト取得  
フウゾクイキタイ  
女性風俗のリスク  
安全な女性向け風俗経営したい  
[はてなアノニマスダイアリー：離婚した](https://anond.hatelabo.jp/20200317003307)  
建設業界は男社会  
男尊女卑に甘んじて乗っかってしまっている  
日本の社会の縮図  
トヨタをトップとした地方都市　愛知  
構造を見出す  
年功序列じゃない男女別の内線電話番号表  
その電話番号表を作った人は悪気はない  
[Bob Marley - Jammin](https://www.youtube.com/watch?v=oFRbZJXjWIA) 
---  
template: BlogPost  
path: /48 
date: 2020-06-02T06:15:50.738Z  
title: ep48:在宅勤務
thumbnail: /assets/ep48.png
metaDescription: df sdf df  
---  
![ep48](/assets/ep48.png)  

<iframe src="https://open.spotify.com/embed/episode/4y9GANPVEelLjmp4pjWcSm" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

エニタイム復活して嬉しい @naoharu  
朝ドアに足をぶつけて流血した @dadaism  
久しぶりの収録はzoomでリモート収録  
ヘッドセットだと音質微妙だったのでコンデンサマイクで撮るようにしました  
フィードバックはtwitterなどで #jammingfm まで  
会社のデスクよりも快適な環境にできた   
印刷できないのが意外と困る（設計書などは紙で見たい）  
通勤時間が無くなって2時間くらい時間が増えた  
[Majestouch Convertible 2 Tenkeyless 黒軸](https://www.diatec.co.jp/products/det.php?prod_c=2637)  
良いキーボードだと憂鬱なメールも打てる  
[パームレスト自作には1x4角材がおすすめ](https://blog.skeg.jp/archives/2017/06/palm-rest-diy.html)  
jamming.fm収録できてなかった間にまた斧を研いだ  
[木こりのジレンマ](https://jamming.fm/32)  
[gatsby](https://www.gatsbyjs.org/)  
[AWS Amplify](https://aws.amazon.com/jp/amplify/)  
[jamming.fm](https://jamming.fm/)  
[@naoharu のトークテーマ](https://twitter.com/search?q=%23%E3%83%88%E3%83%BC%E3%82%AF%E3%83%86%E3%83%BC%E3%83%9E%E3%81%AB%E8%BF%BD%E5%8A%A0&src=typed_query&f=live)  
取り扱って欲しいテーマなどがあれば教えてください  ---  
template: BlogPost  
path: /49 
date: 2020-06-14T06:15:50.738Z  
title: ep49:本気なら楽しめる
thumbnail: /assets/ep49.png
metaDescription: df sdf df  
---  
![ep49](/assets/ep49.png)  

<iframe src="https://open.spotify.com/embed/episode/2LX3uQY2kZSb2XnjUflwBA" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

それぞのエニタイムライフ  
自転車買うとQoLあがる  
自転車が買えない理由  
リモート収録ふりかえり  
収録ファイルの同期問題  
編集するときなんかズレが生まれる  
映画のカチンコ  
同期ポイント「お疲れ様です！」  
[黒沢最強伝説](https://amzn.to/3Etvy9e)  
自分たちの仕事でも同じことある  
やらされ仕事はつらい  
自分でやりたいと心から思うこと  
終業後４秒で飲酒  
在宅はいつでも仕事できちゃう  
一石N鳥の成果の取り出し方  
みんなで成果を取り出すチーム運営  
[一石N鳥のツイート](https://twitter.com/naoharu/status/1250336789002248194)  
[アジャイルレトロスペクティブズ　強いチームを育てる「ふりかえり」の手引き](https://amzn.to/3jO4jOu)  
ふりかえりがない方がいい仕事はあるのか  
スクラムは打ち合わせが多い  
スクラムは自分たちでどうやるかを決める、何が成果かも決める  
そういうことだったのか！感の高さ  
本気でWhyを突き詰めたらきづける  
本気度の違いだった  
---  
template: BlogPost  
path: /50 
date: 2020-06-17T06:15:50.738Z  
title: ep50:マネーの虎をふりかえる -相手を許す勇気-
thumbnail: /assets/ep50.png
metaDescription: df sdf df  
---  
![ep50](/assets/ep50.png)  

<iframe src="https://open.spotify.com/embed/episode/1ShsygwNgLGuRdHuNN9ExF" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

誕生日  
[siroca コーン式全自動コーヒーメーカー](https://www.siroca.co.jp/kitchen/autocoffeecone/index.html)  
[アフィリンク](https://amzn.to/3GyUpdt)  
フィードバック紹介  
[Shifter](https://www.getshifter.io/)  
[¥マネーの虎](https://ja.wikipedia.org/wiki/%C2%A5%E3%83%9E%E3%83%8D%E3%83%BC%E3%81%AE%E8%99%8E)  
[川原社長 なんでんかんでんフーズ](https://ja.wikipedia.org/wiki/%E3%81%AA%E3%82%93%E3%81%A7%E3%82%93%E3%81%8B%E3%82%93%E3%81%A7%E3%82%93%E3%83%95%E3%83%BC%E3%82%BA)  
[マネーの虎 大食いラーメン店回](https://www.youtube.com/watch?v=_6qF68X8CxU)  
正義感と怒りのパワーで転職が続く  
まわりから信頼されていないで正論を言う  
こういう対立多い  
[他者と働く──「わかりあえなさ」から始める組織論](https://amzn.to/3bji97b)  
相手を許す勇気  
マネーの虎をふりかえる  
川原社長は当時36歳  
[岩井社長のYouTubeチャンネル 令和の虎CHANNEL](https://www.youtube.com/channel/UCTyKZzmKi95wxmCg9rU-j6Q)  
[マネーの虎 低価格　讃岐うどん店回](https://www.youtube.com/watch?v=p0Pq_mBI26w)  
[Youtube ハリウッドザコシショウの謙虚になれよ！](https://www.youtube.com/watch?v=3wZl67j1bMc)  
[うんこの爆発で発電する志願者　なだぎ・ザコシのコント動画　ヤッホーＴＶ　#02](https://www.youtube.com/watch?v=s-h-EAxEEI0)  
---  
template: BlogPost  
path: /51
date: 2020-06-25T06:15:50.738Z  
title: ep51:雑談の効能（ゲスト：しおかわさん）
thumbnail: /assets/ep51.png
metaDescription: df sdf df  
---  
![ep50](/assets/ep51.png)  

<iframe src="https://open.spotify.com/embed/episode/1VFRulLxTpsMBQNENjosC4" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

サ道をみている @dada_ism  
ふとった @naoharu  
筋肉体操をやってる しおかわ  
筋肉体操  
しおかわさんはゴールドジムユーザー  
最近のコロナtips  
お客さん含めたフルリモートは論理どこでもドア  
リモートの課題は、雑談がなくなる  
内容が仕事よりか私生活よりか、形式がフォーマルかカジュアルかの四象限  
仕事かつカジュアル＝クライアントミーティングの帰りの電車  
お客さんとの打ち合わせの帰りの電車での会話ってめっちゃ気づきある  
[アイデアの作り方](https://amzn.to/3mrcdzc)  
リラックス状態をどう作るか、仕事おわりがいいんじゃないか。  
古き良き時代の飲み会はそれが目的だったのでは  
カジュアルかつ仕事＝愚痴になりそうだけど、愚痴はOK  
@naoharu は相当お茶会に救われた  
マルチタスクは生産性を落とすがマルチイシューは創造性を上げる  
いろんな人の悩みを知っていることが大事なのでは  
濱口秀志とマルチイシュー  
入山先生「創造性はその人の累積移動距離に比例する」  
@naoharu が認定スクラムプロダクトオーナー研修で学んだこと  
NASAとSpace X  
出口戦略を限定されないことが成果の多彩さを生むのでは  
Zoomの無料アカウントの制限のため終わり  

---  
template: BlogPost  
path: /52
date: 2020-07-02T06:15:50.738Z  
title: ep52:日報じゃなくて日記（ゲスト：しおかわさん）
thumbnail: /assets/ep52.png
metaDescription: df sdf df  
---  
![ep52](/assets/ep52.png)  

<iframe src="https://open.spotify.com/embed/episode/3HqL59GoBcX7vYT8xdwfU6" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

ラグがなかった収録  
やっぱり @naoharu のバーチャル背景でCPU使ってて音ズレが生じていたのでは  
Zoomのバーチャル背景をoffにした @naoharu  
iMacを買い換えたい @dada\_ism  
老眼が進んできた（？）しおかわさん  
すごい近いオブジェクトが見えにくくなる    
リモートワーク で季節感が失われる  
@dada\_ism を誘って @mic_u に会いに行った  
一石N鳥はメモリ不足を起こさないか？  
寝かせてもタスク良いやつは寝かしとく  
トレロにダンプすると直近のタスクになるから結果的に頻繁に見直す  
なんでも言っても良い場「お茶会」  
日報じゃなくて「日記」  
「めっちゃサイコパスなPMに飲みに連れてかれた」  
その日の気分を一言言ってく会  
愚痴は長くなるからやめよう、でも言いたいならそれは検知すべき  
濱口秀志に敬称がつかないのは偉人枠だから  
リモートによる利益「せっかく集まったんだから」がなくなる  ---  
template: BlogPost  
path: /53
date: 2020-07-08T06:15:50.738Z  
title: ep53:2020年東京都知事選挙
thumbnail: /assets/ep53.png
metaDescription: df sdf df  
---  
![ep53](/assets/ep53.png)  

<iframe src="https://open.spotify.com/embed/episode/3JIGCQBCJTOOtVzS77wrv0" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

7月に入って物理出社してる @dada_ism  
東京都のコロナ感染者が増え始めている  
[移動自粛を全面解除、安倍首相が表明「経済を回す」](http://asahi.com/articles/ASN6L74D5N6LUTFK017.html)  
[トヨタ、在宅勤務を拡充　９月から、一部技能職も](https://www.jiji.com/jc/article?k=2020070200876&g=eco)  
[「単身赴任、意味なくない？」カルビー、改革の舞台裏](http://asahi.com/articles/ASN72722PN72ULFA00S.html)  
マッサージガンを購入した @naoharu  
[HYPERVOLT](https://hyperice.jp/products/hypervolt)  
電マとの違い  
東京都知事選挙  
期日前投票は不正選挙に利用される？  
コロナ治療薬開発に成功した人  
コロナはただの風邪っていう人  
消費税撤廃は都政でできないだろ  
北区都議会議員補欠選挙の候補者は全員女性  
ホリエモン新党  
N国党  
[DaiGo - Boys be シコリシャスの科学的有効性ついて解説します【後藤輝樹さん政権放送】](https://www.youtube.com/watch?v=5QZDHtJll7o)  
[スーパークレイジー君](https://ja.wikipedia.org/wiki/%E3%82%B9%E3%83%BC%E3%83%91%E3%83%BC%E3%82%AF%E3%83%AC%E3%82%A4%E3%82%B8%E3%83%BC%E5%90%9B)  
[接触確認アプリCOCOAの炎上](https://togetter.com/li/1547376)  
[kusunoki先生のTweet](https://twitter.com/masanork/status/1274846070539837440)  
[ネットで他人を中傷することはセックスのように気持ちいい[橘玲の日々刻刻](https://diamond.jp/articles/-/195918)  
[マックの女子高生](https://togetter.com/li/1213684)  
[嘘松](https://dic.nicovideo.jp/a/%E5%98%98%E6%9D%BE#:~:text=%E5%98%98%E6%9D%BE%E3%81%A8%E3%81%AF%E3%80%81Twitter,%E3%81%AE%E3%81%AF%E4%BA%8B%E5%AE%9F%E3%81%A7%E3%81%AF%E3%81%AA%E3%81%84%E3%80%82)  
そっ閉じスキル  
ある種ポルノ  
[『テラスハウス』花さん自殺は、SNSじゃなくて番組事業者の問題だ](https://gendai.ismedia.jp/articles/-/72869)  
[箕輪厚介さんのセクハラ問題](https://news.yahoo.co.jp/articles/22356db0563da5f88902486785e83b49bc3f66bf)  
[Black Lives Matter](https://ja.wikipedia.org/wiki/%E3%83%96%E3%83%A9%E3%83%83%E3%82%AF%E3%83%BB%E3%83%A9%E3%82%A4%E3%83%B4%E3%82%BA%E3%83%BB%E3%83%9E%E3%82%BF%E3%83%BC)  
問題の中にいると解決ができない  
トロッコ問題  
ポスト構造主義的論破  
[rebuildの宇宙人の話](https://rebuild.fm/272/)  
dadaismテンション低い  ---  
template: BlogPost  
path: /54
date: 2020-07-24T06:15:50.738Z  
title: ep54:よく遊びよく学べ
thumbnail: /assets/ep54.png
metaDescription: df sdf df  
---  
![ep54](/assets/ep54.png)  

<iframe src="https://open.spotify.com/embed/episode/3LUWEiylS7d8Q4sKKWFPsW" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

忙しくなってきた @naoharu  
東京を脱出した @dada\_ism  
@dada\_ism は車内録音  
車中泊旅行だと人に接触せずに旅ができる  
[日本沈没2020](https://japansinks2020.com/)  
[ドラゴンヘッド](https://www.amazon.co.jp/dp/B009SN9GCI)  
韓国や中国の映画製作会社  
子供たちが虫取りにハマっている  
[木場公園](https://www.tokyo-park.or.jp/park/format/index020.html)  
ストレッチゾーンを越えると成長が止まる説  
入社10年目のリフレッシュ休暇  
この状況のなかで旅行する工夫を楽しむ  
雑な旅行の行きかたをやめたい  
最強のツール「公園」  
遊び方を遊ぶ、遊ぶ遊び  
秘密基地  
マインクラフト  
ペアマイクラ  
外壁がベッドで出来ている家  
捨ててあるエロ本  
大人になってエロ本を捨てていないならシステムからの搾取  
2人の子供時代の町  
「jammingしてねえな〜」  
[特定の年代が反応するまとめ](https://matome.naver.jp/odai/2146753547986926201)  
[ドラクエバトル鉛筆](https://ja.wikipedia.org/wiki/%E3%83%90%E3%83%88%E3%83%AB%E3%81%88%E3%82%93%E3%81%B4%E3%81%A4)  
これから北上する @dada\_ism  
これから今日の遊び方を考える @naoharu  ---  
template: BlogPost  
path: /55
date: 2020-08-12T06:15:50.738Z  
title: ep55:いつもと違う夏
thumbnail: /assets/ep55.png
metaDescription: df sdf df  
---  
![ep55](/assets/ep55.png)  

<iframe src="https://open.spotify.com/embed/episode/71h7jhZWG3cxhVvwpfHXCf" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

課内ハッカソンやってつかれた @dadaism  
コンテキストスイッチ切り替えないけど適度に休憩いれないとつらい  
AIBOが脱臼した @naoharu  
問診票書いてこれから入院  
AIBOはサブスクなので契約している限り保証される  
レバノン行ったことある？  
[ベイルートの大爆発](https://wired.jp/2020/08/08/beirut-port-explosion-physics/)  
郡山市の温野菜大爆発  
硝酸アンモニウムが爆発  
[映像の世紀](https://www.nhk.or.jp/special/eizo/program/)  
911のような雰囲気あった  
車中泊旅行2020夏その後  
佐渡のビーチは閉まっていた  
野生のトキを見た  
コロナ渦においての旅行  
ゲーム理論  
価値観が分散されている  
人ってどれだけ人の影響を受けるのか  
予約するときに都内在住か確認が入った  
コロナ感染者の差別  
[スノーピークヘッドクォーターズ　キャンプフィールド](https://sbs.snowpeak.co.jp/headquarters/camp/index.html)  
[トレーラーハウス　住箱](https://www.snowpeak.co.jp/sp/jyubako/)  
英語の勉強している  
英語は中学校1年であきらめた  
機械翻訳の精度向上がすごい  
[オールドガード](https://www.netflix.com/jp/title/81038963)  
[池澤夏樹 科学する　科学する心](https://amzn.to/3BoYE7F)  
アマチュア科学者でいいじゃない  
[三体](https://amzn.to/2ZwwSsP)  
娘「あのアニメみたいんだけど…」  
[本好きの下剋上](http://booklove-anime.jp/)  
アニメに身代わりになってもらって仕事が捗る  
　---  
template: BlogPost  
path: /56
date: 2020-08-15T06:15:50.738Z  
title: ep56:出会って15分でオファー（ゲスト：トッシーさん）
thumbnail: /assets/ep56.png
metaDescription: df sdf df  
---  
![ep56](/assets/ep56.png)  

<iframe src="https://open.spotify.com/embed/episode/2IgUtG5sXBQuocTdhkz3Fe" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

障害対応明けで眠い @naoharu  
セブンのアイスコーヒー好きな @dada\_ism  
0歳５ヶ月の子供が毎朝5時に起きる　とっしー  
とっしーは @naoharu と同じ会社で新卒入社同期  
[沼レンズ](https://cweb.canon.jp/ef/lineup/standard/ef50-f18stm/index.html)  
@dada\_ism はニコン派　とっしーと @naoharu はキヤノン派  
とっしーはクラウドインフラエンジニアとしてキャリアをスタート  
「クラウド流行ってるのでクラウドやりたいです」  
会社間を超えたAWSコミュニティ  
対外的にも露出が多かったとっしー  
[ONE JAPAN](https://onejapan.jp/)  
変化に強くなるためには転職してたほうがいいのでは  
「自分のキャリアにGAFAとかあってもいいな」というミーハー心  
仁義なきリクルーティング  
BtoB SaaSの会社に転職  
[スモックパイセンにつぐゴリゴリのスタートアッパー](https://jamming.fm/41)  
自分の得意領域を捨てて飛び込む怖さ  
頭では異動したいけど体はビビってる  
某社CTOとご飯を食べにいったら社長と副社長もいた  
出会って15分でオファー  
オファーにサインした2時間後にまさかの異動  
転職はサインした、異動は決まった、自分の上司は何もしらない  
とっしーロミオとジュリエット状態  ---  
template: BlogPost  
path: /57
date: 2020-08-20T06:15:50.738Z  
title: ep57:オファーレター　サイン　辞退（ゲスト：トッシーさん）
thumbnail: /assets/ep57.png
metaDescription: df sdf df  
---  
![ep57](/assets/ep57.png)  

<iframe src="https://open.spotify.com/embed/episode/6bEmRbmiw0VqCgfHPi0pch" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

GuestのZoom有料アカウントの恩恵をうけるjammingfm @naoharu  
週１出社になった　@dadaism  
車を買った＠とっしー  
SUVが入る駐車場がぜんぜんない  
Twitterに感想かいてください＠とっしー  
自分の得意領域捨てるのは悩ましい  
能力の掛け合わせ  
ベクトルが違えば違うほど希少性上がる  
50歳手前で転職した人の話  
30歳半ばは中堅でここから変えるのは勇気いるけども、、労働人生ではまだ5分の1  
3ヶ月シンガポールに行ってた  
人生はアップデートの連続だが、10年に一度はアップグレードしたほうがよい  
オファーレターもらってからのこと  
なんとなくかっこいいからGに行きたい！  
立ち上げ期でもあり資本もあるのでむちゃくちゃ楽しい  
とっしーがうちの会社に来ることメリットプレゼン  
給与や待遇の変化  
5年後自分が　何をしているかわからない  
サインをしてから  
役員と銀座  
グループ会社出向先の上司と  
異動と転職が決まっているが上司知らない、現場のメンバー知らない状態  
70日後に転職するとっしー  
異動してから辞めるまで  
技術顧問として契約  
辞める＝裏切り者  
[アルムナイ](https://hrnote.jp/contents/contents-8627/)  
企業間レンタル移籍斡旋業  
当たり前を疑う  
[スモックパイセン回](https://jamming.fm/41)  ---  
template: BlogPost  
path: /58
date: 2020-09-06T06:15:50.738Z  
title: ep58:自ら変化する（ゲスト：トッシーさん）
thumbnail: /assets/ep58.png
metaDescription: df sdf df  
---  
![ep58](/assets/ep58.png)  

<iframe src="https://open.spotify.com/embed/episode/5M7EPIXzHVNtJN3jDClukC" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

[コストコのお寿司](https://ultimate-setsuko.com/costco-sushi-matome/) が美味しい @naoharu  
髪が伸びてパーマかかってる @dadaism  
髪が伸びてパーマかかってる とっし  
転職後のとっしについて  
二軸ある（サイズ感、外資へ）  
人事部のプロ、マーケのプロ、みたいな採用はなかった気がする前職  
打ち合わせへの真剣度合いが違う  
カルチャードリブン、ビジョンドリブン  
余計なことをしない色と、進んでやるという色  
請負開発の力学  
新しいものを生み出す、売る、というマインド  
「あなたもこう変わることでメリットありますよ」  
[FABE](https://www.innovation.co.jp/urumo/fabe/)  
相手の営業スタイルがわかる  
フレームワークを知った上で経験しないとできない  
会社が違うと文化（使う筋肉）が違う  
足りない筋肉をいかに成長させるか  
「自ら動く」「周りが変化したから変わらざるをえない」の違い  
どこまで時代に追従するかって難しくない？  
5年スパンでエスカレーターが総入れ替えする時代  
給料という呪い  
そのための副業兼業ってあるのではないか  
利益構造上生まれる大企業で不満に対して  
自分の幅を広げるための長期製造戦略  
健全な危機感と緊張感  
言われたことをこなすのは子供  
自分で考えてやるのが大人  
---  
template: BlogPost  
path: /59
date: 2020-09-12T06:15:50.738Z  
title: ep59:家買いました
thumbnail: /assets/ep59.png
metaDescription: df sdf df  
---  
![ep59](/assets/ep59.png)  

<iframe src="https://open.spotify.com/embed/episode/1XGEAzGmymD0pgTZuSR1UN" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

カロリー摂取量記録している@dada  
食欲は幻想  
タバコ吸いたいと余分な食欲は似ている  
[マッスルグリルにハマっている@naoharu](https://www.youtube.com/channel/UCHoOFVQAhK-QyoXgf0iaZIg)  
[沼](https://www.youtube.com/watch?v=NJtgQEXAjNI&feature=emb_title)  
思ったよりタンパク質摂れてない  
中間管理録トネガワ見終わった  
家買った  
[ep14:持ち家 vs 賃貸](https://jamming.fm/14)  
清澄白河周辺高い  
田端信太郎に完全同意  
2年後に入居  
妥協して購入  
入居せずに売却もある  
FX フルレバ 25倍！の怖さ  
転勤族の子供  
中国の資本  
マンション購入もサブスクリプション  
新築賃貸となにが違うの？  
コロナでもあそこのプラウドは大丈夫！  
抽選落ち妬み乙  
マンション情報交換BBS  
持ち家賃貸ご意見募集中  
ファイナンシャルプランナーに聞いた  
[男はつらいよ](https://www.cinemaclassics.jp/tora-san/)  
毎回同じパターン  
共感性羞恥  
やめてーってなる  
ギネス記録らしい  
同じパターンを繰り返す  
江戸っ子のカタルシス  
集中力のいる映画  
今の時代どう再解釈すればよいのか  
寅さんは全国いろんなところを旅する  
渥美清さんのインタビュー動画  
ドコモ口座気をつけて  
---  
template: BlogPost  
path: /60
date: 2020-09-15T06:15:50.738Z  
title: ep60:kaggle Grandmaster
thumbnail: /assets/ep60.png
metaDescription:  
---  
![ep60](/assets/ep60.png)  

<iframe src="https://open.spotify.com/embed/episode/4oQaUPBf6ZAq5eJ0KZT1bl" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

Guest:onodera  
Senior Deep Learning Data Scientist at NVIDIA  
[Twitter](https://twitter.com/0verfit)  
[kaggle](https://www.kaggle.com/onodera)  

---  


久々に下北沢に行ったら変わってた ＠dada  
暑い＠naoharu  
スプラトゥーンやっている＠onodera  
[スプラトゥーン](https://www.nintendo.co.jp/wiiu/agmj/index.html)  
スプラトゥーンは女性のプレイヤー人口多い  
[APEX](https://www.ea.com/ja-jp/games/apex-legends/news)  
[Netflix ハイスコア](https://www.netflix.com/jp/title/81019087)  
[Nintendo TOKUO](https://www.nintendo.co.jp/officialstore/)  
スプラトゥーン極めればスパチャで食っていける  
ウデマエ  
4種目中1種目しかXいっていないのでまだまだ  
今後スプラトゥーンで飯食って  
[onoderaさんはデータサイエンティスト世界ランク28位のグランドマスター](https://www.kaggle.com/onodera/competitions)  
グランドマスターは日本で10人いない  
[kaggle](https://www.kaggle.com/)  
ピクルスではなくて渋谷  
信用の予測  
[Cornell Birdcall Identification](https://www.kaggle.com/c/birdsong-recognition/overview/description)  
脳死で勝てる  
[fMRI](https://www.tfu.ac.jp/research/gp2014_01/explanation.html)  
診ないで勝つ  
詳しくなくてもモデルは作れる  
ドメイン知識よりもデータの性質が大事  
大学で経済を学ぶ  
金融のSEから金融のコンサル  
独学でやった  
群雄割拠の時代を驀進している秘訣  
[21世紀で最もセクシーな職業](https://hbr.org/2012/10/data-scientist-the-sexiest-job-of-the-21st-century)  
打ち込める状態  
機械学習エンジニアとアプリエンジニアの違い  
作ったものをいったん捨てる  
自分の成績が公表されているのは強い  
Yahoo!からDeNA  
[NVIDIAは“謎のAI半導体メーカー”](https://nlab.itmedia.co.jp/nl/articles/1705/22/news113.html)  
[NVIDIAがARM買った](https://www.nikkei.com/article/DGXMZO63790730T10C20A9I00000/)  
[ESPP](https://keiriplus.jp/tips/kabushikihousyu_stockoption/)  
銀行の人が家に来るイベントが発生  
YouTuber目指している  
社内SEのドリーム  
NVIDIAは機械学習のライブラリを作っている  
[アドバイザーとして就任決定](https://prtimes.jp/main/html/rd/p/000000025.000023649.html)  ---  
template: BlogPost  
path: /61
date: 2020-10-02T06:15:50.738Z  
title: ep61:恋と哀れは種一つ（ではない）
thumbnail: /assets/ep61.png
metaDescription:  
---  
![ep61](/assets/ep61.png)  

<iframe src="https://open.spotify.com/embed/episode/5V5fXzov5aDztK08HAjrxt" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

（5）→ -5 になるエクセルの使用を初めてしった [@naoharu](https://twitter.com/naoharu)  
豆アレルギーの [@dada_ism](https://twitter.com/dada_ism)  
[GAASU](https://www.itmedia.co.jp/business/articles/2009/29/news106.html)  
[BEB5軽井沢](https://www.hoshinoresorts.com/resortsandhotels/omobeb/beb/5karuizawa.html)  
GOTO キャンペーン  
[Apple Watch Series 6](https://www.apple.com/jp/apple-watch-series-6/)  
芸能界の最近のニュース  
「可哀想かどうかは俺にきめさせてくれ」  
[尻拭いしてもいいじゃん](https://twitter.com/naoharu/status/1297463310351245313)  
SNSから離れたほうがいい  
[監視資本主義 デジタル社会がもたらす光と影](https://www.netflix.com/title/81254224)  
[ep12:人は喜んで自己の望むものを信じる](https://jamming.fm/12)  
[アメリカ 「ワクチン接種しない」 2人に1人 新型コロナ](https://www3.nhk.or.jp/news/html/20200930/k10012640561000.html)  
SNSでお金を配る人たち  
[前澤友作┃お金配りおじさん](https://twitter.com/yousuck2020)  
お金を配る人をRTしてる人を見たときにcertifiedしてしまう  
偽物をRTしてると、さらに「ほほう」  
「アカウント乗っ取られました」  
よくないねこういう話は  
タブー視されているということは何かがある  ---  
template: BlogPost  
path: /62
date: 2020-10-07T06:15:50.738Z  
title: ep62:個人事業主（ゲスト：あつみさん）
thumbnail: /assets/ep62.jpg
metaDescription:  
---  
![ep62](/assets/ep62.jpg)  


<iframe src="https://open.spotify.com/embed/episode/7e9572sGrU1DlhoFOFQ28W" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

TENETネタバレこわい [@naoharu](https://twitter.com/naoharu)  
国道沿いにハマってる [@dada_ism](https://twitter.com/dada_ism)  
TENETで酔った [@3able_atsumi](https://twitter.com/3able_atsumi)    
高校生のころの憧れ  
早く大人になりたい  
旅行会社法人営業部時代  
サラリーマンが憧れる個人事業主の「自分らしさ」  
大手企業に勤めながらも自立できてる感覚がしない  
どうやって食うの？  
芸人さん、役者さん、みたいな働き方  
イケハヤさんにそそのかされた人は結果インビジブルだけどすげえ
専門職の人たちをコーディネートする力  
共感してもらいたいモチベーション  
アツミさんの看板って何？  
「自分はどこに求められているだろう？」っていうことを考えるサラリーマンの恐怖  
次回へ続きます  ---  
template: BlogPost  
path: /63
date: 2020-10-24T06:15:50.738Z  
title: ep63:傭兵と武士（ゲスト：あつみさん）
thumbnail: /assets/ep63.png
metaDescription:  
---  
![ep63](/assets/ep63.png)  

<iframe src="https://open.spotify.com/embed/episode/6Lm7nkgAMxTLVJ128GOPLw" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***
  
</br>

Guest:Atsumi[@3able_atsumi](https://twitter.com/3able_atsumi)  

[劇場版「鬼滅の刃」無限列車編](https://kimetsu.com/anime/#top )観た [@naoharu](https://twitter.com/naoharu)  
鬼滅の刃は結構暴力シーン多い  
[Tom Sachs RETAIL EXPERIENCE ISETAN SHINJUKU](https://www.mistore.jp/shopping/feature/women_f2/thespace3_tomsachs1_w.html)行った [@dada_ism](https://twitter.com/dada_ism)  
体力測定したら50代判定もらった[@3able_atsumi](https://twitter.com/3able_atsumi)  
[アカツキ](https://aktsk.jp/)で仕事をする  
メディアライティングをする  
大手旅行会社を辞めたときのはなし  
まず東北に帰ってきたけど、スキルをつけるために東京へ  
コミュニティの運営もやりつつ時短で働く  
いろいろな人脈を増やす  
もともとやったことなかったけどライティングが好きになった  
[Zaim](https://zaim.net/)  
やりたいことと仕事は別  
全然知らない会社で働く、やっている事業にこだわりはない  
自分じゃなくてもできること  
幕府に仕える武士（サラリーマン）　と　中世の傭兵（個人事業主）
自分と会社の大義が合っているか  
サラリーマンと個人事業主のちがいってなに  
雇用契約と働き方  
まわりから信用を得る  
社内だけじゃなくて社外で信用を得る評価もある  
ゲストがファシリテーションする回あり  

---  
template: BlogPost  
path: /64
date: 2020-10-29T06:15:50.738Z  
title: ep64:自称クラウド実践者
thumbnail: /assets/ep64.png
metaDescription:  
---  
![ep64](/assets/ep64.png)  

<iframe src="https://open.spotify.com/embed/episode/5FSAhHbUTxI74LEWKpsUf5" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>

減量している [@dada_ism](https://twitter.com/dada_ism)   
マイプロテインの虫混入事件で消費量が減っている [@naoharu](https://twitter.com/naoharu)   
[ビーレジェンド](https://belegend.jp/)はあっさり味   
[マイプロテインミルクティ味](https://amzn.to/3mo75vz)  
人工甘味料の取りすぎはよくない？  
普通に赤コーラ飲むと甘さを感じなくなってしまう  
[AWS Perspective](https://aws.amazon.com/jp/solutions/implementations/aws-perspective/)    
開発プロセスが変わってきている  
プロジェクトに途中参加したときの理解につかえるかも   
[CloudFormer](https://docs.aws.amazon.com/ja_jp/AWSCloudFormation/latest/UserGuide/cfn-using-cloudformer.html)  
[AWS CloudFormation デザイナー](https://docs.aws.amazon.com/ja_jp/AWSCloudFormation/latest/UserGuide/working-with-templates-cfn-designer.html)  
マネコンさわってんじゃねぇ  
[テストできないことをするな](https://jamming.fm/26)  
[AWS Glue Studio](https://dev.classmethod.jp/articles/20200924-aws-glue-studio/)  
[新機能 – Lambda関数の共有ファイルシステム – Amazon Elastic File System for AWS Lambda]( https://aws.amazon.com/jp/blogs/news/new-a-shared-file-system-for-your-lambda-functions/)  
512MB問題を回避した思い出がある  
「容量が足りないから増やせ」だとああいう拡張にはならないのでは  
EFSをLambdaから触りたいっていうユースケースに答えたのでは  
[WordPress を AWS Lambda で運用する](https://keita.blog/2020/07/07/wordpress-%E3%82%92-aws-lambda-%E3%81%A7%E9%81%8B%E7%94%A8%E3%81%99%E3%82%8B/)  
AWSの中の人の気持ちを考える  
Pythonのランタイムを3.6から3.8に上げたら動かなくなった  
[Python の AWS Lambda 関数ハンドラー](https://docs.aws.amazon.com/ja_jp/lambda/latest/dg/python-handler.html)の外の世界で色々やってしまっていた  
仕様変更とバグフィックス  ---  
template: BlogPost  
path: /65
date: 2020-11-01T06:15:50.738Z  
title: ep65:ハイコンテキスト文化
thumbnail: /assets/ep65.png
metaDescription:  
---  
![ep65](/assets/ep65.png)  

<iframe src="https://open.spotify.com/embed/episode/5LwvBc2HDQ5nQt9fqWwNI3" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
***
  
</br>


久しぶりにデジタル一眼レフカメラで写真を撮った [@naoharu](https://twitter.com/naoharu)  
最近写真撮ってない [@dada_ism](https://twitter.com/dada_ism)   
新しいiPhone 12シリーズ（4機種）  
デザイナーの森くん待望のmini  https://jamming.fm/29  
生体認証はFace IDのみ  
マスクと手袋前提の時代が人類史上初めてやってくる？  
FaceIDはセキュリティじゃなくてUX  
false negative / true negative  
最近の歌手やバンドの演奏力・歌唱力  
動画共有サイトやボーカロイドが音楽シーンに与える影響  
[YOASOBI「夜に駆ける」](https://www.youtube.com/watch?v=x8VYWazR5mE)  
[ポルカドットスティングレイ「ヒミツ」](https://www.youtube.com/watch?v=nDEdQ8VHLo4)  
旬な話題に触れることでpodcastリリース作業を早くさせる  
家族でも誰に投票したかは言わない  
[M人を選出する場合、候補者数が次第にM+1人に収束していく](https://ja.wikipedia.org/wiki/%E3%83%87%E3%83%A5%E3%83%B4%E3%82%A7%E3%83%AB%E3%82%B8%E3%82%A7%E3%81%AE%E6%B3%95%E5%89%87)  
ポリコレ疲れ  
尾道の餃子店のマスク騒動  
ホリエモンは昔から変わってない？  
[高橋がなりの「まえむき人生相談」](https://www.youtube.com/channel/UCfHXFGSfNcicGJ46Jra3EPg)  
[ロジックハラスメント](https://qr.ae/pNbPFn)  
[Qアノン](https://ja.wikipedia.org/wiki/Q%E3%82%A2%E3%83%8E%E3%83%B3)  
@dada_ism も [AWS Perspective](https://aws.amazon.com/jp/solutions/implementations/aws-perspective/)試してみた  
影響調査に使えそうな感じ  
マネージドとフルマネージドの違い  
---  
template: BlogPost  
path: /66
date: 2020-11-05T06:15:50.738Z  
title: ep66:地球平面説
thumbnail: /assets/ep66.png
metaDescription:  
---  
![ep66](/assets/ep66.png)  

<iframe src="https://open.spotify.com/embed/episode/0ZCp4ZIntCF73SMasHcO8X" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***
  
</br>


背中が張っている [@naoharu](https://twitter.com/naoharu)   
会社に常駐マッサージ師がいる  
背中の筋トレ気持ちいい[@dada_ism](https://twitter.com/dada_ism)   
筋肉痛痛くていやだ  
久々にマイクラで落下死  
ゾンビが怖いマインクラフト  
[第二回フラットアースオンラインサミット](https://www.youtube.com/watch?v=0N8NhiYMTa8)  
[ビハインド・ザ・カーブ -地球平面説-](https://www.netflix.com/jp/title/81015076)  
頭ごなしに「頭おかしい！」はちょっと違う気がする  
仮説検証＝科学  
自然数の公理　VS　複素数の公理  
今のところ球体のほうが正しい  
だけど平面であることは完全に否定はできない  
子育てしていて気づいたこと  
なんで月はどこに行ってもついてくるの  
科学は仮説検証のエビデンス  
ロジカルを否定したら神が創造したも成り立つ  
[We are FlatEarthers!中村浩三「これからどうする？」構造](https://www.youtube.com/watch?v=ukdROWfMbhs)  
次のアップデートが来るタイミングは  
四次元立方体  
地球平面説信者はアメリカでは結構多い  
理解できないとき人は信じてしまう  
@dada_ismが詐欺にひっかかった話  
宗教やネットワークビジネス勧誘の注意喚起  
[ハワイ 日本の観光客受け入れ](https://news.yahoo.co.jp/pickup/6374930)  
ワイキキビーチで魚群がすごい  
[無限くら寿司](https://news.yahoo.co.jp/byline/shinoharashuji/20201027-00204867/)  
仕様バグだろ  
[鳥貴族の錬金術師](https://howtravel-gourmet.com/news/2004/)  
鬼滅の刃とくら寿司がコラボしているので子どもたちが行きたがる  
頑なに流行に乗らない人たち＝老害  
スラムダンクを4巻まで持っていて続きを見ない人  
---  
template: BlogPost  
path: /67
date: 2020-11-25T06:15:50.738Z  
title: ep67:悪気はないんだけど...
thumbnail: /assets/ep67.png
metaDescription:  
---  
![ep66](/assets/ep67.png)  

<iframe src="https://open.spotify.com/embed/episode/2FTSdJNPgK8yJqS4cUVoEO" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

久しぶりにクライミングをしたら娘にカッコいいって言われた [@naoharu](https://twitter.com/naoharu)   
檜の柱を買った [@dada_ism](https://twitter.com/dada_ism)   
先輩から若手に対して悪気なく詰める感じの言動  
先輩「なんでこの進め方してんの？なんで？意味わかんないんだけど。」  
それに対して過剰に反応して若手がブレーキ踏む現象がある  
先輩に対しても「そのコミュニケーション微妙っす」っていう必要ある  
若手に対しても「単純に聞いてるだけだと思うのでビビる必要はない」と諭す  
「存在しない悪意を作り出すことを抑止する」のはスクラムマスターの非常に大切な責務なのでは  
[権威勾配](qiita.com/hirokidaichi/items/5d8c4294083d85654a04)  
機長と副操縦士  
結局「慣れろ」って話なのか？  
マネジエント一歩手前の（日本企業でいう）35歳くらいの人たちに求められる振る舞い  
この放送も後輩も聞いてるからやりにくいこともあるんだが  
ストリートディープラーニングテクニック発動（不発）  
先輩に悪意が無いのは本当なのか？  
意図せず @naoharu が @dada_ism を詰める  
---  
template: BlogPost  
path: /68
date: 2020-12-06T06:15:50.738Z  
title: ep68:キャンピングカー旅行
thumbnail: /assets/ep68.png
metaDescription:  
---  
![ep68](/assets/ep68.png)  

<iframe src="https://open.spotify.com/embed/episode/2WXwVNOl18S9VwJTv8Jv96" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

加湿器がそろそろ必要 [@naoharu](https://twitter.com/naoharu)   
平壌オリンピック  
平昌オリンピック  
韓国のオンドル  
今年もクリスマスツリーのモミの木を購入した[@dada_ism](https://twitter.com/dada_ism)  
[ワールドデリカッセンニッシンのクリスマスツリー特売](https://www.nissin-world-delicatessen.jp/news/season/847/)  
コロナでクリスマスツリー回収値上げ  
MacBook M1チップ  
天寿を全うしたMacBook Pro 2012 midモデル  
ランニングシューズのサブスク [On Cyclon](https://www.on-running.com/ja-jp/cyclon)  
[牛角サブスク終了](https://business.nikkei.com/atcl/gen/19/00100/010800001/)  
おやつのサブスク[snaq me](https://snaq.me/)  
コーヒーのサブスク　[ポストコーヒー](https://postcoffee.co/)  
ビールのサブスク　[CRAFT BEER PASSPOR](https://www.favy.info/craft_beer_passport/lp_01)  
キャンピングカー旅行した  
マツダのボンゴトラックを改造したキャンピングカー[AMITY](https://atozcamp.com/amity/)  
佐々木家  
来月出産予定  
子供の名前ってどうやって決めたの？  
命名規則があるとかなりラク  
名前の候補は送ってこないでください　  ---  
template: BlogPost  
path: /69
date: 2020-12-15T06:15:50.738Z  
title: ep69:井の中の蛙 大海を知らねども（ゲスト：あおいさん）
thumbnail: /assets/ep69.png
metaDescription:  
---  
![ep69](/assets/ep69.png)  

<iframe src="https://open.spotify.com/embed/episode/2hs2tyF3maxbuhQeA3enaT" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

M1チップMacbookを買った @naoharu  
カメラ買いたいと思っている @dada_ism  
D70が鉄屑になった @aaaoooiii  
ぼちぼちにやってる  
給料のためにに働いている感じ  
ディスって欲しい @naoharu  
わきあいあいとやるってのは大事  
悪気なく追い込んでしまう年代  
老害と思われるのが一番怖い  
ジェネレーションz https://jamming.fm/18  
幼少期の教育について  
アンバランスな幼少期教育  
初め優等生だけとだんだん追いつかれる  
井の中の蛙  
挫折を受け止められる能力  
ちっちゃな井戸を作れる能力  
しめっぽい話になったね  
---  
template: BlogPost  
path: /70
date: 2020-12-25T06:15:50.738Z  
title: ep70:Adobe flash goes EOL
thumbnail: /assets/ep70.png
metaDescription:  
---  
![ep70](/assets/ep70.png)  


<iframe src="https://open.spotify.com/embed/episode/3uUDf4SeqLzp3We6Ytz9Ze" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

M1チップのせいかラグがすくなくなった  
[神津島が星空保護区になって胸キュンした@naoharu](https://www.tokyo-np.co.jp/article/45319)  
小笠原諸島は週1便しかない  
ボールアーサー  
星からしたら保護されていない  
グランピング行ってきた[@dada_ism](https://twitter.com/dada_ism)  
[シャボテン公園グランピングヴィレッジ](https://id-village.jp/glamping/)  
シャボテン公園のハシビロコウは今年亡くなったらしい  
キャンプで桃鉄  
[桃太郎電鉄 ～昭和 平成 令和も定番！](https://www.konami.com/games/momotetsu/teiban/)  
[mac ports](https://www.macports.org/)  
M1 mac mini メモリ16GB SSD 512GB　発注した  
AppleにはMac 無印がない  
MacはOSなのか？  
Macは値が落ちないのでM1出る前に売り抜ける作戦  
井の中の蛙大海を知らず  
自分の価値が通用する場所を広げていく  
トレイ掃除むっちゃうまい  
貨幣経済という井戸  
ニッチな井戸は大きな井戸につながっている  
Flashサポート終了  
[SUNTORY Craft Boss Flash Back Memories](https://www.suntory.co.jp/softdrink/craftboss/flash/)  
古いけど新しい  
文化は巡る  
[ヴェイパーウェーブ](https://ja.wikipedia.org/wiki/%E3%83%B4%E3%82%A7%E3%82%A4%E3%83%91%E3%83%BC%E3%82%A6%E3%82%A7%E3%82%A4%E3%83%B4)  
[バブル期のコカ・コーラCM](https://youtu.be/C8qD8svHG7c)  
ミレニアル世代の映えよりも心地よさ  
SNSは機能をパクり合う  
ExcelにFleetのるのでは  
消えるとうインパクト  
[Casey NeistatがつくったBeme](https://en.wikipedia.org/wiki/Beme_(app))  
どう使うかはユーザーが決める  
---  
template: BlogPost  
path: /71
date: 2021-01-13T06:15:50.738Z  
title: ep71:LDR(陣痛 分娩 回復)
thumbnail: /assets/ep71.png
metaDescription:  
---  
![ep71](/assets/ep71.png)  


<iframe src="https://open.spotify.com/embed/episode/4Ca7gTH9zjwD1JFAI3AdII" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

あけましておめでとうございます  
今年もよろしくお願い致します  
12月24日に無事誕生  
入院から分娩を立ち会った  
LDR - 陣痛（Labor）分娩（Delivery）回復（Recovery）  
計画無痛分娩  
夫も寝泊まりできるタイプの部屋だった  
[名付け検討にはTrelloおすすめ](https://trello.com/ja)  
画数少ないと実用的  
[Mac mini 2020 m1](https://www.apple.com/jp/mac-mini/)  
現像作業が体感5倍くらい速くなった  
レスポンスが速くなると人間の作業も速くなる  
ソフトウェア対応してないものがいくつかある  
Mac miniの問題点  
緊急事態宣言  
コロナ渦で消える文化  
[勃起が止まらない](https://anond.hatelabo.jp/20210106141921)  
2021年は分断の時代  
コロナはみんなが当事者  
[抗議デモのトランプ支持者らが米議会に侵入 女性が撃たれ死亡](https://www3.nhk.or.jp/news/html/20210107/k10012800761000.html)  
地球平面に異を唱える  
ふるさと納税の住所間違えて各自治体に修正の電話するdadaismの妻  
naoharu宛のクリスマスカードの郵便番号が六本木になってた  
SNSの功罪  
jammingしたい  
もうすぐ2周年  
1回100再生から200再生  
ep1は聞くに耐えないけど再生数トップ https://jamming.fm/1  
naoharuのゲストは女性が多い？  
[よなよな子さんの声に勃起した](https://jamming.fm/16)  
---  
template: BlogPost  
path: /72
date: 2021-01-21T06:15:50.738Z  
title: ep72:脊髄感謝
thumbnail: /assets/ep72.png
metaDescription:  
---  
![ep72](/assets/ep72.png)  


<iframe src="https://open.spotify.com/embed/episode/4ONrrsKBsOKWXa2Q7dgH2Q" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

M1 MacBook Pro が文鎮化した [@naoharu](https://twitter.com/naoharu)    
M1 Mac mini と外付けストレージの相性が悪い [@dada_ism](https://twitter.com/dada_ism)    
生誕一ヶ月  
育児休暇をとっていると言うと「すごいですね」「ホワイト企業ですね」って言われる  
課長が育休とったから課長業務を分担してやった  
初めてのことに対して一緒に対応できるのはチームビルディングになるのでは  
寝不足に慣れだした  
[Regional Scrum Gathering Tokyo 2021](https://2021.scrumgatheringtokyo.org/)  
[きょん＠アジャイルコーチ、システムアーキテクト](https://twitter.com/kyon_mm)  
[アジャイルを忘れるチーム Unlearn Agile](https://www.youtube.com/watch?v=5Ro5_c5kFaY)  
[アジャイルを忘れるチーム #RSGT2021 / Unlearn Agile](speakerdeck.com/kyonmm/unlearn-agile)
脊髄感謝  
子供たちの声を積極的に入れていくpodcast  
テレワーク中に子供の声があることで強制的に自己開示が進む  
ティピカル海外のイメージ「同僚と家の庭でBBQ」  
文字や言葉のコミュニケーション以外の部分  
リモートワークでコミュニケーションが取りにくい  
マネージャー「みんなが何をやってるかわからないから出社して」  
[「総合商社ならぬ総合リスクテイカー、というのが近い」"SIerとは何か"を一言で言い当てることの難しさについて](https://togetter.com/li/1633686)  
発注側と受注側のゴール  
「何をつくればいいか？」と「ちゃんと作れるか？」の２つの不確実性  
---  
template: BlogPost  
path: /73
date: 2021-01-31T06:15:50.738Z  
title: ep73:改善
thumbnail: /assets/ep73.png
metaDescription:  
---  
![ep73](/assets/ep73.png)  


<iframe src="https://open.spotify.com/embed/episode/66EGbg9pC3I9SzTAa2eBNs" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

家の近くに美味しいラーメン屋を見つけた[@naoharu](https://twitter.com/naoharu)   
すぐにお腹いっぱいになる福岡  
福岡は歩いて観光という感じではない  
デスク周りの模様替えした[@dada_ism](https://twitter.com/dada_ism)   
ニュースキャスターの自宅スタジオ  
[NBC News Now](https://www.nbcnews.com/now)  
背景込みでアイデンティティ  
[Clubhouse](https://www.joinclubhouse.com/)  
Invitationもらうかwaitlistに登録して待つ  
[DISCORD](https://discord.com/)  
waitlist積み上げるやり方どうなのよ  
@naoharu取れなかったサービスは使わない  
jamming.fmのふりかえりやった  
[miro](https://miro.com/)  
[振り返り結果（miro）](https://miro.com/app/board/o9J_lXMURDA=/)  
[KPT](https://seleck.cc/kpt)  
あたり屋スタイルよかった  
なんのためにやっているのか  
収録の最後にふりかってみる試み  
[インセプションデッキやってみる](https://www.mof-mof.co.jp/blog/column/management-ajile-with-inception-deck)  
我々はなぜここにいるのか  
どうでもいいことをべらべら喋る  
jamming.fmはカイジであるの意味はわすれた  
ファクトに基づいて改善するべきか  
数値的なKPIを安易に作ると短絡的な施策を打ちがち  
ガールズバーUI？  
[良いKPIとはなにか、あるいはガールズバー施策について](https://note.com/hik0107/n/n54c536cc8321)  
CP+   
大衆に迎合しすぎるのもどうなの  
  
***
  
初の試みふりかえりの部  
インセプションデッキの説明があんまりなかった  
聞いてくれてる人は自分たちと共通言語持っていない  
自分たちのやったことを共有するというテーマは珍しい  
収録時の手順リストつくった　Keep  
１トピック１エピソードでやってたけど評価していない  
物理からリモートでエピソード構成が変わった  
今は週１回決まった時間に収録している
運用の方法も振り返りたい  
ペアリリースやってみたい  
---  
template: BlogPost  
path: /74
date: 2021-02-11T06:15:50.738Z  
title: ep74:最も大切な能力
thumbnail: /assets/ep74.png
metaDescription:  
---  
![ep74](/assets/ep74.png)  


<iframe src="https://open.spotify.com/embed/episode/6quRxGDxPBDR0gh0A9IIHe" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>

ゴールド免許になった [@naoharu](https://twitter.com/naoharu)   
マイナンバーカードを手に入れた [@dada_ism](https://twitter.com/dada_ism)   
[本所警察署](https://www.keishicho.metro.tokyo.jp/about_mpd/shokai/ichiran/kankatsu/honjo/index.html)の壁一面に事故の現場写真たくさん貼ってある  
[Ogrish.com - Wikipedia](https://ja.wikipedia.org/wiki/Ogrish.com)  
免許センター「講習受けなくていいのはラッキーですね〜」←罰ゲーム？  
ゴールド免許「講習受けさせてください！」  
[Clubhouse](https://www.joinclubhouse.com/)  でアカウント名naoharuが取れなかった  
[N](https://twitter.com/n)さん  
[情報処理技術者試験](https://www.jitec.ipa.go.jp/1_02annai/r03haru_exam.html)  
キーボード変えたい  
[アミロのキーボード](https://www.fumo-shop.com/manufacturer/varmilo.html)  
カーソルキーの要否  
[西部のカウボーイの話](https://www.pfu.fujitsu.com/hhkeyboard/dr_wada.html)  
[畳の敷き方](https://ja.wikipedia.org/wiki/%E7%95%B3)  
セブンイレブンのATMは静電容量無接点方式  
[生きていく上であったほうが良い能力ってなに](https://togetter.com/li/1654998)  
---  
template: BlogPost  
path: /75
date: 2021-02-22T06:15:50.738Z  
title: ep75:ふりかえりガイドブック
thumbnail: /assets/ep75.png
metaDescription:  
---  
![ep75](/assets/ep75.png)  

<iframe src="https://open.spotify.com/embed/episode/5sSkml2wRq0jSvFYbVEIZz" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>


Mac mini m1のスピーカーとして骨伝導イヤホン導入した[@dada_ism](https://twitter.com/dada_ism)   
[AfterShokz AEROPEX](https://aftershokz.jp/products/aeropex)  
漬けるのにハマっている[@naoharu](https://twitter.com/naoharu)    
人生の効率をアップさせることにはまってる  
食洗機と洗濯機を同時に動かしたい  
対赤ちゃんだと通用しない  
ふりかえりエバンジェリストの森さん [＠viva_tweet_x](https://twitter.com/viva_tweet_x)  
森さんの本[「アジャイルなチームをつくる ふりかえりガイドブック」](https://amzn.to/3vVx2WA)発売！  
ABDで物理本買うか  
アジャイル開発じゃなくて、いろいろな場面でふりかえりやればよい  
[アジャイルレトロスペクティブズ　強いチームを育てる「ふりかえり」の手引き](https://amzn.to/3BrS0xA)  
HowToだけだと地獄のようなふりかえりはなくならない  
本当に大切なのはHowToではなくファシリテートや場づくり  
[SCRUM BOOT CAMP THE BOOK](https://amzn.to/3CuYugg)  
[ふりかえり.am](https://anchor.fm/furikaerisuruo/episodes/ep-1-amPodcast---viva_tweet_xhiguyume-Podcast-ea1lk9)  
[ふりかえり読本](https://hurikaeri.booth.pm/items/1076615)  
とりあえずやってみる精神とインセプションデッキやってみるは相容れない  
しっかり考えつつそれでやる  
家族が増えると人生の難易度が上がる  
ぷよぷよの色が増える感じ  
制約条件が増えてくる  
飲み屋でしたい話題  ---  
template: BlogPost  
path: /76
date: 2021-02-28T06:15:50.738Z  
title: ep76:セレンディピティ
thumbnail: /assets/ep76.png
metaDescription:  
---  
![ep76](/assets/ep76.png)  

<iframe src="https://open.spotify.com/embed/episode/0zxS3BOYllPzIbcVtBXwAL" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

</br>


jetboilで料理作る [@naoharu](https://twitter.com/naoharu)    
一人分の料理を作るのが楽になる  
マイナンバーカードを取りに行った [@dada_ism](https://twitter.com/dada_ism)   
「パスワードをあらかじめ紙に書いてきてください」  
[虐待疑惑で子供と長期別居になる話](https://www.fnn.jp/articles/-/145828)  
どの家庭でも起こり得る悲劇  
[Do no harm](https://www.nbcnews.com/donoharm)  
虐待の疑いがある場合は両親への通知なしに児童相談所に子供に保護される場合がある  
チームでの雑談  
「簡単に作れる美味しい料理はなんですか？」  
簡単に作りたい、は本当か  
料理が楽しければ簡単である必要ではないことに気づく  
[問いを立てておく「マルチイシュー」](https://jamming.fm/52)  
[セレンディピティ](https://ja.wikipedia.org/wiki/%E3%82%BB%E3%83%AC%E3%83%B3%E3%83%87%E3%82%A3%E3%83%94%E3%83%86%E3%82%A3)  
「観察の領域において、偶然は構えのある心にしか恵まれない」  
[niboshism](https://niboshism.com/) もセレンディピティ  
セレンディピティはぷよぷよ？  
[はじめて考えるときのように](https://www.amazon.co.jp/dp/B00H8LI0IS/)  
「子供が欲しがる文房具ってなあに？」  
ふりかえりの時間は放送時間内には入れなくなりました  ---  
template: BlogPost  
path: /77
date: 2021-03-11T06:15:50.738Z  
title: ep77:おまえ、それで成功したことあんの？
thumbnail: /assets/ep77.png
metaDescription:  
---  
![ep77](/assets/ep77.png)  

<iframe src="https://open.spotify.com/embed/episode/4R5JvUHXVHCK2kjs3I5wU1" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
***

</br>

花粉症 [@naoharu](https://twitter.com/naoharu)   
子供の予防接種行ってきた [@dada_ism](https://twitter.com/dada_ism)  
はんこ注射の跡  
はんこ注射のカタチおかしくね？  
[乳幼児医療費助成制度（マル乳）](https://www.fukushihoken.metro.tokyo.lg.jp/iryo/josei/marunyu.html)  
[OCB MUSIC LIBRARY #2 | GREEN ASSASSIN DOLLAR](https://www.youtube.com/watch?v=Ov26TXCVVfQ)  
今までに聴いたことないわ！ってくらいの衝撃  
Podcastに曲いれると請求がくる  
仕事中に音楽聴く？  
音楽聞いてできるような仕事ではないという主張  
フリーアドレスが流行った時代  
営業の人はフリーアドレスにあっている  
コロナ時代でフリーアドレスは死んだ  
固定席だったけど、改善案で席替えをやった  
色んな人とコミュニケーションが増えた  
独り言に突っ込むことはしない  
ToDo管理  
家族のタスクどうしてる  
[Trello](https://trello.com/ja)  
物理のほうがいい場合もある  
いつでもどこでもで不便になること  
ルーチンワークの分担  
あえていつもやらない家事をやる  
大人の味付  
そら豆うまい  
綿菓子売ってない  
[わたパチ ※2016製造終了](http://qa.meiji.co.jp/faq/show/11852?category_id=249&site_domain=default)  
ファミリーマートの店員の名札の星なんで客に見せてる？  
[Yahoo知恵袋の回答](https://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q1373496723)  
[限定ジャンケン](https://ja.wikipedia.org/wiki/%E8%B3%AD%E5%8D%9A%E9%BB%99%E7%A4%BA%E9%8C%B2%E3%82%AB%E3%82%A4%E3%82%B8#%E9%99%90%E5%AE%9A%E3%82%B8%E3%83%A3%E3%83%B3%E3%82%B1%E3%83%B3)  
[1日外出録ハンチョウ](https://ja.wikipedia.org/wiki/1%E6%97%A5%E5%A4%96%E5%87%BA%E9%8C%B2%E3%83%8F%E3%83%B3%E3%83%81%E3%83%A7%E3%82%A6)  
[上京生活録イチジョウ](https://ja.wikipedia.org/wiki/%E4%B8%8A%E4%BA%AC%E7%94%9F%E6%B4%BB%E9%8C%B2%E3%82%A4%E3%83%81%E3%82%B8%E3%83%A7%E3%82%A6)  
[中間管理録トネガワ](https://ja.wikipedia.org/wiki/%E4%B8%AD%E9%96%93%E7%AE%A1%E7%90%86%E9%8C%B2%E3%83%88%E3%83%8D%E3%82%AC%E3%83%AF)  
[真鍋昌平「九条の大罪」](https://bigcomicbros.net/work/35215/)  
[九条の大罪・第１話を法的観点から考察](https://note.com/oredayo_ore/n/ne64be5c33038)  
[ゴールデンカムイ](https://youngjump.jp/goldenkamuy/)  
Linkedinでムカついたこと  
おまえそれで成功したことあんの？  
ブルートフォースアタック(電話)  
[Amazon Polly](https://aws.amazon.com/jp/polly/)  
新橋の駅前の名刺交換  
保険のお姉さん  
団地の保険のおばちゃん  
スマホも変わりつつある  
選べればよい  
---  
template: BlogPost  
path: /78
date: 2021-03-15T06:15:50.738Z  
title: ep78:IT業界で何が流行ってるの？
thumbnail: /assets/ep78.png
metaDescription:  
---  
![ep78](/assets/ep78.png)  

<iframe src="https://open.spotify.com/embed/episode/0gICxSd3cu2OF5r6KsXLUA" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>
***

子供の健康診断行ってきた [@dada_ism](https://twitter.com/dada_ism)  
花粉症の病院行った [@naoharu](https://twitter.com/naoharu)   
ゲスト出演は価値観アップデートが進むのでは  
jammingfm のおたより紹介  
Quoraがおすすめ   
クラブハウスで話す感じ（下ネタあり）   
はんこ注射のカタチおかしくね？  
[おきゃダレBASE【モノマネーの虎】](https://www.youtube.com/channel/UCX1w_lX6enkXibDkNE5VMmA)  
社会人になってから改めて見るマネーの虎めっちゃ刺さる  
人格を否定するような虎の発言  
世代を超えて残る名言  
[ぶち殺すぞ.....ゴミめら....!](https://www.google.com/search?q=fuck+you+%E3%82%B4%E3%83%9F%E3%82%81%E3%82%89&rlz=1C5CHFA_enJP936JP936&sxsrf=ALeKk01vHdN_HZ6LakrO2x4VrksYdI4y8Q:1615819583499&source=lnms&tbm=isch&biw=1671&bih=882)   
[マネーの虎　うどん屋【全版】](https://www.youtube.com/watch?v=96EbDCQnW0w)  
説明する能力  
ぼこぼこにされたからら成功したのでは説  
[@naoharu](https://twitter.com/naoharu)  がいいなと思ったコントリビュータ文化  
どこで情報を集めるか、どうやって探すように設計するか  
いまIT業界で何が流行っているのか？  
何に従事していたいか  
どんな技術にキャッチアップしていきたいか？  
サーバレス vs コンテナ？  
どういう仕事がしたいか？  
ビジネスサイド行きたい  
家族みんなで新しいことをしていきたい  
子供にデバイスをどの程度あたえるべきか問題  
---  
template: BlogPost  
path: /79
date: 2021-03-19T06:15:50.738Z  
title: ep79:WEB2.0
thumbnail: /assets/ep79.png
metaDescription:  
---  
![ep79](/assets/ep79.png)  

<iframe src="https://open.spotify.com/embed/episode/5IS3i7pRaDLaxFYgUG3C0U" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

直火式エスプレッソメーカー（マキネッタ）購入 [@naoharu](https://twitter.com/naoharu)   
[ビアレッティモカエクスプレス](https://bialetti.jp/index.php?dispatch=categories.view&category_id=46)  
エスプレッソはQoL上がる  
下半身の筋トレは筋肉痛が長引く [@dada_ism](https://twitter.com/dada_ism)   
必須アミノ酸効果あるの？  
今日のお品書き  
10年前にLINEなかったけど何で連絡とってた？  
[iPhone 3G](https://ja.wikipedia.org/wiki/IPhone_3G)  
どんなガラケー使ってた？  
[INFOBAR](https://ja.wikipedia.org/wiki/INFOBAR)  
[WILLCOM](https://ja.wikipedia.org/wiki/%E3%82%A6%E3%82%A3%E3%83%AB%E3%82%B3%E3%83%A0)  
[BlackBerry](https://ja.wikipedia.org/wiki/BlackBerry)  
10年前はmixi使ってたけどもっと昔な感じ  
[Twitterは2006年から](https://ja.wikipedia.org/wiki/Twitter)  
RT機能もなかった  
リモート化にともなってなくなった移動時間と作戦会議  
お客さん先のエレベーターの中の会話  
電車の中は答え合わせの時間  
喫煙所の会話がなくなったのと近い？  
[塩川さん回「ep51:雑談の効能」](https://jamming.fm/51)  
勝手に想像するベンチャー像  
飲み会は必要だったというおじさんに近いのかも  
スーツはコスプレ  
俺らにとっての昭和像  
酔っ払ったらネクタイ頭に巻く  
NinjaやSamurai  
肩パッドにダブルのスーツ  
昔ネタばっかりで老害ぽい  
[Disney+](https://disneyplus.disney.co.jp/)  
[The Simpsons](https://disneyplus.disney.co.jp/view/#!/series/detailed/%E3%82%B6%E3%83%BB%E3%82%B7%E3%83%B3%E3%83%97%E3%82%BD%E3%83%B3%E3%82%BA/413323)  
[ディズニープラス 4月以降に配信される大型作品の国内配信日を発表](https://disneyplus.disney.co.jp/news/2021/0225_01.html)  
[「ディズニープラス」、会員数１億人突破－開始からわずか１年４カ月](https://www.bloomberg.co.jp/news/articles/2021-03-09/QPPTVPDWX2PV01)  
[Web 2.0](https://ja.wikipedia.org/wiki/Web_2.0)  
[梅田望夫『ウェブ進化論』](https://www.chikumashobo.co.jp/top/shinka/mokuji.html)  
プラットフォームよりもコンテンツ  
Disney+はアプリがだめ  
デズニーランドはVRのはしり  
デズニーランドで一番好きな乗り物  
スプラッシュマウンテンはディズニー映画『南部の唄』がモデルになっている  
[イッツ・ア・スモールワールド](https://www.tokyodisneyresort.jp/tdl/attraction/detail/172/)   
[シンドバッド・ストーリーブック・ヴォヤッジ](https://www.tokyodisneyresort.jp/tds/attraction/detail/235/)  
It's a small world after all＝世界はまるい  
イッツ・ア・スモールワールド乗っても丸くはなくて平面だと気づく  
自分がゴリゴリに日本の田舎の出身でそのコンテキストで育ってきたときにそれをどう上書きするかと考えた時に自分が考えること  
父親みたいにななりたくないと逆に振れるのでは？  
自己開示をしていく  
愛知県のゲットー　春日井市  
ヘビーリスナーの特権  
[ディアゴスティーニ](https://deagostini.jp/)  
お父さんはABC話せる  
お礼参りに行く  
鬼怒川の鬼怒太  
[ホテル 三日月](http://www.mikazuki.co.jp/kinugawa/)  
インフィニティプールみたいなものがある  
---  
template: BlogPost  
path: /80
date: 2021-03-30T06:15:50.738Z  
title: ep80:褒章
thumbnail: /assets/ep80.png
metaDescription:  
---  
![ep80](/assets/ep80.png)  

<iframe src="https://open.spotify.com/embed/episode/0z2RVezPzGzyAylu075wWt" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***

猫と会話できるようになった [@dada_ism](https://twitter.com/dada_ism)   
雑談の鉄板ネタは天気だと思う [@naoharu](https://twitter.com/naoharu)   
天気の雑談してて地雷踏んだ経験がある方は教えてください  
天気万能説は地球平面説に次ぐ説  
「暑いですね」「謙虚なれよ」  
linkedinで自分の経歴を書くときに英語で書くと心理的ハードルが下がる  
社内の昇格試験で書いた文章をそのまま貼り付けてみた話  
翻訳サイトは盛ってくれない  
半径1km以内の情報をどう集めるか  
ママ友パパ友から得る情報  
物理的な近所付き合いの重要性  
[@dada_ism](https://twitter.com/dada_ism) 「jamming.fmを聞いて、へーって思って持ち帰ってもらいたい」  
天皇陛下から貰える褒章  
紺綬褒章は金さえ払えば漏れなくプレゼント？  
ふるさと納税は紺綬褒章のレギュレーションをクリアするか？  
[@dada_ism](https://twitter.com/dada_ism) 「イクわ」  
[「次はお母さんと一緒に来て」…新米パパが感じた“育児の世界に歓迎されていない”違和感の正体](https://bunshun.jp/articles/-/43866)  
区の児童館「お父さんはセカンダリですよね」  
すげえ褒められる。不良が普通のことをしたら褒められる感じ。  
[なべさんのフィードバック](https://twitter.com/nabe_merchant/status/1373197931403079686)  
[なおこさん回で価値観の上書きが発生したケース](https://jamming.fm/47)  
構造を問題視することで企業価値が上がる  
公務員は競争するモチベーションが無いので競争力が生まれない。そして自覚できてない（推測）  
コントリビューター文化が機能するか否か   
「気づいたやつがやればいい」というモデルがワークする環境って何か   
「気づいた人がやればいい」は繊細に対するフリーライドか？でも評価されるのでは？  
搾取マンとは？  
「多くのリターンを取り出すことを自分の価値だと思っている人」  
---  
template: BlogPost  
path: /81
date: 2021-04-05T06:15:50.738Z  
title: ep81:最短距離の危うさ
thumbnail: /assets/ep81.png
metaDescription:  
---  
![ep81](/assets/ep81.png)  

<iframe src="https://open.spotify.com/embed/episode/1ACViTWVSAc7t6nOLSquz4" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

最寄りの児童館に黒沢があった [@naoharu](https://twitter.com/naoharu)   
[最強伝説 黒沢](https://ja.wikipedia.org/wiki/%E6%9C%80%E5%BC%B7%E4%BC%9D%E8%AA%AC_%E9%BB%92%E6%B2%A2)  
柴又に行った [@dada_ism](https://twitter.com/dada_ism)  
[柴又帝釈天](http://www.taishakuten.or.jp/)  
[田中邦衛](https://ja.wikipedia.org/wiki/%E7%94%B0%E4%B8%AD%E9%82%A6%E8%A1%9B)  
[男はつらいよ 奮闘篇](https://www.cinemaclassics.jp/tora-san/guest/19/)  
[ストライダー](https://www.strider.jp/)  
子供が補助輪なしの自転車に乗れるようになるには  
みんなのためになるtipsを入れていこう  
子供が保育園デビュー  
保育園って大家族っぽい感じ  
菓子折り持っていくことは分断に加担するか  
国内旅行ってもうどこ行っても大体同じ  
子供が他のおうちの子供と話してるの見て感動  
3歳児「（私）2歳！」  
日本国内ってもうどこもぶっちゃけ同じだから旅行先に迷う  
[国道9号](https://ja.wikipedia.org/wiki/%E5%9B%BD%E9%81%939%E5%8F%B7)  
娘たちとプリクラをとったら激烈にむずかった  
naoharuが娘たちと撮った[プリクラ](https://twitter.com/naoharu/status/1377540312399192067)  
百円ショップむちゃくちゃ楽しい  
[マリオカート](https://mariokart-acgpdx.bngames.net/)    
プリクラがスマホに駆逐されない理由  
あの場で合意形成をするのがいいのでは  
ブリリアントジャーク  
[タチの悪い凄腕エンジニア](https://note.com/floyd0/n/n1db7854ca2e2)  
[那須サファリパーク](https://www.nasusafari.com/)  
キリンが後部座席の窓からシフトレバーらへんまで入ってきた  
「最悪自分の車だから掃除すればいいや」は家父長制の考え方なのでは  
[イチロー イチ流のバッティング理論 素振りの仕方](https://www.youtube.com/watch?v=9y-6QsY5UTE)  
<iframe width="560" height="315" src="https://www.youtube.com/embed/9y-6QsY5UTE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

[大規模レガシー環境に立ち向かう有機的な開発フォーメーション](https://www.slideshare.net/i2key/devsumi-152929762)  

<iframe src="//www.slideshare.net/slideshow/embed_code/key/LRsH7jWNZXPAWK" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/i2key/agile-241020629" title="カネとAgile（大企業新規事業編） #rsgt2021" target="_blank">カネとAgile（大企業新規事業編） #rsgt2021</a> </strong> from <strong><a href="https://www.slideshare.net/i2key" target="_blank">Itsuki Kuroda</a></strong> </div>
  

色々な要素技術などに対してまんべんなく「張れる」のは大切  






---  
template: BlogPost  
path: /82
date: 2021-04-12T06:15:50.738Z  
title: ep82:乳児頭蓋変形外来
thumbnail: /assets/ep82.png
metaDescription:  
---  
![ep82](/assets/ep82.png)  

<iframe src="https://open.spotify.com/embed/episode/6tdFzOQRFeJeUsEKsTfPqI" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  

  

保育園に迎えに行くと泣かれる [@dada_ism](https://twitter.com/dada_ism)  
平日ディズニーはよい [@naoharu](https://twitter.com/naoharu)   
待ち時間長くても20分  
[ディズニーランドチケット販売状況](https://www.tokyodisneyresort.jp/ticket/sales_status.html)  
待ち時間UX省略  
流量制御演出省略  
ワクチンパスポートとお薬手帳  
乳児頭蓋変形外来に行った  
絶壁  
[東京女子医科大学附属病院 脳神経外科](http://www.twmu.ac.jp/NIJ/column/pediatric/)  
mac mini 7台=ヘルメット  
夫婦で判断する難しさ  
お医者さんのスキル  
予後が悪いとき  
人は将来のリスクを甘く見積もりすぎる  
自分の子供のことを自分ごととして考えるのは難しい  
納得はすべてに優先する  
実際に安全かどうかよりも安心感  
某政党の若手座談会＠cloubhouse  
○○さんにも評価されてるし  
いきなり寝技  
寝技＝政治  
彼らにとっては立技  
政治家は会食なくなったら困る！  
リチウムイオンバッテリーは純正ではないくて中国製でいいのでは  
純正のバッテリー高い  
中国クオリティ高い  
10年前はあんまりよくなかった  
事故になる恐れはあるので自己責任で  
本当に読んでもらいときはクリック数を下げるとCVRは上がる  
寝技か？  
老害力高めの発想  
ぬるぬるのハイコンテキストコミュニケーター  
トヨタ 豊田章男社長の入社式の話  
<iframe width="560" height="315" src="https://www.youtube.com/embed/qruCc_LAJ20" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

1年間で2000人新卒採用  
そのうち1割が退職  
[ep36:チームワーク(ゲスト：@chia_key)](https://jamming.fm/36)  

　---  
template: BlogPost  
path: /83
date: 2021-04-20T06:15:50.738Z  
title: ep83:ねだるな勝ち取れ
thumbnail: /assets/ep83.png
metaDescription:  
---  
![ep83](/assets/ep83.png)  

<iframe src="https://open.spotify.com/embed/episode/5JAIV9PlNmOXySpJsko1D9" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  

新しいことしたい [@naoharu](https://twitter.com/naoharu)   
今の小学生は将棋やオセロめちゃ強そう  
育児休暇終わる [@dada_ism](https://twitter.com/dada_ism)  
[パイオニア探査機の金属板](https://ja.wikipedia.org/wiki/%E3%83%91%E3%82%A4%E3%82%AA%E3%83%8B%E3%82%A2%E6%8E%A2%E6%9F%BB%E6%A9%9F%E3%81%AE%E9%87%91%E5%B1%9E%E6%9D%BF)  
githubにホストしとく  
<iframe width="560" height="315" src="https://www.youtube.com/embed/fzI9FNjXQ0o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

インスタライブでもやってます  
[東京ディズニーリゾートの交代乗り](https://faq.tokyodisneyresort.jp/tdr/faq_list.html?page=1&category=26)  
[org.springframework.util Class AntPathMatcher](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/util/AntPathMatcher.html)  
preciselyに希望する大切さ  
「なんでもいい！」と「これがやりたい！」    
丿貫 とコラボTを作った niboshism  
[niboshism webshop](https://niboshism.stores.jp/)  

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">【コラボTシャツ情報】<br>灰汁中華丿貫 × niboshism<br>のコラボロングTシャツつくりました！<br><br>今回はこのコラボTシャツを5,500円〜6,000円辺りで受注生産で作ろうかと考えてます。<br><br>みなさんの意見が知りたいです！<br><br>ある程度欲しいが多いいようでしたら募集しようかと思います。<a href="https://twitter.com/hashtag/%E6%8B%A1%E6%95%A3%E5%B8%8C%E6%9C%9B?src=hash&amp;ref_src=twsrc%5Etfw">#拡散希望</a><a href="https://twitter.com/hashtag/niboshism?src=hash&amp;ref_src=twsrc%5Etfw">#niboshism</a></p>&mdash; 横濱丿貫@アソビル【公式】 (@NiboshiYokohama) <a href="https://twitter.com/NiboshiYokohama/status/1379337815331205120?ref_src=twsrc%5Etfw">April 6, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>  
<iframe width="560" height="315" src="https://www.youtube.com/embed/Qp3b-RXtz4w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

検閲されがちがコンテンツ  
ごっつええかんじ  
日光東照宮の🙈🙉🙊  
蒸気機関車の運用  
トーマスとSDGs  

　---  
template: BlogPost  
path: /84
date: 2021-04-26T06:15:50.738Z  
title: ep84:子どもの子どもによる子どものための相談室
thumbnail: /assets/ep84.png
metaDescription:  
---  
![ep84](/assets/ep84.png)  

<iframe src="https://open.spotify.com/embed/episode/4Z7wuDHsGnWCzphiaKhqb1" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  



オクラとツナと塩昆布うまい [@naoharu](https://twitter.com/naoharu)   
子どもがヘルメット装着した [@dada_ism](https://twitter.com/dada_ism)  
[ep82: 乳児頭蓋変形外来](https://jamming.fm/82)  
年度末のアンニュイーな感じの雑談回で、チームで何やりたい？？？って聞いたら意外な答えが聞けた話  
心に余裕がある状態  
自分たちのやったことが価値を出しているのかどうか測りたい  
Instagramライブ配信  
売上げ＝価値ではない  
とりあえずバックログに積んだ  
35歳って立ち振舞いが難しい  
ヌルヌル人材は寝技を繰り出す老害  
ローションを全身に塗ってマットに寝る  
「私は組織の中で潤滑油みたいなもので」  
その武器は安易に持つと破滅するぞ  
辞めるー！ローション捨てるー！  
[ピエトロドレッシング](https://www.pietro.co.jp/products/dressing/)  
[全地球史アトラス　フルストーリー](https://www.youtube.com/watch?v=rTCXFuQSSNU)  
地球の終わり  
[フラットアース](https://jamming.fm/66)  
「それは宇宙の真理なのか？」  
レビューに呼ぶのやめようぜ！  
[Youtube Barney](https://www.youtube.com/channel/UCTn45orVsFOsNZHxAJcZK5g)  
[Wikipedia バーニー&フレンズ](https://ja.wikipedia.org/wiki/%E3%83%90%E3%83%BC%E3%83%8B%E3%83%BC%26%E3%83%95%E3%83%AC%E3%83%B3%E3%82%BA)  
恐竜の顔が笑ってるのがいけない  
[おかあさんといっしょ](https://www.nhk.jp/p/okaasan/ts/ZPW9W9XN42/)  
今ならどんなタイトルになる？  
[みんなのうた](https://www.nhk.or.jp/minna/)  
[賭博黙示録カイジ　電流鉄骨渡り](https://dic.pixiv.net/a/%E9%89%84%E9%AA%A8%E6%B8%A1%E3%82%8A)  
子どもの悩みに子どもが答える  
それができれば子どもたちが自己組織化できるかも  
子どもなのに警察組織ができる  
とある国の小学校のカリキュラムは生徒自ら決める  
[エバッキー(江端 一将さん)の認定スクラムマスター研修](https://www.odd-e.jp/team_01/)  
肘のところに蟻がついて嫌だった  
[漁師とコンサルタント](https://hebinuma.com/2014/09/10/post-4176/)  
Twitter辞めたほうが幸せ
はてなブックマークのコメント見ないほうが幸せ
イシューの質は相対的なもの
お品書き制度について　
---  
template: BlogPost  
path: /85
date: 2021-04-30T06:15:50.738Z  
title: ep85:職人（ゲスト：なおこさん）
thumbnail: /assets/ep85.png
metaDescription:  
---  
![ep85](/assets/ep85.png)  

<iframe src="https://open.spotify.com/embed/episode/5Gqj4iLxqWOrfzKKrL71ch" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  



荒川の土手にいる [@naoharu](https://twitter.com/naoharu)   
荒川の土手にいる [@dada_ism](https://twitter.com/dada_ism)  
[RMS](https://ja.wikipedia.org/wiki/%E3%83%AA%E3%83%81%E3%83%A3%E3%83%BC%E3%83%89%E3%83%BB%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB%E3%83%9E%E3%83%B3)  
一年振りの出演のなおこさん  
災害無線  
[ep45](https://jamming.fm/45),[ep46](https://jamming.fm/46),[ep47](https://jamming.fm/47)  
[水素自動車MIRAI](https://toyota.jp/mirai/)  
赤羽岩淵  
[ロングアイランド](https://ja.wikipedia.org/wiki/%E3%83%AD%E3%83%B3%E3%82%B0%E3%82%A2%E3%82%A4%E3%83%A9%E3%83%B3%E3%83%89)  
奴隷契約書「二度とこの制服の姿には戻れないぞ」  
自分で働いて稼いだ金で飯を食う  
楽しく仕事をしたい  
ジョブチェンジ失敗しても死なない  
「いつかそいつも死ぬし」  
[開先](https://www.weldtool.jp/article/yousetsu-sozai/1149)  
溶接職人  
blue-collar worker  
電気屋  
「ブラックバスじゃ寿司は握れねえよ」って言わない  
道具に名前書いとく必要がある  
借りパク  
ティッシュ泥棒  
トイレ休憩  
上下関係が厳しいのは命の危険があるから  
家族の協力  
飛び込むことで見える世界がある  
フィジカルの限界（目）  
我々世代の価値（雪かき、草刈り）  
[@dada_ism](https://twitter.com/dada_ism)が寒そう  
ある人の人生を定点観測する価値  
---  
template: BlogPost  
path: /86
date: 2021-05-06T06:15:50.738Z  
title: ep86:品質の良し悪しは自分が決めてはいけない
thumbnail: /assets/ep86.png
metaDescription:  
---  
![ep86](/assets/ep86.png)  

<iframe src="https://open.spotify.com/embed/episode/1m7LFP1cQMMwSKIIXLPtJs" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  


3ヶ月健診行ってきた [@dada_ism](https://twitter.com/dada_ism)  
GWはじまった [@naoharu](https://twitter.com/naoharu)   
まちなかでゲーム持って歩いてる人多い  
緊急事態宣言になって安全そうな公営の公園が休みになっている  
Jeepは社内でキャンプできる？  
フィードバック紹介  
[「スクラムマスターってかなり潤滑油な気がするけど違うのかな？」](https://twitter.com/nabe_merchant/status/1387050243632504850)  
あえて衝突を早めに起こして顕在しておく  
何で問題を解決するか  
外部の望むスピードや成果が出ない事が多い  
[ユニコーン企業のひみつ ―Spotifyで学んだソフトウェアづくりと働き方](https://amzn.to/3EscLep)  
どうやったらチームをエンパワーできるのか  
ぬるぬる＋Tec×5年後＝デロイト執行役員  
[デロイトトーマツ](https://www2.deloitte.com/jp/ja.html)  
スタートダッシュ＝高度なハッタリテクニック  
jamming.fmは一旦公開したエピソード取り下げ禁止  
やっぱり消していいことにする  
社内SNSのアイコンについて  
動物をアイコンにする人  
海外の人は宣材写真みたいなフォーマル写真  
アメリカの人はどのツールでもちゃんとした写真を入れる慣例がある  
[イコン](https://ja.wikipedia.org/wiki/%E3%82%A4%E3%82%B3%E3%83%B3)  
アイコン変えすぎると認知しづらくなる  
自分が専門的なことを話さないと価値がでないのでは？という呪いについて  
話すネタを結構消している  
自分だけが知っている、専門性があるってことじゃないと自分じゃなくてもいいのではとなってしまう  
いつも自分自身がスクラムマスターとして言っていること  
写真や動画制作でも同じこと思う  
[ハリウッドザコシショウの地獄大連発チャンネル](https://www.youtube.com/channel/UClLAjXeBRVL3q8qYNY5pdVA)  
まず出さないと何もはじまらない  
ヒカキンの初期の動画もかなり雑  
削除はダメ ICE BOXに入れる  
自分の評価が下がるのでは？  
フォロワー増やしたいというのはあってよいが、何をたら増えるかはわからない  
検証できないといけないけどできていない  
世界で誰も聞く人がいなくてもjamming.fmは続けるか  
[YeLL](https://www.yell4u.jp/)    
まったくなりたく無いストッパーになっていることに人類は気付けるのか  
[中国語の部屋](https://ja.wikipedia.org/wiki/%E4%B8%AD%E5%9B%BD%E8%AA%9E%E3%81%AE%E9%83%A8%E5%B1%8B)  
最初からチーム内にいるといいレビューができる  
[権威勾配](https://team.hatenablog.jp/entry/2015/01/05/094543)  
あえて回答しやすい質問を投げる  
[マクドナルド理論](https://gigazine.net/news/20130502-mcdonalds-theory/)  
ワクチン2回目は副作用けっこうあるらしい  
第2回 Clubhouse回やりたい  
---  
template: BlogPost  
path: /87
date: 2021-05-14T06:15:50.738Z  
title: ep87:スキルとバリュー
thumbnail: /assets/ep87.png
metaDescription:  
---  
![ep87](/assets/ep87.png)  

<iframe src="https://open.spotify.com/embed/episode/1CASRvdAaOAhS58k205cBt" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***  
  
  
やけた [@naoharu](https://twitter.com/naoharu)   
ものをしまう場所がわからない [@dada_ism](https://twitter.com/dada_ism)  
保育園の先生は情報共有力が高い  
[コドモン](https://www.codmon.com/)  
10割蕎麦    
大豆と枝豆  
マッドフラッド  
[Leduo My World](https://item.rakuten.co.jp/pegasuswings/leduo-76010/)  
[シグマ 30mm F1.4 EX DC デジタル専用 HSM キヤノン用](https://amzn.to/2ZAvbdT)  
[Toshihiro Setojima](https://www.linkedin.com/in/toshihirosetojima/)  
[2021 Japan APN Ambassadors / 2021 APN AWS Top Engineers の発表](https://aws.amazon.com/jp/blogs/psa/apn-engineers-award-2021/)  
この仕事で学ぶことを最初に書く  ---  
template: BlogPost  
path: /88
date: 2021-05-21T06:15:50.738Z  
title: ep88:自己憐憫ワード
thumbnail: /assets/ep88.png
metaDescription:  
---  
![ep88](/assets/ep88.png)  


<iframe src="https://open.spotify.com/embed/episode/4n2pzdWkdcFej3hksRIet4" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  


冷やし中華はじめました [@naoharu](https://twitter.com/naoharu)  
野菜を食べる感じ  
冷やし中華あまり食べない [@dada_ism](https://twitter.com/dada_ism)  
冷麺のお酢  
Twitter spacesで生配信中  
結局レゴを買った  
[ep87でレゴ買うか迷った](https://jamming.fm/87)  
レゴ互換製品はやだな〜  
テクニカルインシデント発生  
メモ帳でプログラミングすればいいじゃん  
スペックには現れない気持ちよさ  
[レゴ (LEGO) クラシック 黄色のアイデアボックス スペシャル](https://amzn.to/3Etr7Lq)  
[スピークバディ](https://speakbuddy.jp/)  
「シャイでもできる」  
実戦で使い物にならない恐れ  
AIとしか英会話できな人ができあがるのでは  
[人工無能](https://ja.wikipedia.org/wiki/%E4%BA%BA%E5%B7%A5%E7%84%A1%E8%84%B3)  
[Amazon polly](https://aws.amazon.com/jp/polly/)  
各国のイントネーション  
インポルタント  
自己憐憫ワードがあることに気づいた  
[自己憐憫とは](https://www.weblio.jp/content/%E8%87%AA%E5%B7%B1%E6%86%90%E6%86%AB)  
自虐ではく、自分かわいそう  
対立構造を生む  
[トーンポリシング](https://ja.wikipedia.org/wiki/%E3%83%88%E3%83%BC%E3%83%B3%E3%83%BB%E3%83%9D%E3%83%AA%E3%82%B7%E3%83%B3%E3%82%B0)  
フェミニストにボコボコにされるのでは  
ワンオペ育児  
Twitterで勝手に出てくる自己憐憫ワード  
あーこういうウォーターフォールあるあるですね〜  
自己肯定したいためによくやっちゃうんだけども  
ヌルヌル度高め  
リスナーが0人でもjamming.fmやるか  
リスナーのためにやっているのか？  
デトックスになっている  
単にふたりで雑談するというのは違う  
標準出力ではなくファイルに保存  
思考のシリアライズ  
途中の状態をどう保存するか  
脳のメモリは揮発する  
人生の記録はあまりしない  
その時の感情とその時の言葉でふりかえれる  
[クラスメソッド](https://classmethod.jp/)  
歴史修正主義  
終りが見えない jamming.fm  
最後は自動化したい  
8万年後の世界  
[ボイジャー１号２号](https://www.businessinsider.jp/post-234690)  
動き続けるインスタンス  
アンドロメダリージョン マルチAZ  
[ムーンショット計画](https://www8.cao.go.jp/cstp/moonshot/sub1.html)  
[マトリックス](https://ja.wikipedia.org/wiki/%E3%83%9E%E3%83%88%E3%83%AA%E3%83%83%E3%82%AF%E3%82%B9_(%E6%98%A0%E7%94%BB))  
[サイファー](https://matrix.fandom.com/ja/wiki/%E3%82%B5%E3%82%A4%E3%83%95%E3%82%A1%E3%83%BC)  
　---  
template: BlogPost  
path: /89
date: 2021-05-27T06:15:50.738Z  
title: ep89:やりたいからやる（ゲスト：フラットアーサー中村浩三さん）
thumbnail: /assets/ep89.png
metaDescription:  
---  
![ep89](/assets/ep89.png)  


<iframe src="https://open.spotify.com/embed/episode/4rLfiKfb7hEmCa5XBo4a2J" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  



ボールアーサーの [@dada_ism](https://twitter.com/dada_ism)  
暫定ボールアーサーの [@naoharu](https://twitter.com/naoharu)  
趣味でフラットアーサーをやっている [@kozonakamuratv](https://twitter.com/kozonakamuratv)  
[地球平面説【フラットアース】の世界](https://amzn.to/3jNXIDO)  
[【フラットアース】超入門](https://amzn.to/3CqYXAl)  
映えそうなテーマなら何でもいい  
フラットアースは再生数が伸びる  
中村浩三はビジネスフラットアーサー？  
[YouTube講演家 鴨頭嘉人 公式HP（かもがしら よしひと）](https://kamogashira.com/)  
市議会議員の4年間  
1日1動画  

<iframe width="560" height="315" src="https://www.youtube.com/embed/uec4lJtaBsY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

宝くじが当たっても今の仕事をするか  
好きでやっていることは咎められない  
計画経済とか共産主義国家なのでは？  
OSSやwikipedia  
肩書きとしてのマネージャー不要説  
みんなが宝くじ当たったとしたらやりたくない仕事は何か？  
「あなたがやりたいことはそれですか？」  
フラットーアーサーを論破したいボールーアーサー何なの  

---  
template: BlogPost  
path: /90
date: 2021-05-31T06:15:50.738Z  
title: ep90:jamming.fmのおもしろさ（ゲスト：hymxさん）
thumbnail: /assets/ep90.png
metaDescription:  
---  
![ep90](/assets/ep90.png)  


<iframe src="https://open.spotify.com/embed/episode/5brnBU5urai9vxU9EBCO0O" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  


お茶の消費量がすごい[@naoharu](https://twitter.com/naoharu)  
自主隔離中の[@dada_ism](https://twitter.com/dada_ism)  
家事代行サービスに興味があるjamming.fmリスナー代表[@hymx](https://twitter.com/hymx)  
汐留のオフィス  
hymaxさんのjamming.fmを聞くスタイル  
jamming.fmリスナーが語るjamming.fmのここが良い  
考え方が変化する  
[ep14:持ち家 vs 賃貸](https://jamming.fm/14)  
[ep59:家買いました](https://jamming.fm/59)  
音質が良い  
[レコーダー ZOOM H5](https://zoomcorp.com/ja/jp/handheld-recorders/handheld-recorders/h5/)  
掛け合いがおもしろい  
[グルテンフリーってなに？](https://jamming.fm/87)  
お悩みの相談のコーナー　横浜市 hymaxさんより  
子供ができてから自分の時間の時間がなくなった  
どうやって自分の時間を作っているのか？どうやって家族をどうやって説得しているのか？  
寝る時間を削る　睡眠時間は5,6時間  
[Udemy](https://www.udemy.com/)  
長期スパンで人生や仕事のマイルストーンを共有している  
どっちがパーソナリティなのかわからなくなってきたぞ  
[コメダ珈琲店](http://www.komeda.co.jp/)  
キッズウェルカムのコメダ  
1年間で36回 ほぼ毎週コメダに行く生活  
[小倉トースト](https://www.komeda.co.jp/menu/morning.html)  
コーヒーチェーンランキングに3位くらいに食い込んでる  
[アイスコーヒー　たっぷりサイズ](http://www.komeda.co.jp/menu/coffee.html)  
40億年後にミルキーウェイ銀河とアンドロメダ銀河が衝突する  
コーヒーフレッシュ銀河  
[ミルコメダ銀河](https://ja.wikipedia.org/wiki/%E9%8A%80%E6%B2%B3%E7%B3%BB%E3%81%A8%E3%82%A2%E3%83%B3%E3%83%89%E3%83%AD%E3%83%A1%E3%83%80%E9%8A%80%E6%B2%B3%E3%81%AE%E8%A1%9D%E7%AA%81%E5%90%88%E4%BD%93#cite_note-15)  
子供と銭湯  
娘はお風呂はいるときと寝る前のベッドの上でよく話してくれる  
女の子が男湯に入れるのは幼稚園まで  
銭湯にいるおっちゃんとの交流がいい  
台風の日に露天風呂は最高  
[ヒンナ](https://twitter.com/naoharu/status/1395352145646419974)  
[ゴールデンカムイ](https://youngjump.jp/goldenkamuy/)  
レバ刺し  
何いってんだこいつ  
[肉の田じま](https://www.nikunotajima.com/)  
精肉店の良さ  
コロナになって大学からするスポーツが人が集まらない  
186cmあるので横浜国立大学の入試でアメフト部に勧誘された  
結果的にバレーボールサークルに入部  
[スラムダンク 田岡茂一](https://dic.pixiv.net/a/%E7%94%B0%E5%B2%A1%E8%8C%82%E4%B8%80)  
身長だけじゃダメだった  
dada_ismは中学高校大学バレーボールやってた  
これでメモは終わりになります  
hymaxさん次回マンショントークで2本いける  ---  
template: BlogPost  
path: /91
date: 2021-06-03T06:15:50.738Z  
title: ep91:セカンドペンギン
thumbnail: /assets/ep91.png
metaDescription:  
---  
![ep91](/assets/ep91.png)  


<iframe src="https://open.spotify.com/embed/episode/4w4okLjNYA0Sk5o1L2zmTG" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***  



アカスリで死にかけた[@naoharu](https://twitter.com/naoharu)  
[スパ三日月](http://www.mikazuki.co.jp/ryugu/pool_spa/)  
[木更津 倉庫店](https://www.costco.co.jp/store-finder/Kisarazu)  
[紙やすり番手](https://www.monotaro.com/s/pages/productinfo/sandpaper_howtochoose/)  

<img src="/assets/binbo.jpg"/>   

翌日健康診断の[@dada_ism](https://twitter.com/dada_ism)  
胃カメラがおすすめ  
何か変革を起こしたいときに率先して変えようとしたらウザがられる  
変わったってことをどう体験するか  
[TED2010 社会運動はどうやって起こすか](https://www.ted.com/talks/derek_sivers_how_to_start_a_movement?language=ja)  
動機の認知  
各メンバーのベクトルの内積  
セカンドペンギンは評価されない？  
世に言われる「自分から行動を起こせ！」とどうリコンサイルするか？  
セカンドペンギンというリーダーシップは存在するのでは  
スマートホーム計画玄関編  
ハンズフリー解錠  
[nature remo](https://nature.global/)  
[Qrio Lock](https://qrio.me/smartlock/)  
おもちゃみたいな感じでイジるのを楽しむ大切さ  
手段を持っていないと見えてこない目的ってあるのでは  
スマートデバイスが不動産にビルドインされない理由  
[アップル、「Podcastサブスクリプションサービス」の開始を6月に延期との報道](https://japan.cnet.com/article/35171534/)  
[@naoharu](https://twitter.com/naoharu)  の転職話を有料限定でやるのは何故嫌なのか  
どういうプラットフォームでこういう話をすればいいのか問題  

---  
template: BlogPost  
path: /92
date: 2021-06-12T06:15:50.738Z  
title: ep92:稼働率の違和感
thumbnail: /assets/ep92.png
metaDescription:  
---  
![ep92](/assets/ep92.png)  

<iframe src="https://open.spotify.com/embed/episode/2CKwHaaoDBmJkXxa1tNLSD" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


東京都北区7月から30代もワクチン接種開始 [@dada_ism](https://twitter.com/dada_ism)  
星の王子 ニューヨークに行く2 を見た [@naoharu](https://twitter.com/naoharu)  
[星の王子 ニューヨークに行く2](https://eiga.com/movie/94182/)    
[星の王子さま](https://amzn.to/3BnYUUp)    
星の王子 ニューヨークに行くのあらすじ  
エディ・マーフィ演じるアキーム王子  
何も自分でやらずに育ってきた  
[ニューヨークのクイーンズ区](https://ja.wikipedia.org/wiki/%E3%82%AF%E3%82%A4%E3%83%BC%E3%83%B3%E3%82%BA%E5%8C%BA)    
テレワークと育児の両立ができない  
子どもが保育園で発熱した  
解熱後24時間は登園できないルールある  
有給すぐなくなってしまうのでは  
[共働き家庭はどうしてんの？](https://twitter.com/dada_ism/status/1402765118975348738?s=20)    
病児保育・ジジババ保育・テレワーク  
テレワークと育児両方無理かも  
無理って誰が決めてるの？  
テレワークがなかったら育児と仕事の両立ができない  
ものすごい斜に構えてる人がいる  
物事を素直に捉えれない人  
自己憐憫に近い  
愚痴  
クビにしろ！  
それを言うことにメリットがあるから言う  
自分が傷つかないための精神安定のため  
政権批判なども同じなのでは  
斜に構えてないでなにかアクションしろよ  
愚痴を言うなって言ったことがある  
クビにしろ！（2回目）  
愚痴を言う人と一緒にいる人が愚痴っぽくなる  
[Google Cloud Natural Language エンティティ感情分析 API](https://cloud.google.com/natural-language/docs/analyzing-entity-sentiment?hl=ja)  
斜に構えるのと良い点を見つけるのを交互にやる  
稼働率という違和感　
単価に対してどれほどの外貨を獲得したか＝稼働率  
アベイラブル  
お茶おひく  
稼働率を埋めるために仕事をすること  
稼働率＝価値ではない  
作れば作るほど価値が出る時代ではない  
ランチビュッフェの食べ放題でどれだけ多く食べれるか  
体験の価値を最大化するのが本来も目的なはず  
稼働率は優しい世界  
管理会計上の指標  
資産性のあるものを作ってはいけない  
高速道路は渋滞して方が得する？  
[ep52 1石N鳥](https://jamming.fm/52)  
[MVP](https://www.sansokan.jp/akinai/faq/detail.san?H_FAQ_CL=0&H_FAQ_NO=1468)  
いかに効率よく変更容易性高める  
部品を先に作ると価値が最初に出てこない  
技術的負債になる  
捨てれるか？  
価値の最大化をどこにもってくるか？  
内省できるか  
エピソードのタイトルどうするか  
自分の中で悩んでると熱くなってしまう  
　　---  
template: BlogPost  
path: /93
date: 2021-06-16T06:15:50.738Z  
title: ep93:野村総合研究所を退職しました
thumbnail: /assets/ep93.png
metaDescription:  
---  
![ep93](/assets/ep93.png)  

<iframe src="https://open.spotify.com/embed/episode/32w7fozBhBHQDQkfhUMtL2" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

今日はメインMC一人の [@dada_ism](https://twitter.com/dada_ism)  
ビャンビャン麺を昼に食べた [@naoharu](https://twitter.com/naoharu)  
今日は突っ込んで聞いてみる  
大学院卒業してNRIに入社し今にいたる（完）  
新卒時代にNRIを選んだ理由  
NRIにネガティブな印象は「ほとんど」無い  
その転職を意識したのはどういう理由か  
本部が違えば違う会社  
いまいちワンチームになれない瞬間  
プチ受発注関係  
会社全体の文化として誠実というのはあるのでは  
転職活動を始めたキッカケ  
社内の昇格面接の資料をコピってLinkedInにペースト  
ぬるぬるはパッシブスキル  
テンション上がった企業 AWS RedHat...  
Candidate Experience  
転職活動って改めて自分の価値観を見つめ直す機会になる  
コロナ禍での転職って良いの悪いの  
ぬるぬるの市場価値  
35才SIer勤務の人ってどう見られるのか  
[GitLab](https://about.gitlab.com/)  
[Senior Solutions Architect](https://about.gitlab.com/job-families/sales/solutions-architect/)  
[1200人以上の全社員がリモートワーク。GitLabが公開する「リモートワークマニフェスト」は何を教えているか？](https://www.publickey1.jp/blog/20/120066gitlab.html)  
[GitLab.comが操作ミスで本番データベース喪失。5つあったはずのバックアップ手段は役立たず、頼みの綱は6時間前に偶然取ったスナップショット](https://www.publickey1.jp/blog/17/gitlabcom56.html)  
今の俺からすると非連続っぽい変化  
「俺のことどうやって評価するんですか？」「評価しないけど？」  
naoharuって英語できるの？  
島根のインターネット回線状況  
[GitLabは何にいくら経費使っていい（どんくらい会社から払い戻される）か公開している](https://about.gitlab.com/handbook/finance/expenses/)  
入社後にもいろんな驚きを紹介してほしい  
全ての会社がソフトウェアカンパニーになる  
[「すべての企業がソフトウエア会社に」、マイクロソフトのナデラCEOが語る](https://xtech.nikkei.com/atcl/nxt/news/18/03240/)  
今まで転職活動を恐れていたのは何を恐れていたのか  
嫁ブロック  
メンバーシップ型雇用とジョブ型雇用  
クビになったらもっかいおなしゃす  
転職とは？  



---
template: BlogPost
path: /94
date: 2021-07-15T06:15:50.738Z
title: ep94:GitLabに入社しました
thumbnail: /assets/ep94.png
metaDescription:
---
![ep94](/assets/ep94.png)

<iframe src="https://open.spotify.com/embed/episode/6eolJaORAYZ51XcjZYrAZk" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

収録久々で戸惑っている [@naoharu](https://twitter.com/naoharu)  
ほぼ１ヶ月ぶり  
今日はメインMC一人の [@dada_ism](https://twitter.com/dada_ism)  
[最近YouTubeはじめてみた](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  
[GitLabに入社した](https://about.gitlab.com/)  
[Handbook | GitLab](https://about.gitlab.com/handbook/)  
[Welcome to your GitLab Onboarding Issue! 🎉](https://gitlab.com/gitlab-com/people-group/people-operations/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding.md)  
[It's OK to look away](https://about.gitlab.com/company/culture/all-remote/meetings/#its-ok-to-look-away)  
会議中に自分が関係ないと感じたら内職OK  
会議に呼ぶ開催者が参加者に期待することを明確にする文化  
Face to Face は手段  
[CEOが「自分達のことをタヌキって呼ぶのはどう？」って提案が却下される](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/24447)  
どういう結果になるかよりどう決まってきたかのほうが大事なのでは  
「なんか言ってる人」VS「自分達」  
自分達で見つけた課題を自分達で解決するプロセスが大事  
一回話したトピックをもう一回話そうとするポンコツ[@naoharu](https://twitter.com/naoharu)  
[jamming.fmチャンネル](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  
---
template: BlogPost
path: /amplify-to-netlify
date: 2021-07-17T06:15:50.738Z
title: AWS Amplify+AWS CodeCommitで配信しているGatsbyサイトをNetlify+GitLabに引越す
metaDescription:
---

AWS Amplify+AWS CodeCommitで配信しているGatsbyサイトをNetlify+GitLabに引越す際の作業動画をまとめました。  

手順として参考にした情報は下記です。
- [GatsbyとNetlifyで簡単にブログを作成](https://qiita.com/k-penguin-sato/items/7554e5e7e90aa10ae225)
- [【Netlify】カスタムドメインを設定する](https://qiita.com/NaokiIshimura/items/64e060ccc244e38d0c15)


### リポジトリ移行（AWS CodeCommit -> GitLab ）およびnetlifyへのpushトリガでのbuild設定  
<iframe width="560" height="315" src="https://www.youtube.com/embed/TTeGNBXvA94" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  


### カスタムドメインの設定  
<iframe width="560" height="315" src="https://www.youtube.com/embed/DXfuwlTdtck" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    
  

### 編集していないペア作業全体の動画
<iframe width="560" height="315" src="https://www.youtube.com/embed/9wEwweAWWB0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  ---
template: BlogPost
path: /95
date: 2021-07-27T06:15:50.738Z
title: ep95:ポモドーロテクニック
thumbnail: /assets/ep95.png
metaDescription:
---
![ep95](/assets/ep95.png)

<iframe src="https://open.spotify.com/embed/episode/5Bg2kCDydSMRtAnc39oZop" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

今日は1人でお届けする [@naoharu](https://twitter.com/naoharu)  
フィードバック紹介
- https://twitter.com/tossi_104/status/1417266127994986507
- https://twitter.com/nabe_merchant/status/1416744156966187010
- https://twitter.com/hymx/status/1406504428987768834

転職自動化  
転職エージェントと話すのを定例化して、そこで全部調整してもらうと楽  
「俺ってどんな価値があるんだろう？」を考えるプロ  

ワクチン接種戦略
ワクチンに対する目的関数の変化  
- ３ヶ月前くらい  
  当初はワクチンは少量しかなくて、それをどうあって、あとはどれだけ効率的に打つかみたいなのが目的  
　出社せざるをえない若者とか働き盛りの人に売ったほうがいいのか  
　体力のない、重症化の可能性がたかい高齢者にうったほうがいいのか  
- １ヶ月まえくらい  
　ワクチンはたくさんあって、1個の費用対効果を最大化することより、  
　バンバン打ちまくって面積的なカバレッジを獲得していったほうがいいんじゃないか  
- 今はまた数が少なくなってきている

[ポモドーロテクニック](https://ja.wikipedia.org/wiki/%E3%83%9D%E3%83%A2%E3%83%89%E3%83%BC%E3%83%AD%E3%83%BB%E3%83%86%E3%82%AF%E3%83%8B%E3%83%83%E3%82%AF)  
[Focus To-Do](https://apps.apple.com/jp/app/focus-to-do-%25E3%2583%259D%25E3%2583%25A2%25E3%2583%2589%25E3%2583%25BC%25E3%2583%25AD%25E6%258A%2580%25E8%25A1%2593-%25E3%2582%25BF%25E3%2582%25B9%25E3%2582%25AF%25E7%25AE%25A1%25E7%2590%2586/id966057213?uo=4)  

---
template: BlogPost
path: /colowide
date: 2021-07-29T06:15:50.738Z
title: 【無知の知】かっぱ寿司はま寿司戦争調べたら闇が深かった
metaDescription:
---

かっぱ寿司はま寿司戦争調べたら闇が深かったことについて2人で話しをした結果、  
予想外にブラックっぽいサムシングを見つけてしまいました。

- [「かっぱ寿司」運営会社を警視庁が家宅捜索](https://newspicks.com/news/5991136/)


### かっぱ寿司はま寿司戦争調べたら闇が深かった
<iframe width="560" height="315" src="https://www.youtube.com/embed/ylWdjwU-XO8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
---
template: BlogPost
path: /96
date: 2021-08-08T06:15:50.738Z
title: ep96:育児休暇とったら評価がさがった
thumbnail: /assets/ep96.png
metaDescription:
---
![ep96](/assets/ep96.png)

<iframe src="https://open.spotify.com/embed/episode/0nyjbiCg00zzfUhjBbedDZ" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


CBDグミ食べてる[@dada_ism](https://twitter.com/dada_ism)  
舌下投与で摂取する  
最近早起き[@naoharu](https://twitter.com/naoharu) 
ほぼ気絶してる  
英語が疲れて肩こりやばい  
最近自転車買ったのでサイクリングしてる  
ひと玉4000円の台＝沼  
分割キーボードはじめました   
[BAROCCO MD770](https://archisite.co.jp/products/mistel/barocco-md770/)  
手を前に出さないといけないキーボードは不自然  
打つ単語によって使う指が変わる  
同じキーボードを2台使えば分割キーボードと同じ効能を得れる  
PC「２個キーボードついてる、、、だと？！」  
タオルを総入れ替えした  
[タオルの洗い方](https://www.pfsonline.jp/html/page94.html)  
[Panasonicドラム式洗濯機 IKEUCHI ORGANIC 監修「タオル専用」コース](https://panasonic.jp/wash/products/vx/heatpump/towel.html)  
信頼関係を築くには  
完全無欠？  
それよりも弱みや自己開示  
スクラムで学んだことは、感覚で言うといけない、感情は排除すべき  
そんな私情を伝えられても困るんですけど  
自己開示は善意の押し付けでは  
育児休暇のとった期の評価  
ボーナス×（出勤日数ー欠勤日数）これは納得できる  
では評価が下がった理由は？  
【実績解除】会社でひとに怒る  
【実績解除】上司に怒る  
人事曰く育児休暇をとったから評価が下がることはない  
フルで働いた人よりも育児休暇で休んだ分アウトプット少ないので低い評価になります  
育児休暇とって降格になった人もいる  
育児休暇とると評価が上がる名古屋市職員  
ここは譲れない自分の発火するポイント  
なんでこの人ずっと怒っているな  
@naoharuの原動力  
@dada_ism の母親のキャリア  
優先席を否定するの？  
[奴隷解放宣言](https://ja.wikipedia.org/wiki/奴隷解放宣言)    
[Equality(平等)とEquity(公平)](https://note.com/helixmakimaki/n/n2328e4130d6c)  
制度としてあるけども  
放送に耐えないところはあった（でもだいぶがんばってほとんどカットしてません）  
転んではただでは起きない  
分割キーボードは2倍速で聴いてください  
jamming.fmのドメインが失効した  
.fmドメインは高い　約100ドル  
ドメイン高すぎて刻んでしまう  
このPodcastの経費見える化したほうがいい  ---
template: BlogPost
path: /97
date: 2021-08-12T06:15:50.738Z
title: ep97:日本語で喋りたい
thumbnail: /assets/ep97.png
metaDescription:
---
![ep97](/assets/ep97.png)

<iframe src="https://open.spotify.com/embed/episode/1yQsTetMta6nHHlqI37lhF" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

サキュレータを導入した [@naoharu](https://twitter.com/naoharu)   
オリンピックを見ていない [@dada_ism](https://twitter.com/dada_ism)  
レンタルバイクなのにカスタマイズしている [Toshitaka Ito](https://jp.linkedin.com/in/toshitaka-ito)   
GitLabのToshitaka Ito  
前の会社でもnaoharuのパイセン  
GitLab日本法人  
[Jonathan Lim](https://sg.linkedin.com/in/jonlmr)  
[SAST](https://www.synopsys.com/ja-jp/glossary/what-is-sast.html)  
[NRI北京](https://www.nri.com/jp/company/map/overseas/asia/beijing)  
よりグローバルな環境を求めて転職  
小さいくて一から自分たちで決められる楽しさ  
フルリモートワークだからこそ新しい働き方じゃないと成り立たないという期待  
メールとエクセル文化  
「役職が上になればなるほどメールしか見ない人たちはいなくなれば良いと思っている」  
[jamming.fm](https://jamming.fm) っぽくなってきた！  
ログインが挟まると偉い人のコンバージョンレートが著しく下がる  
「ほんとに見て欲しい議事録は添付ファイルじゃなくてメールのインラインに貼る」ってテクニックが蔓延る  
@dada_ism「転職前の@naoharu、めっちゃそれ大事っていってたけどな」  
日本のSAの人数が倍増した  
正直GitLabもGitHubもちょっと使うくらいなら無料版で全然良いでしょ  
オンプレじゃなくて[gitlab.com](https://gitlab.com)使ってもらえれば良いと思う  
リファラル採用の難しさ  
テクニカル（開発、マネジメント）、会話などちゃんとしてる、ベンチャー、外資  
「英語ほんと大丈夫っすか？」  
それってあなたの感想ですよね  
GitLabのcoffee chat文化  

<iframe width="560" height="315" src="https://www.youtube.com/embed/L9D2S8QIAKk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

日本好きの[Muhamed Huseinbašić](https://www.linkedin.com/in/muhamedhuseinbasic/)  
「お前ちの近くのグーグルマップ見せて」  
[9ヶ月に1回は世界のどこかに全社員集合する](https://about.gitlab.com/events/gitlab-contribute/)  
外資系企業はsalesキックオフで全員集まるとかはあるかも  
想像以上に「60点でいいから話そうぜ」感がある  
beer chat  
DevOpsの盛り上がり、潮目としては来ている  
ウェブ系にキャッチアップする  
クラウドが一般化したのは大きいのでは  
ハードウェアとソフトウェアが融合してくる  
内製化・手の内化  
ファーストキャリアでSIerを選んだのは意外と良かったのでは  









---
template: BlogPost
path: /98
date: 2021-08-25T06:15:50.738Z
title: ep98:在宅勤務＆育児
thumbnail: /assets/ep98.png
metaDescription:
---
![ep98](/assets/ep98.png)

<iframe src="https://open.spotify.com/embed/episode/1Q4AaLMRvNaliiYCoreLXi" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

コストコのビールにハマっている [@naoharu](https://twitter.com/naoharu)   
[シトラホップ セッションIPA](https://mitok.info/?p=213599)  
近くのクラフトビール屋さんに行った [@dada_ism](https://twitter.com/dada_ism)  
[UNDERGROUND赤羽](https://www.instagram.com/underground_craft_beer/)  
[ゴールデンカムイ無料公開](https://tonarinoyj.jp/episode/10834108156629615343)   
在宅勤務で育児と仕事を両方やる  
なかなか大変  
パフォーマンスは1/3で疲れは2倍  
子供が寝てる時の自分の集中力  
[マイクラ](https://www.minecraft.net/ja-jp)    
[貸切風呂](https://www.his-vacation.com/onsen/kashikiri/)  
子供がいると温泉にゆっくり入れない  
[コドモン](https://podcasts.apple.com/no/podcast/158-why-codmon-with-di-cartapesta/id1503383609?i=1000519147847)  
「なんで小学校にはコドモンは無いの？」  
ユーザ側から火をつければ良いじゃん！の逆サイド  
勝者の戦略  
「なんでjamming.fm聞いてないの？」  



---
template: BlogPost
path: /toukenrambumusou
date: 2021-08-25T06:15:50.738Z
title: 【無知の知】「刀剣乱舞無双」を知らない人たちのための動画
metaDescription:
---


###【無知の知】とは  
世の中のあらゆるテーマを取り上げて、知らないことは恥ずかしいという気持ちを捨て、知らないことに対して正直になろうとする2人がそのテーマを深堀りしていくyoutube配信です。  
[YouTubeチャンネルJamming.fm](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  

本日のテーマ:[刀剣乱舞無双](https://touken-musou.com/)

### 【無知の知】「刀剣乱舞無双」を知らない人たちのための動画
<iframe width="560" height="315" src="https://www.youtube.com/embed/xOiFuXxPdbc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
---
template: BlogPost
path: /tsurugakehi
date: 2021-08-28T06:15:50.738Z
title: 【無知の知】敦賀気比ってなに？
metaDescription:
---

### 【無知の知】とは  
世の中のあらゆるテーマを取り上げて、知らないことは恥ずかしいという気持ちを捨て、知らないことに対して正直になろうとする2人がそのテーマを深堀りしていくyoutube配信です。  
[YouTubeチャンネルJamming.fm](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  

本日のテーマ:敦賀気比  
[「監督の責任は重い」敦賀気比サヨナラ負け…東監督がさい配悔いる「選手に悪かった」【夏の甲子園】](https://news.yahoo.co.jp/articles/e23473f99a249a6ee643b7466c7c74eed670f48b)  

### 【無知の知】敦賀気比ってなに？
<iframe width="560" height="315" src="https://www.youtube.com/embed/XKzzzrxjorU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---
template: BlogPost
path: /99
date: 2021-08-30T06:15:50.738Z
title: ep99:家族の幸せ
thumbnail: /assets/ep99.png
metaDescription:
---
![ep99](/assets/ep99.png)

<iframe src="https://open.spotify.com/embed/episode/1opv3y0SwDbrffa8XVwCu3" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

[CICD coference 2021](https://event.cloudnativedays.jp/cicd2021) でQA対応する [@naoharu](https://twitter.com/naoharu)   
特にそんなこともない [@dada_ism](https://twitter.com/dada_ism)  
脳のノイズキャンセリング機能  
[rainymood](https://rainymood.com/)  
[Bose QuietComfort® Earbuds](https://www.bose.co.jp/ja_jp/products/headphones/earbuds/quietcomfort-earbuds.html#v=qc_earbuds_black)  
dadaのチームのふりかえりでも同じことをいっていた  
在宅だと音を気にしてしまう  
会社だと気にならない音が気になる  
どういう時に「俺らのチームは最高だ」って思うか  
個人としての成功と家族としての成功  
俺らって何を大切にしてるんだっけ？  
個人の喜びを家族にむける  
家族の各メンバーの幸せ度  
「今の自分たちってどうなんだろう」って思うのは大事  
他者と比較しがち  
リファラル採用  
自分がこうなりたいって未来像をyoutubeで一緒に見る  
旅行のスケジュールを決めたい  [@dada_ism](https://twitter.com/dada_ism)    
寝坊するdada嫁  
「また来ればいいじゃん」  
うまく計画を立てない  
naoharu の老害化が激しい  
自分が思っていることが言えるか  
意外にヌルヌルあるぞ  



---
template: BlogPost
path: /100
date: 2021-09-04T06:15:50.738Z
title: ep100:100回目
thumbnail: /assets/ep100.png
metaDescription:
---
![ep100](/assets/ep100.png)

<iframe src="https://open.spotify.com/embed/episode/58xc25iLrIkTt233PNbO03" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

雨だったので朝ランしなかった [@naoharu](https://twitter.com/naoharu)  
保育園が閉鎖された [@dada_ism](https://twitter.com/dada_ism)   
100回記念  
「同年代で大企業の中で悶々している人向け」  
自分が悶々としていることを発信すればそのフィードバックがあり課題が深化する  
自分が言ったことで自分が勇気づけられる  
普段合わないような人と話せるゲスト会  
よなよなこさん回  
[ep15:豊かな心がなければ、富は醜い物乞いでしかない。（ゲスト：よなよなこさん）](https://jamming.fm/15/)   
[ep16:マッチングアプリ概論（ゲスト：よなよなこさん）](https://jamming.fm/16/)   
カシオレ君  
ゲストを迎えると自分たちの知らない世界の話題が取り扱える  
jamming.fm がレジュメになる  
[linked.in naoharu](https://www.linkedin.com/in/naoharu-sasaki-8b578783/)   
[石の上にも三年](https://dictionary.goo.ne.jp/word/%E7%9F%B3%E3%81%AE%E4%B8%8A%E3%81%AB%E3%82%82%E4%B8%89%E5%B9%B4/)  
[@dada_ism](https://twitter.com/dada_ism) はjamming.fmやりながら家族構成が変わった  
話題も変わってきた  
一本締め(sync point)  
DINKSはいいよな  
[weworkビール](https://www.softbank.jp/sbnews/entry/20180416_01)  
コロナ禍で失われる3rd place  
仕事、家族、それ以外の場所  
jamming.fmの未来でやりたいこと  
[AWS polly](https://aws.amazon.com/jp/polly/)  
[フラットアーサー回](https://soundcloud.com/jammingfm/ep89)  
[無知の知](https://www.youtube.com/watch?v=6YCjxASZFL8&list=PLsZOYv5KsFqxnft_zOd7IyZqGMlSs49nW)   
日本のボイスメディア事情  
200回目は無くはない  
[タダしいyouに見える](https://tdkdx.com/)  
マイクロコンテンツ  
200回目にはティックトッカーになってるのでは  
[Inhyeok Y.](https://www.linkedin.com/in/inhyeok-yeo/)  


---
template: BlogPost
path: /vsdamashii
date: 2021-09-07T06:15:50.738Z
title: 【無知の知】VS魂ってなに？
metaDescription:
---

### 【無知の知】とは  
世の中のあらゆるテーマを取り上げて、知らないことは恥ずかしいという気持ちを捨て、知らないことに対して正直になろうとする2人がそのテーマを深堀りしていくyoutube配信です。  
[YouTubeチャンネルJamming.fm](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  

本日のテーマ:[VS魂](https://www.fujitv.co.jp/vs_damashii/)  

### 【無知の知】VS魂ってなに？
<iframe width="560" height="315" src="https://www.youtube.com/embed/9RA6wqReG1s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
---
template: BlogPost
path: /101
date: 2021-09-13T06:15:50.738Z
title: ep101:1on1ミーティング
thumbnail: /assets/ep101.png
metaDescription:
---
![ep101](/assets/ep101.png)

<iframe src="https://open.spotify.com/embed/episode/1QXdpC3wX5gW5Vhu0bj2ne" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***

anytime fitness赤羽北店模様替え [@dada_ism](https://twitter.com/dada_ism)   
資格試験たくさん受けてる [@naoharu](https://twitter.com/naoharu)  
[CKAD](https://training.linuxfoundation.org/ja/certification/certified-kubernetes-application-developer-ckad/)  
[ボイラー技師](https://www.exam.or.jp/exmn/H_shikaku122.htm)  
[年100万円まで研修や学習に経費精算できるGitLab](https://about.gitlab.com/handbook/finance/expenses/#-work-related-online-courses-and-professional-development-certifications)  
[プラットフォーム](https://www.netflix.com/jp-en/title/81128579)  

<iframe width="560" height="315" src="https://www.youtube.com/embed/pLO6udXU648" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[cube](https://movies.shochiku.co.jp/cube/)  
[saw](https://ja.wikipedia.org/wiki/%E3%82%BD%E3%82%A6_(%E6%98%A0%E7%94%BB))  
低予算映画  
作品の冪等性  
GitLabのマネジメントスタイル  
naohaurの上司[adrian](https://www.linkedin.com/in/adriansmolski)  
1on1  
評価のすれ違いをなくす  
githubは本名でやったほうがいいか  

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">日本では、SNS上にアカウントを作る際に、本名ではなく「ハンドルネーム」を使う人が多い。それを踏襲して、エンジニアがGitHubにアカウントを作る際にまでハンドルネームを使うのは大きな間違い。GitHub上の活動は、エンジニアにとって履歴書であり財産。 採用の際に重要な役割を果たします。</p>&mdash; Satoshi Nakajima (@snakajima) <a href="https://twitter.com/snakajima/status/1435754177708249089?ref_src=twsrc%5Etfw">September 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>  

自分のキャリアをインターネットに晒していくか論  
[きゅんぽっぽい名前](https://dic.nicovideo.jp/a/%E9%99%90%E7%95%8C%E3%82%AA%E3%82%BF%E3%82%AF)  
今後は個人商店感もひろがってくるのでは  
  
---
template: BlogPost
path: /yokohamaryusei
date: 2021-09-18T06:15:50.738Z
title: 【無知の知】横浜流星さんの名字の秘密
metaDescription:
---

### 【無知の知】とは  
世の中のあらゆるテーマを取り上げて、知らないことは恥ずかしいという気持ちを捨て、知らないことに対して正直になろうとする2人がそのテーマを深堀りしていくyoutube配信です。  
[YouTubeチャンネルJamming.fm](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  

本日のテーマ:[横浜流星](https://official.stardust.co.jp/yokohamaryusei/)  
横浜流星さんの名字の秘密から日本の名字の秘密に迫ります。

### 【無知の知】横浜流星さんの名字の秘密
<iframe width="560" height="315" src="https://www.youtube.com/embed/_akcfjZabCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>---
template: BlogPost
path: /102
date: 2021-09-20T06:15:50.738Z
title: ep102:母さんこの味どうかしら
thumbnail: /assets/ep102.png
metaDescription:
---
![ep102](/assets/ep102.png)

<iframe src="https://open.spotify.com/embed/episode/5Ij2eWLbdBa8LtSUH6PNpO" width="100%" height="232" frameBorder="0" allowtransparency="true" allow="encrypted-media"></iframe>

***

1エピソード1トピック制にしてみた  
妻の誕生日に料理を振る舞ったけど失敗した[@dada_ism](https://twitter.com/dada_ism)   
参考にした料理動画がビーガンの人だった  
卵を使っていないことに違和感を持てなかった  
「なんでそれがわからないかわからない」のに自分がなった版  
お菓子って絶対ビッグバンリリースにならない？  
オーブンでブレーカー落ちた  
移行リハしろ  
子供の満足度と親の頑張り度は関係無い  
[モンテローザ系列](https://www.monteroza.co.jp/) での[@dada_ism](https://twitter.com/dada_ism) のバイト経験  
食材、手順を同じにしても、同じ味にならない謎  

<iframe width="560" height="315" src="https://www.youtube.com/embed/mhJ1PVwZFg8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
  
<iframe width="560" height="315" src="https://www.youtube.com/embed/3wZl67j1bMc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

細かくイテレーションを回しても「味わかんねーわ」  
料理って結果のフィードバックはあるけど制作工程へのフィードバックって無いがち  
[ABC Cocking](https://www.abc-cooking.co.jp/)  
料理のセンス＝運動神経？  
バスケのドリブルでボールを蹴っちゃう   
母さんこの味どうかしら  

---
template: BlogPost
path: /103
date: 2021-10-02T06:15:50.738Z
title: ep103:詳細設計書
thumbnail: /assets/ep103.png
metaDescription:
---
![ep103](/assets/ep103.png)

<iframe src="https://open.spotify.com/embed/episode/6t6AWD2D9CwKfhZpwtkje2" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

SIer仕草  
ソースコードと一対一対応するような詳細設計書  
[GitHub co-pilot](https://copilot.github.com/)  
[IntelliJ IDEA ](https://www.jetbrains.com/ja-jp/idea/)  
オフショア開発  
[Gitpod](https://www.gitpod.io/)  
IDEの恩恵を受けて設計をする  
.sh って新規でも使う？  
[「GitHub Copilot」はAPIの秘密キーまで知っている？ 優秀過ぎるAI（相）棒に恐れを抱く開発者も](https://forest.watch.impress.co.jp/docs/serial/yajiuma/1336988.html)  
[GitHub Copilotとライセンス問題](https://kotamorishita.com/github-copilot-copyright-issue/)  
[質問箱](https://peing.net/ja/)  
---
template: BlogPost
path: /104
date: 2021-10-04T06:15:50.738Z
title: ep104:Best Buy 昇降デスク
thumbnail: /assets/ep104.png
metaDescription:
---
![ep103](/assets/ep104.png)

<iframe src="https://open.spotify.com/embed/episode/5NunBADcXH1B5kmvuesVNN" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


昇降デスク用の足を買った  
[Herman Miller アーロンチェア](https://www.hermanmiller.com/ja_jp/products/seating/office-chairs/aeron-chairs/)  
使ってみてわかる机の高さが帰らっれるよさ  
自分にフィットした机の高さ  
立ち位置にもいろいろある  
揺れるトマック（ポーランド出身 オーストラリア在住）  
昇降デスクにもいろいろなモデルがある  
FLEXISOPOT 昇降デスク脚   
dadaism購入 [FLEXISPOT EJ2](https://amzn.to/3Bto97T)  
より昇降幅のある [FLEXISPOT E7](https://amzn.to/3EmC7dq)  
障害物検知  
昇降幅  
昇降デスクの耐荷重  
机は重いと安定する  
[GitLab Expenses - Height-adjustable desk](https://about.gitlab.com/handbook/finance/expenses/)  
来月引っ越す  
[NTT リモートワーク](https://www.itmedia.co.jp/news/articles/2109/29/news091.html)  
[LINE リモートワーク](https://linecorp.com/ja/pr/news/ja/2021/3912)  
[メルカリ リモートワーク](https://about.mercari.com/press/news/articles/20210901_yourchoice/)  
[【論文掲載】座っている時間が長いほど死亡リスクが増加する ~その効果は、余暇時間の運動活動量を増やしても、完全に抑制されない~](https://www.kpu-m.ac.jp/doc/news/2021/20210625.html)  
立ち続けるのも病気になる  
[NASA認証](https://ameblo.jp/setoworks/entry-12681949078.html)  
NASAで使われることはすごい  
[宮内庁御用達](https://ja.wikipedia.org/wiki/%E5%BE%A1%E7%94%A8%E9%81%94)  
[スクラムアライアンス](https://www.scrumalliance.org/)  
jamming.fm certified  
jamming.fmはポッドキャストプラットフォームを引っ越し巻いた  
[Anchor jamming.fm](https://anchor.fm/jammingfm)  
[SoundCloud限定トラック](https://soundcloud.com/jammingfm/jammingfm)  
リスナー 男性 95%  
若い人が多い  
---
template: BlogPost
path: /105
date: 2021-10-09T06:15:50.738Z
title: ep105:どれが正解？
thumbnail: /assets/ep105.png
metaDescription:
---
![ep105](/assets/ep105.png)

<iframe src="https://open.spotify.com/embed/episode/6wHg24BxKQ2EjMy17p4m99" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

[M1 MacでDockerを使った開発環境構築にハマった](https://fixel.co.jp/blog/docker-m1mac/)  
[ランボー3/怒りのアフガン](https://ja.wikipedia.org/wiki/%E3%83%A9%E3%83%B3%E3%83%9C%E3%83%BC3/%E6%80%92%E3%82%8A%E3%81%AE%E3%82%A2%E3%83%95%E3%82%AC%E3%83%B3)  
[udemy](https://www.udemy.com/)  
[認定Kubernetes管理者（CKA）試験](https://training.linuxfoundation.org/ja/certification/certified-kubernetes-administrator-cka/)  
[伝説的なCKA対策コース](https://www.udemy.com/course/certified-kubernetes-administrator-with-practice-tests/)  
[怪しいCKA問題集](https://www.udemy.com/course/certified-kubernetes-administrator-cka-2020-mock-exams/)  
1行になったyamlを選ばされる  

***

![ckamock](/assets/ckamock.jpeg)

***

[yaml](https://ja.wikipedia.org/wiki/YAML)  
[コースの返金方法](https://support.udemy.com/hc/ja/articles/229604248-%E3%82%B3%E3%83%BC%E3%82%B9%E3%81%AE%E8%BF%94%E9%87%91%E6%96%B9%E6%B3%95)  
[各社が公開した新人エンジニア向けの研修資料が話題に　人気資料まとめ](https://www.itmedia.co.jp/news/articles/2107/30/news111.html)  
[Scrum Alliance®公認クラス認定スクラムマスター](https://www.abi-agile.com/%E8%AA%8D%E5%AE%9A%E3%82%B9%E3%82%AF%E3%83%A9%E3%83%A0%E3%83%9E%E3%82%B9%E3%82%BF%E3%83%BC/)    
[Kubernetesがクラウド界の「Linux」と呼ばれる2つの理由](https://atmarkit.itmedia.co.jp/ait/articles/1904/08/news014.html)  
k8s学習コスト高い  
みんなで学習するプラットフォーム  
動画は差し替えが難しい  
[EdCast](https://www.edcast.com/)  
[ベクター画像 - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%99%E3%82%AF%E3%82%BF%E3%83%BC%E7%94%BB%E5%83%8F)  
naoharuが落ちた  

---
template: BlogPost
path: /106
date: 2021-10-12T06:15:50.738Z
title: ep106:支配的なマネジメントとリモートワーク
thumbnail: /assets/ep106.png
metaDescription:
---
![ep106](/assets/ep106.png)

<iframe src="https://open.spotify.com/embed/episode/3tL7rnuXR8vAu03Ff4LXhX" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

2021/10/1 緊急事態宣言＆まん延防止等重点措置解除  
ただしお酒提供は制限あり  
会社も連動して対応が出た  
緊急事態宣言下は在宅勤務週5日OK  
平時は週1回在宅OKだった  
緊急事態宣言解除で週1日出社することになった  
会社の対応は指示しかないくてWhyがない  
Whyの噂が色々流れてくる  
リモートだとマネジメントできない  
逆にコミュニケーションとるようになってマネジメント感高まった  
恐怖で支配するマネジメント  
出社してるから仕事してるかはわからない  
方針を明言しないマネジメント  
ハイコンテクストな会話にしたい  
可視化は不利益になる  
リモートワークは可視化が進む  
[GitLab上場](https://www.sec.gov/Archives/edgar/data/0001653482/000162828021018818/gitlab-sx1.htm)  
ぬるぬる労働組合  
無言のプレッシャーをかけたいマネージャー  
今あるファシリティを有効活用したい経営層  
[WHO YOU ARE](https://amzn.to/3vSU6Fy)  
企業文化の大切さ  
リモートワークが理解できない経営層  
リモートワークへのヘイト  
リモートワークで成り立つ仕事は虚業  
[ブルシット・ジョブ　クソどうでもいい仕事の理論](https://amzn.to/3GwXLxC)  
マスクでしか付き合いのない人たち  
素顔を見たときの衝撃  
見えない部分を都合がいいように補完してしまう  
マスク美人  
仮面舞踏会  
見ちゃいけないものが見えてる  
ポロリしてる  
外的要因で人間の思考も変わる  
マーケティングってそういうものでは  
[KALDI エンゲルヒェン](https://www.kaldi.co.jp/ec/pro/disp/1/4560148209278)  



---
template: BlogPost
path: /107
date: 2021-10-19T06:15:50.738Z
title: ep107:Non-Fungible Token
thumbnail: /assets/ep107.png
metaDescription:
---
![ep107](/assets/ep107.png)

<iframe src="https://open.spotify.com/embed/episode/55ijFXRTUo8rVmYb13sel7?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

[Non-Fungible Token](https://ja.wikipedia.org/wiki/%E9%9D%9E%E4%BB%A3%E6%9B%BF%E6%80%A7%E3%83%88%E3%83%BC%E3%82%AF%E3%83%B3)  
[今話題のNFTアート作品とは？仕組みや作り方と販売・購入方法を詳しく解説【デジタルアート】](https://www.fisco.co.jp/media/crypto/nftart-about/)  
[niboshism](https://www.instagram.com/niboshism/?hl=ja)  


<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/CVMGR9ch4AF/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CVMGR9ch4AF/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;">この投稿をInstagramで見る</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CVMGR9ch4AF/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Daily niboshi(@niboshism)がシェアした投稿</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">朝起きたら、、、、<br><br>凄いことが起きてた。。。。。<br><br>自由研究で描いた、<br>8才息子(<a href="https://twitter.com/ZombieZooArt?ref_src=twsrc%5Etfw">@ZombieZooArt</a> ) <br>の描いたドット絵が、、、<br><br>二次流通でスティーブ青木さん(<a href="https://twitter.com/steveaoki?ref_src=twsrc%5Etfw">@steveaoki</a> )に買われてた。<br><br>しかも1個2ETH×2(約160万円)も。。。。。。。。。😂<a href="https://twitter.com/hashtag/nft%E3%82%A2%E3%83%BC%E3%83%88?src=hash&amp;ref_src=twsrc%5Etfw">#nftアート</a> <a href="https://twitter.com/hashtag/zombiezoo?src=hash&amp;ref_src=twsrc%5Etfw">#zombiezoo</a> <a href="https://t.co/sNfDWlVVId">pic.twitter.com/sNfDWlVVId</a></p>&mdash; Emi (Zombie Zoo Mom) (@emikusano) <a href="https://twitter.com/emikusano/status/1438629531590397955?ref_src=twsrc%5Etfw">September 16, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

[株式会社](https://ja.wikipedia.org/wiki/%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BE)  
[有限責任](https://ja.wikipedia.org/wiki/%E6%9C%89%E9%99%90%E8%B2%AC%E4%BB%BB)  
[京急電鉄](https://www.keikyu.co.jp/)  
[宝くじ](https://www.takarakuji-official.jp/)  
[高額当選が出た場所](https://www.takarakujinet.co.jp/jumbo/lucky.html)  
[競馬](https://sp.jra.jp/)  
[競艇](https://www.boatrace.jp/)  
[競輪](http://keirin.jp/pc/top)  
[Make America Great Again](https://ja.wikipedia.org/wiki/Make_America_Great_Again)  
[地球平面協会](https://ja.wikipedia.org/wiki/%E5%9C%B0%E7%90%83%E5%B9%B3%E9%9D%A2%E5%8D%94%E4%BC%9A)  
---
template: BlogPost
path: /108
date: 2021-10-24T06:15:50.738Z
title: ep108:すべては繰り返す
thumbnail: /assets/ep108.png
metaDescription:
---
![ep108](/assets/ep108.png)

<iframe src="https://open.spotify.com/embed/episode/7j8w3cq8hA6vOwOHYljv9g?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

[出社勤務で気づいたこと](https://jamming.fm/106)  
通勤時間がひとりの時間  
通勤時間で本が消化できる  
[GitLab NASDAQ上場](https://www.publickey1.jp/blog/21/gitlabnasdaq12000.html)  
銀座のお店貸し切りで上場記念パーティ  
[キュンポさん 限界オタク](https://dic.nicovideo.jp/a/%E9%99%90%E7%95%8C%E3%82%AA%E3%82%BF%E3%82%AF)  
疎通確認後は普通の会話できる  
[TCP/IP](https://ja.wikipedia.org/wiki/%E3%82%A4%E3%83%B3%E3%82%BF%E3%83%BC%E3%83%8D%E3%83%83%E3%83%88%E3%83%BB%E3%83%97%E3%83%AD%E3%83%88%E3%82%B3%E3%83%AB%E3%83%BB%E3%82%B9%E3%82%A4%E3%83%BC%E3%83%88)  
リモートワーク企業がリアルで会うとOFF会になる  
これが起こるのはアジア・パシフィック地域のみ  
APAC陰キャ  
[G7首脳の中でぽつん 菅首相の「ディスタンス」に批判と同情](https://mainichi.jp/articles/20210614/k00/00m/010/144000c)  
まずは間合いを計りに行く日本人  
[直前に収録してた動画「Certified Kubernetes Administrator（CKA）の模試を受けてみた」](https://www.youtube.com/watch?v=2rQ6l635_R0)  
[スペシャルチャット](https://spatial.chat/)  
[早稲田大学 世界旅行研究会](https://www.waseda.jp/inst/shinkan/circle/1187)  
コロナで途絶えるもの  
リモートワークを推し進めたのはCEOでもないCOVIDである  
危機によって人は動く  
戻すのは結構ラクかもしれない  
[メタバース](https://ja.wikipedia.org/wiki/%E3%83%A1%E3%82%BF%E3%83%90%E3%83%BC%E3%82%B9)  
[セカンドライフ](https://secondlife.com/?lang=ja-JP)  
[フェイスブックが目指す次世代SNS、メタバースとは](https://jbpress.ismedia.jp/articles/-/67381)  
[ブラックミラー](https://www.netflix.com/jp/title/70264888)  
[ブラックミラー　宇宙船カリスター号](https://www.netflix.com/watch/80131567)  
振り子ではなく螺旋状
強くてニューゲーム
抗うな
振り子のように揺れろ  
[直前に収録してた動画「Certified Kubernetes Administrator（CKA）の模試を受けてみた」](https://www.youtube.com/watch?v=2rQ6l635_R0)  
酩酊模試  
[YouTube jamming.fm チャンネル](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  ---
template: BlogPost
path: /109
date: 2021-10-30T06:15:50.738Z
title: ep109:親子関係
thumbnail: /assets/ep109.png
metaDescription:
---
![ep109](/assets/ep109.png)

<iframe src="https://open.spotify.com/embed/episode/7DlTmWlJz1ATnCMkSnon1P?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

幼稚園児入園面談  
[デジタル庁におけるガバメント・クラウド整備のためのクラウドサービスの提供－令和3年度地方公共団体による先行事業及びデジタル庁WEBサイト構築業務－の公募結果について](https://www.digital.go.jp/posts/zytWmjcS)  
[DX白書2021](https://www.ipa.go.jp/ikc/publish/dx_hakusho.html)  
聞かれること  
志望理由  
幼児期の教育で大切なことは何か  
将来どんな人物に育ってほしいか  
[トキワ荘](https://ja.wikipedia.org/wiki/%E3%83%88%E3%82%AD%E3%83%AF%E8%8D%98)  
[スポーツ少年団](https://www.japan-sports.or.jp/club/tabid66.html)  
[寺子屋](https://ja.wikipedia.org/wiki/%E5%AF%BA%E5%AD%90%E5%B1%8B)  
子供に何の習い事をさせるか問題  
辞めやすさは大事  
サブスク型習い事  
[島根大学](https://www.shimane-u.ac.jp/)  
[最高学府](https://kotobank.jp/word/%E6%9C%80%E9%AB%98%E5%AD%A6%E5%BA%9C-507521)  
[ep108:すべては繰り返す](https://jamming.fm/108)  
---
template: BlogPost
path: /110
date: 2021-11-20T06:15:50.738Z
title: ep110:隣の芝生は青い
thumbnail: /assets/ep110.png
metaDescription:
---
![ep110](/assets/ep110.png)

<iframe src="https://open.spotify.com/embed/episode/16E9Wf0z2k4faS731N9vwB?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

[Solutions Architect](https://www.linkedin.com/jobs/solutions-architect-jobs/?originalSubdomain=jp)  
[AWS Lambda](https://aws.amazon.com/jp/lambda/)  
[AWS Certified Solutions Architect - Professional](https://aws.amazon.com/jp/certification/certified-solutions-architect-professional/)  
[Solutions Architects Handbook](https://about.gitlab.com/handbook/customer-success/solutions-architects/)  
時間軸のコンテキストスイッチが多い  
「夜中にコール鳴らないからむっちゃNRIより楽じゃん！」は甘かった  
耕すフェーズと刈り取るフェーズ  
[隣の芝生は青い](https://kotobank.jp/word/%E9%9A%A3%E3%81%AE%E8%8A%9D%E7%94%9F%E3%81%AF%E9%9D%92%E3%81%84-584301)  
中途で入ってきた人に「入って思ったここの会社のここが変！ってありますか？」って聞いてた  
入ってきた人の意見は貴重  
[Avoid Direct messages](https://about.gitlab.com/handbook/communication/#avoid-direct-messages)  




---
template: BlogPost
path: /111
date: 2021-11-27T06:15:50.738Z
title: ep111:覗き穴から見えたもの
thumbnail: /assets/ep111.png
metaDescription:
---
![ep111](/assets/ep111.png)

<iframe src="https://open.spotify.com/embed/episode/5wNLynzzQKfZ1BsvdP6Vpa?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

[ep109:親子関係](https://jamming.fm/109)   
保育園の参観日  
真珠でマウントする母  
保育園の覗き穴  
３歳児にお前がんばれよ  
首すわりですぐ保育園  
人類は家の中で子育てするようにはできていないのかもしれない  
自分も覗かれてるのではない  
シミュレーション仮説  
[マトリックス　レザレクションズ](https://wwws.warnerbros.co.jp/matrix-movie/)  
何が現代のほころびなのか？  
３次元は少ない  
光は遅い  
この世界の創造主は無課金  
身も蓋もない話になった  


---
template: BlogPost
path: /come-come-everybody
date: 2021-12-06T06:15:50.738Z
title: 【無知の知】カムカムエヴリバディってなに？
metaDescription:
---

### 【無知の知】とは  
世の中のあらゆるテーマを取り上げて、知らないことは恥ずかしいという気持ちを捨て、知らないことに対して正直になろうとする2人がそのテーマを深堀りしていくyoutube配信です。  
[YouTubeチャンネルJamming.fm](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  

本日のテーマ:[カムカムエヴリバディ](https://www.nhk.or.jp/comecome/)  
NHK連続テレビ小説『カムカムエヴリバディ』を知らず、3個のうち1個が超酸っぱいカムカムレモンと勘違いしてしまう！（しかもそれはカムカムレモンじゃなくて「すっぱいレモンにご用心」）  

### 【無知の知】カムカムエヴリバディ
<iframe width="560" height="315" src="https://www.youtube.com/embed/lxjJTF3JZGc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
---
template: BlogPost
path: /112
date: 2021-12-14T06:15:50.738Z
title: ep112:プロダクト開発の悩み
thumbnail: /assets/ep112.png
metaDescription:
---
![ep11２](/assets/ep112.png)

<iframe src="https://open.spotify.com/embed/episode/3fjJwHT5cc5M32AQNiQoBW?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

認定スクラムマスター、認定プロダクトオーナー [@naoharu](https://twitter.com/naoharu)   
事業のプロダクト開発をスクラムでやっている [@dada_ism](https://twitter.com/dada_ism)  
[Nature Remo](https://nature.global/nature-remo/)  
温度と湿度が遠隔でわかると便利では  
まずはログイン機能はいらない  
でもいつかは作らなきゃいけないものがある  
だったら最初から作ったほうがいいのでは  
いかに簡単に作るか  
[Firbase](https://firebase.google.com/?hl=ja)  
自分たちで作る必要なくない？  
作りすぎてしまう  
既存のサービスを使うことの制約  
[Grafana](https://grafana.com/)  
POの孤独  
POは人類には早すぎる  
チームもPOの目線になる  
腹三分  
みんな不安  
はやく成功体験がしたい  
失敗してもクビにならない  
全力でやってるって状況をどう作るのか  
POに「決めてください」はよくない気がしてきた  
[鶏と豚 Agile Animal Farm - Pigs, Chickens, and more](http://cmforagile.blogspot.com/2011/08/agile-animal-farm.html)  
[Certified Scrum Trainer®（CST）江端 一将](https://www.odd-e.jp/team_01/)  
---
template: BlogPost
path: /113
date: 2022-01-15T06:15:50.738Z
title: ep113:コワーキングスペース飲み
thumbnail: /assets/ep113.png
metaDescription:
---
![ep113](/assets/ep113.png)

<iframe src="https://open.spotify.com/embed/episode/3guziyXDtkgENf580Q3XqL?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

新年あけましておめでとうございます。  
コワーキングスペース新年会  
イノベーションの条件  

<iframe src="https://player.vimeo.com/video/48997854?h=d5a57db40e" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/48997854">WSL_濱口秀司_「認知バイアス」にイノベーションのカギがある</a> from <a href="https://vimeo.com/user13303544">worksightjp</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

家でもない友達んちでもない居酒屋でもない会社でもない  
[RAKUNA 神田【神田駅・秋葉原駅】無料wi-fi使い放題！プロジェクター完備！コンビニ至近！](https://www.spacee.jp/listings/2995)  
[金麦](https://www.suntory.co.jp/beer/kinmugi/)  
jamming.fm が始めたコワーキングスペース飲み  
コロナじゃなかったら発見できなかった  
ブレイクザバイアス  
[tad murakami](https://www.linkedin.com/in/tadashi-murakami-453994b/?originalSubdomain=jp)  

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">カイジとトネガワのぶち殺すぞゴミめらの比較。全然違う。 <a href="https://twitter.com/hashtag/%E8%B3%AD%E5%8D%9A%E7%9B%AE%E6%AC%A1%E9%8C%B2%E3%82%AB%E3%82%A4%E3%82%B8?src=hash&amp;ref_src=twsrc%5Etfw">#賭博目次録カイジ</a> <a href="https://twitter.com/hashtag/%E4%B8%AD%E9%96%93%E7%AE%A1%E7%90%86%E8%81%B7%E3%83%88%E3%83%8D%E3%82%AC%E3%83%AF?src=hash&amp;ref_src=twsrc%5Etfw">#中間管理職トネガワ</a> <a href="https://t.co/cvCjeqSci7">pic.twitter.com/cvCjeqSci7</a></p>&mdash; ガルンチョくんUC (@jormunchan) <a href="https://twitter.com/jormunchan/status/914454340814770176?ref_src=twsrc%5Etfw">October 1, 2017</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>  

[Airbnb](https://www.airbnb.jp/)はレイブの民主化  
テレクラ  
[楽天オーネット](https://onet.co.jp/)  
[ep16:マッチングアプリ概論（ゲスト：よなよなこさん）](https://jamming.fm/16)  
普段やらないことをやってみる  
[見つけよう、 次の瞬感を。 - TikTok](https://www.tiktok.com/ja-JP/)  
悪名は無名に勝る  
ゲスト回をコワーキングスペースでやるのは良いのでは  


---
template: BlogPost
path: /Takasugi_Mahiro
date: 2022-01-24T06:15:50.738Z
title: 【無知の知】高杉真宙さんとスーパー戦隊シリーズとセーラームーンと廃人
metaDescription:
---

### 【無知の知】とは  
世の中のあらゆるテーマを取り上げて、知らないことは恥ずかしいという気持ちを捨て、知らないことに対して正直になろうとする2人がそのテーマを深堀りしていくyoutube配信です。  
[YouTubeチャンネルJamming.fm](https://www.youtube.com/channel/UCobMDbV2byoiGHb6Iw7zGhw)  

本日のテーマ:[高杉真宙さん](https://ja.wikipedia.org/wiki/%E9%AB%98%E6%9D%89%E7%9C%9F%E5%AE%99)  
 

### 【無知の知】高杉真宙さんとスーパー戦隊シリーズとセーラームーンと廃人
<iframe width="560" height="315" src="https://www.youtube.com/embed/404cz5UTars" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>---
template: BlogPost
path: /114
date: 2022-02-05T06:15:50.738Z
title: ep114:理想的な結婚相手の選び方から学ぶ不動産の選び方
thumbnail: /assets/ep114.png
metaDescription:
---
![ep114](/assets/ep114.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6bGSi5bVOJ7OW8Kvqci26N?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


妻の異動で引っ越しを考えている[@dada_ism](https://twitter.com/dada_ism)  
家が狭くなってきた  
リモートが多くなって部屋が欲しい  
千葉県 八千代市  
帝都 赤羽  
3月末は引っ越しが高い  
一軒家借りてみたい  
輸入住宅　120平米　庭付き　駐車場2台  
マンションの間取りは同じ  
リセールバリュー原理主義  
通勤1時間半  
保土ヶ谷  
二俣川  
YBP  
ほぼ監獄  
どうやって探すべきか  
[引越し侍](https://hikkoshizamurai.jp/)  
[SUUMO 関東](https://suumo.jp/chintai/kanto/)  
[足で探した赤羽の駐車場](https://jamming.fm/4)  
ひろゆきの故郷　赤羽  
相続で揉めてて物件が出てこない  
地場の不動産あるある  
手書きで書かれている物件広告  
町内会仲良くしておく  
インターネットに出ない理由  
ポストに連絡先を入れる戦略  
まだまインターネットの網目があらすぎる  
町内会アチい  
逆DX  
AX【Analog Transformation】  
アナログ庁  
物件は出会い  
結婚相手をどう決めるか  
合理的な結婚相手の決め方  
【お詫び】合理的な結婚相手の決め方の紹介に誤りがありました。正しくは「[最適停止問題](https://ja.wikipedia.org/wiki/%E6%9C%80%E9%81%A9%E5%81%9C%E6%AD%A2%E5%95%8F%E9%A1%8C)」をご参照ください  
[4月に引っ越す@naoharu](https://twitter.com/naoharu)  
[車庫飛ばし](https://www.hikaku.com/sell-car/shakotobashi/)  
[【悲報】東京の子供の運動能力、ヤバすぎるwwwwwwwwwwwww](http://blog.livedoor.jp/news23vip/archives/5886716.html?utm_source=dlvr.it)  
[プレイグラウンドを遊ぶアメリカのユーチューバー Blippi Toys](https://www.youtube.com/channel/UC-Gm4EN7nNNR3k67J8ywF4g)  
[ベン図](http://www.mathlion.jp/article/ar093.html)  
住んでる地域で親の違いが結構ある  
だだいまー！からエロ本を探して怒られて逃げる  
jamming.fm公式アカウント[@jammingfm](https://twitter.com/jammingfm)  ---
template: BlogPost
path: /115
date: 2022-02-10T06:15:50.738Z
title: ep115:ショートフィルム配信サービス「SAMANSA」
thumbnail: /assets/ep115.png
metaDescription:
---
![ep115](/assets/ep115.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/0UmCwrfRa0JEd0Bv6Cw8x7?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


wordle  
[NYタイムズが人気の単語ゲーム「Wordle」を買収、数億円で](https://forbesjapan.com/articles/detail/45614)  
スタティックコンテンツだけでexit  
世界中誰もが同じ単語に向かう  
[SAMANSA](https://samansa.com/)  
[絶妙な生煮え感](https://www.aboutsamansa.com/)  
月額250円  
映画を作りたいけどスポンサーが集まらない人の登竜門  
マイクロコンテンツ全盛期  
短くてクオリティ高いって映像、意外と無いのでは  
[9メートル](https://samansa.com/) ※直リンクが貼れなかったためサイトで探してください😭 
<blockquote class="twitter-tweet" data-lang="ja" data-theme="light"><p lang="ja" dir="ltr">10分で呼吸するのも忘れるほどの緊張感と絶望を味わえる「CURVE」という短編映画すごかった…！！！<br>目覚めたらコンクリートで出来た絶妙なカーブに放置されてて、少しでも動いたら奈落の底にずり落ちる恐怖映画。<br>音による体験が凄いのでイヤホン視聴をおすすめします…<a href="https://t.co/8iarcUYEBO">https://t.co/8iarcUYEBO</a> <a href="https://t.co/dXqV5JImTW">pic.twitter.com/dXqV5JImTW</a></p>&mdash; DIZ (@DIZfilms) <a href="https://twitter.com/DIZfilms/status/1270275491920900097?ref_src=twsrc%5Etfw">2020年6月9日</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

---
template: BlogPost
path: /116
date: 2022-03-04T06:15:50.738Z
title: ep116:歩み寄ることの大事さ
thumbnail: /assets/ep116.png
metaDescription:
---
![ep116](/assets/ep116.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3T8qG9Xlm9iimzvVEETKnM?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

[ロシアの軍事侵攻](https://www3.nhk.or.jp/news/word/0002039.html?word_result=%E3%82%A6%E3%82%AF%E3%83%A9%E3%82%A4%E3%83%8A%E6%83%85%E5%8B%A2&utm_int=all_header_tag_001)  
[ゴールデンカムイ](https://youngjump.jp/goldenkamuy/)  
トトロ見たことない、には追いつけない  
<iframe width="560" height="315" src="https://www.youtube.com/embed/GonPXvzfY1o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  

プロセス変革の際にぶつかる壁  
今思うともうちょっとうまくできたな  
[ハムエッグ](https://blog.a-know.me/entry/2015/06/24/215900)  
対立が生まれたときのスクラムマスターの振る舞いの難しさ  
教えるんじゃなくて、自分で気づいたと思ってもらいたい  
「今はチームビルディングをしてるんです」  
3年前と比べてアジャイルなどの言葉が経営にも浸透してきていると感じる [@dada](https://twitter.com/dada_ism)  
またスクラムマスターやってみたくなってきた [@naoharu](https://twitter.com/naoharu)  
GitLabでのリリースキックオフ
<iframe width="560" height="315" src="https://www.youtube.com/embed/6xhiRxdkZpY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  
  
[jamming.fm公式twitterアカウント](https://twitter.com/jammingfm)  


---
template: BlogPost
path: /117
date: 2022-03-10T06:15:50.738Z
title: ep117:保育園休園問題
thumbnail: /assets/ep117.png
metaDescription:
---
![ep11７](/assets/ep117.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/4n7vnzXUiQE7BjeiyioUzK?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


コロナで保育園休園問題  
保育園はマスクできないのでよく休園になる  
マスクしてないと濃厚接触者になってします  
濃厚接触者になっても検査は必須ではない  
[病児保育は便利だぞ](https://ja.wikipedia.org/wiki/%E7%97%85%E5%85%90%E4%BF%9D%E8%82%B2)  
インフルエンザのワクチンはかかるけど重症化しない  
コロナのワクチンはどうなの  
コドモンの休園連絡恐怖  
保育園の先生のコドモン連絡のタイトルのセンス  
古き良きインターネットを彷彿とさせるコドモン  
シッターの補助が出る自治体があるらしい  
ひとり罹ったときに休園はやりすぎでは！  
自分の子供が濃厚接触者になるとちゃんと対策しろよ！  
この矛盾  
子どもが掛かるとファミリークラスタ確定  
[ep38:なぜHowを言うべきではないのか 新型肺炎　](https://jamming.fm/38)  
「今年もコロナ第25波やってまいりました！」  
第6波スプリント回してみて、振り返りして７波に備える！  
ラップアップ  
---
template: BlogPost
path: /118
date: 2022-03-27T06:15:50.738Z
title: ep118:不動産仲介業者やウェディングプランナーとの戦い方
thumbnail: /assets/ep118.png
metaDescription:
---
![ep118](/assets/ep118.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/4N6eMi4sHK70k3Gl4V6RDM?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


引越しの進捗  
一軒家賃貸探し  
一軒家賃貸の内見  
大家さんが転勤や海外駐在で定期借家  
大家さんは住んでないとわからないカタログスペックに表れない売り込みしてくる  
借り主ー仲介業者ー管理会社ー大家さん  
不動産仲介業者がよくなかった  
蕎麦屋の出前状態  
盛り盛りの初期費用  
交渉しないといけない  
不動産仲介業者ガチャ  
引越しする時点で弱者になる  
高い職業倫理を持たなければならない  
結婚式も同じ  
[レインズ](https://system.reins.jp/)  
高い職業倫理を持った会社に出会えた  
不動産の仲介手数料は家賃１ヶ月分が最大  
お得感を出す演出  
この感情は大学時代のインド・バングラデシュで感じた  
心のデフォルト値をどこに持つか  
錦糸町の出会い  
[マレーシアでブラックジャック詐欺にあった話](https://jamming.fm/11)  
[深夜特急　沢木耕太郎](https://ja.wikipedia.org/wiki/%E6%B7%B1%E5%A4%9C%E7%89%B9%E6%80%A5)  
[ノブレス・オブリージュ](https://ja.wikipedia.org/wiki/%E3%83%8E%E3%83%96%E3%83%AC%E3%82%B9%E3%83%BB%E3%82%AA%E3%83%96%E3%83%AA%E3%83%BC%E3%82%B8%E3%83%A5)  
一軒家を建てる  
極限まで価格を下げようと思うと人生の幸福度や品質は下がる  
搾り取ることを突き詰めると自分が損する  
@naoharuの結婚式場探し  
[【閉店】マノワール・ディノ](https://tabelog.com/tokyo/A1306/A130602/13001278/)  
１年後でもほぼ埋まってるらしい  
まだギリギリ空いてます  
レストランに罪はない  
納得感あるセールストーク  
エンジニアの人が営業をするとき  
誇張しているかどうかよりも相手が幸せになるかどうかにフォーカスしろ  
納得は全てに優先する  
客「納得させてくれ！」  
[「謎の個人ブログの「同じPCトラブルが出た人の為に備忘録を残しておきます」の頼もしさは異常　PCトラブル以外で人生とかも言及しといて欲しい」](https://twitter.com/consultnt_a/status/1500348105836793856)  
人生で数回しかないことについて語って記録に残す  
日本一事故率が高い自動車学校は次回  


---
template: BlogPost
path: /119
date: 2022-04-13T06:15:50.738Z
title: ep119:移民.zip（ゲスト：shiroさん）
thumbnail: /assets/ep119.png
metaDescription:
---
![ep119](/assets/ep119.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6OfUcq5FJPBd9Bz3ZFQbkm?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>


***

[shiroさん](https://twitter.com/euroGoGo55)  
部旅行のバスの隣の席  
[shiroさん](https://twitter.com/euroGoGo55)「@naoharuさんってどういうAV好きなんすか？」  
宇都宮しをん  
indeedとかで探してら発見した日本の会社のオランダ現地法人に転職  
オランダの実態（コーヒーショップ、飾り窓）  
DevOps周りのインフラエンジニア  
手を動かす機会をどうやって確保するか問題  
海外でもオフショアって構造はある  
[epam](https://www.epam.com/)  
移民の人がエンジニアをやってる？  
高付加価値という雰囲気  
プロパーは既得権益？  
不利な雇用条件でカモられる  
不動産の価値がグングン上がっていく  
未来を重視する政策  
5000円バラマキ  
<iframe width="560" height="315" src="https://www.youtube.com/embed/7ZLOnoqjI5Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

移民はノウハウ  
移民.zip  
世界中のどこにでもあるチャイナタウン  
[クレイジーリッチアジアン](https://www.amazon.co.jp/dp/B07KDYJCD9)「一代の成功よりも文化の継承を大事にするから繁栄できる」  

<iframe width="560" height="315" src="https://www.youtube.com/embed/h8ZVcRFXZhU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

移民2世で移民1世分を回収  
オランダ人は家が大好き  
[Tomorrowland](https://tabippo.net/tomorrowland/)  
リファラル採用と移民  
[Toshitaka Ito回](https://jamming.fm/97)  
アジア人でつるんでしまう  
SaaSはタイムゾーンを越える  

---
template: BlogPost
path: /120
date: 2022-04-23T06:15:50.738Z
title: ep120:コロナ禍フィリピン珍道中
thumbnail: /assets/ep120.png
metaDescription:
---
![ep120](/assets/ep120.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1QQQuzlrU5CQ9x8fQ0e6On?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

会社のオフサイトでフィリピンの[セブ島](https://www.travelbook.co.jp/topic/266)に行った  
初めてチームメンバーと物理的に会った  
実際まだまだプライベート海外旅行は難しいと感じた  
2つのチェックポイント（行き先の国に入国できるか、帰国時に日本に再入国できるか）  
[【感染症情報】フィリピンにおける新型コロナウイルス感染症（COVID-19）の対応について（その200：外国人のフィリピンへの入国に係る要件等：変更）(3月10日発表)](https://www.ph.emb-japan.go.jp/itpr_ja/11_000001_00785.html)  
[新型コロナワクチン接種証明書アプリ](https://www.digital.go.jp/policies/vaccinecert/)  
[One Health Pass to Travel to the Philippines](https://www.philippinesvisa.com/one-health-pass/)  
48時間以内PCR検査があるから、行けるかわからない状態で航空券を買わないといけない  
成田で入国拒否されたらどうなる？   
[ターミナル (映画)](https://ja.wikipedia.org/wiki/%E3%82%BF%E3%83%BC%E3%83%9F%E3%83%8A%E3%83%AB_(%E6%98%A0%E7%94%BB))  
村人A「確か西の方にPCR検査を受けられる病院があったそうだ」  
[イースター](https://ja.wikipedia.org/wiki/%E5%BE%A9%E6%B4%BB%E7%A5%AD)  
翌日結果PCRをプライマリにして、セカンダリで即日PCRを構えておく作戦  
村人B「北西の建物でPCR検査の結果をもらえるらしいぞ」  
日本再入国時の抗原検査  
レモンとうめぼしの絵を見させられて唾液を出させられる  
空港の検査スタッフの粗暴な対応  
[デブサミの発表](https://www.publickey1.jp/blog/22/gitlab2022.html)がバズって否定的なRTがむっちゃ怖かった  
「GitLabみたいな会社だからできるのだけじゃん」  
即ブロック  
[GitLabでは日本で一人Solutions Archtectを募集しています](https://boards.greenhouse.io/gitlab/jobs/6016812002)  
[シャイでもできるAI英会話](https://app.speakbuddy.me/)  
英会話の前に陽キャになる必要がある  
感情的になると母国語でキレる  
英語でキレる実績解除  
[早稲田大学世界旅行研究会](https://www.waseda.jp/inst/shinkan/circle/3119)  
反ワクチンで海外行きたい人はどうすれば  
反ワクチンの人って親ロシア派の人多くない？  
[「世界緊急放送」「逮捕開始」「トランプが日本人全員に6億円配布」Jアノンさんの妄想が止まらない](https://togetter.com/li/1650980)  


---
template: BlogPost
path: /121
date: 2022-05-02T06:15:50.738Z
title: ep121:人生で１回くらいしかない経験を共有するシリーズ「やばい自動車学校」
thumbnail: /assets/ep121.png
metaDescription:
---
![ep121](/assets/ep121.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/7hRPWj8IXNg4s1pYrBfTGR?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***


新年度初収録  
ラン活  
4月1日からコロナ休園でてんやわんや  
初めてのことで戦うのは苦手  
自動車学校は人生1回  
20代前半では高い決済  
[東京都23区でも安かった　尾久自動車学校](https://www.ogu.co.jp/)  
[島根　増田ドライビングスクール　Mランド](https://mland-masuda.jp/)  
[島根県立やかみ高校](https://www.yakami.ed.jp/)  
検索【　免許　最速　】  
[二毛作](https://ja.wikipedia.org/wiki/%E4%BA%8C%E6%AF%9B%E4%BD%9C)  
日本一事故率が高いMDS  
高速道路でばーちゃん出てくるMDS  
運転免許合宿ほど人種の坩堝はない  
入学したら現金没収  
Mマネー  
カイジのエスポワール号  
ヤンキー「ガリ勉おるぞ！」  
謎のイキり  
罪人の行き着くところMDS  
MDSの卒業パーティー  
違反ゼロはむずい？  
Mマネーの手に入れ方  
生徒に仕事やらせてるから安いのでは？  
MマネーでTボーンステーキ喰う班長はいなかった  
寝ぼけて違う部屋に入ってヤンキーに詰められる  
通いで終わりきらなかった  
社会人になっちゃって貴重な土曜日を潰すことになった  
ヤンキーよりもカマしてる@naoharu  
[Mランド公式Instagramアカウント](https://www.instagram.com/p/Can-Y_fPFVY/)  
[メルカリに出品されるMマネー](https://jp.mercari.com/item/m81539595904)  
[外車に乗れるコヤマドライビングスクール](https://www.koyama.co.jp/)  
ハッタリをカマしてなんとかギリギリ間に合わせる性格  
ハッタリドリブン  
自信持って俺これできるがあまりない  
人生Mランド仮説  
人に対して教えることばっかりやってる人は人格が狂う  
日本一厳しい鮫洲運転免許試験場  
[鮫洲には近寄るな](https://gtokio.tumblr.com/post/580523710/%E9%AE%AB%E6%B4%B2%E3%81%AB%E8%BF%91%E3%81%A5%E3%81%8F%E3%81%AA)  
運転免許試験場の運用は自動化したい  
一番できない人に合わせて設計しなければならない  
来週仕事でセブ島に行く@naoharu  
---
template: BlogPost
path: /122
date: 2022-05-18T06:15:50.738Z
title: ep122:引っ越しで失敗した話
thumbnail: /assets/ep122.png
metaDescription:
---
![ep122](/assets/ep122.png)

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/07GpZTvEBEMLHF59NcyCUI?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

上京して６回目の引っ越しでいろいろ失敗した  
引っ越しは朝イチは時間が保証されている  
大学時代引っ越しのバイトをしていた@naoharu  
これ全部入らないっすね〜  
２トンロングトラック  
家具のトリアージをする羽目に  
Web見積もり  
しょうもないものがいっぱいある  
面積あたりの荷物量が通常の２倍  
商用バン（キャラバン）を借りて自分で引越  
家の前の道の工事が重なり過ぎて運び出しが困難に  
レンタカー延長できず  
自家用車で引っ越し  
[トトロの引っ越しシーンみたいになった](https://twitter.com/hayashimunehiro/status/1162652113492516865?lang=ca)  
来週引っ越しを控えてる@naoharu  
[プラウドタワー亀戸クロス](https://www.proud-web.jp/mansion/b115280/)  
家具が傷ついた  
ベッドを２階を上げれなかった  
釣り上げオプション＋25000円  
[善管注意義務違反](https://www.pfa.or.jp/yogoshu/se/se10.html)  
昇降デスクも2階上がらなかった  
人生なれてないことは辛い  
[不動産会社の話](https://jamming.fm/118)  
[自動車学校の話](https://jamming.fm/121)  
電力自由化  
ライフライン契約・解約代行（無料）つかったらやばかった  
引っ越し当日にライフラインが何も契約されてなかった  
電気がなかったら江戸時代  
[コーナン リアカー](https://pro.kohnan-eshop.com/shop/g/g4969182195996/)  
退去時Tips  
こいつはめんどくさい奴  ---
template: BlogPost
path: /123
date: 2022-05-25T06:15:50.738Z
title: ep123:新しい事への挑戦（ゲスト：mifuyuさん）
thumbnail: /assets/ep123.png
metaDescription:
---
![ep123](/assets/ep123.png)


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/22rcrQea467XrmxxouKLnn?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

ゲスト：[naoharu](https://twitter.com/naoharu)  と保小中高を同じレールで過ごしたmifuyuさん  
多彩な選択肢がない島根県  
[ジョー・バイデンが八芳園に来た日](https://ja.wikipedia.org/wiki/%E5%85%AB%E8%8A%B3%E5%9C%92)と八芳園で結婚式を挙げた[naoharu](https://twitter.com/naoharu)  とmifuyu  
女の子のほうがマセてる    
子供「中学受験したいんだけど」  
あいみょん、藤井風に対応できない[dada](https://twitter.com/dada_ism)と[naoharu](https://twitter.com/naoharu)  
中学受験はまわりの有識者に聞くのが一番  
バックキャスト思考  
医者になりたい　→　中学受験だ！  
俺らの10歳ごろってエロ本  
親がどう受験にかかわるべきか  
カーリングに憧れる子供  
何かを獲得するという感覚を覚えてくれればいい  
naoharuが知らないmifuyuさん  
[島根県立大学総合政策学部](https://hamada.u-shimane.ac.jp/department/sogoseisaku/)  
入社3年目で第一子出産  
通勤が辛いのと新しいことをやりたいということで異動希望  
調布、国領、柴崎  
新しいことを勉強することの楽しさ  
早めに産休育休をとったことに対する同期の吸収力を取り戻したい気持ち  
100%仕事にいけてない[dada](https://twitter.com/dada_ism)  
子育てって実質ダブルワークでは  
忙殺されていると失うトリガをどう自分の中につくるか  
自分がやりたかったことをやるmifuyuさんの父親  
島根でジャニバレしたところで  
[naoharu](https://twitter.com/naoharu)のタイムマネジメントミス  





---
template: BlogPost
path: /124
date: 2022-06-04T06:15:50.738Z
title: ep124:親の介護が必要になったらあなたは介護をしたいですか？（ゲスト：mifuyuさん）
thumbnail: /assets/ep124.png
metaDescription:
---
![ep124](/assets/ep124.png)


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6IT9N1nys6WH4yvLptRYCb?utm_source=generator" width="100%" height="232" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>

***

西海岸の人と打ち合ち合わせで眠い[@naoharu](https://twitter.com/naoharu)  
プロダクティビティ０の通勤  
[テスラＣＥＯ、従業員に「出社か退職」の2択迫る](https://jp.reuters.com/article/health-coronavirus-tesla-idJPKBN2NI4D6)  
@naoharuの島根の友達mifuyuさん  
[ep123:新しい事への挑戦（ゲスト：mifuyuさん）](https://jamming.fm/123)  
フィードバック紹介  
アラウンドフォーティの意味  
35-44歳？  
親と離れて暮らしてる人たちの悩み  
しょぼくれていく親  
長男だし  
佐々木家を継ぐのは＠naoharu  
@naoharu父「俺らに何かあったら施設にぶち込んでくれたらいい」  
帰ってこい！よりはいい  
誰の手も借りず人生を全うすることはない  
老老介護  
@naoharuは姉と話すのに気を使う  
家を重視する@naoharu  
こういう問題に立ち向かうときに夫婦で一緒に考えれる  
親に当時どうだったの？を聞いてみる  
自宅で介護  
自分が介護で苦労したので自分は施設に入りたい  
都内の老人ホーム  
環境の変化に弱い親父  
親父を見ると共感性羞恥で血を抜きたくなる  
似た者同士が話すとだめなので間に入ってくれる人が必要かも  
あなたは介護が必要になったら介護をしたいか？  
たまにしか会わないと優しくできる  
アウトソースすることによる後ろめたさ  
ガチで話す場を設定することが大事  
親がどうしたい？と聞く前に自分たちはどうしたいも事前に考えとくべき  
親も自分も変わっている  
一緒に住んでいたときと同じではない  
お墓どうする問題  
墓を閉めちゃう  
自宅にある墓はお金かからないのか？  
みんな直面するこの問題の攻略法ないの？  
海外の人も同じ悩みを抱えてる  
継続的に人生を追跡  






---
template: BlogPost
path: /125
date: 2022-10-29T06:15:50.738Z
title: ep125:コミュニケーションにおける「距離」って？
thumbnail: /assets/ep125.png
metaDescription:
---
![ep125](/assets/ep125.png)


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/70DAW8EwzoI9WorZ1QOIqO?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

ひさしぶりのポッドキャスティング  
最近はYouTubeが多かった  
TikTokも始めた  
全然再生回数が伸びない  
TikTokだとレコメンドもランダムだからまだ勝てるかも？  
最後に購読を押されるのが俺らのコンバージョン  
[開発PM勉強会でトークさせていただいた](https://peer-quest.connpass.com/event/261103/)  
リモートでどう偶発性をつくっていくか  
コミュニケーションが難しいのはリモートだからなのか？  
リモートワークが超絶デフォルトになって＆テクノロジーが超絶進化したとき、「コミュニケーションコスト」は何を指し示すか  
jamming.fm で言う「着ぐるみを着る」は、本当に相手の気持ちになれているのか?  
最古のリモートコミュニケーションは狼煙？  
シンプルな組織運営はマンモスの時代にある  
いつも同じ話しをしてて老害感を感じる [@naoharu](https://twitter.com/naoharu)  
人類はコミュニケーションしたい  
子供「お父さんは話が難しい」  
子供とのお医者さんごっこ  
父「どうですか？」医者（娘）「体調悪い」


<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/CO7tYswDeuJ/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="14" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:16px;"> <a href="https://www.instagram.com/p/CO7tYswDeuJ/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank"> <div style=" display: flex; flex-direction: row; align-items: center;"> <div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;"></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;"></div></div></div><div style="padding: 19% 0;"></div> <div style="display:block; height:50px; margin:0 auto 12px; width:50px;"><svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-511.000000, -20.000000)" fill="#000000"><g><path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path></g></g></g></svg></div><div style="padding-top: 8px;"> <div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;">この投稿をInstagramで見る</div></div><div style="padding: 12.5% 0;"></div> <div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;"><div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div> <div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div> <div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div></div><div style="margin-left: 8px;"> <div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div> <div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div></div><div style="margin-left: auto;"> <div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div> <div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div> <div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div></div></div> <div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;"> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div> <div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div></div></a><p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;"><a href="https://www.instagram.com/p/CO7tYswDeuJ/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Naoharu SASAKI(@naoharu)がシェアした投稿</a></p></div></blockquote> <script async src="//www.instagram.com/embed.js"></script>  
  
リモートをやめている会社  
[タイム・マシン (小説)](https://ja.wikipedia.org/wiki/%E3%82%BF%E3%82%A4%E3%83%A0%E3%83%BB%E3%83%9E%E3%82%B7%E3%83%B3_(%E5%B0%8F%E8%AA%AC))  



---
template: BlogPost
path: /126
date: 2022-12-29T06:15:50.738Z
title: ep126:お掃除ロボットを借りて即購入した
thumbnail: /assets/ep126.png
metaDescription:
---
![ep126](/assets/ep126.png)


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3D3auDSnkL7zJZZxTkvelu?utm_source=generator&theme=0" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

[jamming.fm youtubeチャンネル](https://www.youtube.com/@user-ww2ub4ph3j)   
youtubeのコンテンツのふりかえりをpodcasetでやってみる試み  
[ルンバ j7+](https://amzn.to/3WQyFBb)  
[ルンバ s9+](https://amzn.to/3WU9Qo9)   
[ルンバ コンボj7+](https://amzn.to/3G0B9Gu)


<iframe width="560" height="315" src="https://www.youtube.com/embed/19FR-1X1C58" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/kxYwzAZgiTY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


---
template: BlogPost
path: /127
date: 2023-02-23T06:15:50.738Z
title: ep127:冒険的な出張 in コロラド
thumbnail: /assets/ep127.png
metaDescription:
---
![ep127](/assets/ep127.png)  


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/72rtZx8xSlhbGwEe90ppjC?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

[コロラド州 - Wikipedia](https://ja.wikipedia.org/wiki/%E3%82%B3%E3%83%AD%E3%83%A9%E3%83%89%E5%B7%9E)  
[ep120:コロナ禍フィリピン珍道中](https://jamming.fm/120)  
[米国への渡航 COVID-19ワクチン接種の要件](https://jp.usembassy.gov/ja/us-travel-requirements-ja/)  
[【水際対策】出国前検査証明書](https://www.mhlw.go.jp/stf/seisakunitsuite/bunya/0000121431_00248.html)  
[新型コロナワクチン接種証明書（アプリ）の再発行にはマイナンバーカードが必要](https://www.kojinbango-card.go.jp/220506_1/)  
[新型コロナウイルス感染症　予防接種証明書（接種証明書）について](https://www.mhlw.go.jp/stf/seisakunitsuite/bunya/vaccine_certificate.html)  
[デルタ航空、米国への渡航手続きを簡素化するオンラインツールを提供](https://news.delta.com/upload-and-go-deltas-new-online-tools-make-travel-us-easy-JP)  
[ESTA（エスタ）申請 - 在日米国大使館と領事館](https://jp.usembassy.gov/ja/visas-ja/esta-information-ja/)  
[デンバー - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%87%E3%83%B3%E3%83%90%E3%83%BC)  
[USB Type-A](https://e-words.jp/w/USB_Type-A.html)  
[ニューヨークでは「蒸気」を使った暖房や給湯システムが現役で稼働中、一体どのように機能しているのか？](https://gigazine.net/news/20221106-new-york-city-steam-system/)  
[アメリカ / デンバーの時差と現在時刻](https://www.time-j.net/WorldTime/location/America/Denver)  
[皆既月食・天王星食（2022年11月）](https://www.nao.ac.jp/astro/sky/2022/11-topics02.html)  
[デンバー美術館 - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%87%E3%83%B3%E3%83%90%E3%83%BC%E7%BE%8E%E8%A1%93%E9%A4%A8)  
[ホールフーズ・マーケット - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%9B%E3%83%BC%E3%83%AB%E3%83%95%E3%83%BC%E3%82%BA%E3%83%BB%E3%83%9E%E3%83%BC%E3%82%B1%E3%83%83%E3%83%88)  
[Eldora](https://www.eldora.com/)  

<iframe width="560" height="315" src="https://www.youtube.com/embed/vDcAQyXk7x0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>  

[ボルダー (コロラド州) - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%9C%E3%83%AB%E3%83%80%E3%83%BC_(%E3%82%B3%E3%83%AD%E3%83%A9%E3%83%89%E5%B7%9E))  
[国外運転免許証取得手続（本人による申請）](https://www.keishicho.metro.tokyo.lg.jp/menkyo/menkyo/kokugai/kokugai01.html)  
[なかなか慣れない！アメリカの「赤信号でも右折OK」ルール](https://www.sanikleen.co.jp/autolife/topic/1310/)  
[ep121:人生で１回くらいしかない経験を共有するシリーズ「やばい自動車学校」](https://jamming.fm/121)  
[グアムで運転の際の注意点](https://www.iguam.jp/blog_jp/driving_at_guam)  
[首都高は怖い… なぜ左側ではなく右側に出入口が多いのか　都心部ならではの理由が存在](https://kuruma-news.jp/post/99612)  

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Lane Assistance Guidance showing up finally in the UK on iOS 11.2 in Apple Maps! <a href="https://t.co/G5KMfSipWC">pic.twitter.com/G5KMfSipWC</a></p>&mdash; Drew Post (@drewpost) <a href="https://twitter.com/drewpost/status/929300913273090048?ref_src=twsrc%5Etfw">November 11, 2017</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

[帝国単位 - Wikipedia](https://ja.wikipedia.org/wiki/%E5%B8%9D%E5%9B%BD%E5%8D%98%E4%BD%8D)  
[摂氏・華氏(計算式、変換ツール、換算表)](https://kunisan.jp/gomi/sesshi_kashi.html)  
[Visit Japan Webサービス](https://www.digital.go.jp/policies/visit_japan_web/)  
[入国（帰国）時における「携帯品・別送品申告書」の提出について](https://www.customs.go.jp/kaigairyoko/shinkokusho.htm)  
ようやくコロナ前くらいの感覚で海外旅行に行けそう  

---
template: BlogPost
path: /128
date: 2023-04-22T06:15:50.738Z
title: ep128:社会人的同窓会とActive Book Dialogue
thumbnail: /assets/ep128.png
metaDescription:
---
![ep127](/assets/ep128.png)  


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6zNJb7Uot9gJl9JCt4IUDu?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

前職に新卒入社したときの部の人たちと同窓会チックなことをした @[naoharu](https://twitter.com/naoharu)  
弊Podcastのリスナーの方もいらっしゃった  
YouTubeもやりはじめた（外資系IT企業と伝統的日本企業を働く30代）  
@[dada](https://twitter.com/dada_ism) も異動とかも結構している  
人生の変遷を公開する実験  
新卒で入ったことの時をどう感じるか？  
JTCあるある  
また出た、ヌルヌル人材  
SaaS売ってやってて感じる2つのフェーズ  
導入のフェーズと拡大のフェーズ  
[マンションポエム](https://twitter.com/MansionPoem_bot)の外資IT版  
日本にHQがあるJTCもいろいろ大変  
[アクティブブックダイアログ](https://www.abd-abd.com/)を久々に始めた    





---
template: BlogPost
path: /129
date: 2023-05-02T06:15:50.738Z
title: ep129:日常系ポッドキャスト
thumbnail: /assets/ep129.png
metaDescription:
---
![ep127](/assets/ep129.png)  


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/0RZIzE8QNgmoQL149iRrmw?utm_source=generator" width="100%" height="352" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

[AWS Summit Tokyo 2023](https://aws.amazon.com/jp/summits/tokyo/?sc_channel=sm&sc_campaign=AWS_Summit_Japan_2023)  
[FPMをみんなで座って聴く](https://twitter.com/awscloud_jp/status/1646664573141889027)  
久々の大規模イベント  
プライベートサウナに行った  
男女が水着やラッシュガード着用で混浴できる  
パブリックなサウナで騒いでる輩が気になる  
身内でサウナでおしゃべりできる新感覚  
[サウナOOO](https://soowood.co.jp/project/ooo-sauna/)  
外の緑の変化を楽しむダダ  
部屋の中にも緑を置いてみている  
数年単位の楽しみ  
わびさびみたいな趣味が増えるのは環境なのか年齢なのか  
悔しさドリブンとか目標ドリブンみたいなの無くなってきた  
加齢に伴う集中力の低下  
[日常系マンガ](https://animestore.docomo.ne.jp/animestore/CP/CP00001541)  
日常系ポッドキャスト  
細々と開業




---
template: BlogPost
path: /130
date: 2023-05-17T06:15:50.738Z
title: ep130:車と長距離フェリーで行く国内旅行のススメ
thumbnail: /assets/ep130.png
metaDescription:
---
![ep1３0](/assets/ep130.png)  


<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6aYj9wghzE67Jc1Fw5CRYh?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

松屋のスパイシーカレーは本当に辛い  
[松屋のUIがひどくて燃えてる](https://biz-journal.jp/2023/05/post_340501.html)  
[2週間で改善したっぽい](https://togetter.com/li/2142962)  
おなじみ鬼怒川 ホテル三日月 @naoharu  
[ホテル三日月＠鬼怒川](https://www.mikazuki.co.jp/kinugawa/)  
[星野リゾート 界 鬼怒川](https://www.hoshinoresorts.com/resortsandhotels/kai/kinugawa.html)  
[佐野ラーメン](https://sanoramenkai.jp/)  
[佐野ブランドキャラクター「さのまる」](https://www.city.sano.lg.jp/sanomaru/index.html)  
基本戦略はこどもは水につけてふやかす  
小学校のプール後はめっちゃだるい思い出  
[イギリスのTVに出たとにかく明るい安村](https://nlab.itmedia.co.jp/nl/articles/2304/24/news083.html)  
安心してください！履いてませんよ！（全裸）  
@naoharu妻は下ネタNG  
鬼怒川　ライン下り  
[ライン下りの事故](https://www.saitama-np.co.jp/articles/20369)  
車で奈良に行った@dada-ism  
千葉→愛知→奈良→京都→福井→兵庫→徳島→東京→千葉  
[実際のルート](https://twitter.com/dada_ism/status/1655057606199889920)  
[中川政七商店 奈良本店](https://www.nakagawa-masashichi.jp/staffblog/store/s165002/)  
[天橋立](https://www.amanohashidate.jp/spot/amanohashidate/)  
[伊根の舟屋](https://www.uminokyoto.jp/course/detail.php?cid=34)  
[福井 恐竜博物館](https://www.dinosaur.pref.fukui.jp/)  
[越前がにミュージアム](https://www.echizenkk.jp/kanimuseum)  
[jamming.fm youtube 敦賀気比って何？](youtube.com/watch?v=XKzzzrxjorU)  
[徳島と東京を結ぶオーシャン東九フェリー](https://www.otf.jp/)  
急遽福井から徳島へ  
[明石焼き ゴ](https://www.go-akashiyaki.com/)  
淡路島で新玉ねぎ収穫体験　  
鳴門海峡で徳島側から渦を見る　  
[長距離フェリーのすすめ](http://www.jlc-ferry.jp/kouro.html)  
[東京九州フェリー　横須賀〜新門司](http://www.jlc-ferry.jp/kouro2.html?num=15&ph=15&sita=38&ue=39)  
フェリーの露天風呂は最高　サウナもある  
燃料費＋高速代＋宿泊費考えるとフェリーは安い  
島根に買えるとき新幹線はこども辛い  
[サンライズ出雲も部屋が広いのでよい](https://www.jr-odekake.net/train/sunriseseto_izumo/)  
東京から鳥取までキャンピングカーで行った  
乗り捨てで家族で移動したい  
子供乗せ自転車を東京中にばらまいてシェアしたい  
キャンピングカー買って乗り捨て先で売る  
旅行も生き方も移動がしたい  
[高城剛](https://ja.wikipedia.org/wiki/%E9%AB%98%E5%9F%8E%E5%89%9B)  
[移動することに脳が幸せを感じる](https://globe.asahi.com/article/14502555)  
マイクラで安定してくると飽きる  
じんわりした楽しみ  
地のものを食べるのが楽しい  
小さなこどもがいる家庭の旅行先でイオンに行くのはおすすめ  
島根来たら無人市おすすめ  
エッジコンピューティング無人市  
イマドキお賽銭はぺいぺい払い  
---
template: BlogPost
path: /131
date: 2023-05-31T06:15:50.738Z
title: ep131:Different families raise their children differently
thumbnail: /assets/ep131.png
metaDescription:
---
![ep1３1](/assets/ep131.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/77QfSL4azoOnSJ6oqZgF3s?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>  

***  

[日清どん兵衛](https://www.donbei.jp/)  
アクティブブックダイアログ  
デジタルトランスフォーメーションジャーニー  
日本CTO協会  
[DX Criteria](https://dxcriteria.cto-a.org/)  
人生に一回しかしないことはみんな失敗しがち  
[ep122:引っ越しで失敗した話](https://jamming.fm/122)  
[ep118:不動産仲介業者やウェディングプランナーとの戦い方](https://jamming.fm/118)  
[ep121:人生で１回くらいしかない経験を共有するシリーズ「やばい自動車学校」](https://jamming.fm/121)  
人生に役立つチェックリストが欲しい  
dada家はママ友にビックリされることがよくある  
😥「それ大丈夫？」    
おむつを変える頻度    
ミルクを冷ますのではなくて水で薄める   
子育て習慣をあんまり他の家庭に言わない  
複数の家族でどっか行った時に明らかになる文化的な差異  
dada家は自分たちが世界一幸せな家族運営をしている  
「悪魔ができあがる」  
常識のギャップが家族間で広いと大変だろうな  
ポッドキャストを3年くらい続けて感じた良さ  
過去の自分と同じ悩みにぶつかった人たちに紹介できる  
ep82:乳児頭蓋変形外来  
dadaの人生をさも自分のものかのように喋るnaoharu  
住宅賃貸購入論争はまた別途実施したい  
[ネタを貯める場所をgitlabにした](https://gitlab.com/jammingfm/jamming.fm/-/boards/2811158)  
  
---
template: BlogPost
path: /132
date: 2023-06-04T06:15:50.738Z
title: ep132:持ち家 or 賃貸 2023
thumbnail: /assets/ep132.png
metaDescription:
---
![ep1３2](/assets/ep132.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/4yqbA49uC18UoutzOy0dlx?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>


***
  

[大人の障害物レース スパルタンレース](https://spartanracejapan.jp/)  
疲れすぎて熱が出た  
[SEA TO SUMMIT](http://www.seatosummit.jp/)  
3年半振りのテーマ  
[pe14: 持ち家 vs 賃貸 2019](https://jamming.fm/14)  
2023年版を録る  
2019年はお互いに賃貸  
2020年にマンション購入した@naoharu  
[ep59 マンション買った](https://jamming.fm/59)  
家を買ったときはNRIだった  
日本トップレベルの信用  
買ってすぐ転職した  
なんで買いたいと思ったのか  
子供が5歳になったときに前の家が手狭になった  
広いところに住む必要が出てきた  
賃貸を選ばなかった理由  
一族が住んでるのでここに住む  
都内で賃貸で３LDKは高い  
所有しないことに高いお金を払うべきか  
どんな価値観でその判断に至ったのかを明らかにしたい  
人生1回きりの判断は間違ってない買ったと思わないとメンタル保てない  
元々は買いたいと思ってなかった  
奥さんが家ほしいと言って買う人も多い  
戸建賃貸　庭が60平米、駐車場2台  
そもそも持ち家という選択肢がない@dada_ism  
注文住宅はリセールバリュー考えちゃいけない  
注文住宅楽しそう  
ただ知識がないと失敗しそう  
マンションはそういう失敗は少ない  
マンションのデメリット  
コピペの部屋に住んでる  
ずっと住むって思ってない場合  
買ったらリセールバリュー落とさないようにしなければいけない  
という意識がストレス  
新築も2週間でどうでもよくなる  
子供ありがとう  
解脱  
自分でやらかしたい  
賃貸でも壁紙張り替えたり床を張り替えたりしている  
[ep114:理想的な結婚相手の選び方から学ぶ不動産の選び方](https://jamming.fm/114)  
家を購入したら仕事に身が入ることはなかった  
家を買ったら転勤が決まるあるある  
住宅ローン減税享受したくて泣く泣く単身赴任  
購入派「売ればいいじゃん」は結構きびしい  
その地域で一番いいモノを選ぶべき  
定期的にこのテーマ録りたい  
ポッドキャストの定点観測性はおもしろい  
エチエチ看護師  ---
template: BlogPost
path: /133
date: 2023-06-25T06:15:50.738Z
title: ep133:子供の習い事
thumbnail: /assets/ep133.png
metaDescription:
---
![ep1３2](/assets/ep133.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3AC6DM7awtHpzNhvJ27SLq?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

[伝説のすた丼](https://sutadonya.com/)のレシピを完全再現した  

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">男の子を育てる時は父親が積極的に介入した方が良いな。父母の集まりで「3歳になったら息子に空手でも習わせようかと思ってる。なんだかんだ男の子社会は『こいつを怒らせたら怖いな』っていうのが抑止力になるから」って言ったら男親は「確かに」って反応だったのに女親はピンと来てなかったもんな。</p>&mdash; ヨッピー (@yoppymodel) <a href="https://twitter.com/yoppymodel/status/1670583342838984704?ref_src=twsrc%5Etfw">June 19, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>  

ビビらせることによる不利益
舐められることによっていいこともある  
抑止力としての空手  
「こいつにはこいつの考え方があるな」と一目置かれる感じ  
[空手の型ができると自動車学校の合宿で一芸を披露できる](_data/blog/2023-06-04-ep132.md)  


<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">空手やフィジカルに限らず「初手で舐められない」っていうのが大事な時期があり、そのあと自信や知識や確固たるものが自分の中で生まれ「初手で舐めてマウント取りに来てくれた方が後の先を取れて楽😚」みたいなフェーズがあり（20代後半くらい）、その後に（どうでもいいなこういうの）フェーズある👴</p>&mdash; Naoharu (@naoharu) <a href="https://twitter.com/naoharu/status/1671126874100240386?ref_src=twsrc%5Etfw">June 20, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

naoharuが高2のころベッカムヘアーにしたら先輩にシメられた  
空手って現代のいじめに通用しないのでは  

外資系のキャリア  
日本企業って最終的にはマネジメントにいかないといけない？  
ManagerとIC（Indivisual Contributor）  
「自分は現場でプレーヤーでいたい」「自分はマネジメントをやっていきたい」もいいんだけど、組織の誰もマネジメントをしたくないっていいんかな？  
Job Descriptionを適切にはみ出すことが最初の一漕ぎ  
「出、出〜〜〜ww　自転車操業格安旅行代理店最後一漕渡米奴〜〜〜ww」  
マネージャーになりたいと思わせることはマネージャーの大事な仕事なのでは  
メールやSlack、Teamsの[予約送信機能](https://support.microsoft.com/ja-jp/office/teams-%E3%81%A7%E3%83%81%E3%83%A3%E3%83%83%E3%83%88-%E3%83%A1%E3%83%83%E3%82%BB%E3%83%BC%E3%82%B8%E3%82%92%E3%82%B9%E3%82%B1%E3%82%B8%E3%83%A5%E3%83%BC%E3%83%AB%E3%81%99%E3%82%8B-2fc5ea77-7bb4-4511-8f59-e62bac1c0f6a)  


---
template: BlogPost
path: /134
date: 2023-07-14T06:15:50.738Z
title: ep134:喪主になる前に聴きたい話(ゲスト：スモックパイセン)
thumbnail: /assets/ep134.png
metaDescription:
---
![ep1３4](/assets/ep134.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1OB251CiHC5Qgg2m8ylpPO?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

3年ぶりのスモックパイセン  
[ep41:戦略的職探し（ゲスト：スモックさん）](https://jamming.fm/41)  
[ep42:ファッションはすたれるがスタイルは永遠だ（ゲスト：スモックさん）](https://jamming.fm/42)  
[ep43:職業訓練（ゲスト：スモックさん）](https://jamming.fm/43)  
[ep44:ワークアズライフ（ゲスト：スモックさん）](https://jamming.fm/44)  
スモックパイセン回、リスナーからのフィードバックが多い  
前回までのあらすじ  
実はもっと前？3年半！？  
日曜のザ・ノンフィクション  
コロナでプレスから異動になった  
ECサイトの部署へ  
ずっとやりたかったWEBの仕事に  
ハードコア人生終了？  
実は先週辞令が出てまたプレスに異動  
引き継ぎしないといけないけどしたくない  
スマホでIllustrator使う世代  
論文をフリック入力、音声入力で書く  
また転職したい  
アパレルはどんどん人気がなくなっている  
市場衰退  
AIはガチ  
ホワイトカラー全員やばい  
60歳超えたらあと遊ぶのは10年しかない  
スモックパイセンも遊びたい  
有給で3連休にしたことがない  
水族館、動物園すごい楽しい！  
[勝新太郎はビールがオレンジジュースみたいになった](https://www.youtube.com/watch?v=Iqw4zJ9OJfQ)  
最近母が亡くなった  
親の介護  
週末を介護のために休める仕事を選んだ  
県を跨ぐ外出禁止  
お父さんが急に倒れた  
上司「辛いのはお前だけじゃない」  
母を施設に入れる  
入所一ヶ月で入院  
今すぐ決めてください。看取る OR 胃瘻  
1ヶ月に一度10分のテレビ電話  
最期の決断 看取る OR 延命措置  
事前に親の意思を聞いておくこと  
自分の意思も伝えておくべきだけど悩む  
自分に判断能力がなくなったとき  
亡くなった直後  
刑事さんとの会話  
病院の対応  
葬儀場での再開  
家族葬全然安くない  
葬儀の支払いはキャッシュOR振込  
火葬場がいっぱい  
葬式も結婚式みたい  
エンゼルメイク  
最期のお風呂「湯灌」  
最期に家の前を通る  
火葬場にて  
喪主めっちゃ大変  
相続  
お墓どうする  
墓石が高いらしい  
表向きは忌引、結局仕事  
初盆はいつなの？  
お葬式の情報はなかなかアクセスしない  
喪主の挨拶  
スモックパイセンのおすすめコンテンツ  
[インディージョーンズと運命のダイアル](https://www.disney.co.jp/movie/indianajones-dial)  
ハリソン・フォード80歳  
80年代の悪趣味な殺し方  
やっぱり気持ち悪かった  
インディージョーンズ最後の聖戦  
[ヨルダン ペトラ遺跡](https://www.google.com/maps/place/%E3%83%9A%E3%83%88%E3%83%A9/@30.3284544,35.4421735,17z/data=!3m1!4b1!4m6!3m5!1s0x15016ef1703b6071:0x199bf908679a2291!8m2!3d30.3284544!4d35.4443622!16zL20vMGM3enk?hl=ja&entry=ttu)  
[ザ・フラッシュ](https://wwws.warnerbros.co.jp/flash/)  
[Netflix ブラック・ミラー](https://www.netflix.com/jp/title/70264888)  
[Netflix サンクチュアリ](https://www.netflix.com/jp/title/81144910)  
忽那汐里にイキコクしかけた   
[イキコク（いきなり告白）「キモオタがイキコクする理由」](https://twitter.com/kashiwaF/status/946716955787931648)  
収録があると人生の歯車が回りだす  
---
template: BlogPost
path: /135
date: 2023-07-23T06:15:50.738Z
title: ep135:初めての交通事故
thumbnail: /assets/ep135.png
metaDescription:
---
![ep1３5](/assets/ep135.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6N6HOkzBxkhVboURec0rLY?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>


***

京都でAPACのSolutions Architectのチームでトレーニング合宿  
[⁠ep120:コロナ禍フィリピン珍道中⁠](https://jamming.fm/120)  
[⁠Thousand Hotel Kyoto - The Thousand Kyoto⁠](https://goo.gl/maps/ASwLni7A5uhX3R5q8)  
⁠[東本願寺](https://ja.wikipedia.org/wiki/%E6%9D%B1%E6%9C%AC%E9%A1%98%E5%AF%BA)⁠  
[⁠川床とは？「かわどこ」と「ゆか」にこめた京都人の愛について⁠](https://jp.pokke.in/blog/6867/)  
⁠濃い麦茶がタバコの味(におい) がするのはなせなぜです何故ですか？⁠  
魚民は小皿料理をたくさん頼めるから外国人に人気だった  
関西までもどったから実家の島根まで戻った @⁠[naoharu](https://twitter.com/naoharu)⁠  
携帯充電器レンタル ⁠[ChargeSPOT](https://chargespot.jp/)⁠  
⁠石見プラザショッピングセンター⁠  
⁠[携帯の2年縛り⁠](https://www.hikari-collaboration.jp/column/%E6%9C%80%E5%AE%89%E5%80%A4%E7%B4%9A%E3%81%AE%E3%82%B9%E3%83%9E%E3%83%9B%E3%81%AB%E4%B9%97%E3%82%8A%E6%8F%9B%E3%81%88%E3%82%8B%E6%96%B9%E6%B3%95/)  
携帯ショップ定員「今月でピッタです」  
⁠石見の方言⁠  
初めて対人の交通事故に遭遇した @⁠[dada_ism](https://twitter.com/dada_ism)⁠  
幸い双方に怪我無し  
先方がかあちゃん呼んできた  
もし先方が「ちょっと！何やってるんですかー！」って来られたらダルい  
何を言ったかなどは過失割合に影響するかも  
まずは冷静に「大丈夫ですか？」などがベター。（初手謝罪は悪手）  
ドライブレコーダーは必須  
ドラレコで撮ってると言ったら過失割合が 2:8 から 1:9 になった  
交渉代行（保険会社）では 0:10 に出来ない  
[⁠ep121:人生で１回くらいしかない経験を共有するシリーズ「やばい自動車学校」⁠](https://jamming.fm/121)  

---
template: BlogPost
path: /136
date: 2023-08-02T06:15:50.738Z
title: ep136:何者かにならなければいけないという焦り
thumbnail: /assets/ep136.png
metaDescription:
---
![ep1３6](/assets/ep136.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/4CZh9iryuNu9gqK7X48Yb4?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>


***

子どもが夏休み  
大人の夏休みは休んでない  
Xの読みはクロス  
[アートや美大周辺によくある”何者かになれ”という強迫で「普通」を軽視して壊れていくのを散々見てきた→「病み」が良いものを作るは本当？](https://togetter.com/li/2174802)   
美大生じゃないけど生きていく上ではある  
何者かにならなきゃいけないというのは高1くらいからある@naoharu   
田舎「興味の対象が他者にある」  
もっと自分ないの？自我2.0が芽生えて島根を出た  
島根を出てもやってることが他者との比較  
何者かになる＝他者がいないと成立しない  
30代前後で人生の有限感を感じる  
会社で感じる人生の有限感  
認知的不協和があることでなれなくてもいい理由を探す  
子どもができると何者かになれる  
我執の炎が消えるタイミングが来る  
何者かになる人は何者かになりたいと思っていない  
[バガボンド](https://morning.kodansha.co.jp/c/vagabond.html)   
井上雄彦ハラスメント  
自分とはなにか  
エゴイズム  
そういう熱量はあるか  
いかにいい状態で死ねるかにフォーカスしている  
[DIE WITH ZERO](https://example.com/die_with_zero)   
何者かになりたい　ではなく　何者かになる  
いろんな状態になっておく  
島耕作になれなかった俺たち  
弘兼先生には現代版島耕作を描いてほしい  
[ブラック・ミラー シーズン6 「ジョーンはひどい人」](https://www.netflix.com/jp/title/70264888)   
生成系AIに作ってもらう島耕作    
■タイトル: 「島耕作の冒険者日記」  
プロット概要: 主人公、島耕作は冒険と探検が大好きな青年です。彼は普通の会社員ではなく、自由な生活を選び、世界中を旅して回る冒険者です。彼の仕事は未知の場所を探索し、謎を解き明かすことです。

開幕の冒険: 島耕作は美しい秘境の島に辿り着きます。そこで彼は古代の遺跡を発見し、謎の文明の謎に迫ります。島には不思議な生物や危険なトラップが待ち受けています。
知識と友情の獲得: 島耕作は他の冒険者たちと出会い、彼らと協力して困難な局面を乗り越えます。新たな知識やスキルを獲得しながら、彼は困難を乗り越えて成長していきます。
悪の組織との対決: 島耕作は島に潜む悪の組織による陰謀を知ることになります。彼は正義を貫き、その組織との壮絶な戦いに挑みます。
世界中の冒険: 「島耕作の冒険者日記」は、様々な地域や文化を舞台にしたエピソードで構成されます。島耕作は世界中を旅し、様々な謎や冒険に立ち向かいます。
自然との共鳴: 島耕作は自然との繋がりを大切にし、環境保護の重要性を伝えるエピソードもあります。彼は自然を尊重し、自然の力を利用して冒険を成功させる方法を見つけます。
成長と目標: 島耕作は冒険の中で成長し、自分の目標や使命に向かって進んでいきます。彼は自らの過去と向き合い、未来に向けて歩を進める姿勢を見せます。

「島耕作の冒険者日記」シリーズは、アクション、冒険、謎解き、友情、そして自然愛をテーマにした心躍るストーリーとなります。島耕作の個性的なキャラクターと、世界中での様々な冒険が読者を魅了することでしょう。

■タイトル: 「島耕作の幻想旅人」  
プロット概要: 「島耕作の幻想旅人」は、ファンタジーと冒険が融合したシリーズです。島耕作は現実世界とは異なる異世界や幻想的な場所を冒険する旅人として描かれます。

異世界の扉: 島耕作はある日、古代の魔法によって現実世界から異世界へと引き込まれます。彼は最初は混乱してしまいますが、新しい世界の美しさや謎に魅了され、冒険の旅に出発します。
奇妙な仲間たち: 異世界では様々な種族や魔法使い、ドラゴン、妖精などが存在します。島耕作は個性豊かな仲間たちと出会い、彼らとの友情を深めていきます。仲間たちの力を合わせ、困難な敵と対峙します。
魔法と戦闘: 島耕作は異世界で魔法の才能を見出され、魔法の修行を始めます。彼は強力な敵と戦うために魔法を駆使し、冒険の中で成長していきます。
王国との対立: 異世界には様々な王国や勢力が存在し、彼らの抗争や陰謀が島耕作の冒険を襲います。彼は無邪気な世界に立ち向かい、正義を貫きます。
謎と神秘: 異世界には未知の神秘が満ちています。島耕作は古代の遺跡を探索し、失われた伝説や秘密を解き明かす冒険に身を投じます。
帰還と新たな決意: 島耕作は現実世界に戻る方法を見つけるために、壮大な冒険を経て成長します。しかし、彼は冒険の経験を胸に新たな決意を秘め、再び異世界へと旅立つことを決断します。

「島耕作の幻想旅人」シリーズは、ファンタジーの魅力と冒険のスリルが共存する物語となります。読者は島耕作と共に未知の世界を冒険し、ファンタジーの魔法に満ちた世界に夢中になることでしょう。

■タイトル: 「島耕作と失われた時の秘密」
プロット概要: 「島耕作と失われた時の秘密」は、タイムトラベルと謎解きがテーマのシリーズです。島耕作はある事件をきっかけに、過去や未来へのタイムトラベルの能力を手に入れます。

タイムトラベルの始まり: 島耕作はある謎めいたアーティファクトを手に入れたことで、過去や未来へのタイムトラベルが可能になります。彼はその力に戸惑いながらも、その使い方を学び始めます。
歴史の謎を追って: 島耕作はタイムトラベルの力を使って、歴史上の重要な事件や失われた秘密に迫ります。彼は過去の謎を解き明かすことで、現在の事件にも光を当てていきます。
時間のパラドックス: タイムトラベルには過去を変える可能性も伴います。島耕作は時空のパラドックスに直面し、過去を改変することのリスクや倫理的な問題に立ち向かいます。
未来の危機: 島耕作は未来の世界に飛び、そこで起こる危機に直面します。未来の世界では何らかの災厄が起こり、それを防ぐために彼は戦いに身を投じます。
タイムトラベラーの共同体: 島耕作はタイムトラベルの力を持つ他のトラベラーたちと出会います。彼らは時を超える力を持つことによって特別な責任を背負い、共に行動することになります。
タイムトラベルの終わりと新たな始まり: 島耕作はタイムトラベルの力を使って多くの冒険と謎解きを経験しますが、やがてその力を手放さなければなりません。彼は過去と未来の世界に別れを告げ、新たな冒険への扉を開くのです。

「島耕作と失われた時の秘密」シリーズは、タイムトラベルの謎や歴史の謎解きに加えて、人間の選択や善悪、時間の意味についても考えさせられる物語となります。読者は島耕作と共に時空を超える冒険に身を委ねることでしょう。
---
template: BlogPost
path: /137
date: 2023-08-12T06:15:50.738Z
title: ep137:家族で行く東南アジア
thumbnail: /assets/ep137.png
metaDescription:
---
![ep1３6](/assets/ep137.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/0nchyoJpiRRooDF8vlW7G9?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

コワーキングスペースで収録に参加している[@naoharu](https://twitter.com/naoharu)  
久々の海外旅行  
シンガポールとマレーシア  
将来移住するとしたらどこに住むか  
海外のほうが家族連れに寛容  
亜熱帯地域の過ごしやすさ  
[マリーナベイサンズ](https://jp.marinabaysands.com/)  
インフィニティプールのはしり  
シンガポールの物価  
家賃がニューヨークとかの相場  
日本人が普通に住む部屋レベルでも億ション  
[シンガポール名物屋台グルメ、ホーカーズ](https://www.mapple.net/global/article/6844/#:~:text=%E3%83%9B%E3%83%BC%E3%82%AB%E3%83%BC%E3%82%BA%E3%81%A3%E3%81%A6%E4%BD%95%EF%BC%9F,%E3%82%B9%E3%83%9D%E3%83%83%E3%83%88%E3%81%A8%E3%81%AA%E3%81%A3%E3%81%A6%E3%81%84%E3%82%8B%E3%80%82)  
ひさしぶりにマレーシアにいったら発展具合がすごかった  
[マレーシアの国の平均年齢は28.5歳](https://denko-ad.co.jp/asean/news/news_191101.html#:~:text=%E5%B9%B3%E5%9D%87%E5%B9%B4%E9%BD%A228.5%E6%AD%B3%20%E9%9D%9E%E5%B8%B8%E3%81%AB%E8%8B%A5%E3%81%84%E5%9B%BD&text=%E3%81%BE%E3%81%9F%E3%80%81%E5%B9%B3%E5%9D%87%E5%B9%B4%E9%BD%A2%E3%81%AF%E7%B4%84,%E3%81%82%E3%82%8B%E3%81%93%E3%81%A8%E3%81%8C%E3%82%8F%E3%81%8B%E3%82%8A%E3%81%BE%E3%81%99%E3%80%82)  
[マクドナルドを現地では圧倒するファストフードチェーン「Jolibee」](https://www.jollibeefoods.com/)  
[東南アジアのTaxiアプリ「Grab」](https://www.grab.com/th/en/transport/taxi/)  
[GrabはGitLabユーザー](https://engineering.grab.com/how-we-reduced-our-ci-yaml)  
日本でも[S.ride](https://www.sride.jp/jp/)や[Go](https://go.mo-t.com/)はある程度便利  
もう福岡でよくないか？  
[CloudNative Days Fukuoka 2023](https://event.cloudnativedays.jp/cndf2023)の熱気  
[門司港](https://www.mojiko.info/)  
[唐戸市場](https://www.karatoichiba.com/)の寿司が美味しかった  
[Visa・JCB・American Expressタッチ決済 実証実験中 ｜JR九州](https://www.jrkyushu.co.jp/railway/touch/)  
[コンウェイの法則](https://makitani.net/shimauma/conways-law#:~:text=%E3%82%B3%E3%83%B3%E3%82%A6%E3%82%A7%E3%82%A4%E3%81%AE%E6%B3%95%E5%89%87%EF%BC%88Conway's%20law,Edward%20Conway%EF%BC%89%E3%81%8C%E6%8F%90%E5%94%B1%E3%81%97%E3%81%9F%E3%80%82)  
海外旅行に行った時の外貨小銭どうするか問題  
「小銭は駅で交通系ICカードにチャージすべし」は意外と外国人の人に有用なTipなのでは  
---
template: BlogPost
path: /138
date: 2023-08-22T06:15:50.738Z
title: ep138:夏の流星群を観察しよう
thumbnail: /assets/ep138.png
metaDescription:
---
![ep1３8](/assets/ep138.png)    

<iframe src="https://podcasters.spotify.com/pod/show/jammingfm/embed/episodes/ep138-e28csi6" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>

***

8月のお盆はコンビニの棚に秋っぽいものが並び始める時期  
@naoharuが島根に帰省した  
たぬきが出た  
なおはるの故郷は典型的な田舎  
[サマーウォーズ](https://www.amazon.co.jp/%E3%82%B5%E3%83%9E%E3%83%BC%E3%82%A6%E3%82%A9%E3%83%BC%E3%82%BA-%E7%A5%9E%E6%9C%A8%E9%9A%86%E4%B9%8B%E4%BB%8B/dp/B07CLCVH9G)  
お盆ラッシュで新幹線で帰った  
子連れにうれしいグリーン席  
未就学児童は無料  
1席に子ども2人座ると4人家族で3席で十分  
乗る前に疲れさせる  
広島空港の場所が最悪  
台風で計画運休  
JRの発表から見るコンウェイの法則  
払い戻し対応も適切だった  
[ペルセウス座流星群](https://www.nao.ac.jp/astro/basic/perseid.html)  
隕石が降ってきてる  
もしものときのことを両親と話した  
簡単に片付く話ではない  
田舎のひとだから  
花火大会行った  
[佐倉市民花火大会](https://www.city.sakura.lg.jp/soshiki/sakuranomiryoku/event_kanko/15850.html)  
数万円の有料席  
子どもは迷子になる  
やっと町内会に入った  
運用が途絶えたサイト、SNS  
子「ばぁば何言ってるかわからない」  
害虫を殺すばぁば  
大きくなると虫が触れなくなる  
セミファイナル  
水系はやだ  
早いから怖いのかも  
飲み会の割り勘  
LINE頑なに使わない民  
建て替えて現金を得る  
---
template: BlogPost
path: /139
date: 2023-09-04T06:15:50.738Z
title: ep139:本を監修させていただきました
thumbnail: /assets/ep139.png
metaDescription:
---
![ep1３8](/assets/ep139.png)    

<iframe src="https://podcasters.spotify.com/pod/show/jammingfm/embed/episodes/ep139-e28sj0d" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>

***


千葉県八千代市の梨園  
梨は大人でもウヒョーってなる  
おすすめの梨「かおり」  
アンデルセン公園  
服に対する興味の観点が変わってきた  
乾燥機にかけられるか否か  
外資系IT企業のTシャツは着やすい  
千葉ニュータウンのコストコ  
コストコは娯楽  
コストコでもうすぐ出る車を見つけるTip  
英語を勉強し始めてはや3年  
英語は賞味期限あるスキルか  
言語や文化を相対化できる  
自動翻訳ってどこまでいけるのか  
本を監修した @naoharu  
[GitLabに学ぶ 世界最先端のリモート組織のつくりかた ドキュメントの活用でオフィスなしでも最大の成果を出すグローバル企業のしくみ](https://www.amazon.co.jp/ebook/dp/B0CBR9GYF6)  
監修とは何をする人か？  
書かれている内容を確認して正しいかどうかをチェックする  
最高の働き方  
[GitLabで学んだ最高の働き方 Developers Summit 2022-02-18](https://learn.gitlab.com/c/gitlab-presentation-developers-summit?x=jbqxmq)  
SSOT(Single Source of Truth)  
お金の話  
[【保存版】Amazonで総合1位を取るためにやったことの紹介をするよ](https://kensuu.com/n/ne61fd7d2d72b)  
「いま本を書いてます」という準備段階の話を出すのは良い宣伝になるらしい  
リモートワークに限らず実施してほしい内容を記載  
  ---
template: BlogPost
path: /140
date: 2023-10-03T06:15:50.738Z
title: ep140:習慣化するコツ
thumbnail: /assets/ep140.png
metaDescription:
---
![ep140](/assets/ep140.png)    

<iframe src="https://podcasters.spotify.com/pod/show/jammingfm/embed/episodes/ep140-e2a3lrp" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>


***

花粉がきてる@naoharu 　  
千葉県香取市佐原 　  
歴史的建造物が残る街 　  
あまり知られてない 　  
伊能忠敬の故郷 　  
17歳に婿養子で佐原に来る 　  
商人として大金持ちになる 　  
趣味でガイドしているおじい 　  
三菱館にある資料館のDVDの伊能忠敬の生い立ちチャプターがおすすめ 　  
職員に言えばそこだけ見せてもらえる 　  
[安芸高田市 市長](https://www.akitakata.jp/ja/shichoushitsu/) 　  
[X 石丸伸二（安芸高田市長）](https://twitter.com/shinji_ishimaru) 　  
[安芸高田市長、市議会から「どう喝」　ツイッターで市議の居眠り指摘後　「敵に回すなら政策に反対するぞ」](https://www.chugoku-np.co.jp/articles/-/56309) 　  
[市長と議会の対立続く安芸高田市　今度は「議会を評価する市民アンケート」めぐり論戦](https://newsdig.tbs.co.jp/articles/rcc/150271) 　  
次の選挙はどうなるのか 　  
英会話の先生に日本の英語教育について詳しい人がいる 　  
日本の英語教育はネガティブ 　  
日本人受講生がそういう言い訳を言ってるからでは 　  
日本人はシャイなのでそもそもコミュニケーションが苦手 　  
明治の知識人が高等教育まで日本語でできるようにしてしまった 　  
そうではない国は英語でしか高等教育が受けれない 　  
日本の市場がでかかった 　  
[習慣アプリ　Streaks](https://apps.apple.com/jp/app/streaks/id963034692) 　  
[habitica](https://habitica.com/) 　  
習慣化は以下に小さく刻めるか 　  
[目標を達成できなければ、お金を没収？　習慣化アプリ「SIZLY（シズリー）」発表](https://www.itmedia.co.jp/business/articles/2102/22/news112.html) 　  
SNSがインプレッションに対して報酬を出し始めてる 　  
モチベーションがおかしくなりそう 　  
---
template: BlogPost
path: /141
date: 2023-10-14T06:15:50.738Z
title: ep141:年一回くらいの習慣
thumbnail: /assets/ep141.png
metaDescription:
---
![ep141](/assets/ep141.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3VdOcOTeY2ed9quGhuiAvz?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

あと13回ほど年を過ごせば50歳  
その時にはもう子供は家を出ている  
一年は人生を区切るには長すぎる  
テクノロジーが進化した今、時間感覚のアップデートが必要  
四半期を1つの区切りと考えたらせかせかしそう  
毎日の習慣も大事だけど一年に一回必ずやり続けることも大事  
Evanさんは子供の頃には「エバン」と習ったけど今は「エヴァン」  
初めての「ヴ」との出会い  
[Viridian green](https://www.google.com/search?q=viridian%2Bgreen)  
[東京ヴェルディ / Tokyo Verdy](_data/blog/2023-10-03-ep140.md)  
Vに該当する子音が日本語にはない  
[“ヴ”を生み出したのは福澤諭吉でした。「福澤諭吉が広めた日本語」](https://www.kateigaho.com/article/detail/88369)  
それが諸悪の根源  
I love youに対応する日本語  
俺らが肌色と読んでた色は今何色なのか  
[【GitLab Handbookを読む】#10 Boring solutions ／ 退屈な解決策](https://www.youtube.com/watch?v=XOvY0t0b77w)  

---
template: BlogPost
path: /142
date: 2023-10-23T06:15:50.738Z
title: ep142:遺伝子検査キット購入編
thumbnail: /assets/ep142.png
metaDescription:
---
![ep142](/assets/ep142.png)    

<iframe src="https://podcasters.spotify.com/pod/show/jammingfm/embed/episodes/ep142-e2auigl/a-aagv50a" height="102px" width="400px" frameborder="0" scrolling="no"></iframe>

***

[ChatGPT](https://chat.openai.com/) 　  
[GPT-4V](https://openai.com/blog/chatgpt-can-now-see-hear-and-speak) 　  
音声がほぼ人 　  
アシスタントがガイジン 　  
端末側で音声合成していない？ 　  
英会話もこれでできる 　  
[GPT-4をセラピストとして実行し、「認知の歪み」を診断させるためのフレームワーク『Diagnosis of Thought (DoT)』と実行プロンプト](https://aiboom.net/archives/56696) 　  
遺伝子検査キット買った 　  
[新 [GeneLife Genesis2.0 Plus] ジーンライフ 360項目のプレミアム遺伝子検査 / がんなどの疾患リスクや肥満体質など解析](https://amzn.to/3Q6slnk) 　  
EDです〜バーン！ 　  
祖先がわかる 　  
未成年は使えない 　  
NIPT(新型出生前診断) 　  ---
template: BlogPost
path: /143
date: 2023-11-11T06:15:50.738Z
title: ep143:子供の髪の結び方
thumbnail: /assets/ep143.png
metaDescription:
---
![ep143](/assets/ep143.png)    

<iframe src="https://podcasters.spotify.com/pod/show/jammingfm/embed/episodes/ep143-e2bpd8i/a-aajibm1"  width="100%" frameborder="0" scrolling="no"></iframe>

***

[MacBook Pro 14インチ](https://www.apple.com/jp/shop/buy-mac/macbook-pro/14%E3%82%A4%E3%83%B3%E3%83%81) 　  
[Intel Mac](https://ja.wikipedia.org/wiki/Intel_Mac) 　  
忙しいと事務処理能力がポンコツになる  
子供の髪を結んでみた（三つ編み）  
頭では理解できているんだけど手が動かない  
「今日はお父さんが結びました」  
髪を結んでいる時間はグルーミング説  
こういう時間が多い方が親子の絆が深まるのでは   
電化製品などの箱をとっておくか否か  
リセールバリューを考慮したらとっておいてしまう  
箱専用の部屋  

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr">【衝撃】生成AIで音声だけでなく、口の唇動きまで再現<br><br>AIを使い、英語から日本語に翻訳した動画がすごい。<br><br>こちらの動画は英語でのスピーチ。<br><br>リプ欄に続く動画と比較して変化をみよう。 <a href="https://t.co/3aihM1j35L">pic.twitter.com/3aihM1j35L</a></p>&mdash; 榊原清一 / EMOLVA Founder &amp; CEO (@sakakibara_sns) <a href="https://twitter.com/sakakibara_sns/status/1719511408570941464?ref_src=twsrc%5Etfw">November 1, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

AIによって外国語を話せるスキルは賞味期限切れを起こすのか  
生成AIによる英語翻訳はビジネスの場で使えるか否か  
言語のハードルが下がることによって文化のハードルに早めにぶつかるようになるのでは説    
AIに支援されるマネジメント業務  
ヌルヌル人材の役割が終わる  
---
template: BlogPost
path: /144
date: 2023-11-23T06:15:50.738Z
title: ep144:私には「たたかう」コマンドしかない（ゲスト：なおこさん）
thumbnail: /assets/ep144.png
metaDescription:
---
![ep144](/assets/ep144.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3u617OSgcXxVS2YNz63g5D?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

過去のなおこさんゲストエピソード：  
[ep45:サウナイキタイ](https://jamming.fm/45)  
[ep46:カーシェアリングで死にかけた](https://jamming.fm/46)  
[ep47:男尊女卑社会](https://jamming.fm/47)  
[ep85:職人](https://jamming.fm/85)  

2年半前の赤羽一番街  
@naoharuが次亜塩素酸をかけられる  
事務職から溶接職人へ  
奴隷契約書の原本は保管している  
溶接動画  
でも死にはしない  
上司とバチバチ  
心理的安全性  
嫌なことを嫌と言えるようになった  
蟠りをゼロにしたい  
脅される指導の仕方  
古い考えの人ほどストレートにモノを言うほうが信頼してくれる  
あなたいつも喧嘩してるよね  
喧嘩して無駄な体力は使いたくない  
尊敬がなかったら喧嘩もしない  
喧嘩しながら生きる  
死ぬ日を決めている  
65歳で退職して67歳で安楽死  
溶接の最高峰資格を取得  
アーク溶接特別教育  
[電気事業法及び原子炉等規制法に基づく溶接士技能確認試験](https://www.jwes.or.jp/mt/shi_ki/pp/pdf/yousetsushi_tebiki201601.pdf)  
技術を教えてくれる人との信頼関係が無いと取得できない  
動画を公開していきたい  
技術系動画は安全性のリスクはある  
英会話  
もともと外国語学科だった  
[DMM英会話 毎日30分](https://eikaiwa.dmm.com/)  
安楽死するには自分が死に値いすることを説明しなければならない  
認知症になる前に死にたい  
お金の心配をしながら生きるのはつらい  
ゴールを決めると計画が立てられる  
終活  
人生の有限感  
[DIE WITH ZERO](https://amzn.to/3sLxUzU)  
後回しにしたときにやりたいという気持ちもなくなる  
[DMM英会話 フィリピンの先生が良い](https://eikaiwa.dmm.com/)  
trash/rubbishの違い  
駅で英語練習  
[Omegle](https://www.omegle.com/)  
※読み方はオメグルではオミグルでした  
※この収録の4日後にOmegleは14年の歴史に幕を閉じました  
インターネット黎明期の雰囲気  
[オタサーの姫](https://dic.pixiv.net/a/%E3%82%AA%E3%82%BF%E3%82%B5%E3%83%BC%E3%81%AE%E5%A7%AB)  
差別を気軽に受けれるのがいいところがOmegleのいいところ  
この世界が以下に安全かを感じれる  
なおこさんいつも戦っている  
戦闘民族  
「たたかう」コマンドしかない人生  
---
template: BlogPost
path: /145
date: 2023-12-01T06:15:50.738Z
title: ep145:日常でのAIの使い方
thumbnail: /assets/ep145.png
metaDescription:
---
![ep145](/assets/ep145.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/0LkDJPM42RojV7Il25swXV?utm_source=generator&theme=0" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

[ep135:初めての交通事故](https://jamming.fm/135)  
まだ後ろバンパーが壊れた車に乗っている @dada  
怒らないといけない時  
ネガティブなフィードバックをするのは大変  
カーディーラーとのやりとりにChatGPTを使ってみた  
AIが生成した文面でメールしたら翌週に修理された  
新しい車を購入するためのプロセスにもAIを活用してみた  
入手できる確率をAIに聞いて、それを高めていくゲーム感覚  
AIにお願いしていたら、いつのまにか自分が色々動き回っている  
[注文の多い料理店](https://www.aozora.gr.jp/cards/000081/files/43754_17659.html)  
ディーラーでの会話も録音して[Whispere](https://openai.com/research/whisper)で文字起こししてChatGPTに食わせている  
[クイズ☆正解は一年後](https://www.tbs.co.jp/quiz_ichinengo/)  
タスクの細分化、段取り  
腰が思い系のタスクにも良いかも  
[人工知能 (AI) のハイプ・サイクル：2023年](https://www.gartner.co.jp/ja/articles/what-s-new-in-artificial-intelligence-from-the-2023-gartner-hype-cycle)  

***

文字起こし：
ジャミングFMダダですナオハルですエピソード135初めての交通事故覚えてますかあったね私が初めて交通事故にあったというかっていう話であれがですね今年の6月ぐらいだった気がするんですけど今半年ぐらい経ったんですけど実はですねいまだに車の修理完了していないとえっそうなんだ発生したその日にディーラーに連絡して色々手続きを済ませたもののそこからですね部品が手に入るまで時間がかかるというのでずっと待ちの状態でじゃあ今もう後ろバンパー壊れたまま走ってるのかかるもうさそうそうなんかディーラーそこのディーラーなのかよくわかんないけど前もなんかそういう感じで修理したくてお願いしたのに数ヶ月待たされてみたいな預けたら預けたで最初1ヶ月だって言ってたのに2、3ヶ月かかるとか結構そういうこと多くてなるほどねそれからね今の状態で今の状態で今の状態で今の状態でやっぱ外社だからなんじゃないのかなパーツが少ないとかまあ俺もそう思ったよそう思ってるんだけどさでやっぱりさなんだろう待っててもさなんかラチがあかないというかさここはちょっと怒らんといかんなっていうシーンあるじゃんまあ言わないとねもうすぐですって何回も言ってんのに連絡が来ないっていう状態が続いてきたからさすがに待てるなっていう感じになってそういうのってめちゃくちゃストレスじゃんストレスだね怒らなきゃいけない状況とかさこっちが怒ってるんだよっていう空気を出さないといわゆる負のフィードバックをしないといけないっていうねそうなるべく避けたいなそんな状況をそうだねまさにそういう状況でそこでですね私考えたのはリーラーとのやりとりを生成AIに任せるっていうチャレンジしてていいね今までのメールのやりとりを全部食わせて怒ってるっていうメールをかけて命令というか指示を出すと書いてくるはいはいはいもうちょっと怒ってとかちょっと調整してそういう微調整しつつ作ってもらってその一番ストレスのかかる命令を書くっていうタスクをね生成AIにやらせるっていうこれは使えるなと思って投げたそのまま投げたら翌週に入稿できましたそれはできんじゃんそれは怒らんとね物事が進まんのよ本当に怒ったもん勝ちみたいになってんじゃんなんかよくないよなそういうのもあるけどねそううん確かにねまあねじっと我慢してる方が損するみたいなさそうね真面目にさあいやーそれやだな向こうから連絡来ないのやだなそのもうすぐいついつ予定ですっていうのをこうその予定が守らないられないことが最悪いいんだけどうんその時の連絡は向こうから欲しいなああそうだねどうなってるとかまた遅れますっていうのは先に言うと先に言ってほしいよね先に言ってほしいかなうんそうなんですよああいやそういうことかいやそれはいい使い方ですねいやこう結構いい使い方かなって思ってまあなんかディーラーとのやり取りって本当コンテキストも少ないじゃん本当車を修理したいっていうだけのさうんやり取りだからさうんそれ以外の情報ってほとんどないし決まってるからさそういうのもなんか任せちゃうっていうのは結構ありかなってうん日常生活での活用方法としては全然ありかなと思ってああいいねうんはいはいみたいな使い方をしてやっと物事が動き出すっていう感じで先週かな取りに来てくれて普段だってあの持って行ってるんだけどディーラーまで取りに来てくれてうんうんしかもなんか新車のレンタカー借りてきましたよとか言ってためっちゃ逆になんかそっち側に触れてきたかっていうまだ新車に香りがするレンタカーですよっていやもういいからって言ってたからいやもういいからって言ってたから修理してくれって言ってなんかそれはそれでちょっとねちょけとんなーって感じがするよないや本当にそうですよまあ分かりますすごい分かりますそのコストがかかるやつはやってもらうっていうのはあって自分の会社の製品のgit labで言うとうんちょっとね負のフィードバックってことじゃないんだけどうんあるmerge requestのレビューを依頼することをaiがやってくれるってのがあってうんうんSuggested review as っていうのでaiの機能が最近出たんだけどうんそのコードの変更とかそのmerge requestの内容とかを見てあーこの内容だったら誰々さんにレビューしてもらった方がいいですねっていうのを自動的にアサインするっていうのがあってああ結構merge requestのレビュー依頼するの結構難しいしうんうんうんブーイライスの結構俺結構 mp 使う 忙しいのに悪い波てもってなぜかにそれがやっぱりとありがとう一応しかも内容とか も加味してくれてうん なんかそれ心の負担を減らす効果はいいよなーっ て思ったなぁ確かに確かに相手も多分今後相手も例えば俺に今の ディーラーの話だと相手がもう一旦 ai挟むと思うよそのうちそうで受け取る時 に受けてるってきてねめっちゃストレスすごいじゃんそうさあなんか電話の サポートとか絶対ウェイみたいなめっちゃ飛んでくるじゃんねそういうのメンタルって 結構強く持たないとできないだろうなと思うしそうだねうん 一旦その間に挟んで柔らかくして受け取るっていうお互い全面に ai ああああああいやいいかもねそれは これはであれなんですよ我々が目指してたぬるぬる人材なんですよ だからねそうなんだよ完全に代替されてるんですよはいはい はいじゃあ もういっかっていうね俺は俺もそれとねまあその車関係の話でねもう1個 ね d ラートにやりとりしてて自分が最近車欲しいなと思ってるってまあ買い替えと いうか開会もちょっと考えたりしてて欲しい車が まああってまぁ1年くらい前からちょっと狙って全然まだ発売もまだされてないんだ けど発表が半年くらい前に半年くらいのかさんに3ヶ月がもやってその前にやっぱり ディーラーと関係作っておこうと思って家の近くにディーラー入ってでいろんな情報 交換したりしてして情報もらうっていうのやってたんだけどまあそのやりとりもここ 最近 ai にちょっとお願いしようかなみたいな うんまあそのメール入れてでその会に対してはその車をまあ手に入れるっていうこと がタスクのゴールですっていうのを言って でその車っていうのはあの今月もうすぐ発売されるんだけどめちゃくちゃ 人気が高くて各販売店2台とかしか 出てこない2年間で2台とかええしか手に入らないぐらい生産量も 少なくて人気も高いんでほぼ抽選みたいななるほどねなるっていうのがあって まあでもそれを手に入れるっていうのをゴール して ai にもそのやりとりを聞かせてを渡してでそのインターネットで 調査させてこんだけあの入手困難だっていうのを分からせて でどうアクションすべきかっていうのをアドバイスをもらいはいはいまあ ディーラーの人に対してはこういうふうに接してこういうことをしてみたいなタスク を出してもらうみたいな感じでやっててでそれを俺がこうやってくっていうそれは 具体的などういうタスクがサジェストされるの?なんかだからお菓子の箱の下に小判を入れて 持って行った方がいいよとかそういういやもうそんなんじゃなくてその購入までの プロセスっていっぱいあるじゃんだからこの段階でこれを確認しなきゃいけないとか 購入方法を聞かなきゃいけないとかあーなるほどねそうその抽選の方法だったりそのその その前に準備しなきゃいけないことのリストみたいなとかディーラーにここでこれを 確認しなきゃいけないことのリストみたいなのを作ってもらってでそれをあのこなしていくっていうかあーそういうことかそのどっちかっていうとそのなんだろうダダのやるべきこととかを出してくれて まあ確認してほうがいいこととかをやってくれてどっちかっていうともっとプロアクティブに 買収するとかそういうなんかこうあーそうそうそういうねもっとこうそういう野心的なものではないってことかではないけどねまあでもなんか他の販売店に行って複数のエントリーするとかそれは結構NGだったりするから方法としてはなんかあったりだとか関係はやっぱり気づいてそのディーラーの営業担当の中での優先をどんどん上げるためにこういうことをした方がいい返事が来たらもうすぐ返すべきだよとかあーなるほどねそういうなんかどういう態度で接すればいいかみたいなのもアドバイスしてくれてまあその通りその通りこれがでメールもメールに返信も書いてもらってあーそうするとなんか向こうがメールじゃ言えないこともあるので会いましょうみたいなメールが来てそれをAIに返せるとこれはかなり良い兆候ですあーなるほどねそれはいい残したくないものもあるからねっていうねそうそうそうそう言えないことをこう言ってくれるっていうことなんでじゃあここでこれを確認しましょうみたいなのを言ったりだとかで面白いのがねなんかねあのその手に入れれる確率っていうのを無理やり出してもらうよAIに今あなたのそのこの状況そのインターネットで調べた感じだとめちゃくちゃ倍率が高いですうんで抽選になりそうですで今現在あなたが入手できる確率はうん5パーセントから20パーセントですとかって出してもらううんその無理やりそれを無理やり出してもらうそれが正しいかどうかはわかんなんだけど出してもらって次にディーラーからこうね会いましょうってメールが来るじゃんうんそのメールを渡すわけそうするとそのパーセントがちょっとあるなるほどねそうそれが20パーから40パーになりましたみたいなみたいになるわけ リアルがはいはいはい これ面白いな面白いね いやそれなんかいいね もうちょっとなんか野心的に提案ができると面白いんだけど 僕らはあのなんだろう三森最後の時のもう一声みたいな値下げ 交渉は僕らはしませんのでもう定価で大丈夫なんでって言った ほうがいいですよとかなんかそういうのとかないのなんかもうちょっと ああこうなるほどねわかんないそういう のが出てくるのかもしれない今後交渉の中で うんとかそう 車庫証明車庫証明まあなんだろうね 残価設定ローンのやつとかはもうやりますんでとかなんか 販売店が利益が出るようになるべくとかね そうねあでもそういうテクニックだと思 うよやっぱりうんそうだよね 優先度上げるっていう ところはうん ああそっかいやいいね でも思うのがさなんか俺が最初さそういうゴールをさそのaiに対して 与える与えたはずなのになんか全部俺がやってんだよ なるほどねあれやってこれやってみたいな さaiとの競争ってこれかってちょっとね あれって思ってなんかあれなんで俺焼きそばパン買いに行ってる のかなみたいななんかお願いしたのになあってお願いしたのになあって いい焼きそばパンを手に入れるためにはまずは玄関に行って靴を履きましょう ってそうそうそうそう あれって宮沢賢治のさ注文のお入りのやつ あれあれってね 自分で粉パタパタやってそうそうそうおいしい料理が食べ たいと思って行ったらなぜか最後自分が食べられる準備をしていた みたいなやつなんかあの気持ちになるみたいな そうねいいですね本当aiは賢いからさ全然 俺よりaiまあそのでも譲れない条件とか さ例えばそういうのも言ったりすんの 例えばaiが勝手にさもうボディのカラーは何でもいいんでとかって 言ってたらちょいちょいちょいってなるじゃん例えば でもそこはだからねちょっとちょっとちょっとちょっとちょっとちょっと ちょっとちょっとちょっとちょっとちょっとちょっとちょっとちょっと すっごくいいねあそこはだから自分の好きなそこは 修正とは全然できるしまあまあもちろんね さすがにいやいやそれはちょっとってまあでもそれしたら多分通りやすい とかあるんだったらそこはやっぱ人間が判断するべきところだから まあ提案だけしてもらってねそうそうそう それプロンプト次第で本当に一番手に入れる方法をとにかく出して くれるっていったらそれは多分出てくるし 出てくるかもしれないあーなるほどね あー面白いあー面白い いやこれがもういいかなーって思ったら面白い 有効活用してますね面白い面白いというか いろいろ試してるんだけどさディーラー行って そのディーラーで話したことも報告しようと思ってさiPhoneで録音しててさ それを文字起こしでOpenAIのWhisperっていうAPIで文字起こししてそれでそれを報告するみたいな こういう話をしたみたいなのを言ったりとかそういうのをやったりとかしてるけどね なるほどねもうなんかすごい世界だなって思うけどでもこのやり方 今のこのTipを1年後の機引き直したらどうなるのかっていうのは気になるけどねこんなことしてたの?みたいな 本当にこのスピード感でいくと1年後全く予想ができんなって感じになってなんか えーみたいになってそうだよね1年前チャットGPT出たぐらいの話だったプロンプトとかも全然わからんって状態だったからなすごいよね 1年後ちょっと今予想してよこの車のやりとりどうなるかっていうクイズ1年後に答え合わせみたいなやつあるじゃん テレビで知らない?そうなの知らない 誰々が離婚してるとかそういうなんとか予想するやつだよね でもそういうのもあるしねそういうやつがあってさまずあれだ 多分さっきも言ったけどディーラーもAIになってるんじゃないかなわかんないけど 早いところはもうなんかそういうメールのさばきとかは気づかないだろうけどやってたりするのかなはいはい確かに技術的にはもう全然可能だし受信ボックスに入った瞬間に変身のドラフトがパンって作られてあとは人間がポチッと押すだけみたいな状態になるっていうのはなんか全然ありそうかなとは思うけどね確かにいやーいいっすね どうなってるかうんうんそうねなんかAIのいることの良さっていうのはやっぱそういうのもやってると思うけどなんかね自分一人でやってると自信がないんだよ特にそういうやったことないこと初めてのことって自信ないじゃんインターネットから調べるけどこれで本当にいいかなって思うじゃんうんうんうんでもそのAIがいてくれるとあのAIって結構チューニングされててやっぱり人間のことを否定しないっていううん感じでやってくれるのよ変身としてくれるのよやっぱり意図を汲み取って否定しそれダメとかって絶対言ってこないからはいはいはいいいですねっていう感じでじゃあその前提でこっちが与えたことが正しいという前提で話をしてくれるからなんかね背中を押してくれるというかなるほどね物事が進みやすくなるっていうのはあるこれって我々がこのジャミングFMで言ってたあの何回も出てくる納得は全てに優先するっていうはいはいところをやってくれてるんだろうなってすごくね感じるんだよねいいねいいねでかつかつこのボードキャストで前回言ってる人生最初の生き方を一回目のやつは結構失敗しがちみたいなのをサポートしてくれるっていうそうそう私そのAIの振る舞いっていうのはGitLabの価値観でもあった悪意を想定しないっていう感じで接してくれてるなっていう感じがすごくするなるほどねAssuming positive intentはいはいそうはいはいはいはいなんかね物事がどんどんこう進みやすくなる感じがするあーいいですねまあ俺が結構自信ないから動けなくなることは結構あるからって話なのかもしれないけどはいはいはい確認してこれでいいって言ってもらえるとじゃあそれで行こうってなるしなるっていう感じがすごくねあるしタスクを出してくれるっていうのはアクションに移しやすいからさやっぱりうんそうねそういう意味ですごく効率化というかいろんな物事が進むなーって感じがする確かにアクションが明確になるってことはねアクションが明確になるっていうのが確かにいいねなんかうんうんそれは重要だな何したらいいかがね人間のその仕事できるできないもそこじゃそうそうそうそういやマジで同じこと言おうとしたそうなんだよタスクの細分化とかねそういう段取ってやるっていうのがねうんいやー本当にすごい世界ですよもう我々やることないよこの先10年後とか本当にそうね分かりますそれはいやーすごいすごいなーって思いますけどうんまあでも本当にこうどんどんなんかさ俺さ結構あのー家事とかやる時ってさ特にその洗濯機とか食洗機とかルンバとかってうん常に何か動かしてたい人わかるわかるわかるリソースを自分が違うことにそうそうそうそうわかるわかる止まってるとなんかうわって思っちゃうんだよねあーだから自分が何かしてる間に何かが動いてるって状態が特に家事においてはうんすごく思っちゃうんだよねうんうんうんリソースを効率的に回すみたいな感じのわかりますその中でその感じがね最近わかりますわかりますわかりますあの生成AIとのやり取りも感じてて生成AIってすぐ回答出てこないじゃんあーそうねそうでやり取りをこう何回もしなきゃいけないじゃんもう常に右手にスマホを抱えてそれとやり取りしながら一回やり取りしてポケット入れてしばらくやったら出てきてるかなって見てで常に動かすってそれをしかも複数のスレッドでやるみたいなことを最近やっててもうなんかLINE開いてるよりチャットGPTアプリ開いてるほうがなんか長いんじゃねーかみたいななるほどねなんかねそんな状態になってんだよねはいはいすごいよなんかねあの深津さんが出してるエージェントあのGPTって結構カスタマイズできるモデルが出てで深津さんが出してるオートエージェントっていうやつがあるんですけどこれは結構優秀でなんかタスクをポンって最初にプロンプト入れるとまあ結構曖昧なやつでもいいんだけどそれをまずタスクに分解してくれるんですけどでそのタスクを問題なければ上からそのエージェントが一つずつこなしてってくれるみたいななるほどねそれはもうあのインターネットで完結するものだけで出てくるんだけどうんそう調べてまとめてとかまずこれを調べてからみたいな感じでうんそのタスクを全部出してくれるでそれはもうほんとね最初にプロンプト入れてあと次次次っていっていってればすべてタスクが完了するみたいなはいはいそういうエージェントだったりするんだけどそれを動かすとかああ動かし続けるみたいななるほどねはいこんな感じっすよもうあー確かにいいねそういうまあいいねその常に何かぶん回しておかないと損した気になるっていうのはちょっと健康的かは知らないけどそうそのなんか思いっこしまあ結構だから前半の話と似てるのかな嫌なメールに対応するのとかもそうだしちょっと腰が重い系のタスク年末調整とか確定申告とかやりゃすぐなんだとは分かってるんだけどみたいなのを後押ししてくれるのはいいかもねなんかやんないといけないことリスト化してとかさなんかこうそこまでブレイクダウンされると確かにっていう気もするしそうそうそうそううんなるほどねそうそういう感じかそういう感じかまあ2023年の11月時点ではこういう感じか今の時点ではねどうなってるか分かんないけどうんそうですねもうなんか今週今月はめちゃくちゃねオープンAIのデブデイがあってマイクロソフトのイグナイトがあってうんいろんな話もあったしねサムアルトマンが解雇されてMSに入るとかさねすごい1週間ですげえことなってたねすごいうん今まさに本当に勝ちって感じのなってるしそうねうん先週自分もねあのビッグサイトでやったGoogle Cloud Nextはいはいジャパン行ったんだけどもうね登壇の98%くらいもAIなんだよね登壇テーマのそうだろうねそうええみたいなそうでパートナーブースもほぼほぼねそうだねほぼ全部AIみたいなうんうんまあなんでねちょっとすごいなって思う一方でただまあガートナーのハイアップカーブでジェネレティブAI今あの1番ピークなところにいるからうんうんうんていうことも忘れずにいたいなってちょっとは思うけどうんうんうんうんなんでねうんうんいつかはゲンメツケ来るっていういったん落ち着くっていうねうんうんうんいやでもまだ実はもうね 坂を登り始めたとこでしたみたいなまだまだやりましたあと何もない毎年なんか全然変わってくる気づいたら下がってるとかあれみたいな戻ってますけどみたいなあるある分からんけどそうね世の中があまりに早すぎるというかうんなんかSFの世界になってきたなって感じが確かにね確かに確かにこんなこんな選手いなかったもんねこんな10年毎年一応バズワード出るじゃんうんウェブ3とかさそうウェブ3とかねちょっとね違うもんねブロックチェーンブロックチェーンとかねあんまり身近な人は身近なんだろうけどここまで生活に影響があるレベルっていうかうん結構影響範囲大きいからそうそうねかなりそっか車ねうんいやいいね車ねもううちもねちょっとほんま欲しいなって思って駐車場がなくてちょっとね本当抽選とかになるってこと?そう取れなくて周りのやつ高いしねうんそうそうそうちょっとそれAIに聞いてみるわいい駐車場の探し方とかなんかあなたは本当にもう大変なエリアにちょっとそれアドバイスまたしてくださいどういう風にプロンポットすればいいかあーはいはい多分あのAIは我々が話した過去のエピソードで話した駐車場は足で探せっていうエピソードを学習して多分それを言ってくるじゃんそれやっぱ結局俺が探すってことね?うんそれやっぱ結局俺が探すってことね?はいはははは結局まず靴を履いてくださいってはははははいはいやりますそういう感じだよまあねはいはい了解しましたちょっと次の打ち合わせが始まるんではい今日はそんなとこっすかねはいフィードバック等はhashinojamming.fmでXでポストしてくださいショーの後はjamming.fmで公開してますねそちらもご覧くださいはいはいじゃあ今日はAIネタでしたはいはいお疲れさまでしたお疲れさまでした
---
template: BlogPost
path: /146
date: 2023-12-18T06:15:50.738Z
title: ep146:神経締め
thumbnail: /assets/ep146.png
metaDescription:
---
![ep146](/assets/ep146.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3zguEP5rJnsOnKVqoSOHHV?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

AIに希少な車購入のためのアドバイスをしてもらったら本当に抽選に当選して購入できた 　  
[MacBook Pro M3 Max 16インチ スペースブラック](https://www.apple.com/jp/shop/buy-mac/macbook-pro/16%E3%82%A4%E3%83%B3%E3%83%81-m3-max) 　  
静かすぎる 　  
Max使用用途：複雑な3Dコンテンツをレンダリング、数十億のパラメータを使ってTransformerモデルを開発 　  
MagSafe 　  
140Wは環境負荷が高い 　  
小さいACアダプタは発熱がすごい 　  
[AOHI 140W PD 充電器 USB-A & USB-C 3ポート](https://amzn.to/3t2p4OB) 　  
有線キーボードもかっこいい 　  
[HHKB Studio](https://happyhackingkb.com/jp/products/studio/) 　  
静電容量無接点方式ではない 　  
[ソフトバンクのiPhone14安い](https://iphone-mania.jp/news-561171/) 　  
[ahamoのローミング](https://ahamo.com/services/roaming-data/index.html) 　  
牛久市で娘(小学2年生)の器械体操の大会があった @naoharu 　  
チーム競技がある 　  
上には上がいる 　  
いつからその経験をすべきか 　  
水泳はやっててよかった 　  
[茨城限定 イタリアンレストラン Groovy](https://www.pasta-groovy.co.jp/) 　  
パスタとの相性が悪い@dadaism 　  
4年ぶりに長崎出張 　  
[天洋丸](https://www.tenyo-maru.com/) 　  
@dada_ismが４年前に撮った映像[長崎橘湾 中型まき網船団 天洋丸 Full ver.](https://www.youtube.com/watch?v=v2_ws5m9j98) 　  
天洋丸の新事業 ニボサバのPR 　  
神経締め 　  
鮮度が高いとアニサキスが少ない 　  
2024年3月に第1回全国煮干しサミットが開かれるらしい 　  
佐賀県 嬉野温泉 　  
佐賀県 唐津市 　  
田舎は世代交代がすごい 　  
[一年漁師](https://tenyo-maru.com/index_qhm.php?%E4%B8%80%E5%B9%B4%E6%BC%81%E5%B8%AB) 　  ---
template: BlogPost
path: /147
date: 2024-01-18T06:15:50.738Z
title: ep147:遺伝子検査キット結果編
thumbnail: /assets/ep147.png
metaDescription:
---
![ep147](/assets/ep147.png)    



***

<a href="https://jamming.fm/142" target="_blank" rel="ugc noopener noreferrer">ep142:遺伝子検査キット購入編</a>   
<a href="https://www.amazon.co.jp/%E9%81%BA%E4%BC%9D%E5%AD%90%E6%A4%9C%E6%9F%BB%E3%82%AD%E3%83%83%E3%83%88%EF%BC%9CGeneLife-Genesis2-0-%E3%82%B8%E3%83%BC%E3%83%B3%E3%83%A9%E3%82%A4%E3%83%95-%E3%82%B8%E3%82%A7%E3%83%8D%E3%82%B7%E3%82%B9-%E3%83%97%E3%83%A9%E3%82%B9/dp/B09CGN1L5F" target="_blank" rel="ugc noopener noreferrer">新 [GeneLife Genesis2.0 Plus] ジーンライフ 360項目のプレミアム遺伝子検査 / がんなどの疾患リスクや肥満体質など解析</a>  

<iframe width="560" height="315" src="https://www.youtube.com/embed/yTAnFRuvVAw?si=ZAm7JMGf85mYIUAD&amp;start=2200" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

「どうも、紅海を渡ってきたグループです」  
EDなのかが気になる  
日本人だけでも十分に多様だ（そもそもDNAレベルで多様性があるなら国籍なんて最近できた枠組みは関係ない）  
<a href="https://ja.wikipedia.org/wiki/%E3%83%92%E3%83%88%E3%82%B2%E3%83%8E%E3%83%A0%E8%A8%88%E7%94%BB" rel="ugc noopener noreferrer" target="_blank">ヒトゲノム計画</a>  
---
template: BlogPost
path: /148
date: 2024-01-20T06:15:50.738Z
title: ep148:2024年やってみたいこと
thumbnail: /assets/ep148.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/7fM51elVN59cSNAKjHd8J2?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

去年車のエンジンがかからなくなった 　  
ガス欠？ 　  
原因はバッテリー故障 　  
空手の通信講座 　  
[トルクレンチ](https://amzn.to/3RYpFsY) 　  
[禅とオートバイ修理技術](https://www.amazon.co.jp/dp/415050332X) 　  
自転車のメンテ楽しい 　  
自分でやるのは安くはならない 　  
  
  
***
  
  
文字起こし：  
ジャミングFMダダですナオハルです2024年になりましたおめでとうございますおめでとうございます今年何やろうみたいな年初の今年の目標じゃないけど最近ちょっと新しくチャレンジしたいなって思ってることがあって年末にこのエピソードもちょっと話した車を修理出すっていう話があったと思うんですけどあの付近でここで言ってないかもしれないけどエンジンがかからなくなったんだよね急にそうなのそうで修理とはまた別の話なんだけどそれで修理したってわけじゃないんだけどその修理を出す直前にエンジンがかからなくなってで直前に妻が乗っててそうなってで最後にガソリン少ないランプがついてたっていう状況はいはいはいだからそれで結構乗ってたっていうからまず疑ったのはガス欠かなみたいなって思ってねで妻がガソリンスタンドでオイルガス買ってで入れたんだけど動かないとで結果JAF読んだんだけどで結果要因はバッテリーが死んでたっていうなるほどそうそうそうで確かにもう5年半ぐらい乗っててバッテリーもその間変えてなかったんでメーカーで大体3年交換とか言うんだけど実質もっと使えるんだけど5年とかやっぱり寿命が来るってことはあるんだけどうんでバッテリーが死んでたっていうのでバッテリー交換をその時に修理ちょうど預ける時だったんでそのバッテリー交換もやってもらったみたいな経緯があってはいはいはいでバッテリー交換もねあれ1回やると作業工賃含めて多分5、6万するんですよねあーそんなにすんだねそうそうそうバッテリー自体の値段も含め?あ、含めだあー多分バッテリー自体2万とかじゃないかなあーなるほどねはいはいそうで工賃とか2、3万そうねそれでプラス工賃みたいな感じだと思うんだけどうんなのでなんかそういうのをさ聞いててさなんかバッテリー交換ぐらいだったらなんかさやれんじゃないかなみたいな思ってさうんあーわかんないけどまあまあなんかうんできそうなバッテリー交換するみたいにさはいはいそうやってなかったっけないかなってはいはいはいはいはいで今年はさそういうのもあってさちょっとなんか車車の整備自分でやりたいなって思っててうんうんそういうなんかちょっと簡易的な自分でできそうなものからやっていきたいなって思っててはいはいでなんかちょっと調べたんだけどさあんまそういうのをさ教えてくれるところってないないんだよねなんかあーやってくれるとこはあるけどあーそうそうそう教えてくれるっていうところかなるほどそうそうそうはいそういうのをさ何かそうそうなるほどねなんかね車整備教室みたいなやつってさあんまないじゃん聞いたことないでしょないねそうなんだよだから自分でやらない自分でねその調べてやらないといけないっていう感じでまあ最近はYouTubeとかあるからさまあねまあそれ見ればいいって話なんだけどまあ実際やるのとさ見るのってなんか全然違うじゃんまあ確かにねあってもいいような気がするね筆ペン囚人みたいな感じでああそうそうそうそう自動車整備通信講座赤ペン先生がこうやってくれるどうやってやるのってやる通信講座空手の通信講座みたいななるほどねはいはいそうあんまないなと思ってまあでちょっとそれチャレンジしようかなっていうのは今年ですね素晴らしいでまあでもね結局さ車ってさやっぱ命に関わるじゃんまあ乗る方もそうだしそうだねまあバッテリー交換とかまあそうだね整備不良的な意味もそうだよねバッテリー交換とかはちょっとあのなんていうの全然知識なくてなんかやっちゃったら危ないっていうのもあるし例えばそうそうそう作業中って意味でも危ないしああそうですね作業中も危ないしもしなんかブレーキとかだったらさもっと大事故につながるそうだねそうだねそういうのもあるからちょっと怖いっていうのもあるんだけどうんまあ確かになんかうんまあちょっとそこチャレンジしてみたいなうんっていうのは今年かなっていうねなるほど早速あのトルクレンチを購入したっていうねああトルクレンチってあの締め付けるそのうんトルクの強さがそうそうそう測れる測れるっていうねだいたいあの車のボトルナットってその何ニュートンニュートンでこう締めるみたいなの決まってるんだよねそうそうねはいはいそれを測れるっていうねそう買ったりとか素晴らしいまあそういうのからやっててうんねバイクやってる人とか結構自分でそういうの直したりするじゃんああそうだねうんそうそうそうなんかああいうのは憧れるし憧れるねねうんあのあの本ゼントオートバイ修理技術うんでも紹介されてるそうそうそう自分で理解してやるっていうことの大事さとかまあねあの辺そういう哲学じゃんなんだろうそういうそう技術的なテクニカル的な部分の哲学みたいなうんそう話なんでああいうの読んでるちょっと自分でやってみたいなとかもちょっとねあの本はそういううん本だねなんかまあ大きく2つ書いてるよねその自分で全部把握してなんか悪いこと不具合とかは起こるんだけどそれは全部把握全能全知全能そのこのオートバイに関してはっていう領域に達することはないんだけどそこに常に目指してるんだけど結局っていうところでも読め 話 Grêmand Brølarınそういうパートいった表現僕が意味では話言ってしまうそういう意味ではないし Grêmand Brøcourseそういうふうな口が来られたっていそういうものでウソつくりつけそうてないです crownこっちももっと行くよと羽目みなさんも言ってましたね面白いねでもクレーマーはそれの一番複雑なやつな気もするけどそううんまああんまりこうねエンジンとかさそういう深いところは行かないけどまあなんかタイヤ交換とかはねタイヤ交換はマジでできた方がいいと思うパンクした時とかはね後ろのスペア出してジャッキでシャーシ上げてとかはできないと困るからねそうなんかそういうのもさなんかYouTubeとか見てるとほんのそのTipsみたいなものでこうしとくといいですみたいのがあるんよやっぱりああこうした時にこういうの事前にやっとくと安全ですとかはいはいはいなんかそういうものがいっぱいあるような気がしてて確かに確かに一通りなんか目の前でやってくれると説明してくれるとかそういうの拾えそうだけどうんあんまりこうYouTubeとか研修されたもの見ちゃうとなんかそういうとこ抜け落ちちゃうみたいなのがありそうでちょっと怖いんだよね確かにそうねうん自分も学生時代から社会人始め4,5年目ぐらいまで自転車のピストっていうの乗ってて全部の自転車自分で交換ばらしてベアリングも投油で洗ってとかできるようになったんだけど楽しいやっぱそこはそれを全部自分でメンテするっていうのはすごい楽しい楽しいけどコツとしては自分でやった方が安いから自分でやるっていうのは成り立たなくてっていうのは失敗とか間違った工具を買うとか間違ったパーツを買っちゃうとか壊しちゃうとかっていうのを最初から予算に組み込んだ方が絶対俺はいいと思っててそうだろうね失敗も楽しいからあーこうやったらここはまらないじゃんとか言ってあーみたいなとかそれはあるなやっぱ失敗をあらかじめ失敗できるようにしとくっていうのはエンジニアリングとしてすごい重要な姿勢だなってあれで思ったっていうかそうねそれはあると思うなんでなんかそれができるようになったらやっぱ新車の時ってそれが怖くてできなくて今だからできるみたいなあーなるほどねそういいねそれはみたいな感じですはいまた失敗したらちょっとここで話すんであーそれもいいねそれを共有していくのはそんな話を残しておくんだけどまたこれを10年後とか聞いたらやらんかったなーみたいな思うんだろうな結局みたいなね一応残しておくと言うだけタラなんで今はそう思ってるとはいはいじゃあフィードバックはハッシュのチャーミングFMまでお願いしますはい以上ですはいありがとうございましたはいありがとうございましたお疲れ様でした---
template: BlogPost
path: /149
date: 2024-01-27T06:15:50.738Z
title: ep149:高校受験の日にダイアルQ2の請求19万円が自宅に届き心臓バクバク
thumbnail: /assets/ep149.png
metaDescription:
---
<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1NAfn3yUnwecMZmUGyNkPQ?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

<a href="https://ja.wikipedia.org/wiki/%E3%83%80%E3%82%A4%E3%83%A4%E3%83%AB%E3%82%A2%E3%83%83%E3%83%97%E6%8E%A5%E7%B6%9A#:~:text=%E3%83%80%E3%82%A4%E3%83%A4%E3%83%AB%E3%82%A2%E3%83%83%E3%83%97%E6%8E%A5%E7%B6%9A%EF%BC%88%E3%83%80%E3%82%A4%E3%83%A4%E3%83%AB%E3%82%A2%E3%83%83%E3%83%97,%E3%81%97%E3%81%A6%E6%8E%A5%E7%B6%9A%E3%82%92%E8%A1%8C%E3%81%86%E3%80%82">ダイヤルアップ接続</a>  
<a href="https://ja.wikipedia.org/wiki/ISDN">ISDN</a>  
<a href="https://ja.wikipedia.org/wiki/%E3%83%80%E3%82%A4%E3%83%A4%E3%83%ABQ2">ダイヤルQ2</a>  
<a href="https://ja.wikipedia.org/wiki/%E3%81%BE%E3%82%93%E3%81%8C%E6%97%A5%E6%9C%AC%E6%98%94%E3%81%B0%E3%81%AA%E3%81%97">まんが日本昔ばなし</a>  
<a href="https://ja.wikipedia.org/wiki/%E3%83%96%E3%83%A9%E3%82%AF%E3%83%A9">ブラクラ</a>  
<br>  

  ---
template: BlogPost
path: /150
date: 2024-02-04T06:15:50.738Z
title: ep150:今度キューバ行こうと思うんだけどおすすめある？
thumbnail: /assets/ep150.png
metaDescription:
---
<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/05h6ITNEKWClljtAHaVQcp?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***
  

<a href="https://about.gitlab.com/events/summit-las-vegas/" target="_blank" rel="ugc noopener noreferrer">GitLab Summit 2024 in Las Vegas</a>  
<a href="https://reinvent.awsevents.com/" rel="ugc noopener noreferrer" target="_blank">AWS re:Invent | Amazon Web Services</a>  
<a href="https://www.youtube.com/watch?v=7r9mo-QwBbM" rel="ugc noopener noreferrer" target="_blank">GitLab Summit - Live from Crete Day 5 - YouTube</a>  
<a href="https://ja.wikipedia.org/wiki/%E3%82%AD%E3%83%A5%E3%83%BC%E3%83%90" rel="ugc noopener noreferrer" target="_blank">キューバ</a>  
<a href="https://www.waseda.jp/inst/shinkan/circle/3119" rel="ugc noopener noreferrer" target="_blank">世界旅行研究会 – 紺碧の春</a>  
<a href="http://dada-ism.net/category/travel/cuba/">Cuba - dadaism.</a>
キューバの入国スタンプいらない  
キューバの通貨は二種類ある  
<br>
<br>
  ---
template: BlogPost
path: /151
date: 2024-02-13T06:15:50.738Z
title: ep151:マインドフルネス講座を受けた
thumbnail: /assets/ep151.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/2NaVVajPBCRFYi8GPq3Cd9?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***  


JTC ジャパニーズ・トラディショナル・カンパニー（伝統的な日本企業）  
マインドフルネス講座  
認定マインドフルネスインストラクター  
マインドフルネス＝瞑想？  
[鈴木俊隆](https://ja.wikipedia.org/wiki/%E9%88%B4%E6%9C%A8%E4%BF%8A%E9%9A%86)  
[ジョン・カバット・ジン](https://ja.wikipedia.org/wiki/%E3%82%B8%E3%83%A7%E3%83%B3%E3%83%BB%E3%82%AB%E3%83%90%E3%83%83%E3%83%88%E3%83%BB%E3%82%B8%E3%83%B3)  
カウンターカルチャー  
幸福度は50%は遺伝で決まる  
お金の幸福度への影響は10%  
幸せを考えるにはキューバに行った方がいい  
40%は意図的な行い  
[｢幸福度の50%は遺伝､10%は環境で決まる｣では残り40%は…心理学の研究が明かす驚きの"幸福度の円グラフ"](https://president.jp/articles/-/77921)  
呼吸に集中する  

<br>

***  

文字起こし：  
ジャミングFMダダですナオハルです最近ちょっと会社のつながりというか自分の会社の人じゃないんだけど社外の勉強会平たく言うと勉強会みたいなやつがあって半年くらい前から半年以上かやってるんだけどそこにはいわゆるJTC企業のITDX関連の人が各社集まっててJTCって何でしたっけジャパントラディショナルカンパニーっていう日本のキピカル大企業みたいなそうそうそう結構そういう企業がいろんな業界も電力から物流からメーカーからとかいろんなとこから集まってるっていう会があってなんかそれで自分も参加しててさ2ヶ月に1回ぐらいどっかで集まって各グループになるんでそれぞれ5、6人ぐらいのそれぞれがテーマにテーマが与えられてそれの企画を考えて企画というか何か考えてアウトプットを作ってみんなの前でプレゼンするみたいなのをやっててでプレゼンとプレゼンの間はそれぞれのグループワークみたいなのでやるんだけどそういう活動があってそれをやっててそれで仲良くなった人がいるんだけどなんかその人と飲んでて話してた時になんかその時のテーマが何かねえと思ってたんだけどなんかそういう感じだったのがなんかマインドフルネスの話になってなんか自分もマインドフルネスって聞いたことあるなと思ってたんだけどよく分かんなかったんだけどなんかその人がマインドフルネスを教えるライセンスがあるみたいななんかサーティフィケイティットマインドフルネスインストラクターみたいなのが持ってるらしくてはいはいはいCMICMIサーティファイドマインドフルネスインストラクターみたいなのがあるとそうそう G&うんなんだそれはと思ったんだけどそれでじゃあぜひぜひやってよって言ってここの前グループのその実際の物理で会おうってなった時に会議室取ってその時にちょっと1時間半2時間弱ぐらいその講座をやってもらってマインドフルネスのそのね歴史からどういうものなのかみたいなとかワークショップとかもこうやってなんとなくちょっとマインドフルネスの扉を開いたっていうの最近あってですねそうですねマインドフルネスって何なんすかその俺もねよくわかんない俺あのサーティフィケイトじゃないんでちゃんと説明できないと思うんだけどあのいわゆる瞑想に近いあーそのイメージはなんかあるそうそうそういわゆる瞑想だねうん言ってしまえばうんで向こうの方でそういう禅をあの広めてた時にまああの時の時代って結構さやっぱりあのねヒッピー文化じゃないけどこの西洋的な思想からこう東洋的な思想をどんどん取り入れる時代だったんでやっぱそういう禅みたいなのって流行ってた時期じゃんうんまさにあのスティーブ・ジョブズもそうだけどさそうだねそうその時にあのジョン・カバット・ジンジョン・カバット・ジンっていう人がアメリカ人なんだけどうんマサチューセッツ大学の医学の教授はいはいがいてその人がその禅のそういうのをマインドフルネスっていう体系化した学問として作り上げたっていうほうのがあの最初だっていうのを教えてもらったうんうんへーまあそういう中ねそういう禅をちゃんと体系的なこう学問としてこうやるっていう感じでまあ何なのかって言うとなんて言うんだろうななんかあのこんな俺が説明してもなんかすげえ薄っぺらくなっちゃってなんかすっげえ申し訳ないまあそれはなんか気持ちをこうコントロールするじゃないけどなんかそういうものはぁメタ認知的なもの自分はこう感じてるとかうんっていうそのなんだろう感覚を集中させてそれを感じる感じるっていうなんて言うんだろうねそんなエアプっていうかさ本で読んだくらいだけど禅とかがスティーブジョブズがよしと思ったらヒッピー文化でヒッピー文化ってカウンターカルチャーっていうかさそうだね物質的な幸福とかあってそれほんまかみたいなさっていうのでやってるんじゃんマインドフルネスとかってそういうのもそういう系なのかなそうそう幸福っていうのもすごくね関わってる幸せであるみたいなところとかストレスとか悩みとかそういうものに対してどう向き合うかっていうのの一つとしてマインドフルネスみたいなのが使われるみたいな感じなのかもしれないうんうん幸運幸福度幸せであるっていうのはどんな要因で幸せか不幸せかって決まるのかみたいな話があってちょっと面白かったのは50%は遺伝って言ってたんだよねお遺伝ここに出てきた要は親がポジティブというか幸せと感じやすいか不幸せと感じやすいかで子供も同じようになるみたいな要は気持ちなんだけどさ幸せかどうかってさ10%が環境って言ってたね環境って何かっていうとお金とか健康あとは子供がいるとか結婚してるとか現代の幸せってこのお金とか健康とかそこに結構フォーカスされてるんだけどそれは10%しかないんだよっていう話なるほどねうんもしになりがちだから一定の別軸を持とうぜとかさその幸せって本当にそうかっていうのは大事なんだけどさそんな余裕ねえよっていうのがめっちゃあるキューバに行ったほうがいいよそしたらキューバ行けないんだって前の放送でやったから気になる人は1個前のエピソード聞いてくださいキューバなんかねめっちゃ金なくてなんかボロボロのやつやってるけどめっちゃみんな幸せそうだから絶対そうだと思うんだよねいやめっちゃもうそれねそれが見たいそれ行きたいでもアメリカからじゃ行きたいのよ行けないんだよアメリカは金あるやつが幸せっていうのが主軸のテーマの国だからそうなんだよなそういう価値観でそういう価値観の中でいたらやっぱ金持ちが幸せってなるんだよなるからその1個上のもうちょっと資産を上げなきゃいけないってことだよだからキューバ生きてるんだよだからでも行けないんだからほんとみんなさなんか道端でもうベロベロにさラムとか飲みながらさペットボトルに出てなんか暇から裸でなんかゲラゲラしてる感じがさなんか仕事しろよって思いましたなんで仕事って何っていうねそうそう何のために生産するのとかそういう話日本でもあるけどねかまたとかねそこら辺は結構そういう感じだけどまあまあそうなんですよまあだからまあそういうまあ10%が仕事仕事じゃねえか金とかそういう環境で残り40がなんか意図的な行動みたいなこと言っててなるほどまあそれはあんまりここもあの自分も深くわかってないんだけどまあそういうなんか普段の行いとかここにあのマインドフルネスが入ってくるんだけどなるほどそういうことをしとくとまあまあそういう感じでね幸せって感じることが多い感じれるなるっていうなるほど研究があったりだとかまあ宗教とかってもここの40パーっていう部分にかなりあの宗教のそのなんで教会に行くとかなんかそういういいことをするとか他人に対してなんかそういうのがこの40%のかなって聞いてたり思ったりしたんだけどいやまあそういう意味でのマインドフルネスっていうのもあるとはい感じでしたけどうんちょっとあのなんていうんだろうかな手放しですげえとは思わない思わないっていうかなんかそういうオルタナンタビューを自分の中で持つっていうのは大事なんだけどな負けねえぞっていうなんかさ資本主義の犬でやっていくぞっていう感じもあるからさなんていうのだからなおはるはさ多分どっちかというとその幸せって感じがあるからね感じにくいタイプなんて思うんだよ幸せになるためにはそのお金が必要って思ってる方でしょ多分そうだと思う一定の条件がないとそんなことは思えないよって思っちゃうタイプかもしれないねそうあんまりこう不幸せなネガティブじゃないけどそういう気持ちというかマインドを持ってる人なんでうんでもそういう人の方がねやっぱね成長というかこうなんかドライブする力みたいなのもあるからねあるしなんか不思議なんだよねうんなーんそうほんまよよ分からんなでも成長っていうのは完全に俺ら側の概念だからね成長なんて成長ってだってそんな資本主義的なさ発想じゃん生産性とかさそうだね別にキューバの人多分そんなの考えてないじゃん言ったことないけど考えてますもう国が仕事を与えてくれてもそれだけやってればいいみたいなみたいなね知らないけどキューバ行きてーなー本当に行ったほうがいい価値観はそれは行きたいんだよなー北朝鮮でもいいじゃんって思うけどねそれだったらね競争とかっていうのじゃないっていうんだったら思うんだけどちょっとそこはまた別の難しさがあるしうんはいちょっとねそんなマインドフルネスっていうまあですねまあなんかいろんなやり方があるみたいでさ俺は教えてもらった一番オーソドックスなやつはその何呼吸に集中するみたいな自分がやってることに意識的になるっていうやつだっけそうそうそうするとさなんかあのまあ数分こうその呼吸に集中するとまあいろんなこう頭の中にいろんなことがこう出てきちゃうんだけどまあ本当はそれをあんまり考えないしなきゃいけないんだけどでもなんかその人が言ってるにはなんかその何かを考えてしまったっていう自分に気づいてでそれをまたそこを追求せずに一旦また押し込めて押し込めるというかでまた何か出てきたらそれを感じてその何自分が何を思ったかをそれをメタ認知って言ってたんだけどこう思ったんだなって客観視するっていうことですかねそうそう自分がこういうことを思ってたんだけど思ったっていうことをそれはそれを評価しちゃいけないんでそれが良い悪いっていうのなくなくて評価しちゃいけなくてはいはいその感情が出たっていうことを認知するじゃないけど認識するっていうやり方簡単に言えばなるほどねなんか自分がすごくあイライラして奥さんに対してイライラしてるなとか子供に対してなんかイライラしてるなとかっていう感情が出てきてそれをこう認知するとかみたいなのがそれ認知してるなーってなってるんだけどだだけでもやっぱ違うのかな別にそれが悪い良い悪いとかじゃなくて自分はこういうことにこう思うんだなーって思うだけでも違うっていうことなのかね多分それがストレスの低減につながったりとかなんかその気分がすごくこうなんて言うんだろうその人言ってたんだよくプレゼン前とか結構緊張するじゃんめっちゃそうそういう時にそれを一旦挟むとすごくこうこうらんじゃうとか落ち着くとかなるほどそういうことをやって使ってるみたいななんか筋トレみたいなもんでいきなりパッてやるからちゃんとできるようになるわけじゃなくてそういうなんか習慣にしてだんだんそういうコントロール自分でその感情とかそういう気持ちの部分をコントロールできるようになるっていうのが積み重ねみたいなもんだよっていう自分もめっちゃでかいディールの提案とかの前にやってみるかなんかうんえーなおはるくんってこういう時に緊張するんですねあなたはこういうね企業の人がすごいと思っててとかってなんのかねわかんないけど絶対うんまあまあそういうのもあるんだなまあ結構ね俺もねなんか結構周りで出世してる人とかって結構バインドフルーズやってる人多かったりするしえーそうなんか意外にうんまあなんかそういうのもやってもいいのかなみたいなちょっと思ったりはしたけどなるほどね心が安定するんじゃない知らないけどはいはいちょっとじゃあちょっとそんなのはいやってる人一緒だけどなツイッターとかでツイッターXとかでちょっと聞いてみたいです感想ぜひうんやってよっていううん全然俺の言ったようだ違うよってあったら教えてくださいはいはいじゃあお疲れ様でしたはいお疲れ様でした---
template: BlogPost
path: /152
date: 2024-02-16T06:15:50.738Z
title: ep152:セミパーソナルのジムに行き始めた
thumbnail: /assets/ep152.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1Rr64JQxnmwlRqIZpgDudp?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***  

[エニタイムフィットネス](https://www.anytimefitness.co.jp/)  
セミパーソナル  
1人のトレーナーが複数のトレーニーに教える  
[ドロップセット](https://joyfit.jp/akajoy/health_knowledge/post08/)はずっとコチョコチョされて変になる感じ  
[BUFF Personal Gym](https://funthink.jp/)  

---
template: BlogPost
path: /153
date: 2024-02-21T06:15:50.738Z
title: ep153:ラーメンの多様性から生き方を考える
thumbnail: /assets/ep153.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3RR9Kw0RVlWw4OXQcicoD5?utm_source=generator&theme=0" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>  
  
***  
  
最近ラーメン屋とのつながりが多い  
[横浜丿貫](https://tabelog.com/kanagawa/A1401/A140101/14075484/)  
ラーメンはそばよりも種類が多い  
ラーメンの歴史は100年くらい  
そばの歴史は平安時代くらいから続く  
時間を経て地域や文化と結びつくのでは  
島根の出雲蕎麦  
名古屋は濃いめ  
時間の洗礼  
国の歴史  
人間の年齢  
多様性受容曲線  
老害も受け入れられるべき  
[ソフト老害](https://toyokeizai.net/articles/-/732959)  
その世代しかいない世界が理想？  
人口オーナス期  
俺は俺の価値観で俺の人生を閉じる  
でも子供は俺の価値観に染まってほしくはない  
トリスハイボール4杯目

***  

ダダですなおはるですあの最近さ昔からSNSで煮干しの写真をアップするってやっててそうすると結構ねラーメン屋さんと繋がることが多くてラーメンラーメン屋さんのユニフォームを作ったりっていう去年はドイツのラーメン屋さんのユニフォームを作ったりとかであと横浜のヘチカンっていうヘチカンめっちゃ有名じゃんそう結構勢いあるあそこはあそことはちゃんとヘチカンっていうロゴも使ってちゃんと専用のユニフォームとか作ったりしててそうで先週福岡の方に知り合い知り合いというというかそのインスタ経由で仲良くなった人がいてその人がなんか福岡でラーメン屋をやること急遽ラーメン屋をやることになったんでなんか協力してくれみたいな話があったりとかSNSとかでそんなやりとかあんの?協力してくれとかさその人はもう3,4年くらい前から仲良いっていうか繋がって実際何回も会ったことあるし仲の良い漁師さん仲の良い漁師さんとのやりとりの間にも入ってきてる人でなんか結構すげえそうそうそう店舗とかも持ってて福岡の方とかにそこにこう自分の出してるグッズと卸したりしてる中で結構一番仲良いぐらいの人なんですけどすげえなそんな感じでめっちゃSNSを活用してビジネスしてるじゃんそういうノリジャミングウェイフにまでなかったんだけど嘘だっけ?そう言ってないっけ?いやでも去年青色してた人があの会議を届け出したとかしてるからそうそうそうかそうかじゃあ具体的にはそういう活動が行われてるわけっていうそうそうそうそうほうほうほうほうなるほどみたいなのあってさその話とあんま関係ないんだけどさラーメン屋って自分はラーメン食べるけどそこまで好きじゃないんだけどなんかさラーメンってさめちゃくちゃなんか種類多くない?種類多い?ラーメンってものの種類醤油とか塩とか?そうそうまさにそういう家系、二郎系とかさまあそうねとんこつ、博多のとんこつとかさ北海道味噌みたいなさめっちゃ多様だなって思ったんだよね確かにねで一方でその時思ったのは一方で蕎麦って一種類じゃほぼ?そうかなと?ほぼなんかさ蕎麦って言ったらさわんこ蕎麦あるけどさなんか大体麺つゆとあの蕎麦粉で練られた麺じゃんそれはちょっと待てそれは受け手の解像度の問題じゃないのかまあそうかわかんない、いやそうかないやラーメンほどはないじゃん絶対ラーメンもでも小麦の麺になんかのスープにうん、そうかも。でもこの辺りはこうやって食べるからね。スープ足してるだけでしょ でもスープが多様か確かにもうだって魚もそうだし 豚骨と魚介があるっていうのが全然違う鶏もあるじゃん そうだねバリエーションの幅が全然違うっていう 確かにこれ何でだろうって思って ふと思ってで行き着いたのが 歴史を調べてたのよそれぞれなるほど ラーメンの歴史みたいなでそばの歴史で調べてて ラーメンって意外に歴史が浅いんだよねそもそもラーメンって 日本で生まれたっていうよりもどっちかっていうと中国とか 中国でしょうねそう中華そばっていう 最初は中華そばって言われてたんだけどちゃんとこう入ってきて 日本でラーメン店としてちゃんと店舗として売り出したのが 1900年ぐらいまだ歴史としては多分100年もないぐらいの 100年ちょいぐらいの歴史でそこから広まったみたいな もともとはやっぱり中華街ができて日本にできてとかって そういう流れだったらしいんだけど中国のラーメンと ラーメンみたいなものとはもう日本も違うじゃん 別の進化を遂げててもうあれもほぼ日本のもの みたいになってるし一方でそばって なんかもう平安時代とかなんかもう そっからあるらしいんだよなるほどそう記録的に だからもう歴史が全然違うのよラーメンとそばってはいはいはいはいうん だからそこなんじゃないかなと思ってなるほどねうん 実はさそばもさ 生まれた時はなんかめっちゃ多様だったけどこう時間を 歴史と時間をかけことによってそばっていうものが これがそばっていうのは固まっちゃって今現代においては ラーメンほどのバリエーションがないっていううん なあまあうん なるほどねそれはなぜかっていうと 時間がかかると何が変わってくるかっていうとなんかね そこに住んでる人の習慣とか地域との結びつきみたいなところが 強くなってくるかなと思ってうん例えばさ 年越しそばっていう文化そばにはあるけどさ ラーメンにないじゃんないね絶対この時に食べるみたいなのとかさうんそのそばっていうのは これって決まってるじゃんうんだからそうなってくると もうね固まってきちゃうんだよねなんかもうそばといえばこれみたいなうんそういうもんなっていうことになってくるからそばってもうなんかあまりこう変化させにくいというかそれはもうそばじゃないみたいななるほどねうんだからそばって言うと 元みたいなことになってくのかなって思ったんだよねでもそばにもいろいろさダイバースな部分はあるわけじゃんそれはどう考える?うんわんこそばとかさ 出雲そば出雲そばって結構三段のこのあれがあるわけよ出雲そば食うってことある?俺島根だからさ出身うんなめてんのか? 島根ちょっと分かんないけど知らないって言っただけはいはいはい違う違う違うあのなんだろうなそばってさかん…駄々はでも駄々はでも東名阪じゃないっていうか名古屋出身名古屋っていうかうん東海出身でしょ?うん東海のそばって何なの?東海そば?どっちのほうになるの?関西らしいの?台湾ラーメン台湾ラーメンは全然違うでしょだし?そばってある?名古屋のそばっていうのは名古屋そばあるのかなあんまり名古屋独自のはない気がするけどだしはどっちなんだろうあーでも多分東京の方か東の方だと思うどっちか黒い濃い濃いそう黒いやつって名古屋濃いからあーそかそかそこの多様性はでもあるわけだよねそばもまあそこもさそんななんかラーメンほどじゃないじゃんラーメンほどじゃあ確かにないね確かに出身のそばも見たけどなんか見た目はなんか普通のわんこそばみたいなねわんこそばっていうか3段済みのこうそばのやつだね三段積みの薬味が違うそばがあるっていうねあれうまいけどなあれめっちゃうまいけどなうんまあなんかそういう感じでさやっぱこう時間時間なんじゃないかっていう仮説なんだけどラーメンも結構ねそういうあのだいぶ時間が経ってきててなんかさ外国でさ作られてるラーメン見たときにさなんかラーメンじゃねえと思うときないめっちゃあるよ俺コロナでとんこつラーメン食ったとき絶望したもん嘘でしょって思ったそうそういうことや日本人がもうなんかラーメンはこれってもうなんかなんと固まってきちゃってんだよそういうの受け入れられなくなってきてるっていうその多様性がねそういうことなのかなみたいなさそうかもねうーん時の洗礼を経るってことかまあなんかそれってまあもっと言うとさなんかそっから思ったのはなんか国の歴史とかさうんアメリカとかって国の歴史が浅いからさすごい多様じゃんそこってまあそうだねまあもともと国の始まりがそういう思想だってのあるけどさまあなんかその古い歴史を持ってるところってもうあんまりこう多様なことはない多様なことはないまあそののを受け入れなかったりとかこうであるみたいなのあるけど何でもあるみたいなのが多分アメリカとかっていう考えでさそれもなんか国の歴史だったりもするしまあ古いものを大切にすべきっていう発想はまずないよね俺らみたいな古いものは貴重だっていう発想はガチでないなってもまあそれはアメリカもそうだしオーストラリアとかももうそうAPACだとねそうだと思うしうん今から作るっていう感じがあるっていうかねそうそうそううんそれはあるそうそうそうそううんまあそれもなんか自分の中のその人生において10代の頃の多様受け入れてる器が40代になるとなくなるみたいなのがなんかあるじゃんそこにつなげてきますかいやいやそういやなんかそこまで行ったね俺はそこラーメンからうんこういうなんか多様受け入れ曲線みたいなのがあってさなるほどうんだんだんこう受け入れなくなってくみたいなうん時間とともになるほどねそううんそうああ確かにねうんなんでなんか意識的にそのそのなんか歴史っていうのをゼロにするっていうことも必要なんじゃないかみたいないやそれは要はなんか新しいこと始めるとかさうんそしたらまあゼロになるからさうんうんみたいないやーどうしようかないやーいやーうんそんなだって今までの積み上げでやってきてる俺らにはそれ捨てろっていうのは酷ですよそういう考えのことだからさそんなの10歳の人考えないからいやいや俺ら俺それで食ってるから俺は俺はこんな大体なことしてきたんだからお前らもやれよって思ってやってるタイプだ老害だな本当にもうひどいなまあそれも受け入れるけど受け入れられるべきだけど確かになーなるほどねそれは気づきが深いうんうんそうででもこれ結構染み付いてると思うけどね日本の人とかってなんて言うんだろうねその俺らはこれでやってきたからこれはそうでしょみたいなさ結構あるよねいやあると思うよ日本のまあいい悪いはないと思うけどいや悪いでしょなんでそこに越すのわかんないけどいやいやいい部分もあると思うけどなわかんないけどあんま良しとされてないけどじいちゃんばあちゃん多いからさうちらの国はだからまあそれを簡単には捨てられないっていうかさそこでやってる時に寄り添える人が多いっていうのはあると思うんだけど未来に向けてみると別にねいいことはないよわかんないけどレガシーレガシーですそういうのといかにまあそういうのは抱えてるってことはまあ事実なわけでさじゃあどうやっていくかみたいなさそうでもそこはちゃんとそこのその資産をちゃんと利用可能なものにしないといけないからねそうそんな人を切り離してうんねえやってもさうんROIのRがRじゃないかIが出ないわけだから意味ないんだけどねそういううんちゃんと相手しながら書いていけないといけないんだけどねめっちゃ思いますそれああいや自分がなってるからもはやそっちに片足どころかもうほぼなってるみたいな両足突っ込んでるっていうはははうんソフトローガイってなんか最近見たなうん何それ知らないえーわかんないソフトローガイになってみて多分俺絶対ソフトローガイだわはははわかんないけどその単語聞いた瞬間思ったはははなんか見たうん40代からのソフトローガイってうーんまあねいきなりバンとこうねロ明日からローガイですって訳じゃなくてこうグラデーションだからさもうだいぶそうねうんうんうんうんうんいやーそういうことなるほどはははうーんなんかもうそこの世代だけで生きていくとかできないんかねうんもう切り離されてコミューンみたいなねそうそうお前らはこのここで隔離された惑星で暮らせっていうそうそうそう地球周回軌道のここのお前ここのあれで暮らせっていうねそう全員で暮らせるっていうね全員その世代しかいないっていうでねお前らのねえケツはお前らで拭けっていう感じのねははははははははそれ理想だけどねうんうふふちょっとうーん過激だけどうーんなんか世帯をまたがって責任を持たされるっていうのはさ誰も同意してないけどなんかさ通年上さそれオッケーみたいになってるじゃん年金とかもさあうんそれオッケーみたいになってるじゃん年金とかもさあそうだけどその国とかさいわゆるナショナルっていうものに対してさうんなんか誰もそれ合意してないのになんか若い人はなんかなんかねおっちゃんの医療費払うみたいになったりしてるからそれなんかな なんでもそれをさ選挙のシステムでひっくり返してるからね繰り返そうと思ってもさ人工ピラミッドの関係でそれ無理じゃん まあねそうだねそれだそれさあちょっと不合理だよね結構ねうん だからすだれじゃあもう衰退するってなるわけじゃないそこでね人工オーナー好きって言われるん なんなのっていうな方まあだからどうってわけじゃないんだけどね早く引退したはいいのかな まあ俺は山にて多くでえっもう金稼いで逃げ切って俺の価値観で俺は人生を俺の人生を閉じるっていうねそれに しかんないかなと思っている俺は俺の価値観で俺の人生を閉じるっていう 感じになっちゃってきてるよねまあいいんじゃないそこそれができればさ これがねでもやなんだよなーそれはそれではなんかもっとちょっとそこ悩んでますさいいうとまぁ なんていうかなそうなるわけじゃん 子供に対して何を伝えるかっていうのも女人生の一部だから って考えてるからそこをね別別皿に寄せておけないわけよでなると ちょっと悩ましいんだけどねあーだの自分が影響しすぎちゃうのが怖い ということさあその価値観イエローぞイエスそれに染まってほしくないっていう ねイエスってそこを別皿にしたいんだけどね本当はだけどそんなこと は無理で絶対だってさ だってそうじゃんその子供たちに対してさ 俺の人生の価値観俺はこうやってやってきたけどお前らがそういうわけじゃねー っていうのも俺の価値観になるわけだから絶対染めるわけになるわけよ絶対親っていうのは染めるしかないわけよ 絶対だがだから 染めるんだけどそのお前らは染まってるっていう自覚を持てよ っていうことを言うしかなくてs れはさあなお春のおやじさん との関係でやっぱり染まったと思ったいやーうちの親父はそんなねあの日よね 俺みたいなんさあ優秀な人じゃないから そんなメッセージは出さないわけだけどでも俺は受け取ったよ俺は優秀だから俺 を受け取った親父はそうだなあいつは不器用だから 偉いけど優秀だけど俺にそういうことは伝えなかったっていうのは汲み取ったけど ねなおからできたんだからできるんじゃないの わからんけど子供はいやーうーんブーバー何の話をしてるかもう俺もう トリスハイボールも4回ぐらいのんだよ今日だったら久々今日さ夜収録じゃんだっ たそう久々の久々の夜収録9時スタートだったんですよってちょっともう読みながら の収録なのではいちょっとそんな感じでラーメン の多様性についてラーメンの話しててそこははい はいでははいじゃあはいおつかれさまでした
---
template: BlogPost
path: /154
date: 2024-02-23T06:15:50.738Z
title: ep154:次に来る入力方法
thumbnail: /assets/ep154.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3fqu9zDXWdEtGmqhecPjy0?utm_source=generator&theme=0" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
  
***  
  
PCの入力方法はローマ字入力  
かな入力は今どきないだろ  
スマホの入力  
文字を指で書いて入力する方法があるらしい  
いつからフリック入力になった？  
トグル入力  
ポケベル入力  
言語の数だけ入力方法ありそう  
AIで変わる入力方法  
AIによって誤変換あっても意味を理解してくれるChatGPT  
音声入力が来る  
機械翻訳の入力のコツ  
プロンプトエンジニアリングもそんな感じ

***  
文字起こし：  
[00:00.000 --> 00:02.540] ジャミングウェーブの  
[00:02.540 --> 00:03.880] なおはるです  
[00:03.880 --> 00:05.180] ダズです  
[00:05.180 --> 00:11.220] 最近さふと思ったんだけどさ  
[00:11.220 --> 00:12.900] あんまり  
[00:12.900 --> 00:18.600] 他人がスマホにどうやって  
[00:18.600 --> 00:20.920] 入力してるかって  
[00:20.920 --> 00:23.120] あんまり知らないなと思って  
[00:23.120 --> 00:26.280] まあそうだね  
[00:26.280 --> 00:27.940] パソコンならさ  
[00:27.940 --> 00:29.220] キーボードがさ  
[00:29.220 --> 00:32.080] 並んでるのを打ってるとさ  
[00:32.080 --> 00:34.260] なんとなくあれで打ってんだってわかるじゃん  
[00:34.260 --> 00:34.640] キーボード  
[00:34.640 --> 00:37.300] キーボード以外の入力  
[00:37.300 --> 00:39.360] あんまないでしょパソコン  
[00:39.360 --> 00:41.380] キーボードの中でローマ字なのか  
[00:41.380 --> 00:43.380] ひらがななのかってあるけど  
[00:43.380 --> 00:46.480] ないでしょかな入力  
[00:46.480 --> 00:47.320] ないか  
[00:47.320 --> 00:49.740] スマホなんかもっと見えなくてさ  
[00:49.740 --> 00:52.340] 多様じゃん入力方法もさ  
[00:52.340 --> 00:53.180] 確かに  
[00:53.180 --> 00:55.640] なおはるは何  
[00:55.640 --> 00:56.460] 何入力  
[00:56.460 --> 00:57.660] 俺はフリック  
[00:57.660 --> 01:00.340] フリック入力してる  
[01:00.340 --> 01:02.280] でも子供とか見ると  
[01:02.280 --> 01:03.860] 子供は  
[01:03.860 --> 01:06.140] スマホさ  
[01:06.140 --> 01:08.540] 今小学校2年生の  
[01:08.540 --> 01:10.200] 女の子いるけど  
[01:10.200 --> 01:12.340] あれで書いてる  
[01:12.340 --> 01:13.320] なんていうの  
[01:13.320 --> 01:16.040] なんていうのああいうの  
[01:16.040 --> 01:17.220] 字を入力して  
[01:17.220 --> 01:19.940] 字をなぞったらさ  
[01:19.940 --> 01:22.500] アンドロイドとか  
[01:22.500 --> 01:23.020] ああ  
[01:23.020 --> 01:25.760] 書き順であ  
[01:25.760 --> 01:26.980] なんていうの  
[01:26.980 --> 01:27.640] なんていうの  
[01:27.660 --> 01:28.360] あれ  
[01:38.660 --> 01:47.940] 5917  
[01:51.360 --> 01:52.820] え  
[01:52.820 --> 01:53.340] 自分  
[01:53.340 --> 01:56.620] という senior  
[01:56.620 --> 01:57.020] ん  
[01:57.020 --> 01:57.520] さあ  
[01:57.520 --> 02:00.740] 予測変換の自由版みたいな  
[02:00.740 --> 02:02.060] IMEのさ  
[02:02.060 --> 02:03.120] あるじゃん  
[02:03.120 --> 02:05.900] 書き順だとこうですよっていうのが  
[02:05.900 --> 02:08.500] 推奨されるやつのあれをやってるかな  
[02:08.500 --> 02:10.980] そんなインターフェースあるの知らなかった  
[02:10.980 --> 02:12.460] そんな入力方法知らなかった  
[02:12.460 --> 02:15.580] なるほどね  
[02:15.580 --> 02:16.920] なんでそれやってんの  
[02:16.920 --> 02:18.080] 意味わかんないけど  
[02:18.080 --> 02:20.620] って思ってるけど  
[02:20.620 --> 02:25.280] 自分も周りに聞いたら  
[02:25.280 --> 02:26.940] 意外にフリック入力  
[02:26.940 --> 02:28.980] してない人も多かった  
[02:28.980 --> 02:30.560] まあ確かにね  
[02:30.560 --> 02:31.120] 多いかも  
[02:31.120 --> 02:34.180] 普通にパソコンのキーボードみたいなやつで  
[02:34.180 --> 02:35.140] そうだね  
[02:35.140 --> 02:36.400] ポチポチやってるね  
[02:36.400 --> 02:40.720] フリック入力変えたのっていつ頃  
[02:40.720 --> 02:44.180] フリック入力してるの  
[02:44.180 --> 02:45.520] 俺も  
[02:45.520 --> 02:48.980] iPhone3の頃からずっとフリックですね  
[02:48.980 --> 02:49.860] そうだよね  
[02:49.860 --> 02:54.380] iPhone出て画面だけにならないと  
[02:54.380 --> 02:55.500] フリック入力って  
[02:55.500 --> 02:56.800] できないんで  
[02:56.800 --> 02:56.920] そうだね  
[02:56.920 --> 02:58.200] 写りボタンじゃないんで  
[02:58.200 --> 03:00.840] その辺だよね  
[03:00.840 --> 03:01.860] 結構大変じゃなかった  
[03:01.860 --> 03:04.540] 俺なんか結構  
[03:04.540 --> 03:07.180] 最初結構  
[03:07.180 --> 03:08.820] 大変だったなって  
[03:08.820 --> 03:10.060] 思ったんだけど  
[03:10.060 --> 03:13.840] でも柄系の  
[03:13.840 --> 03:16.540] 時とかの  
[03:16.540 --> 03:19.020] 感覚に近かったけどね  
[03:19.020 --> 03:20.360] 柄系もさ  
[03:20.360 --> 03:22.920] 赤さたな浜  
[03:22.920 --> 03:25.720] 赤さたな浜やらは  
[03:25.720 --> 03:26.080] のさ  
[03:26.920 --> 03:27.920] はいえと  
[03:27.920 --> 03:29.920] なんだっけあの  
[03:29.920 --> 03:32.860] 列で入れて何番目っていうのでさ  
[03:32.860 --> 03:33.620] 入れるじゃん  
[03:33.620 --> 03:34.920] その柄系って  
[03:34.920 --> 03:36.640] 何回押すかで  
[03:36.640 --> 03:37.240] うん  
[03:37.240 --> 03:39.140] あの母音が変わっていくみたいな  
[03:39.140 --> 03:43.040] 母音でそうそう母音が変わってっていう確定するみたいな感じで  
[03:43.040 --> 03:48.400] その母音の確定を親指の方向にするっていうのがフリックの本質だから  
[03:48.400 --> 03:50.220] 割と  
[03:50.220 --> 03:54.920] 割とそこが経由してたから良かったかなって思ったけどね  
[03:55.500 --> 03:55.840] その  
[03:55.840 --> 03:56.760] 確かに  
[03:56.760 --> 03:58.500] でもさ何回押すかとさ  
[03:58.500 --> 04:00.300] 方向感はなんかまた  
[04:00.300 --> 04:02.640] なんかちょっと違っ  
[04:02.640 --> 04:04.920] まあ確かにちょっと違ったかもしんだけど  
[04:04.920 --> 04:06.860] 概念的には近いじゃんその  
[04:06.860 --> 04:10.260] 母音プラス子音で決めるみたいなさ  
[04:10.260 --> 04:11.320] どっちかというと  
[04:11.320 --> 04:13.580] それに近いと思ったのは  
[04:13.580 --> 04:14.680] 俺一時期その  
[04:14.680 --> 04:16.540] 何回押すかじゃなくてあの  
[04:16.540 --> 04:18.700] ポケベルだったかな  
[04:18.700 --> 04:20.820] なんか  
[04:20.820 --> 04:23.220] あって押して  
[04:23.220 --> 04:24.040] あを  
[04:24.040 --> 04:26.440] あって1だと  
[04:26.440 --> 04:26.740] あって1だと  
[04:26.760 --> 04:28.200] あって2だと  
[04:28.200 --> 04:29.540] い  
[04:55.080 --> 04:56.600] いって打ちたかったら  
[04:56.600 --> 04:57.440] いいってことね  
[04:57.440 --> 04:57.600] そうそう  
[04:57.600 --> 04:58.600] 1押して2  
[04:58.600 --> 04:59.260] そうそう  
[04:59.260 --> 05:00.640] 1、2でいいってことね  
[05:00.640 --> 05:01.600] そうそうそう  
[05:01.600 --> 05:02.840] 縦横ってことね  
[05:02.840 --> 05:03.420] そうそうそう  
[05:03.420 --> 05:05.300] 横縦か横縦ってことだよね  
[05:05.300 --> 05:05.740] そうそうそう  
[05:05.740 --> 05:07.460] 50音のあれで表で言うと  
[05:07.460 --> 05:08.000] そう  
[05:08.000 --> 05:11.460] 必ず1文字押すのに2回打たなきゃいけないっていう  
[05:11.460 --> 05:12.220] はい  
[05:12.220 --> 05:12.620] 感じ  
[05:12.620 --> 05:13.540] はい  
[05:13.540 --> 05:15.420] お見てピースしたらなんか  
[05:15.420 --> 05:17.300] ピースしたら今ズームのやつでね  
[05:17.300 --> 05:17.600] 見てる  
[05:17.600 --> 05:19.700] ポッドキャスト見てる人は何も分かんないからもう  
[05:19.700 --> 05:21.220] 飛ばしてくださいもうこれは  
[05:21.220 --> 05:22.220] はい  
[05:22.220 --> 05:23.020] はい  
[05:23.020 --> 05:25.140] そうでそう  
[05:25.140 --> 05:25.820] だからその  
[05:25.820 --> 05:26.080] そう  
[05:26.080 --> 05:28.800] さあ普通のポンポンポンって押すやつだとさ  
[05:28.800 --> 05:31.440] おまでいくのに5回押さなきゃいけないわけでしょ  
[05:31.440 --> 05:34.580] あの普通の柄系の  
[05:34.580 --> 05:35.740] あいうえおで  
[05:35.740 --> 05:38.280] あ1回プッシュするたびに  
[05:38.280 --> 05:40.040] あいうえおっていうやつだと  
[05:40.040 --> 05:40.620] そうだね  
[05:40.620 --> 05:42.160] おを入力するためには  
[05:42.160 --> 05:45.460] 5回押さないとダメですね  
[05:45.460 --> 05:45.720] そう  
[05:45.720 --> 05:46.320] あいうえお  
[05:46.320 --> 05:47.420] そういう意味だと  
[05:47.420 --> 05:50.580] あのかなりこう入力の手間が省けるっていう  
[05:50.580 --> 05:51.440] 一時期そういう  
[05:51.440 --> 05:54.640] 入力方式をやってた時があったなっていう  
[05:54.640 --> 05:55.320] 思い出した  
[05:55.320 --> 05:55.640] うん  
[05:55.640 --> 05:56.020] うん  
[05:56.020 --> 05:56.060] うん  
[05:56.080 --> 06:26.080]   
[06:26.080 --> 06:26.360]  See You  
[06:26.360 --> 06:29.700] acia  
[06:42.740 --> 06:44.780] やん  
[06:46.060 --> 06:47.440] ほー  
[06:47.440 --> 06:48.880] 変わった  
[06:48.880 --> 06:49.540] ね  
[06:49.540 --> 06:49.940] で  
[06:49.940 --> 06:50.860] それ centuries  
[06:50.860 --> 06:51.060] ですね  
[06:51.060 --> 06:52.080] で  
[06:52.080 --> 06:52.500] 最後  
[06:52.500 --> 06:53.200] 練り  
[06:53.200 --> 06:53.420] 連続  
[06:53.420 --> 06:53.880] 世界  
[06:53.880 --> 06:56.020] だ object  
[06:56.020 --> 07:03.020] な人たちとかさ でもどういうさあイノベーティブな人材っていうか  
[07:03.020 --> 07:10.120] ux デザイナー抱えてるのって思ったけどね すごくねそんなそれを統合された  
[07:10.120 --> 07:13.060] デザインでさ  
[07:13.060 --> 07:18.960] なしとゲームっていうの確か もうすごいなぁと思ったよ  
[07:18.960 --> 07:21.080] すごいなぁ うん  
[07:21.080 --> 07:27.080] なんか見たこともないにいる候補とかありそうだねなんか ようわからん分からんっていたんだけど  
[07:27.080 --> 07:31.520] なんかマイナーあるでしょあるでしょ  
[07:31.680 --> 07:36.120] そんなだって そんなにさ  
[07:36.360 --> 07:47.980] そんなに何かシーケンシャルに行けるっていうのもあるじゃんその ああああを入れていいって言えるっていうのもそんなにそうそんなに  
[07:47.980 --> 07:52.040] すごいなぁと思ってたけどね 例えば  
[07:52.040 --> 07:58.840] そのさかのぼってでしか入力できない文字がないのかねないのか 普通のフリック入力でできないと  
[07:58.840 --> 08:02.320] ありそうだけどね  
[08:02.320 --> 08:13.760] ハングルハングルとかハングルとかってどうやって入れてんだろハングル文字てだって 結構パーツで決まるわけでしょ丸の横に過去形だったらここが日本語で言うと  
[08:13.760 --> 08:15.980] なんか  
[08:15.980 --> 08:20.380] あの うん横棒3本と縦棒みたいなさ  
[08:20.380 --> 08:26.660] その 地の中でパーツが意味を持つみたいなさ  
[08:26.780 --> 08:34.820] 文字体系というかいうのもあるじゃんあるねー なんかね韓国語のピキーボード見たことあるけどそのパーツごとにキーが終わり振られ  
[08:34.820 --> 08:38.160] てなんかみたいな へえ  
[08:38.160 --> 08:44.040] その組み合わせよだからみたいな 入力してる姿見たことないけど  
[08:44.040 --> 08:45.980] ん 俺韓国ね  
[08:45.980 --> 08:54.640] 韓国のソリューションが来てふたとさあ仕事のやすいっつも話すねどうやってさあキー 打ってるのっていつも話すんだけど  
[08:54.640 --> 09:01.980] もうねいつも訳わかんなく感じ俺の英語力がたなくて わからずに終われ  
[09:01.980 --> 09:05.640] どうやってやってんなの 無地の  
[09:05.640 --> 09:11.200] パーツことに あなた達というは意味があるんでしょっていう  
[09:11.200 --> 09:15.380] いうのを聞いてねー つもねーわからずにん  
[09:15.980 --> 09:19.260] 終わってる なんか予測変換的なのもあるんだろうね  
[09:19.260 --> 09:22.220] これ入れたら あるんだと思う  
[09:22.220 --> 09:25.100] 全然わからん ようやっとるな  
[09:25.100 --> 09:33.500] 本当にようわからんわ なるほどね  
[09:33.500 --> 09:40.420] スマホの入力方法 でさ最近俺思うんだけどさ  
[09:40.420 --> 09:44.320] またもう一 またちょっと入力方法変わるかなって  
[09:44.320 --> 09:47.500] ここ1年ぐらい思ってて 何かっていうと  
[09:47.500 --> 09:52.360] AIの話になっちゃうんだけど あれが出てくることによって  
[09:52.360 --> 09:59.120] 最近チャットGPTとかってさ あれってさ  
[09:59.120 --> 10:04.740] 要は予測変換の超コンテキスト長い版じゃん  
[10:04.740 --> 10:07.460] チャットGPTもあるんですか そうだね確かに  
[10:07.460 --> 10:11.720] 文脈で判断するわけじゃん 入力されたものが何かって  
[10:11.720 --> 10:14.300] 最近俺やり始めてるんだけど  
[10:14.320 --> 10:17.240] 音声入力をやり始めてて  
[10:17.240 --> 10:22.640] 音声入力って今までって結構さ 変換というかさ  
[10:22.640 --> 10:27.100] 全然誤認識されて 全然違う文字とか出てくるじゃん  
[10:27.100 --> 10:30.700] 今まで結構あるんだよ だいぶ良くなったんだけど  
[10:30.700 --> 10:34.080] それでもまだやっぱあるのよ  
[10:34.080 --> 10:38.100] でさなんか  
[10:38.100 --> 10:43.600] チャットGPTとやり取りしてる時に 音声入力ってすごく良くて  
[10:43.600 --> 10:44.320] なんていうか  
[10:44.320 --> 10:46.200] っていうとその  
[10:46.200 --> 10:47.140] そのなんだろうな  
[10:47.140 --> 10:50.880] 誤入力されてもそこを乗り越えれるのよ  
[10:50.880 --> 10:54.120] 過去の文脈があると  
[10:54.120 --> 10:56.360] 全然違う単語でポンって入っても  
[10:56.360 --> 11:00.700] その今入れた一文だけ見ると 意味通じないんだけど  
[11:00.700 --> 11:02.780] 過去のやり取りでバーって  
[11:02.780 --> 11:06.240] なった時にここの単語が全然違うこと ポンって入れても  
[11:06.240 --> 11:08.700] あでもこいつってこういうこと 言いたかったんだろうなって  
[11:08.700 --> 11:11.080] いうことでちゃんと認識してくれるんだよ  
[11:11.080 --> 11:11.820] はいはいはい  
[11:11.820 --> 11:13.640] そうだから  
[11:13.640 --> 11:15.400] 成立するんだよ音声入力は  
[11:15.400 --> 11:16.460] 全然ご変化あっても  
[11:16.460 --> 11:17.100] なるほど  
[11:17.100 --> 11:20.700] そう乗り越えれるなって最近感じてて  
[11:20.700 --> 11:22.220] これがやっぱできると  
[11:22.220 --> 11:25.700] めっちゃ音声入力と対応できるなって思ってて  
[11:25.700 --> 11:28.760] なるほどね  
[11:28.760 --> 11:30.960] これねきますよ絶対  
[11:30.960 --> 11:32.780] 音声入力  
[11:32.780 --> 11:34.220] だからもう  
[11:34.220 --> 11:36.920] なんだろうねその入力がさ  
[11:36.920 --> 11:39.660] よりセマンティックになるっていうか シンタックスだけじゃなくて  
[11:39.660 --> 11:41.080] セマンティックになる例えば  
[11:41.080 --> 11:43.440] 高校生の女の子同士がさぁ  
[11:43.440 --> 11:47.800] メールさ考路装しの  
[11:47.800 --> 11:50.200] 現実兵 design  
[11:50.200 --> 11:51.400] 日本  
[11:51.400 --> 11:52.760] 内容含め  
[11:52.760 --> 11:55.220] 入れてくれるみたい Crystal  
[11:55.220 --> 12:01.680] そういう話 y  
[12:01.680 --> 12:03.620] 一📦  
[12:03.620 --> 12:04.620] 不相当  
[12:04.620 --> 12:06.760] そう  
[12:06.760 --> 12:08.180] そういうことしてるんだよ  
[12:08.180 --> 12:11.280] もちろん記事それができる not 受験パока  
[12:11.280 --> 12:13.280] ソロて来そうと言うかまあソания  
[12:13.280 --> 12:14.540] 人間の能具  
[12:14.540 --> 12:17.680] 人間について詳しいからねあいつらは  
[12:17.680 --> 12:18.540] そう  
[12:18.540 --> 12:22.240] この文脈でこの単語が出てくるのおかしいから  
[12:22.240 --> 12:24.880] ちょっと多分言い方こいつ間違ってて  
[12:24.880 --> 12:28.960] こっちのことを言おうとしてたんだなっていうことを  
[12:28.960 --> 12:32.020] 考えれるようになったからAIが  
[12:32.020 --> 12:36.080] かなりやりとりが楽になった  
[12:36.080 --> 12:38.960] でタイピングよりめっちゃ早いからやっぱり  
[12:38.960 --> 12:39.940] なるほどね  
[12:39.940 --> 12:44.540] ただねこのフリック入力と一緒で最初が結構ね  
[12:44.540 --> 12:47.200] 音声入力といってもこの人と話すのと  
[12:47.200 --> 12:49.720] なんかね脳の使い方がちょっと違うのよ  
[12:49.720 --> 12:52.960] 入力するときの喋り方ってなんかね  
[12:52.960 --> 12:56.500] ちょっと違うのよ一人で喋るっていうか  
[12:56.500 --> 12:58.180] コマンドとして話すっていう  
[12:58.180 --> 13:00.280] いけないことがあるっていう  
[13:00.280 --> 13:03.980] なんかねうまく喋れない感じがして  
[13:03.980 --> 13:09.920] なんか具体的に気をつけてることはあるの  
[13:09.920 --> 13:12.280] なんだろうなんかね  
[13:12.280 --> 13:13.420] うまく入るようにっていうか  
[13:13.420 --> 13:17.060] なんかこう会話でやってポンポン話すのとは  
[13:17.060 --> 13:19.360] なんかちょっと違う感覚になるのよ  
[13:19.360 --> 13:20.660] 音声入力って  
[13:20.660 --> 13:22.620] なんでかよくわかんないけど  
[13:22.620 --> 13:24.620] ぜひやってみて  
[13:24.620 --> 13:26.520] やってみるか  
[13:26.520 --> 13:27.880] 多分なんかね  
[13:27.880 --> 13:32.560] リズムがやっぱちょっと遅いからなのかもしれないけど  
[13:32.560 --> 13:35.460] ターンが自分のターンで向こうのターン  
[13:35.460 --> 13:38.940] こうのがキャッチオフが結構遅いってのもあるかもしれない  
[13:38.940 --> 13:39.880] なるほどね  
[13:39.920 --> 13:43.060] サムアルトマン今年はなんかそこがやっぱりね  
[13:43.060 --> 13:46.420] 音声入力チャットGPTが音声入力が  
[13:46.420 --> 13:48.420] 良くなくてみんなに使われてないって  
[13:48.420 --> 13:50.160] やっぱそこのインタラクティブのスピードが  
[13:50.160 --> 13:53.100] 良くないっていうのはすごく  
[13:53.100 --> 13:54.380] フィードバックとしてもらってて  
[13:54.380 --> 13:57.280] そこは今年中にかなり改善されるって言ってたから  
[13:57.280 --> 13:58.440] なるほどね  
[13:58.440 --> 14:02.260] だいぶこう普通の会話になってくんじゃないかな  
[14:02.260 --> 14:03.760] って思ってるんだけど  
[14:03.760 --> 14:07.680] AIDWなものをアウトプットするって  
[14:07.680 --> 14:09.840] 人類の能力がより評価される  
[14:09.840 --> 14:09.900] 人類の能力がより評価される  
[14:09.900 --> 14:12.060] 時代が来ましたねそれもう絶対  
[14:12.060 --> 14:15.100] めっちゃそれ感じるわ  
[14:15.100 --> 14:18.880] なんかさ最近  
[14:18.880 --> 14:21.440] これ俺言ってんのかな  
[14:21.440 --> 14:24.340] 英語の文もさ  
[14:24.340 --> 14:26.140] 例えば  
[14:26.140 --> 14:29.940] ちょっと本当に忙しくて  
[14:29.940 --> 14:31.440] 時間ない時って  
[14:31.440 --> 14:33.140] DPLで  
[14:33.140 --> 14:34.260] 回したのよ  
[14:34.260 --> 14:36.420] 丸送りしたいんだけど  
[14:36.420 --> 14:38.960] その時にさ  
[14:39.900 --> 14:41.220] 例えばなんだろうね  
[14:41.220 --> 14:43.940] このアイディアは大変素晴らしいと思いました  
[14:43.940 --> 14:45.280] って  
[14:45.280 --> 14:47.280] DPLに詳しく送ると  
[14:47.280 --> 14:51.340] 主語が未確定なんだよね  
[14:51.340 --> 14:54.740] 誰がそのアイディアを素晴らしかったと思うかっていうのは未確定なんだよ  
[14:54.740 --> 14:56.900] だからそこの時に  
[14:56.900 --> 15:00.080] DPLがそれを補完して  
[15:00.080 --> 15:01.740] They think about  
[15:01.740 --> 15:03.740] This idea is a  
[15:03.740 --> 15:07.180] グレートとかって言うんだけど  
[15:07.180 --> 15:09.380] それは別に  
[15:09.380 --> 15:10.960] 文脈的には俺じゃん  
[15:10.960 --> 15:12.080] 俺がそのアイディアを  
[15:12.080 --> 15:14.900] 素晴らしいと思ったってことを言いたいから  
[15:14.900 --> 15:17.220] 最終的には  
[15:17.220 --> 15:18.780] IとかWeとかをさ  
[15:18.780 --> 15:21.280] 自分で打つわけだけど  
[15:21.280 --> 15:23.960] なんて言うんだろう  
[15:23.960 --> 15:26.020] AIが  
[15:26.020 --> 15:31.200] 出力してくれる時に  
[15:31.200 --> 15:33.620] それを見逃しがちだよなーっていうのを  
[15:33.620 --> 15:35.420] ちゃんと最初から差し込んどくっていう  
[15:35.420 --> 15:36.480] スキルって多分  
[15:36.480 --> 15:39.080] あと2,3年は必要でさ  
[15:39.380 --> 15:40.520] あーなるほどね  
[15:40.520 --> 15:41.380] うん  
[15:41.380 --> 15:42.240] うんうんうん  
[15:42.240 --> 15:43.260] うん  
[15:43.260 --> 15:44.240] っていうのを  
[15:44.240 --> 15:46.040] まあ  
[15:46.040 --> 15:47.180] 黎明期には  
[15:47.180 --> 15:48.260] それで  
[15:48.260 --> 15:50.540] しのぐんだろうなって思ってるっていう  
[15:50.540 --> 15:52.140] まあ  
[15:52.140 --> 15:54.880] それがプロンプトエンジニアリングと言われる領域なんだと思うよ  
[15:54.880 --> 15:59.260] それがプロンプトエンジニアリングってそんなくだらないことなの?  
[15:59.260 --> 16:02.140] あーでもなんかそういう感じな気がするけどな  
[16:02.140 --> 16:04.160] うまく出力を引き出す  
[16:04.160 --> 16:07.260] 正しい成果を引き出すためのテクニックだからさ  
[16:07.260 --> 16:08.460] そんなす  
[16:08.460 --> 16:09.220] そんなー?  
[16:09.380 --> 16:10.540] だからさその  
[16:10.540 --> 16:13.060] ナオハロの言ってたその一部を引き出す前に  
[16:13.060 --> 16:14.720] ちょっと文脈を足すわけでしょ多分  
[16:14.720 --> 16:15.340] 足す足す  
[16:15.340 --> 16:16.140] そうそうそうそう  
[16:16.140 --> 16:18.340] それはプロンプトエンジニアリングでまさにそうだと思う  
[16:18.340 --> 16:20.180] それエンジニアリングでなんでもねーよ  
[16:20.180 --> 16:21.560] それエンジニアリングだろ  
[16:21.560 --> 16:23.800] エンジニアリングってもっと計測可能とかさ  
[16:23.800 --> 16:25.120] なんかこう  
[16:25.120 --> 16:28.280] あーでも今言われてるプロンプトエンジニアリング会話はそういう話だよ  
[16:28.280 --> 16:29.460] 本当に  
[16:29.460 --> 16:31.520] 自然言語でできるようになったから  
[16:31.520 --> 16:33.380] それがエンジニア  
[16:33.380 --> 16:34.820] うーんまあ  
[16:34.820 --> 16:36.700] いやそれが一定のスキルだとは  
[16:36.700 --> 16:38.280] 思うけど  
[16:38.280 --> 16:39.360] エンジニアリングって  
[16:39.380 --> 16:41.200] エンジニアリングかって言われるとちょっと分かんないけどね  
[16:41.200 --> 16:43.320] まあ言葉はいろいろあるけど  
[16:43.320 --> 16:44.260] 言葉はいろいろあるから  
[16:44.260 --> 16:45.340] 使ってる人もいるし  
[16:45.340 --> 16:49.060] まあそれはエンジニア  
[16:49.060 --> 16:51.200] プロンプトエンジニアリングっていう  
[16:51.200 --> 16:53.680] 領域を  
[16:53.680 --> 16:55.640] 作りたかった人にとっては  
[16:55.640 --> 16:57.380] 多分いいと思うけど  
[16:57.380 --> 16:59.240] エンジニアリングではないと思うけど  
[16:59.240 --> 17:00.160] まあ  
[17:00.160 --> 17:03.600] やろうとしてることそのプロンプトエンジニアリングって  
[17:03.600 --> 17:05.560] 仮に言ったときにやろうとしてることは  
[17:05.560 --> 17:06.180] 多分一緒だった  
[17:06.180 --> 17:06.940] そうだね  
[17:06.940 --> 17:07.720] そうだね  
[17:07.720 --> 17:09.360] だから  
[17:09.380 --> 17:39.380]   
[17:39.380 --> 17:41.900] そこの足すっていう発想が生まれるためには  
[17:41.900 --> 17:44.800] ある程度の語学力がないと  
[17:44.800 --> 17:46.700] 僕らはっていうのは  
[17:46.700 --> 17:48.600] 文脈からはないけど  
[17:48.600 --> 17:53.620] その語学的では失われるなっていうのが思わないと  
[17:53.620 --> 17:57.420] AIを活用する側からしたら  
[17:57.420 --> 18:00.160] 僕らはっていうのを足そうって思わないから  
[18:00.160 --> 18:03.220] 結局まだそこは  
[18:03.220 --> 18:05.320] そこなんだよね  
[18:05.320 --> 18:09.200] そこがまだっていうのがあるっていうね  
[18:09.200 --> 18:10.960] そうだね  
[18:10.960 --> 18:12.960] うん  
[18:12.960 --> 18:14.960] うん  
[18:14.960 --> 18:17.640] 絶対入ることが多いんかね  
[18:17.640 --> 18:19.560] まあそんなないから  
[18:19.560 --> 18:20.640] もうあとはもう  
[18:20.640 --> 18:23.800] 2、3ヶ月で追いつくんじゃない  
[18:23.800 --> 18:24.460] AIが  
[18:24.460 --> 18:25.580] 絶対追いつくでしょ  
[18:25.580 --> 18:28.340] 人類だってもうそんな  
[18:28.340 --> 18:30.960] 高度なことはしたいから  
[18:30.960 --> 18:31.480] うん  
[18:31.480 --> 18:33.920] 絶対追いつくよ  
[18:33.920 --> 18:35.460] そうね  
[18:35.460 --> 18:38.960] その人が何をやってるかっていうところまで  
[18:38.960 --> 18:39.040] まあ  
[18:39.040 --> 18:39.180] まあ  
[18:39.200 --> 18:41.200] あの前後関係も  
[18:41.200 --> 18:41.640] 含めて  
[18:41.640 --> 18:43.960] もうなんかレコードしてくるようになる気がしないさ  
[18:43.960 --> 18:44.640] 絶対そうでしょ  
[18:44.640 --> 18:44.960] 全て  
[18:44.960 --> 18:45.760] 絶対なるよ  
[18:45.760 --> 18:46.800] そうするともう  
[18:46.800 --> 18:49.660] なんかバチッと出してくるでしょ  
[18:49.660 --> 18:50.000] そう  
[18:50.000 --> 18:53.900] そんな感じですわ  
[18:53.900 --> 18:54.140] いや  
[18:54.140 --> 18:57.020] みたいなね  
[18:57.020 --> 18:58.360] またAIの話になったけど  
[18:58.360 --> 19:00.500] まあこの辺結構変わってくんで  
[19:00.500 --> 19:02.400] ぜひちょこちょこ残してく  
[19:02.400 --> 19:03.660] 確かにこれ  
[19:03.660 --> 19:05.260] 意識的に  
[19:05.260 --> 19:06.700] 数年後に聞いたら  
[19:06.700 --> 19:07.040] はい  
[19:07.040 --> 19:08.120] どうなるか  
[19:09.200 --> 19:12.880] 今はまだキーボード打ってますけど  
[19:12.880 --> 19:14.880] うん  
[19:14.880 --> 19:18.840] 音声きそうですっていう話でした  
[19:18.840 --> 19:19.540] はい  
[19:19.540 --> 19:21.080] はい  
[19:21.080 --> 19:21.960] はい  
[19:21.960 --> 19:23.960] ではお疲れさまでした  
[19:23.960 --> 19:25.580] お疲れさまでした  
