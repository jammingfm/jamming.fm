---
template: BlogPost
path: /208
date: 2025-02-23T06:15:50.738Z
title: ep208:みんなで叫びながら生野菜と生魚を天高く投げたあとにそれを食べる風習
thumbnail: /assets/ep208.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1sjdbzmx2lllgdBsnxUHhs?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***  

(00:00) だいぶ時間が経ってしまったが旧正月の話  
(01:18) シンガポールの旧正月はクリスマスから盛り上がる  
(02:04) チャイニーズニューイヤーの食習慣「[ローヘイ](https://en.wikipedia.org/wiki/Yusheng)」  
(04:58) まさかの食中毒リスク!? 箸でぐちゃぐちゃに混ぜる風習  
(09:03) 会社が呼ぶ「[ライオンダンス](https://en.wikipedia.org/wiki/Lion_dance)」  
(09:49) [レッドパケット](https://en.wikipedia.org/wiki/Red_envelope) (お年玉)の文化  
(13:37) ビンタン島への週末リゾート旅  
(17:56) ビンタン島のインドネシアビザは現地イミグレで取ると7日間ビザがとれて安い  
(19:20) リゾートホテル[Four Points by Sheraton Bintan](https://www.marriott.com/en-us/hotels/tnjfp-four-points-bintan-lagoi-bay/overview/)宿泊と費用感  
(20:50) 帰りのフェリーターミナルで“[偽ポロ](https://note.com/hancyo/n/n8b5581c1e402)"発見  
(22:21) 軽井沢の冬を満喫：スケートや雪景色  
(30:40) シンガポールの教育と軍事意識  
(33:28) 最近はシンガポールに住む人が多い？  