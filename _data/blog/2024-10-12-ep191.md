---
template: BlogPost
path: /191
date: 2024-10-12T06:15:50.738Z
title: ep191:結婚相談所で相談しよう
thumbnail: /assets/ep191.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/6SUGdsFdjZxPuzaAD6uxFd?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

  
***  

(00:04) シンガポールの雨の特徴  
(02:18) ポッドキャスト収録セットアップの完成  
(03:52) シンガポールの電圧とプラグの違い  
(05:54) 結婚前の話：楽天オーネットでの体験  
(08:38) 最初の面談での印象と即解約  
(14:58) 現在の楽天オーネットの料金体系  
(17:50) マッチングアプリと従来の出会い方の比較  
(19:48) [ブラックミラーのエピソード「Hang the DJ」](https://www.netflix.com/title/70264888)の紹介  
(22:20) AIによる文字起こしの利点と課題  
(23:28) OpenAI GPT-4の新機能と可能性  
(27:34) なおはるとだだのそれぞれの配偶者との出会い方  
