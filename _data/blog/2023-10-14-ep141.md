---
template: BlogPost
path: /141
date: 2023-10-14T06:15:50.738Z
title: ep141:年一回くらいの習慣
thumbnail: /assets/ep141.png
metaDescription:
---
![ep141](/assets/ep141.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/3VdOcOTeY2ed9quGhuiAvz?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***

あと13回ほど年を過ごせば50歳  
その時にはもう子供は家を出ている  
一年は人生を区切るには長すぎる  
テクノロジーが進化した今、時間感覚のアップデートが必要  
四半期を1つの区切りと考えたらせかせかしそう  
毎日の習慣も大事だけど一年に一回必ずやり続けることも大事  
Evanさんは子供の頃には「エバン」と習ったけど今は「エヴァン」  
初めての「ヴ」との出会い  
[Viridian green](https://www.google.com/search?q=viridian%2Bgreen)  
[東京ヴェルディ / Tokyo Verdy](_data/blog/2023-10-03-ep140.md)  
Vに該当する子音が日本語にはない  
[“ヴ”を生み出したのは福澤諭吉でした。「福澤諭吉が広めた日本語」](https://www.kateigaho.com/article/detail/88369)  
それが諸悪の根源  
I love youに対応する日本語  
俺らが肌色と読んでた色は今何色なのか  
[【GitLab Handbookを読む】#10 Boring solutions ／ 退屈な解決策](https://www.youtube.com/watch?v=XOvY0t0b77w)  

