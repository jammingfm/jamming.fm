---
template: BlogPost
path: /205
date: 2025-02-03T06:15:50.738Z
title: ep205:坂本龍一展
thumbnail: /assets/ep205.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/10Kj6wWTskphKZyQumPme2?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
  
***  
  
(00:14) 東京都現代美術館で開催中の坂本隆一展について  
(01:14) 展示を見て感動。当初は故人を偲ぶことへの不安があった  
(02:28) 展示は過去の作品のアーカイブ形式。多くの関係者の思いが詰まっている  
(03:36) 「アクア」という楽曲について。娘の誕生を祝って作られた曲  
(06:44) フォグスカルプチャー（霧の彫刻）の展示について。人工的な霧の中での神秘的な体験  
(08:34) パーソナリティの一人が子供の頃にピアノで「エナジーフロー」を弾いていた思い出  
(10:26) ピアノ教育の意義について。人類の文化遺産に触れる機会としての価値  
(13:30) 近年亡くなった著名人について。最期まで作品を残し続けた坂本隆一氏の姿勢  
(17:04) 展示の実用的な情報。オンラインでのチケット購入を推奨  
