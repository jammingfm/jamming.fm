---
template: BlogPost
path: /179
date: 2024-08-24T06:15:50.738Z
title: ep179:渡星
thumbnail: /assets/ep179.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/23K8jHdoemKBGvYvgdXk1L?utm_source=generator&theme=0" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>
  
***  
  
(00:04) 自分の人生を振り返って  
(00:28) 大学時代から始まった引っ越しの数々  
(01:59) 東京、神奈川、千葉での生活  
(03:39) 次の引っ越しはシンガポール  
(04:35) シンガポールに降り立った1週間前  
(06:30) シンガポールでの生活準備  
(07:15) 住む家の内覧開始  
(09:11) ホテル生活から始まる新生活  
(12:14) 家族が来るまでの単身赴任  
(14:54) 妻のキャリアと移住の影響  
(18:57) シンガポールの物価と生活費  
(19:53) 高額な家賃と給与調整  
(22:54) 現地での英語の壁とシンガポール英語  
(27:05) シンガポールでの仕事環境と新しい挑戦  
(29:38) 生活基盤の整備と今後の展望  
