---
template: BlogPost
path: /131
date: 2023-05-31T06:15:50.738Z
title: ep131:Different families raise their children differently
thumbnail: /assets/ep131.png
metaDescription:
---
![ep1３1](/assets/ep131.png)    

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/77QfSL4azoOnSJ6oqZgF3s?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>  

***  

[日清どん兵衛](https://www.donbei.jp/)  
アクティブブックダイアログ  
デジタルトランスフォーメーションジャーニー  
日本CTO協会  
[DX Criteria](https://dxcriteria.cto-a.org/)  
人生に一回しかしないことはみんな失敗しがち  
[ep122:引っ越しで失敗した話](https://jamming.fm/122)  
[ep118:不動産仲介業者やウェディングプランナーとの戦い方](https://jamming.fm/118)  
[ep121:人生で１回くらいしかない経験を共有するシリーズ「やばい自動車学校」](https://jamming.fm/121)  
人生に役立つチェックリストが欲しい  
dada家はママ友にビックリされることがよくある  
😥「それ大丈夫？」    
おむつを変える頻度    
ミルクを冷ますのではなくて水で薄める   
子育て習慣をあんまり他の家庭に言わない  
複数の家族でどっか行った時に明らかになる文化的な差異  
dada家は自分たちが世界一幸せな家族運営をしている  
「悪魔ができあがる」  
常識のギャップが家族間で広いと大変だろうな  
ポッドキャストを3年くらい続けて感じた良さ  
過去の自分と同じ悩みにぶつかった人たちに紹介できる  
ep82:乳児頭蓋変形外来  
dadaの人生をさも自分のものかのように喋るnaoharu  
住宅賃貸購入論争はまた別途実施したい  
[ネタを貯める場所をgitlabにした](https://gitlab.com/jammingfm/jamming.fm/-/boards/2811158)  
  
