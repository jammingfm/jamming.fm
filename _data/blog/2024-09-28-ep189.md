---
template: BlogPost
path: /189
date: 2024-09-28T06:15:50.738Z
title: ep189:PROGRIT完走
thumbnail: /assets/ep189.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/06kaef4uy15jWZbiGTsH8z?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

(00:05) 3ヶ月のプログリッド完走  
(01:50) カリキュラムの難易度と学習量の増加  
(03:04) バーサントテストで6点上昇  
(04:00) リスニング力の向上を実感
(06:16) スピーキングの自信向上
(07:05) 1分間スピーチの練習効果  
(10:55) 3ヶ月で236時間の学習時間  

<blockquote class="twitter-tweet"><p lang="ja" dir="ltr"><a href="https://twitter.com/hashtag/%E3%83%97%E3%83%AD%E3%82%B0%E3%83%AA%E3%83%83%E3%83%88?src=hash&amp;ref_src=twsrc%5Etfw">#プログリット</a> 完走！<br>今の自分のマネジメント能力だと毎日3時間はやっぱりキツかったけど、それでもなんとか工夫して大きな穴をあけずに継続して出来た！効果も実感してるしテストでも良い結果が出て満足🙌<br>今後も継続して英語学習がんばります💪 <a href="https://t.co/NEgZMsdrQV">pic.twitter.com/NEgZMsdrQV</a></p>&mdash; Naoharu (@naoharu) <a href="https://twitter.com/naoharu/status/1837826746869223654?ref_src=twsrc%5Etfw">September 22, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

(13:27) 大過去形の難しさ  
(15:00) コンサルタントのサポートの重要性  
(18:49) 今後の英語学習計画  
(21:15) 学習後の一時的な燃え尽き症候群  
(22:35) 忘却曲線と効果的な学習方法  
(24:09) シンガポールでの英語使用経験  
(25:35) AI活用による読み書きのサポート  
