---
template: BlogPost
path: /152
date: 2024-02-16T06:15:50.738Z
title: ep152:セミパーソナルのジムに行き始めた
thumbnail: /assets/ep152.png
metaDescription:
---

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/episode/1Rr64JQxnmwlRqIZpgDudp?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

***  

[エニタイムフィットネス](https://www.anytimefitness.co.jp/)  
セミパーソナル  
1人のトレーナーが複数のトレーニーに教える  
[ドロップセット](https://joyfit.jp/akajoy/health_knowledge/post08/)はずっとコチョコチョされて変になる感じ  
[BUFF Personal Gym](https://funthink.jp/)  

